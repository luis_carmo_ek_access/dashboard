<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 25-01-2019
 * Time: 12:55
 */

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Reports;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class CsvReportsController extends Controller {

    public function csvsReportGenerate(Request $request) {

        try {

            $this->validate($request, [
                'event_id'  => 'nullable',
                'event'  => 'nullable'
            ]);

            $this->validate($request, [
                'event_park_id'  => 'nullable',
                'event_park'  => 'nullable'
            ]);

            $this->validate($request, [
                'event_credential_id'  => 'nullable',
                'event_credential'  => 'nullable'
            ]);

            $event_id           = $request['event_id'];
            $event              = $request['event'];
            $event_park_id      = $request['event_park_id'];
            $event_park         = $request['event_park'];
            $event_credential_id= $request['event_credential_id'];
            $event_credential   = $request['event_credential'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if ($event_id && !empty($event)) {

                foreach ($event as $csv) {

                    switch ($csv) {

                        case 'report-whitelist':

                            $data = $this->whitelist($event_id, 'report-whitelist', 'event');

                            $files_structure['event'][] = $data;

                            break;
                        case 'report-whitelist-masters':

                            $data = $this->whitelistMasters($event_id, 'report-whitelist-masters', 'event');

                            $files_structure['event'][] = $data;

                            break;
                        case 'report-blacklist':

                            $data = $this->blacklist($event_id, 'report-blacklist', 'event');

                            $files_structure['event'][] = $data;

                            break;
                        case 'report-entries':

                            $data = $this->getEntries($event_id, 'report-entries', 'event');

                            $files_structure['event'][] = $data;

                            break;
                        case 'report-entries-app':

                            $data = $this->getAppEntries($event_id, 'report-entries-app', 'event');

                            $files_structure['event'][] = $data;

                            break;
                        case 'report-entries-presidential-box':

                            $data = $this->getPresidentialBoxEntries($event_id, 'report-entries-presidential-box', 'event');

                            $files_structure['event'][] = $data;

                            break;
                        case 'report-entries-box':

                            $data = $this->getBoxEntries($event_id, 'report-entries-box', 'event');

                            $files_structure['event'][] = $data;

                            break;
                        case 'report-entries-gate-category':

                            $data = $this->getEntriesByGateAndCategory($event_id, 'report-entries-gate-category', 'event');

                            $files_structure['event'][] = $data;

                            break;
                        case 'report-unique-title-errors':

                            $data = $this->getErrorsUniqueTitle($event_id, 'report-unique-title-errors', 'event');

                            $files_structure['event'][] = $data;

                            break;
                        case 'report-transactions-errors':

                            $data = $this->getErrorsTransactions($event_id, 'report-transactions-errors', 'event');

                            $files_structure['event'][] = $data;

                            break;
                        case 'report-filllevel-minute':

                            $data = $this->getFillLevel($event_id, 'report-filllevel-minute', 'event');

                            $files_structure['event'][] = $data;

                            break;
                        case 'report-occurences-tcp':
                            break;
                        case 'report-ticket-entries-minute':

                            $data = $this->getMinuteTicketEntries($event_id, 'report-ticket-entries-minute', 'event');

                            $files_structure['event'][] = $data;

                            break;
                        case 'report-transactions-ticket-entry-minute':

                            $data = $this->getMinuteTicketTransactions($event_id, 'report-transactions-ticket-entry-minute', 'event');

                            $files_structure['event'][] = $data;

                            break;
                    }

                }

            }

            if ($event_park_id && !empty($event_park)) {

                foreach ($event_park as $csv) {

                    switch ($csv) {

                        case 'report-whitelist':

                            $data = $this->whitelist($event_park_id, 'report-whitelist', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                        case 'report-whitelist-masters':

                            $data = $this->whitelistMasters($event_park_id, 'report-whitelist-masters', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                        case 'report-blacklist':

                            $data = $this->blacklist($event_park_id, 'report-blacklist', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                        case 'report-entries':

                            $data = $this->getEntries($event_park_id, 'report-entries', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                        case 'report-entries-app':

                            $data = $this->getAppEntries($event_park_id, 'report-entries-app', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                        case 'report-entries-presidential-box':

                            $data = $this->getPresidentialBoxEntries($event_park_id, 'report-entries-presidential-box', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                        case 'report-entries-box':

                            $data = $this->getBoxEntries($event_park_id, 'report-entries-box', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                        case 'report-entries-gate-category':

                            $data = $this->getEntriesByGateAndCategory($event_park_id, 'report-entries-gate-category', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                        case 'report-unique-title-errors':

                            $data = $this->getErrorsUniqueTitle($event_park_id, 'report-unique-title-errors', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                        case 'report-transactions-errors':

                            $data = $this->getErrorsTransactions($event_park_id, 'report-transactions-errors', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                        case 'report-filllevel-minute':

                            $data = $this->getFillLevel($event_park_id, 'report-filllevel-minute', 'event-park', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                        case 'report-occurences-tcp':
                            break;
                        case 'report-ticket-entries-minute':

                            $data = $this->getMinuteTicketEntries($event_park_id, 'report-ticket-entries-minute', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                        case 'report-transactions-ticket-entry-minute':

                            $data = $this->getMinuteTicketTransactions($event_park_id, 'report-transactions-ticket-entry-minute', 'event-park');

                            $files_structure['event_park'][] = $data;

                            break;
                    }

                }

            }

            if ($event_credential_id && !empty($event_credential)) {

                foreach ($event_credential as $csv) {

                    switch ($csv) {

                        case 'report-whitelist':

                            $data = $this->whitelist($event_credential_id, 'report-whitelist', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                        case 'report-whitelist-masters':

                            $data = $this->whitelistMasters($event_credential_id, 'report-whitelist-masters', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                        case 'report-blacklist':

                            $data = $this->blacklist($event_credential_id, 'report-blacklist', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                        case 'report-entries':

                            $data = $this->getEntriesCredentials($event_credential_id, 'report-entries', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                        case 'report-entries-app':

                            $data = $this->getAppEntries($event_credential_id, 'report-entries-app', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                        case 'report-entries-presidential-box':

                            $data = $this->getPresidentialBoxEntries($event_credential_id, 'report-entries-presidential-box', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                        case 'report-entries-box':

                            $data = $this->getBoxEntries($event_credential_id, 'report-entries-box', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                        case 'report-entries-gate-category':

                            $data = $this->getEntriesByGateAndCategory($event_credential_id, 'report-entries-gate-category', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                        case 'report-unique-title-errors':

                            $data = $this->getErrorsUniqueTitle($event_credential_id, 'report-unique-title-errors', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                        case 'report-transactions-errors':

                            $data = $this->getErrorsTransactions($event_credential_id, 'report-transactions-errors', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                        case 'report-filllevel-minute':

                            $data = $this->getFillLevel($event_credential_id, 'report-filllevel-minute', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                        case 'report-occurences-tcp':
                            break;
                        case 'report-ticket-entries-minute':

                            $data = $this->getMinuteTicketEntries($event_credential_id, 'report-ticket-entries-minute', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                        case 'report-transactions-ticket-entry-minute':

                            $data = $this->getMinuteTicketTransactions($event_id, 'report-transactions-ticket-entry-minute', 'event-credential');

                            $files_structure['event_credential'][] = $data;

                            break;
                    }

                }

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateWhitelist(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($generate) && empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->whitelist($event_id, 'report-whitelist', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->whitelist($event_park_id, 'report-whitelist', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->whitelist($event_credential_id, 'report-whitelist', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateWhitelistMasters(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->whitelistMasters($event_id, 'report-whitelist-masters', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->whitelistMasters($event_park_id, 'report-whitelist-masters', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->whitelistMasters($event_credential_id, 'report-whitelist-masters', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateBlacklist(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->blacklist($event_id, 'report-blacklist', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->blacklist($event_park_id, 'report-blacklist', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->blacklist($event_credential_id, 'report-blacklist', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateEntries(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->getEntries($event_id, 'report-entries', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->getEntries($event_park_id, 'report-entries', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->getEntries($event_credential_id, 'report-entries', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateEntriesApp(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->getAppEntries($event_id, 'report-entries-app', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->getAppEntries($event_park_id, 'report-entries-app', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->getAppEntries($event_credential_id, 'report-entries-app', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateEntriesPresidentialBox(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->getPresidentialBoxEntries($event_id, 'report-entries-presidential-box', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->getPresidentialBoxEntries($event_park_id, 'report-entries-presidential-box', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->getPresidentialBoxEntries($event_credential_id, 'report-entries-presidential-box', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateEntriesBoxes(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->getBoxEntries($event_id, 'report-entries-box', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->getBoxEntries($event_park_id, 'report-entries-box', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->getBoxEntries($event_credential_id, 'report-entries-box', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateEntriesGateCategory(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->getEntriesByGateAndCategory($event_id, 'report-entries-gate-category', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->getEntriesByGateAndCategory($event_park_id, 'report-entries-gate-category', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->getEntriesByGateAndCategory($event_credential_id, 'report-entries-gate-category', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateErrorsUniqueTitle(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->getErrorsUniqueTitle($event_id, 'report-unique-title-errors', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->getErrorsUniqueTitle($event_park_id, 'report-unique-title-errors', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->getErrorsUniqueTitle($event_credential_id, 'report-unique-title-errors', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateErrorsTransactions(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->getErrorsTransactions($event_id, 'report-transactions-errors', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->getErrorsTransactions($event_park_id, 'report-transactions-errors', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->getErrorsTransactions($event_credential_id, 'report-transactions-errors', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateFillLevelMinte(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->getFillLevel($event_id, 'report-filllevel-minute', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->getFillLevel($event_park_id, 'report-filllevel-minute', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->getFillLevel($event_credential_id, 'report-filllevel-minute', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateEntriesMinuteByTitle(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->getMinuteTicketEntries($event_id, 'report-ticket-entries-minute', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->getMinuteTicketEntries($event_park_id, 'report-ticket-entries-minute', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->getMinuteTicketEntries($event_credential_id, 'report-ticket-entries-minute', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    public function csvsReportGenerateTransactionsMinuteEntries(Request $request) {

        try {

            $validator = $this->validate($request, [
                'event_id'  => 'nullable',
                'event_park_id'  => 'nullable',
                'event_credential_id'  => 'nullable',
                'generate'  => 'nullable'
            ]);

            if (empty($request['event_id']) && $request['event_park_id'] && $request['event_credential_id']) {

                return response()->json(array(), 200);

            }

            if (empty($request['generate'])) {

                return response()->json(array(), 200);

            }

            $event_id           = $request['event_id'];
            $event_park_id      = $request['event_park_id'];
            $event_credential_id= $request['event_credential_id'];
            $generate           = $request['generate'];

            $files_structure = array('event' => array(), 'event_park' => array(), 'event_credential' => array());

            if (!empty($event_id) && in_array('event', $generate)) {

                $data = $this->getMinuteTicketTransactions($event_id, 'report-transactions-ticket-entry-minute', 'event');

                $files_structure['event'][] = $data;

            }

            if (!empty($event_park_id) && in_array('event-park', $generate)) {


                $data = $this->getMinuteTicketTransactions($event_park_id, 'report-transactions-ticket-entry-minute', 'event-park');

                $files_structure['event_park'][] = $data;

            }

            if (!empty($event_credential_id) && in_array('event-credential', $generate)) {

                $data = $this->getMinuteTicketTransactions($event_credential_id, 'report-transactions-ticket-entry-minute', 'event-credential');

                $files_structure['event_credential'][] = $data;

            }

            return response()->json($files_structure, 200);

        } catch (\Exception $e) {

            Log::error('Reports Generate Whitelist Request Error');
            Log::error($e);

        }

    }

    private function getEventOptions($event_id) {

        $options = DB::connection('access')->select("
                SELECT
                    code,
                    dataInicio,
                    dataFim,
                    horaAberturaPortas
                FROM
                    gestacess_evento
                WHERE
                    id = " . (int)$event_id . "
            ");

        $event_options = (array) $options[0];

        return $event_options;

    }

    private function whitelist($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $whitelists = DB::connection('access')->select("
            SELECT
                regras_lista_ticket.VA20 AS 'socio',
                regras_lista_ticket.VA21 AS 'nsocio',
                (CASE
                    WHEN regras_lista_ticket.VA5 = 51 THEN 'Senior com desconto'
                    WHEN regras_lista_ticket.VA5 = 52 THEN 'Senior reformado'
                    WHEN regras_lista_ticket.VA5 = 53 THEN 'Junior' 
                    WHEN regras_lista_ticket.VA5 = 54 THEN 'Infantil' 
                    ELSE regras_lista_ticket.VA5
                END) AS 'categoria',
                regras_lista_ticket.VA2 AS 'porta',
                import_list.id,
                import_list.titulo,
                import_list.isWhiteList,
                CAST(import_list.dataExpiracao AS char) AS 'dataExpiracao',
                import_list.importSystem_id,
                import_list.lockImp,
                IFNULL(import_list.blMessage, '') AS blMessage,
                IFNULL(import_list.fromBL, '') AS fromBL,
                IFNULL(import_list.toBL, '') AS toBL,
                import_list.upid,
                import_list.ativo,
                import_list.apagado,
                import_list.codEvent,
                import_list.propriedades,
                import_list.valores,
                CAST(import_list.dataHoraCriacao AS char) AS 'dataHoraCriacao',
                import_list.utilizadorCriacao,
                CAST(import_list.dataHoraAlteracao AS char) AS 'dataHoraAlteracao',
                import_list.utilizadorAlteracao,
                import_list.utid1
            FROM
                gestacess_import_list AS import_list
                LEFT JOIN gestacess_regras_lista_ticket AS regras_lista_ticket
                    ON
                        (
                            regras_lista_ticket.titulo = import_list.titulo
                        )
                LEFT JOIN gestacess_valores_variaveis_acesso AS valores_variaveis_acesso
                    ON
                        (
                            valores_variaveis_acesso.valor = regras_lista_ticket.VA2 AND valores_variaveis_acesso.variavelAcesso_id = 2
                        )
                LEFT JOIN gestacess_evento AS access_event
                        ON (
                                access_event.code = import_list.codEvent
                            )
            WHERE
                access_event.id = '" . (int)$event_id . "'
                AND
                regras_lista_ticket.evento = import_list.codEvent
                AND
                regras_lista_ticket.VA1 = import_list.codEvent
                AND
                import_list.isWhiteList = 1
                AND
                regras_lista_ticket.VA2 <> '50'
        ");

        $header = 'id,titulo,isWhiteList,dataExpiracao,importSystem_id,lockImp,blMessage,fromBL,toBL,upid,ativo,apagado,codEvent,propriedades,valores,dataHoraCriacao,utilizadorCriacao,dataHoraAlteracao,utilizadorAlteracao,utid1,socio,nsocio,categoria,porta';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Whitelist_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $whitelists, 'from' => $from);

    }

    private function whitelistMasters($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $whitelists_masters = DB::connection('access')->select("
            SELECT
                regras_lista_ticket.VA20 AS 'socio',
                regras_lista_ticket.VA21 AS 'nsocio',
                regras_lista_ticket.VA5 AS 'categoria',
                regras_lista_ticket.VA2 AS 'porta',
                import_list.id,
                import_list.titulo,
                import_list.isWhiteList,
                CAST(import_list.dataExpiracao AS char) AS 'dataExpiracao',
                import_list.importSystem_id,
                import_list.lockImp,
                IFNULL(import_list.blMessage, '') AS blMessage,
                IFNULL(import_list.fromBL, '') AS fromBL,
                IFNULL(import_list.toBL, '') AS toBL,
                import_list.upid,
                import_list.ativo,
                import_list.apagado,
                import_list.codEvent,
                import_list.propriedades,
                import_list.valores,
                CAST(import_list.dataHoraCriacao AS char) AS 'dataHoraCriacao',
                import_list.utilizadorCriacao,
                CAST(import_list.dataHoraAlteracao AS char) AS 'dataHoraAlteracao',
                import_list.utilizadorAlteracao,
                import_list.utid1
            FROM
                gestacess_import_list AS import_list
                LEFT JOIN gestacess_regras_lista_ticket AS regras_lista_ticket
                    ON
                        (
                            regras_lista_ticket.titulo = import_list.titulo
                        )
                LEFT JOIN gestacess_valores_variaveis_acesso AS valores_variaveis_acesso
                    ON
                        (
                            valores_variaveis_acesso.valor = regras_lista_ticket.VA2 AND valores_variaveis_acesso.variavelAcesso_id = 2
                        )
                LEFT JOIN gestacess_evento AS access_event
                        ON (
                                access_event.code = import_list.codEvent
                            )
            WHERE
                access_event.id = '" . (int)$event_id . "'
                AND
                regras_lista_ticket.evento = import_list.codEvent
                AND
                regras_lista_ticket.VA1 = import_list.codEvent
                AND
                import_list.isWhiteList = 1
                AND
                regras_lista_ticket.VA2 = '50'
        ");

        $header = 'id,titulo,isWhiteList,dataExpiracao,importSystem_id,lockImp,blMessage,fromBL,toBL,upid,ativo,apagado,codEvent,propriedades,valores,dataHoraCriacao,utilizadorCriacao,dataHoraAlteracao,utilizadorAlteracao,utid1,socio,nsocio,categoria,porta';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Whitelist_Masters_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $whitelists_masters, 'from' => $from);

    }

    private function blacklist($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $blacklists = DB::connection('access')->select("
            select
                gestacess_regras_lista_ticket.VA20 as 'socio'
                ,gestacess_regras_lista_ticket.VA21 as 'nsocio'
                ,gestacess_regras_lista_ticket.VA5 as 'categoria'
                ,gestacess_regras_lista_ticket.VA2 as 'porta'
                ,gestacess_import_list.id
                ,gestacess_import_list.titulo
                ,gestacess_import_list.isWhiteList
                ,cast(gestacess_import_list.dataExpiracao as char) as 'dataExpiracao'
                ,gestacess_import_list.importSystem_id
                ,gestacess_import_list.lockImp
                ,gestacess_import_list.blMessage
                ,gestacess_import_list.fromBL
                ,gestacess_import_list.toBL
                ,gestacess_import_list.upid
                ,gestacess_import_list.ativo
                ,gestacess_import_list.apagado
                ,gestacess_import_list.codEvent
                ,gestacess_import_list.propriedades
                ,gestacess_import_list.valores
                ,cast(gestacess_import_list.dataHoraCriacao as char) as 'dataHoraCriacao'
                ,gestacess_import_list.utilizadorCriacao
                ,cast(gestacess_import_list.dataHoraAlteracao as char) as 'dataHoraAlteracao'
                ,gestacess_import_list.utilizadorAlteracao
                ,gestacess_import_list.utid1
            from
                gestacess_import_list
                left join gestacess_regras_lista_ticket
                    on (gestacess_import_list.titulo = gestacess_regras_lista_ticket.titulo 
                    and gestacess_regras_lista_ticket.evento = '" . $event_options['code'] . "')
            where
                gestacess_import_list.codEvent = '" . $event_options['code'] . "'
                and (gestacess_regras_lista_ticket.va2 <> '33' or gestacess_regras_lista_ticket.va2 is null)
                and gestacess_import_list.isWhiteList = 0
            group by
                gestacess_regras_lista_ticket.VA20 
                ,gestacess_regras_lista_ticket.VA21 
                ,gestacess_regras_lista_ticket.VA5 
                ,gestacess_regras_lista_ticket.VA2 
                ,gestacess_import_list.id
                ,gestacess_import_list.titulo
                ,gestacess_import_list.isWhiteList
                ,cast(gestacess_import_list.dataExpiracao as char) 
                ,gestacess_import_list.importSystem_id
                ,gestacess_import_list.lockImp
                ,gestacess_import_list.blMessage
                ,gestacess_import_list.fromBL
                ,gestacess_import_list.toBL
                ,gestacess_import_list.upid
                ,gestacess_import_list.ativo
                ,gestacess_import_list.apagado
                ,gestacess_import_list.codEvent
                ,gestacess_import_list.propriedades
                ,gestacess_import_list.valores
                ,cast(gestacess_import_list.dataHoraCriacao as char) 
                ,gestacess_import_list.utilizadorCriacao
                ,cast(gestacess_import_list.dataHoraAlteracao as char) 
                ,gestacess_import_list.utilizadorAlteracao
                ,gestacess_import_list.utid1
        ");

        $header = 'id,titulo,isWhiteList,dataExpiracao,importSystem_id,lockImp,blMessage,fromBL,toBL,upid,ativo,apagado,codEvent,propriedades,valores,dataHoraCriacao,utilizadorCriacao,dataHoraAlteracao,utilizadorAlteracao,utid1,socio,nsocio,categoria,porta';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Blacklists_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $blacklists, 'from' => $from);

    }

    private function getEntries($event_id, $from, $type) {

        if ($type == 'event-credential') {

            $event_options = $this->getEventOptions($event_id);

            $entries = DB::connection('access')->select("
              select
                (
                    SELECT code
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
                ) AS 'evento',
                CAST(TIMESTAMP(gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) AS CHAR) AS 'datahora',
                gestacess_transacoes.titulo AS 'codigobarras',
                gestacess_valores_variaveis_acesso.descricao AS 'tipotitulo',
                gestacess_regras_lista_ticket.va18 AS 'entidade',
                gestacess_regras_lista_ticket.va16 AS 'credenciado',
                gestacess_regras_lista_ticket.va17 AS 'permite_reentrada',
                gs_portas.descricao AS 'porta',
                gestacess_dac.code AS 'dac',
                gestacess_dac.ip AS 'ip'
                FROM
                    gestacess_transacoes
                LEFT JOIN gestacess_template_inst ON gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
                LEFT JOIN gestacess_regras_lista_ticket ON(
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo AND gestacess_regras_lista_ticket.va1 =(
                        SELECT CODE
                    FROM
                        gestacess_evento
                    WHERE
                        id = '" . (int)$event_id . "'
                    )
                    )
                LEFT JOIN gestacess_valores_variaveis_acesso ON(
                        gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor AND gestacess_valores_variaveis_acesso.variavelacesso_id = 3
                    )
                LEFT JOIN(
                    SELECT *
                    FROM
                        gestacess_template_inst
                    WHERE
                        isporta = 1
                ) AS gs_portas
                ON
                    gestacess_template_inst.parent_id = gs_portas.id
                WHERE
                    gestacess_transacoes.entry = 1 AND gestacess_transacoes.apagado = 0 AND gestacess_transacoes.isEntry = 1 AND gestacess_transacoes.eventID_id = '" . (int)$event_id . "' AND gestacess_transacoes.dataTransac >=(
                    SELECT
                        datainicio
                    FROM
                        gestacess_evento
                    WHERE
                        id = '" . (int)$event_id . "'
                ) AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "' AND testmode = 0
                ORDER BY
                    CAST(
                        TIMESTAMP(
                            gestacess_transacoes.dataTransac,
                            gestacess_transacoes.horaTransac
                        ) AS CHAR
                    )
              ");

            $header = 'evento,datahora,codigobarras,tipotitulo,entidade,credenciado,permite_reentrada,porta,dac,ip';

            $report_from = 'credencial_';

            $filename = 'Entradas_' . $report_from . $event_options['code'] . '.csv';

            return array('filename' => $filename, 'header' => $header, 'file' => $entries, 'from' => $from);

        } else {

            $event_options = $this->getEventOptions($event_id);

            $entries = DB::connection('access')->select("
            select
              (select
            code
              from
            gestacess_evento
              where
            id = '" . (int)$event_id . "') as 'jogo'
              , cast(min(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac)) as char) as 'datahora'
              , gestacess_regras_lista_ticket.VA2 as 'porta'
              , gestacess_dac.code as 'dac'
              , gestacess_dac.ip as 'ip'
              , gestacess_transacoes.titulo as 'codigobarras'
              , gestacess_regras_lista_ticket.VA20 as 'socio'
              , gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
              , case when gestacess_regras_lista_ticket.VA5 = 51 then 'Senior com desconto' when gestacess_regras_lista_ticket.VA5 = 52 then 'Senior reformado' when gestacess_regras_lista_ticket.VA5 = 53 then 'Junior' when gestacess_regras_lista_ticket.VA5 = 54 then 'Infantil' else gestacess_regras_lista_ticket.VA5 end as 'categoria'
              , (case
              when gestacess_transacoes.tipoleitor = 1
                then 'CODIGO BARRAS'
              when gestacess_transacoes.tipoleitor = 2
                then 'NFC'
              else ''
            end) as 'tipoleitor',
            '' AS 'camarote'
            from
              (
            select
              min(gestacess_transacoes.id) as id
              , gestacess_transacoes.titulo as 'codigobarras'
            from
              gestacess_transacoes
              left join gestacess_template_inst
                on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
              left join gestacess_dac
                on gestacess_template_inst.dac_id = gestacess_dac.id
              left join gestacess_regras_lista_ticket
                on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                  and gestacess_regras_lista_ticket.va1 = (
                select
                  code
                from
                  gestacess_evento
                where
                  id = '" . (int)$event_id . "'
                  )
                )
              left join gestacess_valores_variaveis_acesso
                on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                  and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
            where
              gestacess_transacoes.entry=1
              and gestacess_transacoes.apagado = 0
              and gestacess_transacoes.isEntry  = 1
              and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
              and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
              and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
              and testmode = 0
              and areaentryid <> 330
              AND
              gestacess_transacoes.vas NOT LIKE '%TCP%'
              and gestacess_transacoes.titulo not in (
                select
                  titulo
                from
                  gestacess_regras_lista_ticket
                where
                  va3 = '91'
                  and va1 = (
                select
                  code
                from
                  gestacess_evento
                where
                  id = '" . (int)$event_id . "'
                  )
              )
            group by
              gestacess_transacoes.titulo
              ) as ent
              left join gestacess_transacoes
            on ent.id = gestacess_transacoes.id
              left join gestacess_template_inst
            on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
              left join gestacess_dac
            on gestacess_template_inst.dac_id = gestacess_dac.id
              left join gestacess_regras_lista_ticket
            on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
              and gestacess_regras_lista_ticket.va1 = (
                select
                  code
                from
                  gestacess_evento
                where
                  id = '" . (int)$event_id . "'
              )
            )
              left join gestacess_valores_variaveis_acesso
            on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
              and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
            where
              gestacess_transacoes.entry=1
              and gestacess_transacoes.apagado = 0
              and gestacess_transacoes.isEntry  = 1
              and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
              and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
              and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
              and testmode = 0
              and areaentryid <> 330
              AND
              gestacess_transacoes.vas NOT LIKE '%TCP%'
              and gestacess_transacoes.titulo not in (
            select
              titulo
            from
              gestacess_regras_lista_ticket
            where
              va3 = '91'
              and va1 = (
                select
                  code
                from
                  gestacess_evento
                where
                  id = '" . (int)$event_id . "'
              )
              )
            group by
              gestacess_regras_lista_ticket.VA2
              , gestacess_dac.code
              , gestacess_dac.ip
              , gestacess_transacoes.titulo
              , gestacess_regras_lista_ticket.VA20
              , gestacess_regras_lista_ticket.VA21
              , gestacess_valores_variaveis_acesso.descricao
              , gestacess_regras_lista_ticket.VA5
              , (case
            when gestacess_transacoes.tipoleitor = 1
              then 'CODIGO BARRAS'
            when gestacess_transacoes.tipoleitor = 2
              then 'NFC'
            else ''
              end) 
        union all 
              select
            (select
              code
            from
              gestacess_evento
            where 
              id = '" . (int)$event_id . "') as 'jogo'
            , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'datahora'
            , gestacess_regras_lista_ticket.VA2 as 'porta'
            , gestacess_dac.code as 'dac'
            , gestacess_dac.ip as 'ip'
            , gestacess_transacoes.titulo as 'codigobarras'
            , '' as 'socio'
            , '' as 'nsocio'
            , 'Livre Transito' as 'tipotitulo'
            , '' as 'categoria'
            , (case
              when gestacess_transacoes.tipoleitor = 1
                then 'CODIGO BARRAS'
              when gestacess_transacoes.tipoleitor = 2
                then 'NFC'
              else ''
            end) as 'tipoleitor',
            '' AS 'camarote'
              from
            gestacess_transacoes
            left join gestacess_template_inst
              on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
            left join gestacess_dac
              on gestacess_template_inst.dac_id = gestacess_dac.id
            left join gestacess_regras_lista_ticket
              on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                and gestacess_regras_lista_ticket.va1 = (
                  select
                code
                  from
                gestacess_evento
                  where
                id = '" . (int)$event_id . "'
                )
              )
            left join gestacess_valores_variaveis_acesso
              on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
              where
            gestacess_transacoes.entry = 1
            and gestacess_transacoes.apagado = 0
            and gestacess_transacoes.isEntry  = 1
            and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
            and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
            and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
            and areaentryid <> 330
            AND
              gestacess_transacoes.vas NOT LIKE '%TCP%'
            and gestacess_transacoes.titulo in (
              select
                titulo
              from
                gestacess_regras_lista_ticket
              where
                va3 = '91'
              and va1 = (
                select
                  code
                from
                  gestacess_evento
                where
                  id = '" . (int)$event_id . "'
              )
              ) 
            order by
              datahora 
        ");

            $extra_entries = DB::connection('access')->select("
            SELECT
                (
                SELECT CODE
            FROM
                gestacess_evento
            WHERE
                id = '" . (int)$event_id . "'
            ) AS 'jogo',
            CAST(
                MIN(
                    TIMESTAMP(
                        gestacess_transacoes.dataTransac,
                        gestacess_transacoes.horaTransac
                    )
                ) AS CHAR
            ) AS 'datahora',
            temp_block.descricao AS 'porta',
            gestacess_dac.code AS 'dac',
            gestacess_dac.ip AS 'ip',
            gestacess_transacoes.titulo AS 'codigobarras',
            gestacess_transacoes.vas AS 'vas',
            '' AS 'socio',
            '' AS 'nsocio',
            'Entradas Extra TCP' AS 'tipotitulo',
            '' AS 'categoria',
            '' AS 'tipoleitor'
            FROM
                (
                SELECT
                    MIN(gestacess_transacoes.id) AS id,
                    gestacess_transacoes.titulo AS 'codigobarras'
                FROM
                    gestacess_transacoes
                LEFT JOIN gestacess_template_inst ON gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
                LEFT JOIN gestacess_regras_lista_ticket ON(
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo AND gestacess_regras_lista_ticket.va1 =(
                        SELECT CODE
                    FROM
                        gestacess_evento
                    WHERE
                        id = '" . (int)$event_id . "'
                    )
                    )
                LEFT JOIN gestacess_valores_variaveis_acesso ON(
                        gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor AND gestacess_valores_variaveis_acesso.variavelacesso_id = 3
                    )
                    LEFT JOIN gestacess_template_inst AS temp_block
                            ON
                                (
                                    temp_block.id = gestacess_transacoes.areaEntryId AND temp_block.isPorta = 1
                                )
                            LEFT JOIN gestacess_template_inst AS temp_template
                            ON
                                (
                                    temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                                )
                            LEFT JOIN gestacess_template AS template
                            ON
                                (
                                    template.id = temp_template.template_id
                                )
                WHERE
                    gestacess_transacoes.entry = 1 AND gestacess_transacoes.apagado = 0 AND gestacess_transacoes.isEntry = 1 AND gestacess_transacoes.eventID_id = '" . (int)$event_id . "' AND gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "' AND testmode = 0 AND areaentryid <> 330 AND gestacess_transacoes.vas LIKE '%TCP%' AND gestacess_transacoes.titulo NOT IN(
                    SELECT
                        titulo
                    FROM
                        gestacess_regras_lista_ticket
                    WHERE
                        va3 = '91' AND va1 =(
                        SELECT CODE
                    FROM
                        gestacess_evento
                    WHERE
                        id = '" . (int)$event_id . "'
                    )
                )
            GROUP BY
                gestacess_transacoes.titulo
            ) AS ent
            LEFT JOIN gestacess_transacoes ON ent.id = gestacess_transacoes.id
            LEFT JOIN gestacess_template_inst ON gestacess_transacoes.template_inst_id = gestacess_template_inst.id
            LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
            LEFT JOIN gestacess_regras_lista_ticket ON(
                    gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo AND gestacess_regras_lista_ticket.va1 =(
                    SELECT CODE
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
                )
                )
            LEFT JOIN gestacess_valores_variaveis_acesso ON(
                    gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor AND gestacess_valores_variaveis_acesso.variavelacesso_id = 3
                )
                LEFT JOIN gestacess_template_inst AS temp_block
                            ON
                                (
                                    temp_block.id = gestacess_transacoes.areaEntryId AND temp_block.isPorta = 1
                                )
                            LEFT JOIN gestacess_template_inst AS temp_template
                            ON
                                (
                                    temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                                )
                            LEFT JOIN gestacess_template AS template
                            ON
                                (
                                    template.id = temp_template.template_id
                                )
            WHERE
                gestacess_transacoes.entry = 1 AND gestacess_transacoes.apagado = 0 AND gestacess_transacoes.isEntry = 1 AND gestacess_transacoes.eventID_id = '" . (int)$event_id . "' AND gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "' AND testmode = 0 AND areaentryid <> 330 AND gestacess_transacoes.vas LIKE '%TCP%' AND gestacess_transacoes.titulo NOT IN(
                SELECT
                    titulo
                FROM
                    gestacess_regras_lista_ticket
                WHERE
                    va3 = '91' AND va1 =(
                    SELECT CODE
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
                )
            )
            GROUP BY
                temp_block.descricao,
                gestacess_dac.code,
                gestacess_dac.ip,
                gestacess_transacoes.titulo,
                gestacess_transacoes.vas,
                '',
                '',
                '',
                '',
                '' 
        ");

            foreach ($extra_entries as $entry) {

                preg_match('/;TCPVA5:\d{1,2};VA24:(\d{1,2})/', $entry->vas, $result);

                if (isset($result[1])) {

                    $entry->camarote = $result[1];

                    if ((int)$result[1] > 24) {

                        $entry->porta = 'Porta 15';

                    } else {

                        $entry->porta = 'Porta 1';

                    }

                }

            }

            $all_entries = array_merge($entries, $extra_entries);

            $header = 'jogo,datahora,porta,camarote,dac,ip,codigobarras,socio,nsocio,tipotitulo,categoria,tipoleitor';

            $report_from = '';

            switch ($type) {

                case 'event':
                    $report_from = 'estadio_';
                    break;
                case 'event-park':
                    $report_from = 'parque_';
                    break;
                case 'event-credential':
                    $report_from = 'credencial_';

            }

            $filename = 'Entradas_' . $report_from . $event_options['code'] . '.csv';

            return array('filename' => $filename, 'header' => $header, 'file' => $all_entries, 'from' => $from);

        }

    }

    private function getEntriesCredentials($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $entries = DB::connection('access')->select("
            select  
              (select
                code
              from
                gestacess_evento
              where
                id = '" . (int)$event_id . "') as 'evento'
              , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'datahora'
              , gestacess_transacoes.titulo as 'codigobarras'
              , gestacess_valores_variaveis_acesso.descricao as 'tipotitulo'
              , replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_regras_lista_ticket.va18 USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'entidade'
                , replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_regras_lista_ticket.va16 USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'credenciado'
              , gestacess_regras_lista_ticket.va17 as 'reentrada'
              , gs_portas.descricao as 'porta'
              , gestacess_dac.code as 'dac'
              , gestacess_dac.ip as 'ip'
            from
              gestacess_transacoes
              left join gestacess_template_inst
                on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
              left join gestacess_dac
                on gestacess_template_inst.dac_id = gestacess_dac.id
              left join gestacess_regras_lista_ticket
                on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo 
                and gestacess_regras_lista_ticket.va1 = (
                  select
                code
                  from
                gestacess_evento
                  where
                id = '" . (int)$event_id . "'
                ))
              left join gestacess_valores_variaveis_acesso 
                on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
              left join (
                select
                  *
                from
                  gestacess_template_inst
                where
                  isporta = 1
              ) as gs_portas
                on gestacess_template_inst.parent_id = gs_portas.id
            where 
              gestacess_transacoes.entry = 1
              and gestacess_transacoes.apagado = 0
              and gestacess_transacoes.isEntry  = 1
              and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
              and gestacess_transacoes.dataTransac >= (
                select
                  datainicio
                from
                  gestacess_evento
                where
                  id = '" . (int)$event_id . "'
              )
              and gestacess_transacoes.horaTransac >= '03:00'
              and testmode = 0
            order by
              cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char)
        ");

        $header = 'evento,datahora,codigobarras,tipotitulo,entidade,credenciado,permite_reentrada,descricao_8,dac,ip';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Entradas_Credenciamento_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $entries, 'from' => $from);

    }

    private function getAppEntries($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $entries = DB::connection('access')->select("
            SELECT
                DISTINCT gestacess_transacoes.titulo AS 'codigobarras',
                CONCAT(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac) AS 'datahora',
                gestacess_regras_lista_ticket.VA20 AS 'socio',
                gestacess_regras_lista_ticket.VA21 AS 'nsocio',
                (
                    SELECT 
                    va_rules_values.descricao
                    FROM
                    gestacess_valores_variaveis_acesso AS va_rules_values
                    WHERE
                    va_rules_values.valor = gestacess_regras_lista_ticket.VA5 AND va_rules_values.variavelAcesso_id = 5
                ) AS 'categoria',
                (
                    SELECT 
                    va_rules_values.descricao
                    FROM
                    gestacess_valores_variaveis_acesso AS va_rules_values
                    WHERE
                    va_rules_values.valor = gestacess_regras_lista_ticket.VA3 AND va_rules_values.variavelAcesso_id = 3
                ) AS 'tipotitulo',
                gestacess_evento.code AS 'jogo',
                temp_gate.descricao AS 'porta',
                gestacess_dac.ip AS 'ip',
                gestacess_dac.descricao AS 'dac',
                (CASE
                    WHEN gestacess_transacoes.tipoleitor = 1
                    THEN 'CODIGO BARRAS'
                    WHEN gestacess_transacoes.tipoleitor = 2
                    THEN 'NFC'
                    ELSE ''
                 END) AS 'tipoleitor' 
            FROM
                gestacess_transacoes
                LEFT JOIN gestacess_evento ON (
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                )
                LEFT JOIN gestacess_template_inst AS temp_inst_dac ON (
                    gestacess_transacoes.template_inst_id = temp_inst_dac.id
                    AND
                    temp_inst_dac.id = gestacess_transacoes.template_inst_id
                )
                LEFT JOIN gestacess_dac ON (
                    gestacess_dac.id = temp_inst_dac.dac_id
                )
                LEFT JOIN gestacess_template_inst AS temp_gate ON (
                    temp_gate.id = temp_inst_dac.parent_id AND temp_gate.isPorta = 1
                )
                LEFT JOIN gestacess_template_inst AS temp_block ON (
                    temp_block.id = temp_gate.parent_id AND temp_block.isZona = 1
                )
                LEFT JOIN gestacess_template_inst AS temp_template ON (
                    temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                )
                LEFT JOIN gestacess_template AS template ON (
                    template.id = temp_template.template_id
                )
                LEFT JOIN gestacess_regras_lista_ticket ON (
                    gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                    AND
                    gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                )
                LEFT JOIN gestacess_valores_variaveis_acesso ON (
                    gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                    AND
                    gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                )
            WHERE
                gestacess_evento.id = '" . (int)$event_id . "'
                AND
                gestacess_evento.code = gestacess_regras_lista_ticket.evento
                AND
                gestacess_transacoes.eventId_id = gestacess_evento.id
                AND
                gestacess_transacoes.entry = '1'
                AND
                gestacess_transacoes.apagado = '0'
                AND
                gestacess_transacoes.isEntry = '1'
                AND
                gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                AND
                gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                AND
                gestacess_transacoes.testMode = '0'
                AND
                gestacess_transacoes.areaEntryId <> '330'
                AND
                gestacess_regras_lista_ticket.VA3 <> '91'
                AND
                LENGTH(gestacess_transacoes.titulo) < 16
                AND
                gestacess_transacoes.tipoleitor = 1
                AND
                (gestacess_valores_variaveis_acesso.descricao like '%Superliga%' or gestacess_valores_variaveis_acesso.descricao like '%Lugar Anual%')
        ");

        $header = 'jogo,datahora,porta,dac,ip,codigobarras,socio,nsocio,tipotitulo,categoria,tipoleitor';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Entradas_APP_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $entries, 'from' => $from);

    }

    private function getPresidentialBoxEntries($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $entries = DB::connection('access')->select("
            SELECT
                DISTINCT transactions.titulo AS 'codigobarras',
                CONCAT(transactions.dataTransac, ' ', transactions.horaTransac) AS 'datahora',
                gestacess_regras_lista_ticket.VA20 AS 'socio',
                gestacess_regras_lista_ticket.VA21 AS 'nsocio',
                (
                    SELECT 
                        va_rules_values.descricao
                    FROM
                        gestacess_valores_variaveis_acesso AS va_rules_values
                    WHERE
                        va_rules_values.valor = gestacess_regras_lista_ticket.VA5 AND va_rules_values.variavelAcesso_id = 5
                ) AS 'categoria',
                (
                    SELECT 
                        va_rules_values.descricao
                    FROM
                        gestacess_valores_variaveis_acesso AS va_rules_values
                    WHERE
                        va_rules_values.valor = gestacess_regras_lista_ticket.VA3 AND va_rules_values.variavelAcesso_id = 3
                ) AS 'tipotitulo',
                gestacess_evento.code as 'jogo',
                temp_gate.descricao AS 'porta',
                dac.ip AS 'ip',
                dac.descricao AS 'dac',
                (CASE
                  WHEN transactions.tipoleitor = 1
                    THEN 'CODIGO BARRAS'
                  WHEN transactions.tipoleitor = 2
                    THEN 'NFC'
                  ELSE ''
                END) AS 'tipoleitor'
            FROM
                gestacess_template_inst AS temp_inst_dac
            LEFT JOIN gestacess_dac AS dac ON (
                dac.id = temp_inst_dac.dac_id 
                AND
                temp_inst_dac.isDac = 1
            )
            LEFT JOIN gestacess_template_inst AS temp_gate
            ON
                (
                    temp_gate.id = temp_inst_dac.parent_id AND temp_gate.isPorta = 1
                )
            LEFT JOIN gestacess_template_inst AS temp_block
            ON
                (
                    temp_block.id = temp_gate.parent_id AND temp_block.isZona = 1
                )
            LEFT JOIN gestacess_template_inst AS temp_template
            ON
                (
                    temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                )
            LEFT JOIN gestacess_template AS template
            ON
                (
                    template.id = temp_template.template_id
                )
            LEFT JOIN gestacess_transacoes AS transactions
            ON
                (
                    temp_inst_dac.id = transactions.template_inst_id
                    AND
                    transactions.entry = 1
                    AND
                    transactions.isEntry = 1
                    AND
                    transactions.apagado = 0
                    AND
                    transactions.testMode = 0
                    AND
                    transactions.areaEntryId <> 330
                )
            LEFT JOIN gestacess_evento ON (
                        transactions.eventId_id = gestacess_evento.id
                    )
            LEFT JOIN gestacess_regras_lista_ticket ON (
                        transactions.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
            LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
            WHERE
                transactions.eventId_id = '" . (int)$event_id . "'
                AND
                transactions.dataTransac >= '" . $event_options['dataInicio'] . "'
                AND
                transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                AND
                gestacess_regras_lista_ticket.VA2 = 29
        ");

        $header = 'jogo,datahora,porta,dac,ip,codigobarras,socio,nsocio,tipotitulo,categoria,tipoleitor';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Entradas_Camarote_Presidencial_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $entries, 'from' => $from);

    }

    private function getBoxEntries($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $entries = DB::connection('access')->select("
            SELECT
                DISTINCT transactions.titulo AS 'titulo',
                CONCAT(transactions.dataTransac, ' ', transactions.horaTransac) AS 'datahora',
                gestacess_regras_lista_ticket.VA20 AS 'socio',
                gestacess_regras_lista_ticket.VA21 AS 'nsocio',
                gestacess_regras_lista_ticket.VA24 AS 'camarote',
                gestacess_regras_lista_ticket.VA26 AS 'entidade',
                (
                    SELECT 
                        va_rules_values.descricao
                    FROM
                        gestacess_valores_variaveis_acesso AS va_rules_values
                    WHERE
                        va_rules_values.valor = gestacess_regras_lista_ticket.VA5 AND va_rules_values.variavelAcesso_id = 5
                ) AS 'categoria',
                (
                    SELECT 
                        va_rules_values.descricao
                    FROM
                        gestacess_valores_variaveis_acesso AS va_rules_values
                    WHERE
                        va_rules_values.valor = gestacess_regras_lista_ticket.VA3 AND va_rules_values.variavelAcesso_id = 3
                ) AS 'tipotitulo',
                gestacess_evento.code as 'jogo',
                temp_gate.descricao AS 'porta',
                dac.ip AS 'ip',
                dac.descricao AS 'dac',
                (CASE
                  WHEN transactions.tipoleitor = 1
                    THEN 'CODIGO BARRAS'
                  WHEN transactions.tipoleitor = 2
                    THEN 'NFC'
                  ELSE ''
                END) AS 'tipoleitor'
            FROM
                gestacess_template_inst AS temp_inst_dac
            LEFT JOIN gestacess_dac AS dac ON (
                dac.id = temp_inst_dac.dac_id 
                AND
                temp_inst_dac.isDac = 1
            )
            LEFT JOIN gestacess_template_inst AS temp_gate
            ON
                (
                    temp_gate.id = temp_inst_dac.parent_id AND temp_gate.isPorta = 1
                )
            LEFT JOIN gestacess_template_inst AS temp_block
            ON
                (
                    temp_block.id = temp_gate.parent_id AND temp_block.isZona = 1
                )
            LEFT JOIN gestacess_template_inst AS temp_template
            ON
                (
                    temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                )
            LEFT JOIN gestacess_template AS template
            ON
                (
                    template.id = temp_template.template_id
                )
            LEFT JOIN gestacess_transacoes AS transactions
            ON
                (
                    temp_inst_dac.id = transactions.template_inst_id
                    AND
                    transactions.entry = 1
                    AND
                    transactions.isEntry = 1
                    AND
                    transactions.apagado = 0
                    AND
                    transactions.testMode = 0
                    AND
                    transactions.areaEntryId <> 330
                )
            LEFT JOIN gestacess_evento ON (
                        transactions.eventId_id = gestacess_evento.id
                    )
            LEFT JOIN gestacess_regras_lista_ticket ON (
                        transactions.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
            LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
            WHERE
                transactions.eventId_id = '" . (int)$event_id . "'
                AND
                transactions.dataTransac = '" . $event_options['dataInicio'] . "'
                AND
                transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                AND
                gestacess_regras_lista_ticket.VA8 LIKE '%Cam%'
                AND
                gestacess_regras_lista_ticket.VA24 < 90
            ORDER BY
            LENGTH(gestacess_regras_lista_ticket.VA24), gestacess_regras_lista_ticket.VA24
        ");

        $entries_presidential_box = DB::connection('access')->select("
            SELECT
                DISTINCT transactions.titulo AS 'titulo',
                CONCAT(transactions.dataTransac, ' ', transactions.horaTransac) AS 'datahora',
                gestacess_regras_lista_ticket.VA20 AS 'socio',
                gestacess_regras_lista_ticket.VA21 AS 'nsocio',
                gestacess_regras_lista_ticket.VA24 AS 'camarote',
                gestacess_regras_lista_ticket.VA26 AS 'entidade',
                (
                    SELECT 
                        va_rules_values.descricao
                    FROM
                        gestacess_valores_variaveis_acesso AS va_rules_values
                    WHERE
                        va_rules_values.valor = gestacess_regras_lista_ticket.VA5 AND va_rules_values.variavelAcesso_id = 5
                ) AS 'categoria',
                (
                    SELECT 
                        va_rules_values.descricao
                    FROM
                        gestacess_valores_variaveis_acesso AS va_rules_values
                    WHERE
                        va_rules_values.valor = gestacess_regras_lista_ticket.VA3 AND va_rules_values.variavelAcesso_id = 3
                ) AS 'tipotitulo',
                gestacess_evento.code as 'jogo',
                temp_gate.descricao AS 'porta',
                dac.ip AS 'ip',
                dac.descricao AS 'dac',
                (CASE
                  WHEN transactions.tipoleitor = 1
                    THEN 'CODIGO BARRAS'
                  WHEN transactions.tipoleitor = 2
                    THEN 'NFC'
                  ELSE ''
                END) AS 'tipoleitor'
            FROM
                gestacess_template_inst AS temp_inst_dac
            LEFT JOIN gestacess_dac AS dac ON (
                dac.id = temp_inst_dac.dac_id 
                AND
                temp_inst_dac.isDac = 1
            )
            LEFT JOIN gestacess_template_inst AS temp_gate
            ON
                (
                    temp_gate.id = temp_inst_dac.parent_id AND temp_gate.isPorta = 1
                )
            LEFT JOIN gestacess_template_inst AS temp_block
            ON
                (
                    temp_block.id = temp_gate.parent_id AND temp_block.isZona = 1
                )
            LEFT JOIN gestacess_template_inst AS temp_template
            ON
                (
                    temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                )
            LEFT JOIN gestacess_template AS template
            ON
                (
                    template.id = temp_template.template_id
                )
            LEFT JOIN gestacess_transacoes AS transactions
            ON
                (
                    temp_inst_dac.id = transactions.template_inst_id
                    AND
                    transactions.entry = 1
                    AND
                    transactions.isEntry = 1
                    AND
                    transactions.apagado = 0
                    AND
                    transactions.testMode = 0
                    AND
                    transactions.areaEntryId <> 330
                )
            LEFT JOIN gestacess_evento ON (
                        transactions.eventId_id = gestacess_evento.id
                    )
            LEFT JOIN gestacess_regras_lista_ticket ON (
                        transactions.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
            LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
            WHERE
                transactions.eventId_id = '" . (int)$event_id . "'
                AND
                transactions.dataTransac >= '" . $event_options['dataInicio'] . "'
                AND
                transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                AND
                gestacess_regras_lista_ticket.VA2 = 29
        ");

        $entries_extra = DB::connection('access')->select("
            SELECT
                    transactions.titulo AS 'titulo',
                    CONCAT(transactions.dataTransac, ' ', transactions.horaTransac) AS 'datahora',
                    '' AS 'socio',
                    '' AS 'nsocio',
                    '' AS 'categoria',
                    '' AS 'camarote',
                    '' AS 'entidade',
                    'Entrada Extra TCP' AS 'tipotitulo',
                    gestacess_evento.code as 'jogo',
                    temp_gate.descricao AS 'porta',
                    '' AS 'ip',
                    'DRG-TCP' AS 'dac',
                    'MANUAL' AS 'tipoleitor',
                    transactions.vas
                FROM
                    gestacess_template_inst AS temp_inst_dac
                LEFT JOIN gestacess_dac AS dac ON (
                	dac.id = temp_inst_dac.dac_id 
                    AND
                    temp_inst_dac.isDac = 1
                )
                LEFT JOIN gestacess_template_inst AS temp_gate
                ON
                    (
                        temp_gate.id = temp_inst_dac.parent_id AND temp_gate.isPorta = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_block
                ON
                    (
                        temp_block.id = temp_gate.parent_id AND temp_block.isZona = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_template
                ON
                    (
                        temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                    )
                LEFT JOIN gestacess_template AS template
                ON
                    (
                        template.id = temp_template.template_id
                    )
                LEFT JOIN gestacess_transacoes AS transactions
                ON
                    (
                        temp_inst_dac.id = transactions.template_inst_id
                        AND
                        transactions.entry = 1
                        AND
                        transactions.isEntry = 1
                        AND
                        transactions.apagado = 0
                        AND
                        transactions.testMode = 0
                        AND
                        transactions.areaEntryId <> 330
                    )
                LEFT JOIN gestacess_evento ON (
                            transactions.eventId_id = gestacess_evento.id
                        )
                WHERE
                    transactions.eventId_id = '" . (int)$event_id . "'
                    AND
                    transactions.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    transactions.vas LIKE '%TCP%'
        ");

        foreach ($entries_extra as $entry) {

            preg_match('/;TCPVA5:\d{1,2};VA24:(\d{1,2})/', $entry->vas, $result);

            if (isset($result[1])) {

                $entry->camarote = $result[1];

                if ((int)$result[1] > 24) {

                    $entry->porta = 'Porta 15';

                } else {

                    $entry->porta = 'Porta 1';

                }

            }

        }

        $all_entries = array_merge($entries, $entries_presidential_box, $entries_extra);

        $header = 'entidade,jogo,datahora,porta,dac,ip,titulo,socio,nsocio,tipotitulo,categoria,tipoleitor,camarote';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Entradas_Camarotes_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $all_entries, 'from' => $from);

    }

    private function getEntriesByGateAndCategory($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $entries = DB::connection('access')->select("
            select
                left(dac,(length(dac)-5)) as 'Porta'
                , sum(if(categoria = 53,1,0)) as '53'
                , sum(if(categoria = 54,1,0)) as '54'
                , sum(if(categoria = 57,1,0)) as '57'
                , sum(if(categoria = 59,1,0)) as '59'
                , sum(if(categoria = 99,1,0)) as '99'
                , sum(if(categoria = 53,1,0))+sum(if(categoria = 54,1,0))+sum(if(categoria = 57,1,0))+sum(if(categoria = 59,1,0))+sum(if(categoria = 99,1,0)) as 'Total'
            from 
              (select
                  (select
                code
                  from
                gestacess_evento
                  where
                id = '" . (int)$event_id . "') as 'jogo'
                  , cast(min(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac)) as char) as 'datahora'
                  , gestacess_regras_lista_ticket.VA2 as 'porta'
                  , gestacess_dac.code as 'dac'
                  , gestacess_dac.ip as 'ip'
                  , gestacess_transacoes.titulo as 'codigobarras'
                  , gestacess_regras_lista_ticket.VA20 as 'socio'
                  , gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                  , gestacess_regras_lista_ticket.VA5 as 'categoria'
                  , (case
                  when gestacess_transacoes.tipoleitor = 1
                    then 'CODIGO BARRAS'
                  when gestacess_transacoes.tipoleitor = 2
                    then 'NFC'
                  else ''
                end) as 'tipoleitor'
                from
                  (
                select
                  min(gestacess_transacoes.id) as id
                  , gestacess_transacoes.titulo as 'codigobarras'
                from
                  gestacess_transacoes
                  left join gestacess_template_inst
                    on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                  left join gestacess_dac
                    on gestacess_template_inst.dac_id = gestacess_dac.id
                  left join gestacess_regras_lista_ticket
                    on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                      and gestacess_regras_lista_ticket.va1 = (
                    select
                      code
                    from
                      gestacess_evento
                    where
                      id = '" . (int)$event_id . "'
                      )
                    )
                  left join gestacess_valores_variaveis_acesso
                    on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                      and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                where
                  gestacess_transacoes.entry=1
                  and gestacess_transacoes.apagado = 0
                  and gestacess_transacoes.isEntry  = 1
                  and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                  and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                  and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                  and testmode = 0
                  and areaentryid <> 330
                  and gestacess_transacoes.titulo not in (
                    select
                      titulo
                    from
                      gestacess_regras_lista_ticket
                    where
                      va3 = '91'
                      and va1 = (
                    select
                      code
                    from
                      gestacess_evento
                    where
                      id = '" . (int)$event_id . "'
                      )
                  )
                group by
                  gestacess_transacoes.titulo
                  ) as ent
                  left join gestacess_transacoes
                on ent.id = gestacess_transacoes.id
                  left join gestacess_template_inst
                on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                  left join gestacess_dac
                on gestacess_template_inst.dac_id = gestacess_dac.id
                  left join gestacess_regras_lista_ticket
                on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                  and gestacess_regras_lista_ticket.va1 = (
                    select
                      code
                    from
                      gestacess_evento
                    where
                      id = '" . (int)$event_id . "'
                  )
                )
                  left join gestacess_valores_variaveis_acesso
                on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                  and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                where
                  gestacess_transacoes.entry=1
                  and gestacess_transacoes.apagado = 0
                  and gestacess_transacoes.isEntry  = 1
                  and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                  and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                  and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                  and testmode = 0
                  and areaentryid <> 330
                  and gestacess_transacoes.titulo not in (
                select
                  titulo
                from
                  gestacess_regras_lista_ticket
                where
                  va3 = '91'
                  and va1 = (
                    select
                      code
                    from
                      gestacess_evento
                    where
                      id = '" . (int)$event_id . "'
                  )
                  )
                group by
                  gestacess_regras_lista_ticket.VA2
                  , gestacess_dac.code
                  , gestacess_dac.ip
                  , gestacess_transacoes.titulo
                  , gestacess_regras_lista_ticket.VA20
                  , gestacess_regras_lista_ticket.VA21
                  , gestacess_valores_variaveis_acesso.descricao
                  , gestacess_regras_lista_ticket.VA5
                  , (case
                when gestacess_transacoes.tipoleitor = 1
                  then 'CODIGO BARRAS'
                when gestacess_transacoes.tipoleitor = 2
                  then 'NFC'
                else ''
                  end) 
            union all 
                  select
                (select
                  code
                from
                  gestacess_evento
                where 
                  id = '" . (int)$event_id . "') as 'jogo'
                , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'datahora'
                , gestacess_regras_lista_ticket.VA2 as 'porta'
                , gestacess_dac.code as 'dac'
                , gestacess_dac.ip as 'ip'
                , gestacess_transacoes.titulo as 'codigobarras'
                , '' as 'socio'
                , '' as 'nsocio'
                , 'Livre Transito' as 'tipotitulo'
                , '' as 'categoria'
                , (case
                  when gestacess_transacoes.tipoleitor = 1
                    then 'CODIGO BARRAS'
                  when gestacess_transacoes.tipoleitor = 2
                    then 'NFC'
                  else ''
                end) as 'tipoleitor'
                  from
                gestacess_transacoes
                left join gestacess_template_inst
                  on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                left join gestacess_dac
                  on gestacess_template_inst.dac_id = gestacess_dac.id
                left join gestacess_regras_lista_ticket
                  on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                    and gestacess_regras_lista_ticket.va1 = (
                      select
                    code
                      from
                    gestacess_evento
                      where
                    id = '" . (int)$event_id . "'
                    )
                  )
                left join gestacess_valores_variaveis_acesso
                  on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                    and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                  where
                gestacess_transacoes.entry = 1
                and gestacess_transacoes.apagado = 0
                and gestacess_transacoes.isEntry  = 1
                and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                and areaentryid <> 330
                and gestacess_transacoes.titulo in (
                  select
                    titulo
                  from
                    gestacess_regras_lista_ticket
                  where
                    va3 = '91'
                  and va1 = (
                    select
                      code
                    from
                      gestacess_evento
                    where
                      id = '" . (int)$event_id . "'
                  )
                  ) 
                order by
                  datahora 
            
              ) as sub
            
            where
              categoria > 50
             
            group by
              left(dac,(length(dac)-5))
            
            order by
              count(datahora) desc
        ");

        $cat_53 = 0;
        $cat_54 = 0;
        $cat_57 = 0;
        $cat_59 = 0;
        $cat_99 = 0;
        $total = 0;

        foreach ($entries AS $entry) {

            $cat_53 = $cat_53 + $entry->{'53'};
            $cat_54 = $cat_54 + $entry->{'54'};
            $cat_57 = $cat_57 + $entry->{'57'};
            $cat_59 = $cat_59 + $entry->{'59'};
            $cat_99 = $cat_99 + $entry->{'99'};
            $total = $total + $entry->{'Total'};

        }

        $entries[] = array(
            'Porta' => '',
            '53'    => '',
            '54'    => '',
            '57'    => '',
            '59'    => '',
            '99'    => '',
            'Total' => ''
        );

        $entries[] = array(
            'Porta' => 'Total',
            '53'    => $cat_53,
            '54'    => $cat_54,
            '57'    => $cat_57,
            '59'    => $cat_59,
            '99'    => $cat_99,
            'Total' => $total
        );

        $header = 'Porta,53,54,57,59,99,Total';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Entradas_por_Porta_e_Categoria_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $entries, 'from' => $from);

    }

    private function getErrorsUniqueTitle($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $errors = DB::connection('access')->select("
            select
              distinct Socio
              , IFNULL(Socio, '') AS Socio_N
              , IFNULL(NSocio, '') AS NSocio
              , IFNULL(Categoria, '') AS Categoria
              , Titulo
              , Tipo
              , Erro
              , IFNULL(Mensagem_Blacklist, '') AS Mensagem_Blacklist
            from (
            
            select
             gestacess_regras_lista_ticket.VA20 as 'Socio'
            ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
            ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
            , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
            , gestacess_transacoes.titulo as 'Titulo'
            , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
            ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
            ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
            , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
            , gestacess_dac.code as 'DAC'
            from gestacess_transacoes
            left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
            left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . (int)$event_id . "'))
            left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
            left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
            left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
            where gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
             and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . (int)$event_id . "')
             and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
             and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
             and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
            
            union all
            
            select
             '' as 'Socio'
            , ' ' as 'NSocio'
            , ' ' as 'Categoria'
            , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
            , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
            ' ' as 'Tipo'
            , gestacess_codigosmensagem.mensagemoperador as 'Erro'
            , ' ' as 'Mensagem_Blacklist'
            , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
            , gestacess_dac.code as 'DAC'
            from gestacess_transacoes
            , gestacess_template_inst
            , gestacess_dac
            , gestacess_codigosmensagem
            where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
             and gestacess_template_inst.dac_id=gestacess_dac.id
             and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
             and gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
             and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
             and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
             and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
             and gestacess_transacoes.titulo not in (
            
            select gestacess_transacoes.titulo
            from gestacess_transacoes
            left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
            left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . (int)$event_id . "'))
            left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
            left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
            left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
            where gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
             and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . (int)$event_id . "')
             and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
             and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
             and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
            
            )
            
            
            order by datetime
            
            
            ) as sub
        ");

        $header = 'Socio,NSocio,Categoria,Titulo,Tipo,Erro,Mensagem_Blacklist';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Erros_Titulos_Unicos_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $errors, 'from' => $from);

    }

    private function getErrorsTransactions($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $errors = DB::connection('access')->select("
            select
                 IFNULL(gestacess_regras_lista_ticket.VA20, '') as 'Socio'
                ,  IFNULL(gestacess_regras_lista_ticket.VA21, '') as 'NSocio'
                ,  IFNULL(case when gestacess_regras_lista_ticket.VA5 = 51 then 'Senior com desconto' when gestacess_regras_lista_ticket.VA5 = 52 then 'Senior reformado' when gestacess_regras_lista_ticket.VA5 = 53 then 'Junior' when gestacess_regras_lista_ticket.VA5 = 54 then 'Infantil' else gestacess_regras_lista_ticket.VA5 end, '') as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , gestacess_transacoes.titulo as 'Titulo'
                , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                ,  IFNULL(gestacess_import_list.blmessage, '') as 'Mensagem_Blacklist'
                , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . (int)$event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . (int)$event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                , gestacess_template_inst
                , gestacess_dac
                , gestacess_codigosmensagem
                where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                 and gestacess_template_inst.dac_id=gestacess_dac.id
                 and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                 and gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and gestacess_transacoes.titulo not in (
                
                select gestacess_transacoes.titulo
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . (int)$event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . (int)$event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                order by datetime
        ");

        $header = 'Socio,NSocio,Categoria,Porta,Titulo,Tipo,Erro,Mensagem_Blacklist,Datetime,DAC';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Erros_Transacoes_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $errors, 'from' => $from);

    }

    private function getFillLevel($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $day_now   = date("Y-m-d");

        $data = DB::connection('access')->select("
            SELECT
                    DATE_FORMAT(
                        MIN(cast(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac) as datetime)), '%Y-%m-%d %H:%i:00'
                    ) AS hour_transaction,
                    COUNT(DISTINCT gestacess_transacoes.titulo) AS entries
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                    LEFT JOIN gestacess_regras_lista_ticket ON (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
                    LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
                WHERE
                    gestacess_evento.id = '" . (int)$event_id . "'
                    AND
                    gestacess_evento.code = gestacess_regras_lista_ticket.evento
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $event_options['dataFim'] .  "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $day_now . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                    AND
                    gestacess_transacoes.vas NOT LIKE '%TCP%'
                    AND
                    gestacess_regras_lista_ticket.VA3 != 91
                GROUP BY
                     DATE_FORMAT(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac), '%Y-%m-%d %H:%i:00')
        ");

        $filllevel = array();

        $entries = 0;

        foreach ($data as $hour) {

            $filllevel[] = array(

                'hour_transaction' => $hour->hour_transaction,
                'entries' => $hour->entries

            );

        }

        if ($type == 'event') {

            $data_masters = DB::connection('access')->select("
            SELECT
                    DATE_FORMAT(
                        MIN(cast(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac) as datetime)), '%Y-%m-%d %H:%i:00'
                    ) AS hour_transaction,
                    COUNT(gestacess_transacoes.titulo) AS entries
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                    LEFT JOIN gestacess_regras_lista_ticket ON (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
                    LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
                WHERE
                    gestacess_evento.id = '" . (int)$event_id . "'
                    AND
                    gestacess_evento.code = gestacess_regras_lista_ticket.evento
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $event_options['dataFim'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $day_now . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                    AND
                    gestacess_transacoes.vas NOT LIKE '%TCP%'
                    AND
                    gestacess_regras_lista_ticket.VA3 = 91
                GROUP BY
                     DATE_FORMAT(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac), '%Y-%m-%d %H:%i:00')
        ");

            $data_extra = DB::connection('access')->select("
                SELECT
                    DATE_FORMAT(
                        MIN(cast(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac) as datetime)), '%Y-%m-%d %H:%i:00'
                    ) AS hour_transaction,
                    COUNT(gestacess_transacoes.titulo) AS entries
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                WHERE
                    gestacess_evento.id = '" . (int)$event_id . "'
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $event_options['dataFim'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $day_now . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                    AND
                    gestacess_transacoes.vas LIKE '%TCP%'
                GROUP BY
                     DATE_FORMAT(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac), '%Y-%m-%d %H:%i:00')
            ");

            foreach ($data_masters as $hour) {

                $index = array_search($hour->hour_transaction, array_column($filllevel, 'hour_transaction'));

                if ($index > -1) {

                    $filllevel[$index]['entries'] = $filllevel[$index]['entries'] + $hour->entries;

                }

            }

            foreach ($data_extra as $hour) {

                $index = array_search($hour->hour_transaction, array_column($filllevel, 'hour_transaction'));

                if ($index > -1) {

                    $filllevel[$index]['entries'] = $filllevel[$index]['entries'] + $hour->entries;

                }

            }

        }

        foreach ($filllevel as $hour) {

            $entries = $entries + $hour['entries'];

            $index = array_search($hour['hour_transaction'], array_column($filllevel, 'hour_transaction'));

            if ($index > -1) {

                $filllevel[$index]['entries'] = $entries;

            }

        }

        $header = 'Hora,Entradas';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Fill_Level_Minuto_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $filllevel, 'from' => $from);

    }

    private function getMinuteTicketEntries($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $tickets = DB::connection('access')->select("
            select gs_erros.Socio,gs_erros.NSocio,gs_erros.Categoria,gs_erros.Titulo from 
                (
                select
                 gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , gestacess_transacoes.titulo as 'Titulo'
                , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . (int)$event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . (int)$event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                , gestacess_template_inst
                , gestacess_dac
                , gestacess_codigosmensagem
                where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                 and gestacess_template_inst.dac_id=gestacess_dac.id
                 and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                 and gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '08:40'
                 and erro_id not in (3,4,8,9,10,11,13,18,19,20,23,26)
                 and gestacess_transacoes.titulo not in (
                
                select gestacess_transacoes.titulo
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . (int)$event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . (int)$event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,18,19,20,23,26)
                
                )
                
                
                order by datetime
                ) as gs_erros
                
                , (
                
                select
                      (select
                    code
                      from
                    gestacess_evento
                      where
                    id = '" . (int)$event_id . "') as 'jogo'
                      , cast(min(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac)) as char) as 'datahora'
                      , gestacess_regras_lista_ticket.VA2 as 'porta'
                      , gestacess_dac.code as 'dac'
                      , gestacess_dac.ip as 'ip'
                      , gestacess_transacoes.titulo as 'codigobarras'
                      , gestacess_regras_lista_ticket.VA20 as 'socio'
                      , gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                      , gestacess_regras_lista_ticket.VA5 as 'categoria'
                      , (case
                      when gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                    from
                      (
                    select
                      min(gestacess_transacoes.id) as id
                      , gestacess_transacoes.titulo as 'codigobarras'
                    from
                      gestacess_transacoes
                      left join gestacess_template_inst
                        on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                      left join gestacess_dac
                        on gestacess_template_inst.dac_id = gestacess_dac.id
                      left join gestacess_regras_lista_ticket
                        on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                          and gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . (int)$event_id . "'
                          )
                        )
                      left join gestacess_valores_variaveis_acesso
                        on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                          and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                      gestacess_transacoes.entry=1
                      and gestacess_transacoes.apagado = 0
                      and gestacess_transacoes.isEntry  = 1
                      and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                      and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and testmode = 0
                      and areaentryid <> 330
                      and gestacess_transacoes.titulo not in (
                        select
                          titulo
                        from
                          gestacess_regras_lista_ticket
                        where
                          va3 = '91'
                          and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . (int)$event_id . "'
                          )
                      )
                    group by
                      gestacess_transacoes.titulo
                      ) as ent
                      left join gestacess_transacoes
                    on ent.id = gestacess_transacoes.id
                      left join gestacess_template_inst
                    on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                      left join gestacess_dac
                    on gestacess_template_inst.dac_id = gestacess_dac.id
                      left join gestacess_regras_lista_ticket
                    on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                      and gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . (int)$event_id . "'
                      )
                    )
                      left join gestacess_valores_variaveis_acesso
                    on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                      and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                      gestacess_transacoes.entry=1
                      and gestacess_transacoes.apagado = 0
                      and gestacess_transacoes.isEntry  = 1
                      and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                      and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and testmode = 0
                      and areaentryid <> 330
                      and gestacess_transacoes.titulo not in (
                    select
                      titulo
                    from
                      gestacess_regras_lista_ticket
                    where
                      va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . (int)$event_id . "'
                      )
                      )
                    group by
                      gestacess_regras_lista_ticket.VA2
                      , gestacess_dac.code
                      , gestacess_dac.ip
                      , gestacess_transacoes.titulo
                      , gestacess_regras_lista_ticket.VA20
                      , gestacess_regras_lista_ticket.VA21
                      , gestacess_valores_variaveis_acesso.descricao
                      , gestacess_regras_lista_ticket.VA5
                      , (case
                    when gestacess_transacoes.tipoleitor = 1
                      then 'CODIGO BARRAS'
                    when gestacess_transacoes.tipoleitor = 2
                      then 'NFC'
                    else ''
                      end) 
                union all 
                      select
                    (select
                      code
                    from
                      gestacess_evento
                    where 
                      id = '" . (int)$event_id . "') as 'jogo'
                    , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'datahora'
                    , gestacess_regras_lista_ticket.VA2 as 'porta'
                    , gestacess_dac.code as 'dac'
                    , gestacess_dac.ip as 'ip'
                    , gestacess_transacoes.titulo as 'codigobarras'
                    , '' as 'socio'
                    , '' as 'nsocio'
                    , 'Livre Transito' as 'tipotitulo'
                    , '' as 'categoria'
                    , (case
                      when gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                      from
                    gestacess_transacoes
                    left join gestacess_template_inst
                      on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                    left join gestacess_dac
                      on gestacess_template_inst.dac_id = gestacess_dac.id
                    left join gestacess_regras_lista_ticket
                      on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        and gestacess_regras_lista_ticket.va1 = (
                          select
                        code
                          from
                        gestacess_evento
                          where
                        id = '" . (int)$event_id . "'
                        )
                      )
                    left join gestacess_valores_variaveis_acesso
                      on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                        and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                      where
                    gestacess_transacoes.entry = 1
                    and gestacess_transacoes.apagado = 0
                    and gestacess_transacoes.isEntry  = 1
                    and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                    and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                    and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    and areaentryid <> 330
                    and gestacess_transacoes.titulo in (
                      select
                        titulo
                      from
                        gestacess_regras_lista_ticket
                      where
                        va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . (int)$event_id . "'
                      )
                      ) 
                    order by
                      datahora 
                
                ) as gs_entradas
                
                where gs_erros.Titulo = gs_entradas.codigobarras
                and (gs_erros.erro = 'JA ENTROU' and timediff(gs_erros.Datetime, gs_entradas.datahora)>'00:00:60')
                group by gs_erros.Socio,gs_erros.NSocio,gs_erros.Categoria,gs_erros.Titulo
        ");

        $header = 'Socio,NSocio,Categoria,Titulo';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Titulo_JaEntrou_1min_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $tickets, 'from' => $from);

    }

    private function getMinuteTicketTransactions($event_id, $from, $type) {

        $event_options = $this->getEventOptions($event_id);

        $transactions = DB::connection('access')->select("
            select gs_entradas.datahora as datahora, 'Entrada' as tipo, gs_entradas.porta as porta, gs_entradas.dac as dac, gs_entradas.codigobarras as codigobarras, gs_entradas.socio as socio, gs_entradas.nsocio as nsocio, gs_entradas.categoria as categoria  from 
                    
                    
                    (
                    select
                     gestacess_regras_lista_ticket.VA20 as 'Socio'
                    ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
                    ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
                    , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                    , gestacess_transacoes.titulo as 'Titulo'
                    , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                    ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                    ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                    , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                    , gestacess_dac.code as 'DAC'
                    from gestacess_transacoes
                    left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                    left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . (int)$event_id . "'))
                    left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                    left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                    left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                    where gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
                     and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . (int)$event_id . "')
                     and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                     and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                     and erro_id not in (3,4,8,9,10,11,13,18,19,20,23,26)
                    
                    union all
                    
                    select
                     '' as 'Socio'
                    , '' as 'NSocio'
                    , '' as 'Categoria'
                    , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                    , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                    '' as 'Tipo'
                    , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                    , '' as 'Mensagem_Blacklist'
                    , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                    , gestacess_dac.code as 'DAC'
                    from gestacess_transacoes
                    , gestacess_template_inst
                    , gestacess_dac
                    , gestacess_codigosmensagem
                    where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                     and gestacess_template_inst.dac_id=gestacess_dac.id
                     and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                     and gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
                     and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                     and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                     and erro_id not in (3,4,8,9,10,11,13,18,19,20,23,26)
                     and gestacess_transacoes.titulo not in (
                    
                    select gestacess_transacoes.titulo
                    from gestacess_transacoes
                    left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                    left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . (int)$event_id . "'))
                    left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                    left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                    left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                    where gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
                     and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . (int)$event_id . "')
                     and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                     and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                     and erro_id not in (3,4,8,9,10,11,13,18,19,20,23,26)
                    
                    )
                    
                    
                    order by datetime
                    ) as gs_erros
                    
                    , (
                    
                    select
                          (select
                        code
                          from
                        gestacess_evento
                          where
                        id = '" . (int)$event_id . "') as 'jogo'
                          , cast(min(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac)) as char) as 'datahora'
                          , gestacess_regras_lista_ticket.VA2 as 'porta'
                          , gestacess_dac.code as 'dac'
                          , gestacess_dac.ip as 'ip'
                          , gestacess_transacoes.titulo as 'codigobarras'
                          , gestacess_regras_lista_ticket.VA20 as 'socio'
                          , gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                          , gestacess_regras_lista_ticket.VA5 as 'categoria'
                          , (case
                          when gestacess_transacoes.tipoleitor = 1
                            then 'CODIGO BARRAS'
                          when gestacess_transacoes.tipoleitor = 2
                            then 'NFC'
                          else ''
                        end) as 'tipoleitor'
                        from
                          (
                        select
                          min(gestacess_transacoes.id) as id
                          , gestacess_transacoes.titulo as 'codigobarras'
                        from
                          gestacess_transacoes
                          left join gestacess_template_inst
                            on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                          left join gestacess_dac
                            on gestacess_template_inst.dac_id = gestacess_dac.id
                          left join gestacess_regras_lista_ticket
                            on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                              and gestacess_regras_lista_ticket.va1 = (
                            select
                              code
                            from
                              gestacess_evento
                            where
                              id = '" . (int)$event_id . "'
                              )
                            )
                          left join gestacess_valores_variaveis_acesso
                            on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                              and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                        where
                          gestacess_transacoes.entry=1
                          and gestacess_transacoes.apagado = 0
                          and gestacess_transacoes.isEntry  = 1
                          and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                          and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                          and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                          and testmode = 0
                          and areaentryid <> 330
                          and gestacess_transacoes.titulo not in (
                            select
                              titulo
                            from
                              gestacess_regras_lista_ticket
                            where
                              va3 = '91'
                              and va1 = (
                            select
                              code
                            from
                              gestacess_evento
                            where
                              id = '" . (int)$event_id . "'
                              )
                          )
                        group by
                          gestacess_transacoes.titulo
                          ) as ent
                          left join gestacess_transacoes
                        on ent.id = gestacess_transacoes.id
                          left join gestacess_template_inst
                        on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                          left join gestacess_dac
                        on gestacess_template_inst.dac_id = gestacess_dac.id
                          left join gestacess_regras_lista_ticket
                        on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                          and gestacess_regras_lista_ticket.va1 = (
                            select
                              code
                            from
                              gestacess_evento
                            where
                              id = '" . (int)$event_id . "'
                          )
                        )
                          left join gestacess_valores_variaveis_acesso
                        on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                          and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                        where
                          gestacess_transacoes.entry=1
                          and gestacess_transacoes.apagado = 0
                          and gestacess_transacoes.isEntry  = 1
                          and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                          and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                          and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                          and testmode = 0
                          and areaentryid <> 330
                          and gestacess_transacoes.titulo not in (
                        select
                          titulo
                        from
                          gestacess_regras_lista_ticket
                        where
                          va3 = '91'
                          and va1 = (
                            select
                              code
                            from
                              gestacess_evento
                            where
                              id = '" . (int)$event_id . "'
                          )
                          )
                        group by
                          gestacess_regras_lista_ticket.VA2
                          , gestacess_dac.code
                          , gestacess_dac.ip
                          , gestacess_transacoes.titulo
                          , gestacess_regras_lista_ticket.VA20
                          , gestacess_regras_lista_ticket.VA21
                          , gestacess_valores_variaveis_acesso.descricao
                          , gestacess_regras_lista_ticket.VA5
                          , (case
                        when gestacess_transacoes.tipoleitor = 1
                          then 'CODIGO BARRAS'
                        when gestacess_transacoes.tipoleitor = 2
                          then 'NFC'
                        else ''
                          end) 
                    union all 
                          select
                        (select
                          code
                        from
                          gestacess_evento
                        where 
                          id = '" . (int)$event_id . "') as 'jogo'
                        , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'datahora'
                        , gestacess_regras_lista_ticket.VA2 as 'porta'
                        , gestacess_dac.code as 'dac'
                        , gestacess_dac.ip as 'ip'
                        , gestacess_transacoes.titulo as 'codigobarras'
                        , '' as 'socio'
                        , '' as 'nsocio'
                        , 'Livre Transito' as 'tipotitulo'
                        , '' as 'categoria'
                        , (case
                          when gestacess_transacoes.tipoleitor = 1
                            then 'CODIGO BARRAS'
                          when gestacess_transacoes.tipoleitor = 2
                            then 'NFC'
                          else ''
                        end) as 'tipoleitor'
                          from
                        gestacess_transacoes
                        left join gestacess_template_inst
                          on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        left join gestacess_dac
                          on gestacess_template_inst.dac_id = gestacess_dac.id
                        left join gestacess_regras_lista_ticket
                          on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                            and gestacess_regras_lista_ticket.va1 = (
                              select
                            code
                              from
                            gestacess_evento
                              where
                            id = '" . (int)$event_id . "'
                            )
                          )
                        left join gestacess_valores_variaveis_acesso
                          on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                            and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                          where
                        gestacess_transacoes.entry = 1
                        and gestacess_transacoes.apagado = 0
                        and gestacess_transacoes.isEntry  = 1
                        and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                        and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                        and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                        and areaentryid <> 330
                        and gestacess_transacoes.titulo in (
                          select
                            titulo
                          from
                            gestacess_regras_lista_ticket
                          where
                            va3 = '91'
                          and va1 = (
                            select
                              code
                            from
                              gestacess_evento
                            where
                              id = '" . (int)$event_id . "'
                          )
                          ) 
                        order by
                          datahora 
                    
                    ) as gs_entradas
                    
                    where gs_erros.Titulo = gs_entradas.codigobarras
                    and (gs_erros.erro = 'JA ENTROU' and timediff(gs_erros.Datetime, gs_entradas.datahora)>'00:00:60')
                    group by gs_entradas.datahora, 'Entrada', gs_entradas.porta, gs_entradas.dac, gs_entradas.codigobarras, gs_entradas.socio, gs_entradas.nsocio, gs_entradas.categoria
                    
                    
                    
                    
                    
                    
                    
                    union all
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    select gs_erros.datetime as datahora, 'Bloqueio' as tipo, gs_erros.Porta as porta, gs_erros.DAC as dac, gs_erros.Titulo as codigobarras, gs_erros.Socio as socio, gs_erros.NSocio as nsocio, gs_erros.Categoria as categoria from 
                    
                    
                    (
                    select
                     gestacess_regras_lista_ticket.VA20 as 'Socio'
                    ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
                    ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
                    , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                    , gestacess_transacoes.titulo as 'Titulo'
                    , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                    ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                    ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                    , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                    , gestacess_dac.code as 'DAC'
                    from gestacess_transacoes
                    left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                    left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . (int)$event_id . "'))
                    left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                    left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                    left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                    where gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
                     and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . (int)$event_id . "')
                     and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                     and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                     and erro_id not in (3,4,8,9,10,11,13,18,19,20,23,26)
                    
                    union all
                    
                    select
                     '' as 'Socio'
                    , '' as 'NSocio'
                    , '' as 'Categoria'
                    , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                    , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                    '' as 'Tipo'
                    , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                    , '' as 'Mensagem_Blacklist'
                    , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                    , gestacess_dac.code as 'DAC'
                    from gestacess_transacoes
                    , gestacess_template_inst
                    , gestacess_dac
                    , gestacess_codigosmensagem
                    where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                     and gestacess_template_inst.dac_id=gestacess_dac.id
                     and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                     and gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
                     and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                     and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                     and erro_id not in (3,4,8,9,10,11,13,18,19,20,23,26)
                     and gestacess_transacoes.titulo not in (
                    
                    select gestacess_transacoes.titulo
                    from gestacess_transacoes
                    left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                    left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . (int)$event_id . "'))
                    left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                    left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                    left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                    where gestacess_transacoes.eventID_id= '" . (int)$event_id . "'
                     and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . (int)$event_id . "')
                     and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                     and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                     and erro_id not in (3,4,8,9,10,11,13,18,19,20,23,26)
                    
                    )
                    
                    
                    order by datetime
                    ) as gs_erros
                    
                    , (
                    
                    select
                          (select
                        code
                          from
                        gestacess_evento
                          where
                        id = '" . (int)$event_id . "') as 'jogo'
                          , cast(min(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac)) as char) as 'datahora'
                          , gestacess_regras_lista_ticket.VA2 as 'porta'
                          , gestacess_dac.code as 'dac'
                          , gestacess_dac.ip as 'ip'
                          , gestacess_transacoes.titulo as 'codigobarras'
                          , gestacess_regras_lista_ticket.VA20 as 'socio'
                          , gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                          , gestacess_regras_lista_ticket.VA5 as 'categoria'
                          , (case
                          when gestacess_transacoes.tipoleitor = 1
                            then 'CODIGO BARRAS'
                          when gestacess_transacoes.tipoleitor = 2
                            then 'NFC'
                          else ''
                        end) as 'tipoleitor'
                        from
                          (
                        select
                          min(gestacess_transacoes.id) as id
                          , gestacess_transacoes.titulo as 'codigobarras'
                        from
                          gestacess_transacoes
                          left join gestacess_template_inst
                            on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                          left join gestacess_dac
                            on gestacess_template_inst.dac_id = gestacess_dac.id
                          left join gestacess_regras_lista_ticket
                            on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                              and gestacess_regras_lista_ticket.va1 = (
                            select
                              code
                            from
                              gestacess_evento
                            where
                              id = '" . (int)$event_id . "'
                              )
                            )
                          left join gestacess_valores_variaveis_acesso
                            on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                              and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                        where
                          gestacess_transacoes.entry=1
                          and gestacess_transacoes.apagado = 0
                          and gestacess_transacoes.isEntry  = 1
                          and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                          and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                          and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                          and testmode = 0
                          and areaentryid <> 330
                          and gestacess_transacoes.titulo not in (
                            select
                              titulo
                            from
                              gestacess_regras_lista_ticket
                            where
                              va3 = '91'
                              and va1 = (
                            select
                              code
                            from
                              gestacess_evento
                            where
                              id = '" . (int)$event_id . "'
                              )
                          )
                        group by
                          gestacess_transacoes.titulo
                          ) as ent
                          left join gestacess_transacoes
                        on ent.id = gestacess_transacoes.id
                          left join gestacess_template_inst
                        on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                          left join gestacess_dac
                        on gestacess_template_inst.dac_id = gestacess_dac.id
                          left join gestacess_regras_lista_ticket
                        on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                          and gestacess_regras_lista_ticket.va1 = (
                            select
                              code
                            from
                              gestacess_evento
                            where
                              id = '" . (int)$event_id . "'
                          )
                        )
                          left join gestacess_valores_variaveis_acesso
                        on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                          and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                        where
                          gestacess_transacoes.entry=1
                          and gestacess_transacoes.apagado = 0
                          and gestacess_transacoes.isEntry  = 1
                          and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                          and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                          and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                          and testmode = 0
                          and areaentryid <> 330
                          and gestacess_transacoes.titulo not in (
                        select
                          titulo
                        from
                          gestacess_regras_lista_ticket
                        where
                          va3 = '91'
                          and va1 = (
                            select
                              code
                            from
                              gestacess_evento
                            where
                              id = '" . (int)$event_id . "'
                          )
                          )
                        group by
                          gestacess_regras_lista_ticket.VA2
                          , gestacess_dac.code
                          , gestacess_dac.ip
                          , gestacess_transacoes.titulo
                          , gestacess_regras_lista_ticket.VA20
                          , gestacess_regras_lista_ticket.VA21
                          , gestacess_valores_variaveis_acesso.descricao
                          , gestacess_regras_lista_ticket.VA5
                          , (case
                        when gestacess_transacoes.tipoleitor = 1
                          then 'CODIGO BARRAS'
                        when gestacess_transacoes.tipoleitor = 2
                          then 'NFC'
                        else ''
                          end) 
                    union all 
                          select
                        (select
                          code
                        from
                          gestacess_evento
                        where 
                          id = '" . (int)$event_id . "') as 'jogo'
                        , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'datahora'
                        , gestacess_regras_lista_ticket.VA2 as 'porta'
                        , gestacess_dac.code as 'dac'
                        , gestacess_dac.ip as 'ip'
                        , gestacess_transacoes.titulo as 'codigobarras'
                        , '' as 'socio'
                        , '' as 'nsocio'
                        , 'Livre Transito' as 'tipotitulo'
                        , '' as 'categoria'
                        , (case
                          when gestacess_transacoes.tipoleitor = 1
                            then 'CODIGO BARRAS'
                          when gestacess_transacoes.tipoleitor = 2
                            then 'NFC'
                          else ''
                        end) as 'tipoleitor'
                          from
                        gestacess_transacoes
                        left join gestacess_template_inst
                          on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        left join gestacess_dac
                          on gestacess_template_inst.dac_id = gestacess_dac.id
                        left join gestacess_regras_lista_ticket
                          on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                            and gestacess_regras_lista_ticket.va1 = (
                              select
                            code
                              from
                            gestacess_evento
                              where
                            id = '" . (int)$event_id . "'
                            )
                          )
                        left join gestacess_valores_variaveis_acesso
                          on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                            and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                          where
                        gestacess_transacoes.entry = 1
                        and gestacess_transacoes.apagado = 0
                        and gestacess_transacoes.isEntry  = 1
                        and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                        and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                        and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                        and areaentryid <> 330
                        and gestacess_transacoes.titulo in (
                          select
                            titulo
                          from
                            gestacess_regras_lista_ticket
                          where
                            va3 = '91'
                          and va1 = (
                            select
                              code
                            from
                              gestacess_evento
                            where
                              id = '" . (int)$event_id . "'
                          )
                          ) 
                        order by
                          datahora 
                    
                    ) as gs_entradas
                    
                    where gs_erros.Titulo = gs_entradas.codigobarras
                    and (gs_erros.erro = 'JA ENTROU' and timediff(gs_erros.Datetime, gs_entradas.datahora)>'00:00:60')
                    
                    
                    order by codigobarras, datahora
        ");

        $header = 'datahora,tipo,porta,dac,codigobarras,socio,nsocio,categoria';

        $report_from = '';

        switch ($type) {

            case 'event':
                $report_from = 'estadio_';
                break;
            case 'event-park':
                $report_from = 'parque_';
                break;
            case 'event-credential':
                $report_from = 'credencial_';

        }

        $filename = 'Transacoes_Titulo_JaEntrou_1min_' . $report_from . $event_options['code'] . '.csv';

        return array('filename' => $filename, 'header' => $header, 'file' => $transactions, 'from' => $from);

    }

}