<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 11-10-2018
 * Time: 11:47
 */

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Reports;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class ReportsController extends Controller {

    /**
     * Create a new controller instance
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getReports(Request $request) {

        try {

            $reports = Reports::all();

            return view('reports.show-reports', compact('reports'));

        } catch (\Exception $e) {

            Log::error('Reports Request Error');
            Log::error($e);

            return view('reports.show-reports', compact(['error' => 'unavailable']));

        }

    }

    public function addReport(Request $request, $report_id) {

        try {

            $report_type = Reports::where('id', '=', $report_id)->get();

            $report = (isset($report_type[0]) ? $report_type[0] : array());

            return view('reports.view-report', compact('report'));

        } catch (\Exception $e) {

            Log::error('Reports Request Error');
            Log::error($e);

            return view('reports.show-reports', compact(['error' => 'unavailable']));

        }

    }

    public function errorReport(Request $request, $report_id) {

        try {

            $report_type = Reports::where('id', '=', $report_id)->get();

            $report = (isset($report_type[0]) ? $report_type[0] : array());

            return view('reports.error-report', compact('report'));

        } catch (\Exception $e) {

            Log::error('Reports Request Error');
            Log::error($e);

            return view('reports.show-reports', compact(['error' => 'unavailable']));

        }

    }

    public function csvsReport(Request $request, $report_id) {

        try {

            $report_type = Reports::where('id', '=', $report_id)->get();

            $report = (isset($report_type[0]) ? $report_type[0] : array());

            return view('reports.csvs-report', compact('report'));

        } catch (\Exception $e) {

            Log::error('Reports Request Error');
            Log::error($e);

            return view('reports.show-reports', compact(['error' => 'unavailable']));

        }

    }

    public function extraTcpReport(Request $request, $report_id) {

        try {

            $report_type = Reports::where('id', '=', $report_id)->get();

            $report = (isset($report_type[0]) ? $report_type[0] : array());

            return view('reports.extra-tcp-report', compact('report'));

        } catch (\Exception $e) {

            Log::error('Reports Request Error');
            Log::error($e);

            return view('reports.show-reports', compact(['error' => 'unavailable']));

        }

    }

    public function boxResume(Request $request, $report_id) {

        try {

            $report_type = Reports::where('id', '=', $report_id)->get();

            $report = (isset($report_type[0]) ? $report_type[0] : array());

            return view('reports.box-resume', compact('report'));

        } catch (\Exception $e) {

            Log::error('Reports Request Error');
            Log::error($e);

            return view('reports.show-reports', compact(['error' => 'unavailable']));

        }

    }

    public function generateReport(Request $request) {

        $validator = $this->validate($request, [
            'event'                  => 'required|max:255|unique:users',
        ]);

        return redirect('reports')->with('success', trans('reports.generated_successfully'));

    }

    public function getZoneEntries(Request $request, $event_id) {

        try {

            $event_options = $this->getEventOptions($event_id);

            $entries = DB::connection('access')->select("
                select
                  dacs.insts_id as 'template_id'
                  , dacs.insts as 'template_desc'
                  , dacs.zona_id as 'block_id'
                  ,dacs.zona as 'block_desc'
                  , dacs.porta_id as 'gate_id'
                  ,dacs.porta as 'gate_desc'
                  , count(*) as 'entries'
                  , sum(case when tipotitulo = 'Livre Transito' then 1 else 0 end) as 'entries_master'
                from 
                  (select
                      (select
                    code
                      from
                    gestacess_evento
                      where
                    id = '" . (int)$event_id . "') as 'jogo'
                      , cast(min(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac)) as char) as 'datahora'
                      , gestacess_regras_lista_ticket.VA2 as 'porta'
                      , gestacess_dac.code as 'dac'
                      , gestacess_dac.ip as 'ip'
                      , gestacess_transacoes.titulo as 'codigobarras'
                      , gestacess_regras_lista_ticket.VA20 as 'socio'
                      , gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                      , gestacess_regras_lista_ticket.VA5 as 'categoria'
                      , ANY_VALUE(gestacess_template_inst.id) as 'iddac'
                      , (case
                      when gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                    from
                      (
                    select
                      min(gestacess_transacoes.id) as id
                      , gestacess_transacoes.titulo as 'codigobarras'
                    from
                      gestacess_transacoes
                      left join gestacess_template_inst
                        on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                      left join gestacess_dac
                        on gestacess_template_inst.dac_id = gestacess_dac.id
                      left join gestacess_regras_lista_ticket
                        on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                          and gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . (int)$event_id . "'
                          )
                        )
                      left join gestacess_valores_variaveis_acesso
                        on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                          and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                      gestacess_transacoes.entry=1
                      and gestacess_transacoes.apagado = 0
                      and gestacess_transacoes.isEntry  = 1
                      and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                      and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and testmode = 0
                      and areaentryid <> 330
                      and gestacess_transacoes.titulo not in (
                        select
                          titulo
                        from
                          gestacess_regras_lista_ticket
                        where
                          va3 = '91'
                          and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_options['horaAberturaPortas'] . "'
                          )
                      )
                    group by
                      gestacess_transacoes.titulo
                      ) as ent
                      left join gestacess_transacoes
                    on ent.id = gestacess_transacoes.id
                      left join gestacess_template_inst
                    on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                      left join gestacess_dac
                    on gestacess_template_inst.dac_id = gestacess_dac.id
                      left join gestacess_regras_lista_ticket
                    on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                      and gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . (int)$event_id . "'
                      )
                    )
                      left join gestacess_valores_variaveis_acesso
                    on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                      and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                      gestacess_transacoes.entry=1
                      and gestacess_transacoes.apagado = 0
                      and gestacess_transacoes.isEntry  = 1
                      and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                      and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and testmode = 0
                      and areaentryid <> 330
                      and gestacess_transacoes.titulo not in (
                    select
                      titulo
                    from
                      gestacess_regras_lista_ticket
                    where
                      va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . (int)$event_id . "'
                      )
                      )
                    group by
                      gestacess_regras_lista_ticket.VA2
                      , gestacess_dac.code
                      , gestacess_dac.ip
                      , gestacess_transacoes.titulo
                      , gestacess_regras_lista_ticket.VA20
                      , gestacess_regras_lista_ticket.VA21
                      , gestacess_valores_variaveis_acesso.descricao
                      , gestacess_regras_lista_ticket.VA5
                      , (case
                    when gestacess_transacoes.tipoleitor = 1
                      then 'CODIGO BARRAS'
                    when gestacess_transacoes.tipoleitor = 2
                      then 'NFC'
                    else ''
                      end) 
                union all 
                      select
                    (select
                      code
                    from
                      gestacess_evento
                    where 
                      id = '" . (int)$event_id . "') as 'jogo'
                    , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'datahora'
                    , gestacess_regras_lista_ticket.VA2 as 'porta'
                    , gestacess_dac.code as 'dac'
                    , gestacess_dac.ip as 'ip'
                    , gestacess_transacoes.titulo as 'codigobarras'
                    , '' as 'socio'
                    , '' as 'nsocio'
                    , 'Livre Transito' as 'tipotitulo'
                    , '' as 'categoria'
                    , gestacess_template_inst.id as 'iddac'
                    , (case
                      when gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                      from
                    gestacess_transacoes
                    left join gestacess_template_inst
                      on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                    left join gestacess_dac
                      on gestacess_template_inst.dac_id = gestacess_dac.id
                    left join gestacess_regras_lista_ticket
                      on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        and gestacess_regras_lista_ticket.va1 = (
                          select
                        code
                          from
                        gestacess_evento
                          where
                        id = '" . (int)$event_id . "'
                        )
                      )
                    left join gestacess_valores_variaveis_acesso
                      on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                        and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                      where
                    gestacess_transacoes.entry = 1
                    and gestacess_transacoes.apagado = 0
                    and gestacess_transacoes.isEntry  = 1
                    and gestacess_transacoes.eventID_id = '" . (int)$event_id . "'
                    and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                    and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    and areaentryid <> 330
                    and gestacess_transacoes.titulo in (
                      select
                        titulo
                      from
                        gestacess_regras_lista_ticket
                      where
                        va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . (int)$event_id . "'
                      )
                      ) 
                    order by
                      datahora 
                
                  ) as sub
                  
                  
                  left join (
                
                select insts.id as 'insts_id', (case when insts.id = 3 then 'Pedroso' when insts.id = 21 then 'Futebol Dragao' when insts.id = 177 then 'Dragao Caixa' when insts.id = 209 then 'CTPG' when insts.id = 238 then 'Museu' when insts.id = 261 then 'Acreditacao' else '' end) as 'insts', zonas.id as 'zona_id', zonas.descricao as 'zona', portas.id as 'porta_id', portas.descricao as 'porta', gestacess_template_inst.id as 'dac_id', gestacess_dac.descricao as 'dac' from gestacess_template_inst left join gestacess_dac on gestacess_template_inst.dac_id = gestacess_dac.id left join (select gestacess_template_inst.id, gestacess_template_inst.descricao, gestacess_template_inst.parent_id from gestacess_template_inst where isporta = 1) as portas on gestacess_template_inst.parent_id = portas.id  
                
                left join (select gestacess_template_inst.id, gestacess_template_inst.descricao, gestacess_template_inst.parent_id from gestacess_template_inst where iszona = 1) as zonas on portas.parent_id = zonas.id  
                
                left join (select gestacess_template_inst.id, gestacess_template_inst.descricao, gestacess_template_inst.parent_id from gestacess_template_inst where istemplate = 1) as insts on zonas.parent_id = insts.id  
                
                where gestacess_template_inst.isdac=1 ) as dacs on sub.iddac = dacs.dac_id
                
                group by
                  dacs.insts_id
                  ,dacs.insts
                  ,dacs.zona_id
                  ,dacs.zona
                  ,dacs.porta_id
                  ,dacs.porta
                
                order by
                  dacs.insts
                  ,dacs.zona
                  ,dacs.porta
            ");

            $entries_extra = DB::connection('access')->select("
                SELECT
                    template.id AS 'template_id',
                    template.descricao AS 'template_desc',
                    temp_block.id AS 'block_id',
                    temp_block.descricao AS 'block_desc',
                    IF(temp_block.descricao = 'Porta 15', '319', '320') AS 'gate_id',
                    IF(temp_block.descricao = 'Porta 15', 'Porta 15', 'Porta 1') AS 'gate_desc',
                    COUNT(DISTINCT transactions.id, transactions.titulo) AS 'entries_extra',
                    COUNT(DISTINCT transactions.id, transactions.titulo) AS 'entries',
                    0 AS 'entries_master'
                FROM
                    gestacess_transacoes AS transactions
                LEFT JOIN gestacess_template_inst AS temp_block
                ON
                    (
                        temp_block.id = transactions.areaEntryId AND temp_block.isPorta = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_template
                ON
                    (
                        temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                    )
                LEFT JOIN gestacess_template AS template
                ON
                    (
                        template.id = temp_template.template_id
                    )
                WHERE
                    transactions.eventId_id = '" . (int)$event_id . "'
                    AND
                    transactions.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    transactions.entry = 1
                    AND
                    transactions.isEntry = 1
                    AND
                    transactions.apagado = 0
                    AND
                    transactions.testMode = 0
                    AND
                    transactions.areaEntryId <> 330
                    AND
                    transactions.vas LIKE '%TCPVA5%'
                GROUP BY
                    template_id,
                    template_desc,
                    block_id,
                    block_desc
                ORDER BY
                    template_desc,
                    block_desc
                ASC
            ");

            $imported_tickets = DB::connection('access')->select("
                SELECT
                    regras_lista_ticket.VA2 AS access_variable,
                    valores_variaveis_acesso.descricao AS description,
                    COUNT(import_list.titulo) AS imported
                FROM
                    gestacess_import_list AS import_list
                LEFT JOIN gestacess_regras_lista_ticket AS regras_lista_ticket
                ON
                    (
                        regras_lista_ticket.titulo = import_list.titulo
                    )
                LEFT JOIN gestacess_valores_variaveis_acesso AS valores_variaveis_acesso
                ON
                    (
                        valores_variaveis_acesso.valor = regras_lista_ticket.VA2 AND valores_variaveis_acesso.variavelAcesso_id = 2
                    )
                WHERE
                    import_list.codEvent = '" . $event_options["code"] . "'
                    AND
                    regras_lista_ticket.evento = import_list.codEvent
                    AND
                    regras_lista_ticket.VA1 = import_list.codEvent
                    AND
                    import_list.isWhiteList = 1
                    AND
                    regras_lista_ticket.VA2 <> '50'
                GROUP BY
                    regras_lista_ticket.VA2,
                    valores_variaveis_acesso.descricao
            ");

            $presidential_box = DB::connection('access')->select("
                SELECT 
                    COUNT(DISTINCT titulo) AS total
                FROM 
                    `gestacess_transacoes` 
                WHERE 
                    `eventId_id` = '" . $event_id . "' 
                    AND 
                    `vas` LIKE '%VA2:29%'
            ");

            $cleaned_import = array();

            $special_gates_14_16 = array(
                'access_variable'   => '',
                'description'       => 'Porta 14 e 16',
                'imported'          => 0
            );

            $special_gates_2_28 = array(
                'access_variable'   => '',
                'description'       => 'Porta 2 e 28',
                'imported'          => 0
            );

            $special_gates_19_20 = array(
                'access_variable'   => '',
                'description'       => 'Porta 19 e 20',
                'imported'          => 0
            );

            foreach ($imported_tickets as $imported) {

                $array = array(
                    'access_variable'   => $imported->access_variable,
                    'description'       => '',
                    'imported'          => $imported->imported
                );

                if (strpos($imported->description, 'Dragao - ') !== false) {

                    $splited = explode( 'Dragao - ', $imported->description );

                    if (count($splited) > 0) {

                        switch ($splited[1]) {
                            case 'Cam. Presidencial':
                                $array['description'] = 'Camarote Presidencial';
                                break;
                            case 'Porta 8 PMR':
                                $array['description'] = 'Porta 8 - PMR';
                                break;
                            case 'Porta 22 PMR':
                                $array['description'] = 'Porta 22 - PMR';
                                break;
                            case 'Porta 14':
                                $special_gates_14_16['imported'] = $special_gates_14_16['imported'] + $imported->imported;
                                break;
                            case 'Porta 16':
                                $special_gates_14_16['imported'] = $special_gates_14_16['imported'] + $imported->imported;
                                break;
                            case 'Porta 2':
                                $special_gates_2_28['imported'] = $special_gates_2_28['imported'] + $imported->imported;
                                break;
                            case 'Porta 28':
                                $special_gates_2_28['imported'] = $special_gates_2_28['imported'] + $imported->imported;
                                break;
                            case 'Porta 19':
                                $array['description'] = $splited[1];
                                $special_gates_19_20['imported'] = $special_gates_19_20['imported'] + $imported->imported;
                                break;
                            case 'Porta 20':
                                $array['description'] = $splited[1];
                                $special_gates_19_20['imported'] = $special_gates_19_20['imported'] + $imported->imported;
                                break;
                            default:
                                $array['description'] = $splited[1];
                                break;
                        }

                    } else {

                        $array['description'] = $splited;

                    }

                } else {

                    $array['description'] = $imported->description;

                }

                array_push($cleaned_import, $array);

            }

            array_push($cleaned_import, $special_gates_14_16);
            array_push($cleaned_import, $special_gates_2_28);
            array_push($cleaned_import, $special_gates_19_20);

            $structure = array("venue" => array());

            $special_sup_south_block_id = DB::connection('access')->select("
                SELECT
                    id
                FROM
                    gestacess_template_inst AS temp_inst
                WHERE
                    temp_inst.descricao = 'Superior Sul'
            ");

            foreach ($entries as $entry) {

                if($entry->block_desc == 'TCP') {

                    continue;

                }

                if ($entry->block_desc == 'Superior Sul - Porta 8') {

                    if (isset($special_sup_south_block_id[0])) {

                        $entry->block_id = $special_sup_south_block_id[0]->id;

                    }

                    $entry->block_desc = 'Superior Sul';

                }

                if ($entry->gate_desc == 'Porta 1') {

                    $entry->block_id = 99999199999;
                    $entry->block_desc  = Lang::get('reports.text_cam_poente');

                }

                if ($entry->gate_desc == 'Porta 15') {

                    $entry->block_id = 999991599999;
                    $entry->block_desc  = Lang::get('reports.text_cam_nascente');

                }

                if ($entry->gate_desc == 'Camarote Presidencial') {

                    $entry->block_id = 99999999999;
                    $entry->block_desc = $entry->gate_desc;

                }

                $imported_index = array_search($entry->gate_desc, array_column($cleaned_import, 'description'));

                if (!isset($structure["venue"][$entry->template_id])) {

                    $structure["venue"][$entry->template_id] = array(
                        "description" => $entry->template_desc,
                        "options" => array(
                            "zones" => array(
                                $entry->block_id => array(
                                    "description" => $entry->block_desc,
                                    "options" => array(
                                        "gates" => array(
                                            $entry->gate_id => array(
                                                "description" => $entry->gate_desc,
                                                "masters" => $entry->entries_master,
                                                "entries" => $entry->entries,
                                                "imported" => ((string)$imported_index == '') ? 0 : $cleaned_import[$imported_index]['imported']
                                            )
                                        )
                                    ),
                                    "masters" => $entry->entries_master,
                                    "total_entries" => $entry->entries,
                                    "imported" => ((string)$imported_index == '') ? 0 : $cleaned_import[$imported_index]['imported']
                                )
                            )
                        ),
                        "masters" => $entry->entries_master,
                        "total_entries" => $entry->entries,
                        "imported" => ((string)$imported_index == '') ? 0 : $cleaned_import[$imported_index]['imported']
                    );

                } else {

                    if (!isset($entry->block_desc, $structure["venue"][$entry->template_id]["options"]["zones"][$entry->block_id])) {

                        $structure["venue"][$entry->template_id]["options"]["zones"][$entry->block_id] = array(
                            "description" => $entry->block_desc,
                            "options" => array(
                                "gates" => array(
                                    $entry->gate_id => array(
                                        "description" => $entry->gate_desc,
                                        "masters" => $entry->entries_master,
                                        "entries" => $entry->entries,
                                        "imported" => ((string)$imported_index == '') ? 0 : $cleaned_import[$imported_index]['imported']
                                    )
                                )
                            ),
                            "masters" => $entry->entries_master,
                            "total_entries" => $entry->entries,
                            "imported" => ((string)$imported_index == '') ? 0 : $cleaned_import[$imported_index]['imported']
                        );

                        $structure["venue"][$entry->template_id]["masters"] = $structure["venue"][$entry->template_id]["masters"] + $entry->entries_master;
                        $structure["venue"][$entry->template_id]["total_entries"] = $structure["venue"][$entry->template_id]["total_entries"] + $entry->entries;
                        $structure["venue"][$entry->template_id]["imported"] = $structure["venue"][$entry->template_id]["imported"] + (((string)$imported_index == '') ? 0 : $cleaned_import[$imported_index]['imported']);

                    } else {

                        $structure["venue"][$entry->template_id]["options"]["zones"][$entry->block_id]["options"]["gates"][$entry->gate_id] = array(
                            "description" => $entry->gate_desc,
                            "entries" => $entry->entries,
                            "masters" => $entry->entries_master,
                            "imported" => ((string)$imported_index == '') ? 0 : $cleaned_import[$imported_index]['imported']
                        );

                        $structure["venue"][$entry->template_id]["options"]["zones"][$entry->block_id]["imported"] = $structure["venue"][$entry->template_id]["options"]["zones"][$entry->block_id]["imported"] + (((string)$imported_index == '') ? 0 : $cleaned_import[$imported_index]['imported']);
                        $structure["venue"][$entry->template_id]["options"]["zones"][$entry->block_id]["masters"] = $structure["venue"][$entry->template_id]["options"]["zones"][$entry->block_id]["masters"] + $entry->entries_master;
                        $structure["venue"][$entry->template_id]["options"]["zones"][$entry->block_id]["total_entries"] = $structure["venue"][$entry->template_id]["options"]["zones"][$entry->block_id]["total_entries"] + $entry->entries;
                        $structure["venue"][$entry->template_id]["total_entries"] = $structure["venue"][$entry->template_id]["total_entries"] + $entry->entries;
                        $structure["venue"][$entry->template_id]["masters"] = $structure["venue"][$entry->template_id]["masters"] + $entry->entries_master;
                        $structure["venue"][$entry->template_id]["imported"] = $structure["venue"][$entry->template_id]["imported"] + (((string)$imported_index == '') ? 0 : $cleaned_import[$imported_index]['imported']);

                    }

                }

            }

            foreach ($entries_extra as $entry_extra) {

                if ($entry_extra->block_desc == 'Superior Sul - Porta 8') {

                    if (isset($special_sup_south_block_id[0])) {

                        $entry_extra->block_id = $special_sup_south_block_id[0]->id;

                    }

                    $entry_extra->block_desc = 'Superior Sul';

                }

                if ($entry_extra->block_desc == 'Porta 1') {

                    $entry_extra->template_id   = 21;
                    $entry_extra->template_desc = 'Futebol Dragao';
                    $entry_extra->block_id = 99999199999;
                    $entry_extra->block_desc  = Lang::get('reports.text_cam_poente');

                }

                if ($entry_extra->block_desc == 'Porta 15') {

                    $entry_extra->template_id   = 21;
                    $entry_extra->template_desc = 'Futebol Dragao';
                    $entry_extra->block_id = 999991599999;
                    $entry_extra->block_desc  = Lang::get('reports.text_cam_nascente');

                }

                if (!isset($structure["venue"][$entry_extra->template_id])) {

                    $structure["venue"][$entry_extra->template_id] = array(
                        "description" => $entry_extra->template_desc,
                        "options" => array(
                            "zones" => array(
                                $entry_extra->block_id => array(
                                    "description" => $entry_extra->block_desc,
                                    "options" => array(
                                        "gates" => array(
                                            $entry_extra->gate_id => array(
                                                "description" => $entry_extra->gate_desc,
                                                "masters" => 0,
                                                "entries" => $entry_extra->entries,
                                                "imported" => 0
                                            )
                                        )
                                    ),
                                    "masters" => 0,
                                    "total_entries" => $entry_extra->entries,
                                    "imported" => 0
                                )
                            )
                        ),
                        "masters" => 0,
                        "total_entries" => $entry_extra->entries,
                        "imported" => 0
                    );

                } else {

                    if (!isset($entry_extra->block_desc, $structure["venue"][$entry_extra->template_id]["options"]["zones"][$entry_extra->block_id])) {

                        $structure["venue"][$entry_extra->template_id]["options"]["zones"][$entry_extra->block_id] = array(
                            "description" => $entry_extra->block_desc,
                            "options" => array(
                                "gates" => array(
                                    $entry_extra->gate_id => array(
                                        "description" => $entry_extra->gate_desc,
                                        "masters" => 0,
                                        "entries" => $entry_extra->entries,
                                        "imported" => 0
                                    )
                                )
                            ),
                            "masters" => 0,
                            "total_entries" => $entry_extra->entries,
                            "imported" => 0
                        );

                        $structure["venue"][$entry_extra->template_id]["total_entries"] = $structure["venue"][$entry_extra->template_id]["total_entries"] + $entry_extra->entries;

                    } else {

                        $structure["venue"][$entry_extra->template_id]["options"]["zones"][$entry_extra->block_id]["options"]["gates"][$entry_extra->gate_id]["entries"] = $structure["venue"][$entry_extra->template_id]["options"]["zones"][$entry_extra->block_id]["options"]["gates"][$entry_extra->gate_id]["entries"] + $entry_extra->entries;

                        $structure["venue"][$entry_extra->template_id]["options"]["zones"][$entry_extra->block_id]["total_entries"] = $structure["venue"][$entry_extra->template_id]["options"]["zones"][$entry_extra->block_id]["total_entries"] + $entry_extra->entries;

                        $structure["venue"][$entry_extra->template_id]["total_entries"] = $structure["venue"][$entry_extra->template_id]["total_entries"] + $entry_extra->entries;

                    }

                }

            }

            $venue_id = 0;
            $zone_id = 0;

            foreach ($structure as $key => $value) {

                foreach ($value as $venue_idx => $venue_values) {

                    $venue_id = $venue_idx;

                    foreach ($venue_values['options']['zones'] as $zone_idx => $zone) {

                        foreach ($zone as $zone_key => $options) {

                            if ($zone_key == 'options') {

                                array_multisort(array_column($structure['venue'][$venue_id]['options']['zones'][$zone_idx]['options']['gates'], 'description'), SORT_NATURAL, $structure['venue'][$venue_id]['options']['zones'][$zone_idx]['options']['gates']);

                            }

                        }

                    }

                }

            }

            array_multisort(array_column($structure['venue'][$venue_id]['options']['zones'], 'description'), SORT_NATURAL, $structure['venue'][$venue_id]['options']['zones']);

            $structure["presidential_box_entries"] = $presidential_box[0]->total;

            return response()->json($structure, 200);

        } catch (\Exception $e) {

            Log::error('Get Zone Entries Error');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

    public function getTopEntitiesEntries(Request $request, $event_id) {

        try {

            $event_options = $this->getEventOptions($event_id);

            $entries = DB::connection('access')->select("
                SELECT
                    gestacess_regras_lista_ticket.va18 AS 'entity',
                    gs_portas.descricao AS 'gate',
                    COUNT(
                        gestacess_regras_lista_ticket.va18
                    ) AS total
                FROM
                    gestacess_transacoes
                LEFT JOIN gestacess_template_inst ON gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
                LEFT JOIN gestacess_regras_lista_ticket ON
                    (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo AND gestacess_regras_lista_ticket.va1 = '" . $event_options['code'] . "'
                    )
                LEFT JOIN gestacess_valores_variaveis_acesso ON
                    (
                        gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor AND gestacess_valores_variaveis_acesso.variavelacesso_id = 3
                    )
                LEFT JOIN(
                    SELECT
                        *
                    FROM
                        gestacess_template_inst
                    WHERE
                        isporta = 1
                ) AS gs_portas
                ON
                    gestacess_template_inst.parent_id = gs_portas.id
                WHERE
                    gestacess_transacoes.entry = 1 AND gestacess_transacoes.apagado = 0 AND gestacess_transacoes.isEntry = 1 AND gestacess_transacoes.eventID_id = '" . $event_id . "' AND gestacess_transacoes.dataTransac >=(
                    SELECT
                        datainicio
                    FROM
                        gestacess_evento
                    WHERE
                        id = '" . $event_id . "'
                ) AND gestacess_transacoes.horaTransac >= '03:00' AND testmode = 0
                GROUP BY
                    gestacess_regras_lista_ticket.VA18,
                    gs_portas.descricao
                ORDER BY
                    total
                DESC
            ");

            $structure = array("entity" => array());

            $cleaned_entities = array();

            foreach ($entries as $entity) {

                $array = array(
                    'description'   => '',
                    'gates'         => array(),
                    'total'         => 0
                );

                $entity_index = array_search($entity->entity, array_column($cleaned_entities, 'description'));

                if ((string)$entity_index == '') {

                    $array['description'] = $entity->entity;
                    $array['gates'][] = array(
                        'description'   => $entity->gate,
                        'total'         => $entity->total
                    );
                    $array['total'] = $entity->total;

                } else {

                    $gate = array(
                        'description'   => $entity->gate,
                        'total'         => $entity->total
                    );

                    array_push($cleaned_entities[$entity_index]['gates'], $gate);
                    $cleaned_entities[$entity_index]['total'] = $cleaned_entities[$entity_index]['total'] + $entity->total;

                }

                array_push($cleaned_entities, $array);

            }

            array_multisort(array_column($cleaned_entities, 'total'), SORT_DESC, $cleaned_entities);

            return response()->json(array_slice($cleaned_entities, 0, 10), 200);

        } catch (\Exception $e) {

            Log::error('Get Top Entities Entries Error');
            Log::error($e);

            response()->json(['error' => 'unavailable'], 200);

        }

        return response()->json(['success' => true], 200);

    }

    public function getMastersEntries(Request $request, $event_id) {

        try {

            $event_options = $this->getEventOptions($event_id);

            $entries_masters = DB::connection('access')->select("
                SELECT
                    DISTINCT transactions.titulo,
                    COUNT(transactions.titulo) AS entries
                FROM
                    gestacess_transacoes AS transactions
                    LEFT JOIN
                        gestacess_regras_lista_ticket AS rules
                        ON
                        (
                            rules.titulo = transactions.titulo
                        )
                    LEFT JOIN
                        gestacess_evento AS gevent
                        ON
                        (
                            gevent.id = transactions.eventId_id
                        )
                WHERE
                    transactions.eventId_id = '" . (int)$event_id . "'
                    AND
                    transactions.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    transactions.entry = 1
                    AND
                    transactions.isEntry = 1
                    AND
                    transactions.apagado = 0
                    AND
                    transactions.testMode = 0
                    AND
                    transactions.areaEntryId <> 330
                    AND
                    rules.VA3 = '91'
                    AND
                    rules.VA1 = gevent.code
                GROUP BY
                    transactions.titulo
            ");

            return response()->json($entries_masters, 200);

        } catch (\Exception $e) {

            Log::error('Get Zone Entries Error');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

    public function getEntriesByCam(Request $request, $event_id)
    {

        try {

            $event_options = $this->getEventOptions($event_id);

            $entries_cam = DB::connection('access')->select("
                SELECT
                    rules.VA24 AS 'cam',
                    rules.VA26 AS 'entity',
                    COUNT(DISTINCT(transactions.titulo)) AS 'total'
                FROM
                    `gestacess_regras_lista_ticket` AS rules
                    LEFT JOIN
                        `gestacess_transacoes` AS transactions 
                        ON (
                            transactions.titulo = rules.titulo  
                        )
                     LEFT JOIN
                        `gestacess_evento` AS events
                        ON (
                            events.id = transactions.eventId_id
                        )
                WHERE
                    events.id = '" . (int)$event_id . "'
                    AND
                    rules.VA8 LIKE '%Cam%'
                    AND
                    rules.VA24 < 90
                    AND
                    rules.VA3 <> 91
                    AND
                    transactions.dataTransac = '" . $event_options['dataInicio'] . "'
                    AND
                    transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    transactions.isEntry = 1
                    AND
                    transactions.entry = 1
                    AND
                    transactions.apagado = 0
                    AND
                    transactions.testMode = 0
                    AND
                    transactions.areaEntryId <> 330
                    AND
                    rules.VA26 IS NOT NULL
                GROUP BY
                    rules.VA24,
                    rules.VA26
                ORDER BY
                    LENGTH(rules.VA24) , rules.VA24
            ");

            $entries_cam_extra = DB::connection('access')->select("
                SELECT
                    COUNT(*) AS total
                FROM
                    gestacess_transacoes
                WHERE
                    eventId_id = '" . (int)$event_id . "'
                    AND
                    vas LIKE '%TCPVA5%'
            ");

            /*
            foreach ($entries_cam_extra as $entry_extra) {

                preg_match('/;TCPVA5:\d{1,2};VA24:(\d{1,2})/', $entry_extra->vas, $result);

                if (isset($result[1])) {

                    $exists = $this->checkBoxExists($entries_cam, $result[1]);

                    if ($exists > -1) {

                        $entries_cam[$exists]->total = $entries_cam[$exists]->total + 1;

                    }

                }

            }
            */

            $total_extra = (array) $entries_cam_extra[0];

            return response()->json(array('total_extra' => $total_extra['total'], 'boxs' => $entries_cam), 200);

        } catch (\Exception $e) {

            Log::error('Get Entries By Cam');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

    private function checkBoxExists($boxs, $searching) {

        foreach ($boxs as $idx => $box) {

            if ($box->cam == $searching) {
                return $idx;
            }

        }

        return -1;

    }

    public function getTicketTypeEntries(Request $request, $event_id) {

        try {

            $event_options = $this->getEventOptions($event_id);

            $entries = DB::connection('access')->select("
                SELECT
                    gestacess_valores_variaveis_acesso.descricao AS 'tickettype',
                    COUNT(DISTINCT gestacess_transacoes.titulo) AS 'entries'
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                    LEFT JOIN gestacess_regras_lista_ticket ON (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
                    LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
                    WHERE
                    gestacess_evento.id = '" . (int)$event_id . "'
                    AND
                    gestacess_evento.code = gestacess_regras_lista_ticket.evento
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_valores_variaveis_acesso.id != '10'
                GROUP BY
                    gestacess_valores_variaveis_acesso.descricao
                ORDER BY
                    gestacess_valores_variaveis_acesso.descricao
            ");

            $entries_master = DB::connection('access')->select("
                SELECT
                    gestacess_valores_variaveis_acesso.descricao AS 'tickettype',
                    COUNT(gestacess_transacoes.titulo) AS 'entries'
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                    LEFT JOIN gestacess_regras_lista_ticket ON (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
                    LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
                    WHERE
                    gestacess_evento.id = '" . (int)$event_id . "'
                    AND
                    gestacess_evento.code = gestacess_regras_lista_ticket.evento
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_valores_variaveis_acesso.id = '10'
                GROUP BY
                    gestacess_valores_variaveis_acesso.descricao
                ORDER BY
                    gestacess_valores_variaveis_acesso.descricao
            ");

            $entries_extra = DB::connection('access')->select("
                    SELECT
                        'Livre Trânsito_Eletrónicos' AS 'tickettype',
                        COUNT(id) AS 'entries'
                    FROM
                        `gestacess_transacoes`
                    WHERE
                        `eventId_id` = '" . (int)$event_id . "' AND `vas` LIKE '%TCPVA5%'
                ");

            $entries[] = $entries_master[0];
            $entries[] = $entries_extra[0];

            return response()->json($entries, 200);

        } catch (\Exception $e) {

            Log::error('Get Tikcet Type Entries Error');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

    public function getFlowrate(Request $request, $event_id) {

        try {

            $event_options = $this->getEventOptions($event_id);

            $flowrate_values = DB::connection('access')->select("
                SELECT
                    DATE_FORMAT(
                        MIN(cast(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac) as datetime)), '%Y-%m-%d %H:%i:00'
                    ) AS 'hour_transaction',
                    COUNT(gestacess_transacoes.id) AS 'entries'
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                    LEFT JOIN gestacess_regras_lista_ticket ON (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
                    LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
                WHERE
                    gestacess_evento.id = '" . $event_id . "'
                    AND
                    gestacess_evento.code = gestacess_regras_lista_ticket.evento
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                    AND
                    TIMESTAMP(gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) >= '" . $event_options['dataInicio'] . "'
                GROUP BY
                    DATE_FORMAT(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac), '%Y-%m-%d %H:%i:00')
            ");

            return response()->json($flowrate_values, 200);

        } catch (\Exception $e) {

            Log::error('Get Flowrate Error');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

    public function getEntriesByGroup(Request $request, $event_id, $event_park_id, $event_credential_id) {

        try {

            $entries = array();

            $event_options = $this->getEventOptions($event_id);

            $event_datetime = $event_options['dataInicio'] . ' ' . $event_options['horaAberturaPortas'];

            $event_credential_options = $this->getEventOptions($event_credential_id);

            $event_park_options = $this->getEventOptions($event_park_id);

            $max_flowrate = DB::connection('access')->select("
                SELECT
                    DATE_FORMAT(
                        MIN(cast(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac) as datetime)), '%Y-%m-%d %H:%i:00'
                    ) AS 'hour',
                    COUNT(DISTINCT gestacess_transacoes.titulo) AS 'entries'
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                    LEFT JOIN gestacess_regras_lista_ticket ON (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
                    LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
                WHERE
                    gestacess_evento.id = '" . $event_id . "'
                    AND
                    gestacess_evento.code = gestacess_regras_lista_ticket.evento
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                    AND
                    TIMESTAMP(gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) >= '" . $event_datetime . "'
                GROUP BY
                    DATE_FORMAT(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac), '%Y-%m-%d %H:%i:00')
                ORDER BY
                    entries DESC
                LIMIT 1
            ");

            $global_entries = DB::connection('access')->select("
                SELECT
                    COUNT(DISTINCT transactions.titulo) AS 'entries'
                FROM
                    gestacess_template_inst AS temp_dac
                LEFT JOIN gestacess_template_inst AS temp_gate
                ON
                    (
                        temp_gate.id = temp_dac.parent_id AND temp_gate.isPorta = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_block
                ON
                    (
                        temp_block.id = temp_gate.parent_id AND temp_block.isZona = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_template
                ON
                    (
                        temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                    )
                LEFT JOIN gestacess_template AS template
                ON
                    (
                        template.id = temp_template.template_id
                    )
                LEFT JOIN gestacess_transacoes AS transactions
                ON
                    (
                        transactions.template_inst_id = temp_dac.id
                        AND
                        transactions.entry = 1
                        AND
                        transactions.isEntry = 1
                        AND
                        transactions.apagado = 0
                        AND
                        transactions.testMode = 0
                        AND
                        transactions.areaEntryId <> 330
                    )
                LEFT JOIN gestacess_evento ON (
                            transactions.eventId_id = gestacess_evento.id
                        )
                LEFT JOIN gestacess_regras_lista_ticket ON (
                            transactions.titulo = gestacess_regras_lista_ticket.titulo
                            AND
                            gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                        )
               	LEFT JOIN gestacess_valores_variaveis_acesso ON (
                            gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                            AND
                            gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                        )
                WHERE
                    transactions.eventId_id = '" . $event_id . "'
                    AND
                    transactions.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_valores_variaveis_acesso.id != '10'
            ");

            $global_entries_master = DB::connection('access')->select("
                SELECT
                    COUNT(transactions.titulo) AS 'entries'
                FROM
                    gestacess_template_inst AS temp_dac
                LEFT JOIN gestacess_template_inst AS temp_gate
                ON
                    (
                        temp_gate.id = temp_dac.parent_id AND temp_gate.isPorta = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_block
                ON
                    (
                        temp_block.id = temp_gate.parent_id AND temp_block.isZona = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_template
                ON
                    (
                        temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                    )
                LEFT JOIN gestacess_template AS template
                ON
                    (
                        template.id = temp_template.template_id
                    )
                LEFT JOIN gestacess_transacoes AS transactions
                ON
                    (
                        transactions.template_inst_id = temp_dac.id
                        AND
                        transactions.entry = 1
                        AND
                        transactions.isEntry = 1
                        AND
                        transactions.apagado = 0
                        AND
                        transactions.testMode = 0
                        AND
                        transactions.areaEntryId <> 330
                    )
                LEFT JOIN gestacess_evento ON (
                            transactions.eventId_id = gestacess_evento.id
                        )
                LEFT JOIN gestacess_regras_lista_ticket ON (
                            transactions.titulo = gestacess_regras_lista_ticket.titulo
                            AND
                            gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                        )
               	LEFT JOIN gestacess_valores_variaveis_acesso ON (
                            gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                            AND
                            gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                        )
                WHERE
                    transactions.eventId_id = '" . $event_id . "'
                    AND
                    transactions.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_valores_variaveis_acesso.id = '10'
            ");

            $global_entries_extra = DB::connection('access')->select("
                SELECT
                    COUNT(id) AS 'entries'
                FROM
                    `gestacess_transacoes`
                WHERE
                    `eventId_id` = '" . (int)$event_id . "' AND `vas` LIKE '%TCPVA5%'
            ");

            $total_global_entries = 0;

            if ($global_entries && $global_entries_master && $global_entries_extra) {

                $total_global_entries = $global_entries[0]->entries + $global_entries_master[0]->entries + $global_entries_extra[0]->entries;

            }

            $app_entries = DB::connection('access')->select("
                SELECT
                    COUNT(DISTINCT gestacess_transacoes.titulo) AS 'entries'
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                    LEFT JOIN gestacess_regras_lista_ticket ON (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
                    LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
                WHERE
                    gestacess_evento.id = '" . $event_id . "'
                    AND
                    gestacess_evento.code = gestacess_regras_lista_ticket.evento
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                    AND
                    gestacess_regras_lista_ticket.VA3 <> '91'
                    AND
                    LENGTH(gestacess_transacoes.titulo) < 16
                    AND
                    gestacess_transacoes.tipoleitor = 1
                    AND
                    (gestacess_valores_variaveis_acesso.descricao like '%Superliga%' or gestacess_valores_variaveis_acesso.descricao like '%Lugar Anual%')
            ");

            $credential_entries = DB::connection('access')->select("
                SELECT
                    COUNT(transactions.id) AS 'entries'
                FROM
                    gestacess_template_inst AS temp_dac
                LEFT JOIN gestacess_template_inst AS temp_gate
                ON
                    (
                        temp_gate.id = temp_dac.parent_id AND temp_gate.isPorta = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_block
                ON
                    (
                        temp_block.id = temp_gate.parent_id AND temp_block.isZona = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_template
                ON
                    (
                        temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                    )
                LEFT JOIN gestacess_template AS template
                ON
                    (
                        template.id = temp_template.template_id
                    )
                LEFT JOIN gestacess_transacoes AS transactions
                ON
                    (
                        transactions.template_inst_id = temp_dac.id
                        AND
                        transactions.entry = 1
                        AND
                        transactions.isEntry = 1
                        AND
                        transactions.apagado = 0
                        AND
                        transactions.testMode = 0
                        AND
                        transactions.areaEntryId <> 330
                    )
                WHERE
                    transactions.eventId_id = '" . $event_credential_id . "'
                    AND
                    transactions.dataTransac >= '" . $event_credential_options['dataInicio'] . "'
                    AND
                    transactions.horaTransac >= '" . $event_credential_options['horaAberturaPortas'] . "'
            ");

            $park_entries = DB::connection('access')->select("
                SELECT
                    COUNT(transactions.id) AS 'entries'
                FROM
                    gestacess_template_inst AS temp_dac
                LEFT JOIN gestacess_template_inst AS temp_gate
                ON
                    (
                        temp_gate.id = temp_dac.parent_id AND temp_gate.isPorta = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_block
                ON
                    (
                        temp_block.id = temp_gate.parent_id AND temp_block.isZona = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_template
                ON
                    (
                        temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                    )
                LEFT JOIN gestacess_template AS template
                ON
                    (
                        template.id = temp_template.template_id
                    )
                LEFT JOIN gestacess_transacoes AS transactions
                ON
                    (
                        transactions.template_inst_id = temp_dac.id
                        AND
                        transactions.entry = 1
                        AND
                        transactions.isEntry = 1
                        AND
                        transactions.apagado = 0
                        AND
                        transactions.testMode = 0
                        AND
                        transactions.areaEntryId <> 330
                    )
                WHERE
                    transactions.eventId_id = '" . $event_park_id . "'
                    AND
                    transactions.dataTransac >= '" . $event_park_options['dataInicio'] . "'
                    AND
                    transactions.horaTransac >= '" . $event_park_options['horaAberturaPortas'] . "'
            ");

            $master_entries = DB::connection('access')->select("
                SELECT
                    COUNT(DISTINCT transactions.id, transactions.titulo) AS 'entries'
                FROM
                    gestacess_template_inst AS temp_dac
                LEFT JOIN gestacess_template_inst AS temp_gate
                ON
                    (
                        temp_gate.id = temp_dac.parent_id AND temp_gate.isPorta = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_block
                ON
                    (
                        temp_block.id = temp_gate.parent_id AND temp_block.isZona = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_template
                ON
                    (
                        temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                    )
                LEFT JOIN gestacess_template AS template
                ON
                    (
                        template.id = temp_template.template_id
                    )
                LEFT JOIN gestacess_transacoes AS transactions
                ON
                    (
                        transactions.template_inst_id = temp_dac.id
                        AND
                        transactions.entry = 1
                        AND
                        transactions.isEntry = 1
                        AND
                        transactions.apagado = 0
                        AND
                        transactions.testMode = 0
                        AND
                        transactions.areaEntryId <> 330
                    )
                RIGHT JOIN 
                    `gestacess_regras_lista_ticket` AS rules
                    ON 
                        (
                            rules.titulo = transactions.titulo
                            AND
                            rules.VA3 = '91'
                        )
                WHERE
                    transactions.eventId_id = '" . $event_id . "'
                    AND
                    transactions.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
            ");

            $wl_estadium = DB::connection('access')->select("
                SELECT
                    COUNT(import_list.titulo) AS total
                FROM
                    gestacess_import_list AS import_list
                LEFT JOIN gestacess_regras_lista_ticket AS regras_lista_ticket
                ON
                    (
                        regras_lista_ticket.titulo = import_list.titulo
                    )
                LEFT JOIN gestacess_valores_variaveis_acesso AS valores_variaveis_acesso
                ON
                    (
                        valores_variaveis_acesso.valor = regras_lista_ticket.VA2 AND valores_variaveis_acesso.variavelAcesso_id = 2
                    )
                WHERE
                    import_list.codEvent = '" . $event_options["code"] . "'
                    AND
                    regras_lista_ticket.evento = import_list.codEvent
                    AND
                    regras_lista_ticket.VA1 = import_list.codEvent
                    AND
                    import_list.isWhiteList = 1
                    AND
                    regras_lista_ticket.VA2 <> '50'
            ");

            $wl_credentials = DB::connection('access')->select("
                SELECT
                    COUNT(import_list.titulo) AS total
                FROM
                    gestacess_import_list AS import_list
                LEFT JOIN gestacess_regras_lista_ticket AS regras_lista_ticket
                ON
                    (
                        regras_lista_ticket.titulo = import_list.titulo
                    )
                LEFT JOIN gestacess_valores_variaveis_acesso AS valores_variaveis_acesso
                ON
                    (
                        valores_variaveis_acesso.valor = regras_lista_ticket.VA2 AND valores_variaveis_acesso.variavelAcesso_id = 2
                    )
                WHERE
                    import_list.codEvent = '" . $event_credential_options['code'] . "'
                    AND
                    regras_lista_ticket.evento = import_list.codEvent
                    AND
                    regras_lista_ticket.VA1 = import_list.codEvent
                    AND
                    import_list.isWhiteList = 1
            ");

            $wl_park = DB::connection('access')->select("
                SELECT
                    COUNT(import_list.titulo) AS total
                FROM
                    gestacess_import_list AS import_list
                LEFT JOIN gestacess_regras_lista_ticket AS regras_lista_ticket
                ON
                    (
                        regras_lista_ticket.titulo = import_list.titulo
                    )
                LEFT JOIN gestacess_valores_variaveis_acesso AS valores_variaveis_acesso
                ON
                    (
                        valores_variaveis_acesso.valor = regras_lista_ticket.VA2 AND valores_variaveis_acesso.variavelAcesso_id = 2
                    )
                WHERE
                    import_list.codEvent = '" . $event_park_options['code'] . "'
                    AND
                    regras_lista_ticket.evento = import_list.codEvent
                    AND
                    regras_lista_ticket.VA1 = import_list.codEvent
                    AND
                    import_list.isWhiteList = 1
                    AND
                    regras_lista_ticket.VA2 <> '50'
            ");

            $wl_masters = DB::connection('access')->select("
                SELECT
                    COUNT(import_list.titulo) AS total
                FROM
                    gestacess_import_list AS import_list
                LEFT JOIN gestacess_regras_lista_ticket AS regras_lista_ticket
                ON
                    (
                        regras_lista_ticket.titulo = import_list.titulo
                    )
                LEFT JOIN gestacess_valores_variaveis_acesso AS valores_variaveis_acesso
                ON
                    (
                        valores_variaveis_acesso.valor = regras_lista_ticket.VA2 AND valores_variaveis_acesso.variavelAcesso_id = 2
                    )
                WHERE
                    import_list.codEvent = '" . $event_options['code'] . "'
                    AND
                    regras_lista_ticket.evento = import_list.codEvent
                    AND
                    regras_lista_ticket.VA1 = import_list.codEvent
                    AND
                    import_list.isWhiteList = 1
                    AND
                    regras_lista_ticket.VA2 = '50'
            ");

            $bl_stadium = DB::connection('access')->select("
                SELECT
                    COUNT(*) AS total
                FROM
                    gestacess_import_list
                WHERE
                    codEvent = '" . $event_options['code'] . "'
                    AND 
                    apagado = 0 
                    AND 
                    isWhiteList = 0
            ");

            $entries['max_flowrate'] = (count($max_flowrate) > 0) ? $max_flowrate[0] : 'unavailable';
            $entries['global'] = ($total_global_entries) ? $total_global_entries : 'unavailable';
            $entries['app'] = (count($app_entries) > 0) ? $app_entries[0]->entries : 'unavailable';
            $entries['master'] = (count($master_entries) > 0) ? $master_entries[0]->entries : 'unavailable';
            $entries['credential'] = (count($credential_entries) > 0) ? $credential_entries[0]->entries : 'unavailable';
            $entries['park'] = (count($park_entries) > 0) ? $park_entries[0]->entries : 'unavailable';

            $entries['wl_stadium'] = (count($wl_estadium) > 0) ? $wl_estadium[0]->total : 'unavailable';
            $entries['wl_masters'] = (count($wl_masters) > 0) ? $wl_masters[0]->total : 'unavailable';
            $entries['wl_credentials'] = (count($wl_credentials) > 0) ? $wl_credentials[0]->total : 'unavailable';
            $entries['wl_park'] = (count($wl_park) > 0) ? $wl_park[0]->total : 'unavailable';
            $entries['bl_stadium'] = (count($bl_stadium) > 0) ? $bl_stadium[0]->total : 'unavailable';

            return response()->json($entries, 200);

        } catch (\Exception $e) {

            Log::error('Get Entries By Group');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

    private function getEventOptions($event_id) {

        $options = DB::connection('access')->select("
                SELECT
                    code,
                    dataInicio,
                    dataFim,
                    horaAberturaPortas
                FROM
                    gestacess_evento
                WHERE
                    id = " . (int)$event_id . "
            ");

        $event_options = (array) $options[0];

        return $event_options;

    }

    public function generateErrorReport(Request $request, $event_id) {

        $event_options = $this->getEventOptions($event_id);

        $errors = DB::connection('access')->select("
            select
                 gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , gestacess_transacoes.titulo as 'Titulo'
                , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_transacoes.msg_blacklist USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist_DAC'
                , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = '" . $event_options['code'] . "')
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = '" . $event_options['code'] . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_transacoes.msg_blacklist USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist_DAC'
                , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                , gestacess_template_inst
                , gestacess_dac
                , gestacess_codigosmensagem
                where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                 and gestacess_template_inst.dac_id=gestacess_dac.id
                 and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                 and gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,18,19,20,23,26)
                 and gestacess_transacoes.titulo not in (
                
                select gestacess_transacoes.titulo
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = '" . $event_options['code'] . "')
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = '" . $event_options['code'] . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,18,19,20,23,26)
                
                )
                order by datetime
        ");

        $filename = 'Erros_' . $event_id . '_' . $event_options['code'] . '.csv';

        return response()->json(array('filename' => $filename, 'errors' => $errors, 200));

    }

    public function getTransactionsErrorByMessage(Request $request, $event_id) {

        $event_options = $this->getEventOptions($event_id);

        $result = DB::connection('access')->select("
            select
                  Mensagem
                  , Quantidade
                  , (format(round((Quantidade*100)/(select sum(Quantidade) from
                  
                
                (
                select
                  (case when Mensagem = 'JA ENTROU' then 'JA ENTROU < 1 minuto' else Mensagem end) as 'Mensagem'
                  , (case when Mensagem = 'JA ENTROU' then (Quantidade - (
                  
                
                
                (select count(codigobarras) from 
                (
                select gs_erros.datetime as datahora, gs_erros.Porta as porta, gs_erros.DAC as dac, gs_erros.Titulo as codigobarras, gs_erros.Socio as socio, gs_erros.NSocio as nsocio, gs_erros.Categoria as categoria from 
                
                
                (
                select
                 gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , gestacess_transacoes.titulo as 'Titulo'
                , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                , gestacess_template_inst
                , gestacess_dac
                , gestacess_codigosmensagem
                where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                 and gestacess_template_inst.dac_id=gestacess_dac.id
                 and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                 and gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and gestacess_transacoes.titulo not in (
                
                select gestacess_transacoes.titulo
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime
                ) as gs_erros
                
                , (
                
                select
                      (select
                    code
                      from
                    gestacess_evento
                      where
                    id = '" . $event_id . "') as 'jogo'
                      , cast(min(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac)) as char) as 'datahora'
                      , gestacess_regras_lista_ticket.VA2 as 'porta'
                      , gestacess_dac.code as 'dac'
                      , gestacess_dac.ip as 'ip'
                      , gestacess_transacoes.titulo as 'codigobarras'
                      , gestacess_regras_lista_ticket.VA20 as 'socio'
                      , gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                      , gestacess_regras_lista_ticket.VA5 as 'categoria'
                      , (case
                      when gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                    from
                      (
                    select
                      min(gestacess_transacoes.id) as id
                      , gestacess_transacoes.titulo as 'codigobarras'
                    from
                      gestacess_transacoes
                      left join gestacess_template_inst
                        on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                      left join gestacess_dac
                        on gestacess_template_inst.dac_id = gestacess_dac.id
                      left join gestacess_regras_lista_ticket
                        on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                          and gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                        )
                      left join gestacess_valores_variaveis_acesso
                        on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                          and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                      gestacess_transacoes.entry=1
                      and gestacess_transacoes.apagado = 0
                      and gestacess_transacoes.isEntry  = 1
                      and gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and testmode = 0
                      and areaentryid <> 330
                      and gestacess_transacoes.titulo not in (
                        select
                          titulo
                        from
                          gestacess_regras_lista_ticket
                        where
                          va3 = '91'
                          and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                      )
                    group by
                      gestacess_transacoes.titulo
                      ) as ent
                      left join gestacess_transacoes
                    on ent.id = gestacess_transacoes.id
                      left join gestacess_template_inst
                    on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                      left join gestacess_dac
                    on gestacess_template_inst.dac_id = gestacess_dac.id
                      left join gestacess_regras_lista_ticket
                    on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                      and gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                    )
                      left join gestacess_valores_variaveis_acesso
                    on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                      and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                      gestacess_transacoes.entry=1
                      and gestacess_transacoes.apagado = 0
                      and gestacess_transacoes.isEntry  = 1
                      and gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and testmode = 0
                      and areaentryid <> 330
                      and gestacess_transacoes.titulo not in (
                    select
                      titulo
                    from
                      gestacess_regras_lista_ticket
                    where
                      va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      )
                    group by
                      gestacess_regras_lista_ticket.VA2
                      , gestacess_dac.code
                      , gestacess_dac.ip
                      , gestacess_transacoes.titulo
                      , gestacess_regras_lista_ticket.VA20
                      , gestacess_regras_lista_ticket.VA21
                      , gestacess_valores_variaveis_acesso.descricao
                      , gestacess_regras_lista_ticket.VA5
                      , (case
                    when gestacess_transacoes.tipoleitor = 1
                      then 'CODIGO BARRAS'
                    when gestacess_transacoes.tipoleitor = 2
                      then 'NFC'
                    else ''
                      end) 
                union all 
                      select
                    (select
                      code
                    from
                      gestacess_evento
                    where 
                      id = '" . $event_id . "') as 'jogo'
                    , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'datahora'
                    , gestacess_regras_lista_ticket.VA2 as 'porta'
                    , gestacess_dac.code as 'dac'
                    , gestacess_dac.ip as 'ip'
                    , gestacess_transacoes.titulo as 'codigobarras'
                    , '' as 'socio'
                    , '' as 'nsocio'
                    , 'Livre Transito' as 'tipotitulo'
                    , '' as 'categoria'
                    , (case
                      when gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                      from
                    gestacess_transacoes
                    left join gestacess_template_inst
                      on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                    left join gestacess_dac
                      on gestacess_template_inst.dac_id = gestacess_dac.id
                    left join gestacess_regras_lista_ticket
                      on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        and gestacess_regras_lista_ticket.va1 = (
                          select
                        code
                          from
                        gestacess_evento
                          where
                        id = '" . $event_id . "'
                        )
                      )
                    left join gestacess_valores_variaveis_acesso
                      on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                        and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                      where
                    gestacess_transacoes.entry = 1
                    and gestacess_transacoes.apagado = 0
                    and gestacess_transacoes.isEntry  = 1
                    and gestacess_transacoes.eventID_id = '" . $event_id . "'
                    and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                    and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    and areaentryid <> 330
                    and gestacess_transacoes.titulo in (
                      select
                        titulo
                      from
                        gestacess_regras_lista_ticket
                      where
                        va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      ) 
                    order by
                      datahora 
                
                ) as gs_entradas
                
                where gs_erros.Titulo = gs_entradas.codigobarras
                and (gs_erros.erro = 'JA ENTROU' and timediff(gs_erros.Datetime, gs_entradas.datahora)>'00:00:60')
                
                
                order by codigobarras, datahora
                ) as gs_transacoes_erro_ja_entrou_maior_minuto0
                )  
                  
                
                  
                  
                  
                  )) else Quantidade end) as 'Quantidade'
                
                from 
                
                
                (
                select 
                
                (case when Erro = 'BLACK LIST' then replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Mensagem_Blacklist USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Erro USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') end) as 'Mensagem'
                , count(ID) as 'Quantidade' 
                , format(round((count(ID)*100)/(select count(ID) from (
                
                select * from (
                
                select
                gestacess_transacoes.id as 'ID'
                , gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , gestacess_transacoes.titulo as 'Titulo'
                , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                gestacess_transacoes.id as 'ID'
                , '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                , gestacess_template_inst
                , gestacess_dac
                , gestacess_codigosmensagem
                where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                 and gestacess_template_inst.dac_id=gestacess_dac.id
                 and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                 and gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and gestacess_transacoes.titulo not in (
                
                select gestacess_transacoes.titulo
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime ) as sub3 ) as sub4),2),2) as '%'
                
                from (
                
                
                select * from (
                
                select
                gestacess_transacoes.id as 'ID'
                , gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , gestacess_transacoes.titulo as 'Titulo'
                , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                  and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                gestacess_transacoes.id as 'ID'
                , '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                , gestacess_template_inst
                , gestacess_dac
                , gestacess_codigosmensagem
                where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                 and gestacess_template_inst.dac_id=gestacess_dac.id
                 and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                 and gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and gestacess_transacoes.titulo not in (
                
                select gestacess_transacoes.titulo
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime ) as sub1 ) as sub2
                
                group by (case when Erro = 'BLACK LIST' then replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Mensagem_Blacklist USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Erro USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') end)
                
                order by count(ID) desc
                
                ) as gs_erros_mensagem_agrupado1
                
                
                
                union all
                
                select
                  'JA ENTROU > 1 minuto' as 'Mensagem'
                  , (select count(codigobarras) from 
                
                
                (
                select gs_erros.datetime as datahora, gs_erros.Porta as porta, gs_erros.DAC as dac, gs_erros.Titulo as codigobarras, gs_erros.Socio as socio, gs_erros.NSocio as nsocio, gs_erros.Categoria as categoria from 
                
                
                (
                select
                 gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , gestacess_transacoes.titulo as 'Titulo'
                , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                , gestacess_template_inst
                , gestacess_dac
                , gestacess_codigosmensagem
                where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                 and gestacess_template_inst.dac_id=gestacess_dac.id
                 and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                 and gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and gestacess_transacoes.titulo not in (
                
                select gestacess_transacoes.titulo
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime
                ) as gs_erros
                
                , (
                
                select
                      (select
                    code
                      from
                    gestacess_evento
                      where
                    id = '" . $event_id . "') as 'jogo'
                      , cast(min(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac)) as char) as 'datahora'
                      , gestacess_regras_lista_ticket.VA2 as 'porta'
                      , gestacess_dac.code as 'dac'
                      , gestacess_dac.ip as 'ip'
                      , gestacess_transacoes.titulo as 'codigobarras'
                      , gestacess_regras_lista_ticket.VA20 as 'socio'
                      , gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                      , gestacess_regras_lista_ticket.VA5 as 'categoria'
                      , (case
                      when gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                    from
                      (
                    select
                      min(gestacess_transacoes.id) as id
                      , gestacess_transacoes.titulo as 'codigobarras'
                    from
                      gestacess_transacoes
                      left join gestacess_template_inst
                        on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                      left join gestacess_dac
                        on gestacess_template_inst.dac_id = gestacess_dac.id
                      left join gestacess_regras_lista_ticket
                        on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                          and gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                        )
                      left join gestacess_valores_variaveis_acesso
                        on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                          and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                      gestacess_transacoes.entry=1
                      and gestacess_transacoes.apagado = 0
                      and gestacess_transacoes.isEntry  = 1
                      and gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and testmode = 0
                      and areaentryid <> 330
                      and gestacess_transacoes.titulo not in (
                        select
                          titulo
                        from
                          gestacess_regras_lista_ticket
                        where
                          va3 = '91'
                          and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                      )
                    group by
                      gestacess_transacoes.titulo
                      ) as ent
                      left join gestacess_transacoes
                    on ent.id = gestacess_transacoes.id
                      left join gestacess_template_inst
                    on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                      left join gestacess_dac
                    on gestacess_template_inst.dac_id = gestacess_dac.id
                      left join gestacess_regras_lista_ticket
                    on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                      and gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                    )
                      left join gestacess_valores_variaveis_acesso
                    on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                      and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                      gestacess_transacoes.entry=1
                      and gestacess_transacoes.apagado = 0
                      and gestacess_transacoes.isEntry  = 1
                      and gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and testmode = 0
                      and areaentryid <> 330
                      and gestacess_transacoes.titulo not in (
                    select
                      titulo
                    from
                      gestacess_regras_lista_ticket
                    where
                      va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      )
                    group by
                      gestacess_regras_lista_ticket.VA2
                      , gestacess_dac.code
                      , gestacess_dac.ip
                      , gestacess_transacoes.titulo
                      , gestacess_regras_lista_ticket.VA20
                      , gestacess_regras_lista_ticket.VA21
                      , gestacess_valores_variaveis_acesso.descricao
                      , gestacess_regras_lista_ticket.VA5
                      , (case
                    when gestacess_transacoes.tipoleitor = 1
                      then 'CODIGO BARRAS'
                    when gestacess_transacoes.tipoleitor = 2
                      then 'NFC'
                    else ''
                      end) 
                union all 
                      select
                    (select
                      code
                    from
                      gestacess_evento
                    where 
                      id = '" . $event_id . "') as 'jogo'
                    , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'datahora'
                    , gestacess_regras_lista_ticket.VA2 as 'porta'
                    , gestacess_dac.code as 'dac'
                    , gestacess_dac.ip as 'ip'
                    , gestacess_transacoes.titulo as 'codigobarras'
                    , '' as 'socio'
                    , '' as 'nsocio'
                    , 'Livre Transito' as 'tipotitulo'
                    , '' as 'categoria'
                    , (case
                      when gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                      from
                    gestacess_transacoes
                    left join gestacess_template_inst
                      on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                    left join gestacess_dac
                      on gestacess_template_inst.dac_id = gestacess_dac.id
                    left join gestacess_regras_lista_ticket
                      on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        and gestacess_regras_lista_ticket.va1 = (
                          select
                        code
                          from
                        gestacess_evento
                          where
                        id = '" . $event_id . "'
                        )
                      )
                    left join gestacess_valores_variaveis_acesso
                      on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                        and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                      where
                    gestacess_transacoes.entry = 1
                    and gestacess_transacoes.apagado = 0
                    and gestacess_transacoes.isEntry  = 1
                    and gestacess_transacoes.eventID_id = '" . $event_id . "'
                    and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                    and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    and areaentryid <> 330
                    and gestacess_transacoes.titulo in (
                      select
                        titulo
                      from
                        gestacess_regras_lista_ticket
                      where
                        va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      ) 
                    order by
                      datahora 
                
                ) as gs_entradas
                
                where gs_erros.Titulo = gs_entradas.codigobarras
                and (gs_erros.erro = 'JA ENTROU' and timediff(gs_erros.Datetime, gs_entradas.datahora)>'00:00:60')
                
                
                order by codigobarras, datahora
                ) as gs_transacoes_erro_ja_entrou_maior_minuto
                
                ) as 'Quantidade'
                
                order by Quantidade desc
                ) as gs_query_percentagem
                
                
                ),2),2)) as 'Percentagem'
                from 
                
                 
                (
                select
                  (case when Mensagem = 'JA ENTROU' then 'JA ENTROU < 1 minuto' else Mensagem end) as 'Mensagem'
                  , (case when Mensagem = 'JA ENTROU' then (Quantidade - (
                  
                  
                
                (select count(codigobarras) from 
                (
                select gs_erros.datetime as datahora, gs_erros.Porta as porta, gs_erros.DAC as dac, gs_erros.Titulo as codigobarras, gs_erros.Socio as socio, gs_erros.NSocio as nsocio, gs_erros.Categoria as categoria from 
                
                
                (
                select
                 gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , gestacess_transacoes.titulo as 'Titulo'
                , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                , gestacess_template_inst
                , gestacess_dac
                , gestacess_codigosmensagem
                where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                 and gestacess_template_inst.dac_id=gestacess_dac.id
                 and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                 and gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and gestacess_transacoes.titulo not in (
                
                select gestacess_transacoes.titulo
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime
                ) as gs_erros
                
                , (
                
                select
                      (select
                    code
                      from
                    gestacess_evento
                      where
                    id = '" . $event_id . "') as 'jogo'
                      , cast(min(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac)) as char) as 'datahora'
                      , gestacess_regras_lista_ticket.VA2 as 'porta'
                      , gestacess_dac.code as 'dac'
                      , gestacess_dac.ip as 'ip'
                      , gestacess_transacoes.titulo as 'codigobarras'
                      , gestacess_regras_lista_ticket.VA20 as 'socio'
                      , gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                      , gestacess_regras_lista_ticket.VA5 as 'categoria'
                      , (case
                      when gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                    from
                      (
                    select
                      min(gestacess_transacoes.id) as id
                      , gestacess_transacoes.titulo as 'codigobarras'
                    from
                      gestacess_transacoes
                      left join gestacess_template_inst
                        on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                      left join gestacess_dac
                        on gestacess_template_inst.dac_id = gestacess_dac.id
                      left join gestacess_regras_lista_ticket
                        on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                          and gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                        )
                      left join gestacess_valores_variaveis_acesso
                        on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                          and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                      gestacess_transacoes.entry=1
                      and gestacess_transacoes.apagado = 0
                      and gestacess_transacoes.isEntry  = 1
                      and gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and testmode = 0
                      and areaentryid <> 330
                      and gestacess_transacoes.titulo not in (
                        select
                          titulo
                        from
                          gestacess_regras_lista_ticket
                        where
                          va3 = '91'
                          and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                      )
                    group by
                      gestacess_transacoes.titulo
                      ) as ent
                      left join gestacess_transacoes
                    on ent.id = gestacess_transacoes.id
                      left join gestacess_template_inst
                    on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                      left join gestacess_dac
                    on gestacess_template_inst.dac_id = gestacess_dac.id
                      left join gestacess_regras_lista_ticket
                    on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                      and gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                    )
                      left join gestacess_valores_variaveis_acesso
                    on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                      and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                      gestacess_transacoes.entry=1
                      and gestacess_transacoes.apagado = 0
                      and gestacess_transacoes.isEntry  = 1
                      and gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and testmode = 0
                      and areaentryid <> 330
                      and gestacess_transacoes.titulo not in (
                    select
                      titulo
                    from
                      gestacess_regras_lista_ticket
                    where
                      va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      )
                    group by
                      gestacess_regras_lista_ticket.VA2
                      , gestacess_dac.code
                      , gestacess_dac.ip
                      , gestacess_transacoes.titulo
                      , gestacess_regras_lista_ticket.VA20
                      , gestacess_regras_lista_ticket.VA21
                      , gestacess_valores_variaveis_acesso.descricao
                      , gestacess_regras_lista_ticket.VA5
                      , (case
                    when gestacess_transacoes.tipoleitor = 1
                      then 'CODIGO BARRAS'
                    when gestacess_transacoes.tipoleitor = 2
                      then 'NFC'
                    else ''
                      end) 
                union all 
                      select
                    (select
                      code
                    from
                      gestacess_evento
                    where 
                      id = '" . $event_id . "') as 'jogo'
                    , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'datahora'
                    , gestacess_regras_lista_ticket.VA2 as 'porta'
                    , gestacess_dac.code as 'dac'
                    , gestacess_dac.ip as 'ip'
                    , gestacess_transacoes.titulo as 'codigobarras'
                    , '' as 'socio'
                    , '' as 'nsocio'
                    , 'Livre Transito' as 'tipotitulo'
                    , '' as 'categoria'
                    , (case
                      when gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                      from
                    gestacess_transacoes
                    left join gestacess_template_inst
                      on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                    left join gestacess_dac
                      on gestacess_template_inst.dac_id = gestacess_dac.id
                    left join gestacess_regras_lista_ticket
                      on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        and gestacess_regras_lista_ticket.va1 = (
                          select
                        code
                          from
                        gestacess_evento
                          where
                        id = '" . $event_id . "'
                        )
                      )
                    left join gestacess_valores_variaveis_acesso
                      on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                        and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                      where
                    gestacess_transacoes.entry = 1
                    and gestacess_transacoes.apagado = 0
                    and gestacess_transacoes.isEntry  = 1
                    and gestacess_transacoes.eventID_id = '" . $event_id . "'
                    and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                    and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    and areaentryid <> 330
                    and gestacess_transacoes.titulo in (
                      select
                        titulo
                      from
                        gestacess_regras_lista_ticket
                      where
                        va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      ) 
                    order by
                      datahora 
                
                ) as gs_entradas
                
                where gs_erros.Titulo = gs_entradas.codigobarras
                and (gs_erros.erro = 'JA ENTROU' and timediff(gs_erros.Datetime, gs_entradas.datahora)>'00:00:60')
                
                
                order by codigobarras, datahora
                ) as gs_transacoes_erro_ja_entrou_maior_minuto0
                )  
                  
                
                  
                  
                  
                  )) else Quantidade end) as 'Quantidade'
                
                from 
                
                
                (
                select 
                
                (case when Erro = 'BLACK LIST' then replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Mensagem_Blacklist USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Erro USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') end) as 'Mensagem'
                , count(ID) as 'Quantidade' 
                , format(round((count(ID)*100)/(select count(ID) from (
                
                select * from (
                
                select
                gestacess_transacoes.id as 'ID'
                , gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , gestacess_transacoes.titulo as 'Titulo'
                , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                gestacess_transacoes.id as 'ID'
                , '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                , gestacess_template_inst
                , gestacess_dac
                , gestacess_codigosmensagem
                where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                 and gestacess_template_inst.dac_id=gestacess_dac.id
                 and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                 and gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and gestacess_transacoes.titulo not in (
                
                select gestacess_transacoes.titulo
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime ) as sub3 ) as sub4),2),2) as '%'
                
                from (
                
                
                select * from (
                
                select
                gestacess_transacoes.id as 'ID'
                , gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , gestacess_transacoes.titulo as 'Titulo'
                , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                  and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                gestacess_transacoes.id as 'ID'
                , '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                , gestacess_template_inst
                , gestacess_dac
                , gestacess_codigosmensagem
                where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                 and gestacess_template_inst.dac_id=gestacess_dac.id
                 and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                 and gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and gestacess_transacoes.titulo not in (
                
                select gestacess_transacoes.titulo
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime ) as sub1 ) as sub2
                
                group by (case when Erro = 'BLACK LIST' then replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Mensagem_Blacklist USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Erro USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') end)
                
                order by count(ID) desc
                
                ) as gs_erros_mensagem_agrupado1
                
                
                
                union all
                
                select
                  'JA ENTROU > 1 minuto' as 'Mensagem'
                  , (select count(codigobarras) from 
                
                
                (
                select gs_erros.datetime as datahora, gs_erros.Porta as porta, gs_erros.DAC as dac, gs_erros.Titulo as codigobarras, gs_erros.Socio as socio, gs_erros.NSocio as nsocio, gs_erros.Categoria as categoria from 
                
                
                (
                select
                 gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,  gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,  gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , gestacess_transacoes.titulo as 'Titulo'
                , case when gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as char) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left(gestacess_dac.code,(length(gestacess_dac.code)-5)) as 'Porta'
                , replace(gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                , gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'Datetime'
                , gestacess_dac.code as 'DAC'
                from gestacess_transacoes
                , gestacess_template_inst
                , gestacess_dac
                , gestacess_codigosmensagem
                where gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                 and gestacess_template_inst.dac_id=gestacess_dac.id
                 and gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                 and gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and gestacess_transacoes.titulo not in (
                
                select gestacess_transacoes.titulo
                from gestacess_transacoes
                left join gestacess_import_list on gestacess_transacoes.titulo = gestacess_import_list.titulo
                left join gestacess_regras_lista_ticket on (gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo and gestacess_regras_lista_ticket.evento = (select code from gestacess_evento where id = '" . $event_id . "'))
                left join gestacess_template_inst on gestacess_transacoes.template_inst_id=gestacess_template_inst.id
                left join gestacess_dac on gestacess_template_inst.dac_id=gestacess_dac.id
                left join gestacess_codigosmensagem on gestacess_transacoes.erro_id=gestacess_codigosmensagem.id
                where gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and gestacess_import_list.codEvent = (select code from gestacess_evento where id = '" . $event_id . "')
                 and gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime
                ) as gs_erros
                
                , (
                
                select
                      (select
                    code
                      from
                    gestacess_evento
                      where
                    id = '" . $event_id . "') as 'jogo'
                      , cast(min(timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac)) as char) as 'datahora'
                      , gestacess_regras_lista_ticket.VA2 as 'porta'
                      , gestacess_dac.code as 'dac'
                      , gestacess_dac.ip as 'ip'
                      , gestacess_transacoes.titulo as 'codigobarras'
                      , gestacess_regras_lista_ticket.VA20 as 'socio'
                      , gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                      , gestacess_regras_lista_ticket.VA5 as 'categoria'
                      , (case
                      when gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                    from
                      (
                    select
                      min(gestacess_transacoes.id) as id
                      , gestacess_transacoes.titulo as 'codigobarras'
                    from
                      gestacess_transacoes
                      left join gestacess_template_inst
                        on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                      left join gestacess_dac
                        on gestacess_template_inst.dac_id = gestacess_dac.id
                      left join gestacess_regras_lista_ticket
                        on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                          and gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                        )
                      left join gestacess_valores_variaveis_acesso
                        on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                          and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                      gestacess_transacoes.entry=1
                      and gestacess_transacoes.apagado = 0
                      and gestacess_transacoes.isEntry  = 1
                      and gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and testmode = 0
                      and areaentryid <> 330
                      and gestacess_transacoes.titulo not in (
                        select
                          titulo
                        from
                          gestacess_regras_lista_ticket
                        where
                          va3 = '91'
                          and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                      )
                    group by
                      gestacess_transacoes.titulo
                      ) as ent
                      left join gestacess_transacoes
                    on ent.id = gestacess_transacoes.id
                      left join gestacess_template_inst
                    on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                      left join gestacess_dac
                    on gestacess_template_inst.dac_id = gestacess_dac.id
                      left join gestacess_regras_lista_ticket
                    on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                      and gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                    )
                      left join gestacess_valores_variaveis_acesso
                    on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                      and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                      gestacess_transacoes.entry=1
                      and gestacess_transacoes.apagado = 0
                      and gestacess_transacoes.isEntry  = 1
                      and gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and testmode = 0
                      and areaentryid <> 330
                      and gestacess_transacoes.titulo not in (
                    select
                      titulo
                    from
                      gestacess_regras_lista_ticket
                    where
                      va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      )
                    group by
                      gestacess_regras_lista_ticket.VA2
                      , gestacess_dac.code
                      , gestacess_dac.ip
                      , gestacess_transacoes.titulo
                      , gestacess_regras_lista_ticket.VA20
                      , gestacess_regras_lista_ticket.VA21
                      , gestacess_valores_variaveis_acesso.descricao
                      , gestacess_regras_lista_ticket.VA5
                      , (case
                    when gestacess_transacoes.tipoleitor = 1
                      then 'CODIGO BARRAS'
                    when gestacess_transacoes.tipoleitor = 2
                      then 'NFC'
                    else ''
                      end) 
                union all 
                      select
                    (select
                      code
                    from
                      gestacess_evento
                    where 
                      id = '" . $event_id . "') as 'jogo'
                    , timestamp(gestacess_transacoes.dataTransac,gestacess_transacoes.horaTransac) as 'datahora'
                    , gestacess_regras_lista_ticket.VA2 as 'porta'
                    , gestacess_dac.code as 'dac'
                    , gestacess_dac.ip as 'ip'
                    , gestacess_transacoes.titulo as 'codigobarras'
                    , '' as 'socio'
                    , '' as 'nsocio'
                    , 'Livre Transito' as 'tipotitulo'
                    , '' as 'categoria'
                    , (case
                      when gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                      from
                    gestacess_transacoes
                    left join gestacess_template_inst
                      on gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                    left join gestacess_dac
                      on gestacess_template_inst.dac_id = gestacess_dac.id
                    left join gestacess_regras_lista_ticket
                      on ( gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        and gestacess_regras_lista_ticket.va1 = (
                          select
                        code
                          from
                        gestacess_evento
                          where
                        id = '" . $event_id . "'
                        )
                      )
                    left join gestacess_valores_variaveis_acesso
                      on (gestacess_regras_lista_ticket.va3 = gestacess_valores_variaveis_acesso.valor
                        and gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                      where
                    gestacess_transacoes.entry = 1
                    and gestacess_transacoes.apagado = 0
                    and gestacess_transacoes.isEntry  = 1
                    and gestacess_transacoes.eventID_id = '" . $event_id . "'
                    and gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                    and gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    and areaentryid <> 330
                    and gestacess_transacoes.titulo in (
                      select
                        titulo
                      from
                        gestacess_regras_lista_ticket
                      where
                        va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                          gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      ) 
                    order by
                      datahora 
                
                ) as gs_entradas
                
                where gs_erros.Titulo = gs_entradas.codigobarras
                and (gs_erros.erro = 'JA ENTROU' and timediff(gs_erros.Datetime, gs_entradas.datahora)>'00:00:60')
                
                
                order by codigobarras, datahora
                ) as gs_transacoes_erro_ja_entrou_maior_minuto
                
                ) as 'Quantidade'
                
                order by Quantidade desc
                ) as gs_query_final

        ");

        if ($event_id == 841) {

            foreach ($result as $line) {

                if (empty($line->{'Mensagem'})) {

                    $line->{'Mensagem'} = 'Controlo TCT';

                }

            }

        }

        return response()->json($result, 200);

    }

    public function getUniqueTitleErrorByMessage(Request $request, $event_id) {

        $event_options = $this->getEventOptions($event_id);

        $result = DB::connection('access')->select("
            select 
                  Mensagem
                  , Quantidade
                  , (format(round((Quantidade*100)/(select sum(Quantidade) from (
                
                
                
                select 
                
                  (case when Mensagem = 'JA ENTROU' then 'JA ENTROU < 1 minuto' else Mensagem end) as 'Mensagem'
                  , (case when Mensagem = 'JA ENTROU' then (Quantidade - (select count(titulo) as 'Quantidade'
                  
                from 
                
                (select gs_erros.Socio,gs_erros.NSocio,gs_erros.Categoria,gs_erros.Titulo from 
                
                
                (
                select
                  gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,   gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,   gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                ,  gestacess_transacoes.titulo as 'Titulo'
                , case when  gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as char) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                , replace( gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                ,  gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                ,  gestacess_template_inst
                ,  gestacess_dac
                ,  gestacess_codigosmensagem
                where  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                 and  gestacess_template_inst.dac_id= gestacess_dac.id
                 and  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                 and  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and  gestacess_transacoes.titulo not in (
                
                select  gestacess_transacoes.titulo
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime
                ) as gs_erros
                
                , (
                
                select
                      (select
                    code
                      from
                     gestacess_evento
                      where
                    id = '" . $event_id . "') as 'jogo'
                      , cast(min(timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac)) as char) as 'datahora'
                      ,  gestacess_regras_lista_ticket.VA2 as 'porta'
                      ,  gestacess_dac.code as 'dac'
                      ,  gestacess_dac.ip as 'ip'
                      ,  gestacess_transacoes.titulo as 'codigobarras'
                      ,  gestacess_regras_lista_ticket.VA20 as 'socio'
                      ,  gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                      ,  gestacess_regras_lista_ticket.VA5 as 'categoria'
                      , (case
                      when  gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when  gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                    from
                      (
                    select
                      min( gestacess_transacoes.id) as id
                      ,  gestacess_transacoes.titulo as 'codigobarras'
                    from
                       gestacess_transacoes
                      left join  gestacess_template_inst
                        on  gestacess_transacoes.template_inst_id =  gestacess_template_inst.id
                      left join  gestacess_dac
                        on  gestacess_template_inst.dac_id =  gestacess_dac.id
                      left join  gestacess_regras_lista_ticket
                        on (  gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo
                          and  gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                        )
                      left join  gestacess_valores_variaveis_acesso
                        on ( gestacess_regras_lista_ticket.va3 =  gestacess_valores_variaveis_acesso.valor
                          and  gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                       gestacess_transacoes.entry=1
                      and  gestacess_transacoes.apagado = 0
                      and  gestacess_transacoes.isEntry  = 1
                      and  gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and  gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and  gestacess_transacoes.testmode = 0
                      and  gestacess_transacoes.areaentryid <> 330
                      and  gestacess_transacoes.titulo not in (
                        select
                          titulo
                        from
                           gestacess_regras_lista_ticket
                        where
                          va3 = '91'
                          and va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                      )
                    group by
                       gestacess_transacoes.titulo
                      ) as ent
                      left join  gestacess_transacoes
                    on ent.id =  gestacess_transacoes.id
                      left join  gestacess_template_inst
                    on  gestacess_transacoes.template_inst_id =  gestacess_template_inst.id
                      left join  gestacess_dac
                    on  gestacess_template_inst.dac_id =  gestacess_dac.id
                      left join  gestacess_regras_lista_ticket
                    on (  gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo
                      and  gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                    )
                      left join  gestacess_valores_variaveis_acesso
                    on ( gestacess_regras_lista_ticket.va3 =  gestacess_valores_variaveis_acesso.valor
                      and  gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                       gestacess_transacoes.entry=1
                      and  gestacess_transacoes.apagado = 0
                      and  gestacess_transacoes.isEntry  = 1
                      and  gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and  gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and  gestacess_transacoes.testmode = 0
                      and  gestacess_transacoes.areaentryid <> 330
                      and  gestacess_transacoes.titulo not in (
                    select
                      titulo
                    from
                       gestacess_regras_lista_ticket
                    where
                      va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      )
                    group by
                       gestacess_regras_lista_ticket.VA2
                      ,  gestacess_dac.code
                      ,  gestacess_dac.ip
                      ,  gestacess_transacoes.titulo
                      ,  gestacess_regras_lista_ticket.VA20
                      ,  gestacess_regras_lista_ticket.VA21
                      ,  gestacess_valores_variaveis_acesso.descricao
                      ,  gestacess_regras_lista_ticket.VA5
                      , (case
                    when  gestacess_transacoes.tipoleitor = 1
                      then 'CODIGO BARRAS'
                    when  gestacess_transacoes.tipoleitor = 2
                      then 'NFC'
                    else ''
                      end) 
                union all 
                      select
                    (select
                      code
                    from
                       gestacess_evento
                    where 
                      id = '" . $event_id . "') as 'jogo'
                    , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'datahora'
                    ,  gestacess_regras_lista_ticket.VA2 as 'porta'
                    ,  gestacess_dac.code as 'dac'
                    ,  gestacess_dac.ip as 'ip'
                    ,  gestacess_transacoes.titulo as 'codigobarras'
                    , '' as 'socio'
                    , '' as 'nsocio'
                    , 'Livre Transito' as 'tipotitulo'
                    , '' as 'categoria'
                    , (case
                      when  gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when  gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                      from
                     gestacess_transacoes
                    left join  gestacess_template_inst
                      on  gestacess_transacoes.template_inst_id =  gestacess_template_inst.id
                    left join  gestacess_dac
                      on  gestacess_template_inst.dac_id =  gestacess_dac.id
                    left join  gestacess_regras_lista_ticket
                      on (  gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo
                        and  gestacess_regras_lista_ticket.va1 = (
                          select
                        code
                          from
                         gestacess_evento
                          where
                        id = '" . $event_id . "'
                        )
                      )
                    left join  gestacess_valores_variaveis_acesso
                      on ( gestacess_regras_lista_ticket.va3 =  gestacess_valores_variaveis_acesso.valor
                        and  gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                      where
                     gestacess_transacoes.entry = 1
                    and  gestacess_transacoes.apagado = 0
                    and  gestacess_transacoes.isEntry  = 1
                    and  gestacess_transacoes.eventID_id = '" . $event_id . "'
                    and  gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                    and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    and  gestacess_transacoes.areaentryid <> 330
                    and  gestacess_transacoes.titulo in (
                      select
                        titulo
                      from
                         gestacess_regras_lista_ticket
                      where
                        va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      ) 
                    order by
                      datahora 
                
                ) as gs_entradas
                
                where gs_erros.Titulo = gs_entradas.codigobarras
                and (gs_erros.erro = 'JA ENTROU' and timediff(gs_erros.Datetime, gs_entradas.datahora)>'00:00:60')
                group by gs_erros.Socio,gs_erros.NSocio,gs_erros.Categoria,gs_erros.Titulo ) as gs_ja_entrou_tu1)) else Quantidade end) as 'Quantidade'
                  
                from 
                
                (select 
                
                (case when Erro = 'BLACK LIST' then replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Mensagem_Blacklist USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Erro USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') end) as 'Mensagem'
                , count(Titulo) as 'Quantidade' 
                , format(round((count(Titulo)*100)/(select count(Titulo) from (
                
                select * from (
                
                select
                  distinct Socio
                  , NSocio
                  , Categoria
                  , Titulo
                  , Tipo
                  , Erro
                  , Mensagem_Blacklist
                
                from (
                
                select
                  gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,   gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,   gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                ,  gestacess_transacoes.titulo as 'Titulo'
                , case when  gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , ' ' as 'NSocio'
                , ' ' as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                , replace( gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                ' ' as 'Tipo'
                ,  gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , ' ' as 'Mensagem_Blacklist'
                , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                ,  gestacess_template_inst
                ,  gestacess_dac
                ,  gestacess_codigosmensagem
                where  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                 and  gestacess_template_inst.dac_id= gestacess_dac.id
                 and  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                 and  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and  gestacess_transacoes.titulo not in (
                
                select  gestacess_transacoes.titulo
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime
                
                
                ) as sub33
                 ) as sub44) as sub55),2),2) as '%'
                
                from (
                
                
                select * from (
                
                select
                  distinct Socio
                  , NSocio
                  , Categoria
                  , Titulo
                  , Tipo
                  , Erro
                  , Mensagem_Blacklist
                
                from (
                
                select
                  gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,   gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,   gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                ,  gestacess_transacoes.titulo as 'Titulo'
                , case when  gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , ' ' as 'NSocio'
                , ' ' as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                , replace( gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                ' ' as 'Tipo'
                ,  gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , ' ' as 'Mensagem_Blacklist'
                , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                ,  gestacess_template_inst
                ,  gestacess_dac
                ,  gestacess_codigosmensagem
                where  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                 and  gestacess_template_inst.dac_id= gestacess_dac.id
                 and  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                 and  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and  gestacess_transacoes.titulo not in (
                
                select  gestacess_transacoes.titulo
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime
                
                
                ) as sub11
                 ) as sub22 ) as sub66
                
                group by (case when Erro = 'BLACK LIST' then replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Mensagem_Blacklist USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Erro USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') end)
                
                order by count(Titulo) desc
                ) as gs_erros_agrupados
                
                
                union all
                
                select Mensagem, Quantidade from 
                
                (select
                
                  'JA ENTROU > 1 minuto' as 'Mensagem'
                  , count(titulo) as 'Quantidade'
                  
                from 
                
                (select gs_erros.Socio,gs_erros.NSocio,gs_erros.Categoria,gs_erros.Titulo from 
                
                
                (
                select
                  gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,   gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,   gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                ,  gestacess_transacoes.titulo as 'Titulo'
                , case when  gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as char) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                , replace( gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                ,  gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                ,  gestacess_template_inst
                ,  gestacess_dac
                ,  gestacess_codigosmensagem
                where  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                 and  gestacess_template_inst.dac_id= gestacess_dac.id
                 and  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                 and  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and  gestacess_transacoes.titulo not in (
                
                select  gestacess_transacoes.titulo
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime
                ) as gs_erros
                
                , (
                
                select
                      (select
                    code
                      from
                     gestacess_evento
                      where
                    id = '" . $event_id . "') as 'jogo'
                      , cast(min(timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac)) as char) as 'datahora'
                      ,  gestacess_regras_lista_ticket.VA2 as 'porta'
                      ,  gestacess_dac.code as 'dac'
                      ,  gestacess_dac.ip as 'ip'
                      ,  gestacess_transacoes.titulo as 'codigobarras'
                      ,  gestacess_regras_lista_ticket.VA20 as 'socio'
                      ,  gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                      ,  gestacess_regras_lista_ticket.VA5 as 'categoria'
                      , (case
                      when  gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when  gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                    from
                      (
                    select
                      min( gestacess_transacoes.id) as id
                      ,  gestacess_transacoes.titulo as 'codigobarras'
                    from
                       gestacess_transacoes
                      left join  gestacess_template_inst
                        on  gestacess_transacoes.template_inst_id =  gestacess_template_inst.id
                      left join  gestacess_dac
                        on  gestacess_template_inst.dac_id =  gestacess_dac.id
                      left join  gestacess_regras_lista_ticket
                        on (  gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo
                          and  gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                        )
                      left join  gestacess_valores_variaveis_acesso
                        on ( gestacess_regras_lista_ticket.va3 =  gestacess_valores_variaveis_acesso.valor
                          and  gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                       gestacess_transacoes.entry=1
                      and  gestacess_transacoes.apagado = 0
                      and  gestacess_transacoes.isEntry  = 1
                      and  gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and  gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and  gestacess_transacoes.testmode = 0
                      and  gestacess_transacoes.areaentryid <> 330
                      and  gestacess_transacoes.titulo not in (
                        select
                          titulo
                        from
                           gestacess_regras_lista_ticket
                        where
                          va3 = '91'
                          and va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                      )
                    group by
                       gestacess_transacoes.titulo
                      ) as ent
                      left join  gestacess_transacoes
                    on ent.id =  gestacess_transacoes.id
                      left join  gestacess_template_inst
                    on  gestacess_transacoes.template_inst_id =  gestacess_template_inst.id
                      left join  gestacess_dac
                    on  gestacess_template_inst.dac_id =  gestacess_dac.id
                      left join  gestacess_regras_lista_ticket
                    on (  gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo
                      and  gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                    )
                      left join  gestacess_valores_variaveis_acesso
                    on ( gestacess_regras_lista_ticket.va3 =  gestacess_valores_variaveis_acesso.valor
                      and  gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                       gestacess_transacoes.entry=1
                      and  gestacess_transacoes.apagado = 0
                      and  gestacess_transacoes.isEntry  = 1
                      and  gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and  gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and  gestacess_transacoes.testmode = 0
                      and  gestacess_transacoes.areaentryid <> 330
                      and  gestacess_transacoes.titulo not in (
                    select
                      titulo
                    from
                       gestacess_regras_lista_ticket
                    where
                      va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      )
                    group by
                       gestacess_regras_lista_ticket.VA2
                      ,  gestacess_dac.code
                      ,  gestacess_dac.ip
                      ,  gestacess_transacoes.titulo
                      ,  gestacess_regras_lista_ticket.VA20
                      ,  gestacess_regras_lista_ticket.VA21
                      ,  gestacess_valores_variaveis_acesso.descricao
                      ,  gestacess_regras_lista_ticket.VA5
                      , (case
                    when  gestacess_transacoes.tipoleitor = 1
                      then 'CODIGO BARRAS'
                    when  gestacess_transacoes.tipoleitor = 2
                      then 'NFC'
                    else ''
                      end) 
                union all 
                      select
                    (select
                      code
                    from
                       gestacess_evento
                    where 
                      id = '" . $event_id . "') as 'jogo'
                    , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'datahora'
                    ,  gestacess_regras_lista_ticket.VA2 as 'porta'
                    ,  gestacess_dac.code as 'dac'
                    ,  gestacess_dac.ip as 'ip'
                    ,  gestacess_transacoes.titulo as 'codigobarras'
                    , '' as 'socio'
                    , '' as 'nsocio'
                    , 'Livre Transito' as 'tipotitulo'
                    , '' as 'categoria'
                    , (case
                      when  gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when  gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                      from
                     gestacess_transacoes
                    left join  gestacess_template_inst
                      on  gestacess_transacoes.template_inst_id =  gestacess_template_inst.id
                    left join  gestacess_dac
                      on  gestacess_template_inst.dac_id =  gestacess_dac.id
                    left join  gestacess_regras_lista_ticket
                      on (  gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo
                        and  gestacess_regras_lista_ticket.va1 = (
                          select
                        code
                          from
                         gestacess_evento
                          where
                        id = '" . $event_id . "'
                        )
                      )
                    left join  gestacess_valores_variaveis_acesso
                      on ( gestacess_regras_lista_ticket.va3 =  gestacess_valores_variaveis_acesso.valor
                        and  gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                      where
                     gestacess_transacoes.entry = 1
                    and  gestacess_transacoes.apagado = 0
                    and  gestacess_transacoes.isEntry  = 1
                    and  gestacess_transacoes.eventID_id = '" . $event_id . "'
                    and  gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                    and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    and  gestacess_transacoes.areaentryid <> 330
                    and  gestacess_transacoes.titulo in (
                      select
                        titulo
                      from
                         gestacess_regras_lista_ticket
                      where
                        va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      ) 
                    order by
                      datahora 
                
                ) as gs_entradas
                
                where gs_erros.Titulo = gs_entradas.codigobarras
                and (gs_erros.erro = 'JA ENTROU' and timediff(gs_erros.Datetime, gs_entradas.datahora)>'00:00:60')
                group by gs_erros.Socio,gs_erros.NSocio,gs_erros.Categoria,gs_erros.Titulo ) as gs_ja_entrou_tu) as gs_jaentrou_maior_minuto ) as gs_total),2),2)) as 'Percentagem'
                
                from (
                
                
                select 
                
                  (case when Mensagem = 'JA ENTROU' then 'JA ENTROU < 1 minuto' else Mensagem end) as 'Mensagem'
                  , (case when Mensagem = 'JA ENTROU' then (Quantidade - (select count(titulo) as 'Quantidade'
                  
                from 
                
                (select gs_erros.Socio,gs_erros.NSocio,gs_erros.Categoria,gs_erros.Titulo from 
                
                
                (
                select
                  gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,   gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,   gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                ,  gestacess_transacoes.titulo as 'Titulo'
                , case when  gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as char) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                , replace( gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                ,  gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                ,  gestacess_template_inst
                ,  gestacess_dac
                ,  gestacess_codigosmensagem
                where  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                 and  gestacess_template_inst.dac_id= gestacess_dac.id
                 and  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                 and  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and  gestacess_transacoes.titulo not in (
                
                select  gestacess_transacoes.titulo
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime
                ) as gs_erros
                
                , (
                
                select
                      (select
                    code
                      from
                     gestacess_evento
                      where
                    id = '" . $event_id . "') as 'jogo'
                      , cast(min(timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac)) as char) as 'datahora'
                      ,  gestacess_regras_lista_ticket.VA2 as 'porta'
                      ,  gestacess_dac.code as 'dac'
                      ,  gestacess_dac.ip as 'ip'
                      ,  gestacess_transacoes.titulo as 'codigobarras'
                      ,  gestacess_regras_lista_ticket.VA20 as 'socio'
                      ,  gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                      ,  gestacess_regras_lista_ticket.VA5 as 'categoria'
                      , (case
                      when  gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when  gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                    from
                      (
                    select
                      min( gestacess_transacoes.id) as id
                      ,  gestacess_transacoes.titulo as 'codigobarras'
                    from
                       gestacess_transacoes
                      left join  gestacess_template_inst
                        on  gestacess_transacoes.template_inst_id =  gestacess_template_inst.id
                      left join  gestacess_dac
                        on  gestacess_template_inst.dac_id =  gestacess_dac.id
                      left join  gestacess_regras_lista_ticket
                        on (  gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo
                          and  gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                        )
                      left join  gestacess_valores_variaveis_acesso
                        on ( gestacess_regras_lista_ticket.va3 =  gestacess_valores_variaveis_acesso.valor
                          and  gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                       gestacess_transacoes.entry=1
                      and  gestacess_transacoes.apagado = 0
                      and  gestacess_transacoes.isEntry  = 1
                      and  gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and  gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and  gestacess_transacoes.testmode = 0
                      and  gestacess_transacoes.areaentryid <> 330
                      and  gestacess_transacoes.titulo not in (
                        select
                          titulo
                        from
                           gestacess_regras_lista_ticket
                        where
                          va3 = '91'
                          and va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                      )
                    group by
                       gestacess_transacoes.titulo
                      ) as ent
                      left join  gestacess_transacoes
                    on ent.id =  gestacess_transacoes.id
                      left join  gestacess_template_inst
                    on  gestacess_transacoes.template_inst_id =  gestacess_template_inst.id
                      left join  gestacess_dac
                    on  gestacess_template_inst.dac_id =  gestacess_dac.id
                      left join  gestacess_regras_lista_ticket
                    on (  gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo
                      and  gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                    )
                      left join  gestacess_valores_variaveis_acesso
                    on ( gestacess_regras_lista_ticket.va3 =  gestacess_valores_variaveis_acesso.valor
                      and  gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                       gestacess_transacoes.entry=1
                      and  gestacess_transacoes.apagado = 0
                      and  gestacess_transacoes.isEntry  = 1
                      and  gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and  gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and  gestacess_transacoes.testmode = 0
                      and  gestacess_transacoes.areaentryid <> 330
                      and  gestacess_transacoes.titulo not in (
                    select
                      titulo
                    from
                       gestacess_regras_lista_ticket
                    where
                      va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      )
                    group by
                       gestacess_regras_lista_ticket.VA2
                      ,  gestacess_dac.code
                      ,  gestacess_dac.ip
                      ,  gestacess_transacoes.titulo
                      ,  gestacess_regras_lista_ticket.VA20
                      ,  gestacess_regras_lista_ticket.VA21
                      ,  gestacess_valores_variaveis_acesso.descricao
                      ,  gestacess_regras_lista_ticket.VA5
                      , (case
                    when  gestacess_transacoes.tipoleitor = 1
                      then 'CODIGO BARRAS'
                    when  gestacess_transacoes.tipoleitor = 2
                      then 'NFC'
                    else ''
                      end) 
                union all 
                      select
                    (select
                      code
                    from
                       gestacess_evento
                    where 
                      id = '" . $event_id . "') as 'jogo'
                    , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'datahora'
                    ,  gestacess_regras_lista_ticket.VA2 as 'porta'
                    ,  gestacess_dac.code as 'dac'
                    ,  gestacess_dac.ip as 'ip'
                    ,  gestacess_transacoes.titulo as 'codigobarras'
                    , '' as 'socio'
                    , '' as 'nsocio'
                    , 'Livre Transito' as 'tipotitulo'
                    , '' as 'categoria'
                    , (case
                      when  gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when  gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                      from
                     gestacess_transacoes
                    left join  gestacess_template_inst
                      on  gestacess_transacoes.template_inst_id =  gestacess_template_inst.id
                    left join  gestacess_dac
                      on  gestacess_template_inst.dac_id =  gestacess_dac.id
                    left join  gestacess_regras_lista_ticket
                      on (  gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo
                        and  gestacess_regras_lista_ticket.va1 = (
                          select
                        code
                          from
                         gestacess_evento
                          where
                        id = '" . $event_id . "'
                        )
                      )
                    left join  gestacess_valores_variaveis_acesso
                      on ( gestacess_regras_lista_ticket.va3 =  gestacess_valores_variaveis_acesso.valor
                        and  gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                      where
                     gestacess_transacoes.entry = 1
                    and  gestacess_transacoes.apagado = 0
                    and  gestacess_transacoes.isEntry  = 1
                    and  gestacess_transacoes.eventID_id = '" . $event_id . "'
                    and  gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                    and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    and  gestacess_transacoes.areaentryid <> 330
                    and  gestacess_transacoes.titulo in (
                      select
                        titulo
                      from
                         gestacess_regras_lista_ticket
                      where
                        va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      ) 
                    order by
                      datahora 
                
                ) as gs_entradas
                
                where gs_erros.Titulo = gs_entradas.codigobarras
                and (gs_erros.erro = 'JA ENTROU' and timediff(gs_erros.Datetime, gs_entradas.datahora)>'00:00:60')
                group by gs_erros.Socio,gs_erros.NSocio,gs_erros.Categoria,gs_erros.Titulo ) as gs_ja_entrou_tu1)) else Quantidade end) as 'Quantidade'
                  
                from 
                
                (select 
                
                (case when Erro = 'BLACK LIST' then replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Mensagem_Blacklist USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Erro USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') end) as 'Mensagem'
                , count(Titulo) as 'Quantidade' 
                , format(round((count(Titulo)*100)/(select count(Titulo) from (
                
                select * from (
                
                select
                  distinct Socio
                  , NSocio
                  , Categoria
                  , Titulo
                  , Tipo
                  , Erro
                  , Mensagem_Blacklist
                
                from (
                
                select
                  gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,   gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,   gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                ,  gestacess_transacoes.titulo as 'Titulo'
                , case when  gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , ' ' as 'NSocio'
                , ' ' as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                , replace( gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                ' ' as 'Tipo'
                ,  gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , ' ' as 'Mensagem_Blacklist'
                , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                ,  gestacess_template_inst
                ,  gestacess_dac
                ,  gestacess_codigosmensagem
                where  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                 and  gestacess_template_inst.dac_id= gestacess_dac.id
                 and  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                 and  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and  gestacess_transacoes.titulo not in (
                
                select  gestacess_transacoes.titulo
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime
                
                
                ) as sub33
                 ) as sub44) as sub55),2),2) as '%'
                
                from (
                
                
                select * from (
                
                select
                  distinct Socio
                  , NSocio
                  , Categoria
                  , Titulo
                  , Tipo
                  , Erro
                  , Mensagem_Blacklist
                
                from (
                
                select
                  gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,   gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,   gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                ,  gestacess_transacoes.titulo as 'Titulo'
                , case when  gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , ' ' as 'NSocio'
                , ' ' as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                , replace( gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                ' ' as 'Tipo'
                ,  gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , ' ' as 'Mensagem_Blacklist'
                , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                ,  gestacess_template_inst
                ,  gestacess_dac
                ,  gestacess_codigosmensagem
                where  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                 and  gestacess_template_inst.dac_id= gestacess_dac.id
                 and  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                 and  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and  gestacess_transacoes.titulo not in (
                
                select  gestacess_transacoes.titulo
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime
                
                
                ) as sub11
                 ) as sub22 ) as sub66
                
                group by (case when Erro = 'BLACK LIST' then replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Mensagem_Blacklist USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT(Erro USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') end)
                
                order by count(Titulo) desc
                ) as gs_erros_agrupados
                
                
                union all
                
                select Mensagem, Quantidade from 
                
                (select
                
                  'JA ENTROU > 1 minuto' as 'Mensagem'
                  , count(titulo) as 'Quantidade'
                  
                from 
                
                (select gs_erros.Socio,gs_erros.NSocio,gs_erros.Categoria,gs_erros.Titulo from 
                
                
                (
                select
                  gestacess_regras_lista_ticket.VA20 as 'Socio'
                ,   gestacess_regras_lista_ticket.VA21 as 'NSocio'
                ,   gestacess_regras_lista_ticket.VA5 as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                ,  gestacess_transacoes.titulo as 'Titulo'
                , case when  gestacess_import_list.isWhitelist = 1 then 'WL' else 'BL' end as 'Tipo'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_codigosmensagem.mensagemoperador USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Erro'
                ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_import_list.blmessage USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'Mensagem_Blacklist'
                , cast(timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as char) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                union all
                
                select
                 '' as 'Socio'
                , '' as 'NSocio'
                , '' as 'Categoria'
                , left( gestacess_dac.code,(length( gestacess_dac.code)-5)) as 'Porta'
                , replace( gestacess_transacoes.titulo, '\r', ' ') as 'Titulo',
                '' as 'Tipo'
                ,  gestacess_codigosmensagem.mensagemoperador as 'Erro'
                , '' as 'Mensagem_Blacklist'
                , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'Datetime'
                ,  gestacess_dac.code as 'DAC'
                from  gestacess_transacoes
                ,  gestacess_template_inst
                ,  gestacess_dac
                ,  gestacess_codigosmensagem
                where  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                 and  gestacess_template_inst.dac_id= gestacess_dac.id
                 and  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                 and  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                 and  gestacess_transacoes.titulo not in (
                
                select  gestacess_transacoes.titulo
                from  gestacess_transacoes
                left join  gestacess_import_list on  gestacess_transacoes.titulo =  gestacess_import_list.titulo
                left join  gestacess_regras_lista_ticket on ( gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo and  gestacess_regras_lista_ticket.evento = (select code from  gestacess_evento where id = '" . $event_id . "'))
                left join  gestacess_template_inst on  gestacess_transacoes.template_inst_id= gestacess_template_inst.id
                left join  gestacess_dac on  gestacess_template_inst.dac_id= gestacess_dac.id
                left join  gestacess_codigosmensagem on  gestacess_transacoes.erro_id= gestacess_codigosmensagem.id
                where  gestacess_transacoes.eventID_id= '" . $event_id . "'
                 and  gestacess_import_list.codEvent = (select code from  gestacess_evento where id = '" . $event_id . "')
                 and  gestacess_transacoes.dataTransac= '" . $event_options['dataInicio'] . "'
                 and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 and  gestacess_transacoes.erro_id not in (3,4,8,9,10,11,13,14,15,16,17,18,19,20,23,26)
                
                )
                
                
                order by datetime
                ) as gs_erros
                
                , (
                
                select
                      (select
                    code
                      from
                     gestacess_evento
                      where
                    id = '" . $event_id . "') as 'jogo'
                      , cast(min(timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac)) as char) as 'datahora'
                      ,  gestacess_regras_lista_ticket.VA2 as 'porta'
                      ,  gestacess_dac.code as 'dac'
                      ,  gestacess_dac.ip as 'ip'
                      ,  gestacess_transacoes.titulo as 'codigobarras'
                      ,  gestacess_regras_lista_ticket.VA20 as 'socio'
                      ,  gestacess_regras_lista_ticket.VA21 as 'nsocio'   ,  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(CONVERT(BINARY CONVERT( gestacess_valores_variaveis_acesso.descricao USING utf8) USING latin1),'Ãº','u'),'Ã¡','a'),'Ã¤','a'),'Ã©','e'),'í©','e'),'Ã³','o'),'íº','u'),'Ã±','n'),'í‘','N'),'Ã­','i'),'â€“','-'),'â€™','\''),'â€¦','...'),'â€“','-'),'â€œ',''),'â€',''),'â€˜','\''),'â€¢','-'),'â€¡','c'),'Â',''),'i§','c'),'iµ','o'),'i£','a'),'iª','e'),'Ã‰','E'),'Ã‡','C'),'Ãƒ','A'),'Ãµ','o'),'Ã¢','a'),'Ã£','a'),'Ãª','e'),'Ã§','c'),'Âª','a'),'Âº','o'),'Ã ','a') as 'tipotitulo'
                      ,  gestacess_regras_lista_ticket.VA5 as 'categoria'
                      , (case
                      when  gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when  gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                    from
                      (
                    select
                      min( gestacess_transacoes.id) as id
                      ,  gestacess_transacoes.titulo as 'codigobarras'
                    from
                       gestacess_transacoes
                      left join  gestacess_template_inst
                        on  gestacess_transacoes.template_inst_id =  gestacess_template_inst.id
                      left join  gestacess_dac
                        on  gestacess_template_inst.dac_id =  gestacess_dac.id
                      left join  gestacess_regras_lista_ticket
                        on (  gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo
                          and  gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                        )
                      left join  gestacess_valores_variaveis_acesso
                        on ( gestacess_regras_lista_ticket.va3 =  gestacess_valores_variaveis_acesso.valor
                          and  gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                       gestacess_transacoes.entry=1
                      and  gestacess_transacoes.apagado = 0
                      and  gestacess_transacoes.isEntry  = 1
                      and  gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and  gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and  gestacess_transacoes.testmode = 0
                      and  gestacess_transacoes.areaentryid <> 330
                      and  gestacess_transacoes.titulo not in (
                        select
                          titulo
                        from
                           gestacess_regras_lista_ticket
                        where
                          va3 = '91'
                          and va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                          )
                      )
                    group by
                       gestacess_transacoes.titulo
                      ) as ent
                      left join  gestacess_transacoes
                    on ent.id =  gestacess_transacoes.id
                      left join  gestacess_template_inst
                    on  gestacess_transacoes.template_inst_id =  gestacess_template_inst.id
                      left join  gestacess_dac
                    on  gestacess_template_inst.dac_id =  gestacess_dac.id
                      left join  gestacess_regras_lista_ticket
                    on (  gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo
                      and  gestacess_regras_lista_ticket.va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                    )
                      left join  gestacess_valores_variaveis_acesso
                    on ( gestacess_regras_lista_ticket.va3 =  gestacess_valores_variaveis_acesso.valor
                      and  gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                    where
                       gestacess_transacoes.entry=1
                      and  gestacess_transacoes.apagado = 0
                      and  gestacess_transacoes.isEntry  = 1
                      and  gestacess_transacoes.eventID_id = '" . $event_id . "'
                      and  gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                      and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                      and  gestacess_transacoes.testmode = 0
                      and  gestacess_transacoes.areaentryid <> 330
                      and  gestacess_transacoes.titulo not in (
                    select
                      titulo
                    from
                       gestacess_regras_lista_ticket
                    where
                      va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      )
                    group by
                       gestacess_regras_lista_ticket.VA2
                      ,  gestacess_dac.code
                      ,  gestacess_dac.ip
                      ,  gestacess_transacoes.titulo
                      ,  gestacess_regras_lista_ticket.VA20
                      ,  gestacess_regras_lista_ticket.VA21
                      ,  gestacess_valores_variaveis_acesso.descricao
                      ,  gestacess_regras_lista_ticket.VA5
                      , (case
                    when  gestacess_transacoes.tipoleitor = 1
                      then 'CODIGO BARRAS'
                    when  gestacess_transacoes.tipoleitor = 2
                      then 'NFC'
                    else ''
                      end) 
                union all 
                      select
                    (select
                      code
                    from
                       gestacess_evento
                    where 
                      id = '" . $event_id . "') as 'jogo'
                    , timestamp( gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) as 'datahora'
                    ,  gestacess_regras_lista_ticket.VA2 as 'porta'
                    ,  gestacess_dac.code as 'dac'
                    ,  gestacess_dac.ip as 'ip'
                    ,  gestacess_transacoes.titulo as 'codigobarras'
                    , '' as 'socio'
                    , '' as 'nsocio'
                    , 'Livre Transito' as 'tipotitulo'
                    , '' as 'categoria'
                    , (case
                      when  gestacess_transacoes.tipoleitor = 1
                        then 'CODIGO BARRAS'
                      when  gestacess_transacoes.tipoleitor = 2
                        then 'NFC'
                      else ''
                    end) as 'tipoleitor'
                      from
                     gestacess_transacoes
                    left join  gestacess_template_inst
                      on  gestacess_transacoes.template_inst_id =  gestacess_template_inst.id
                    left join  gestacess_dac
                      on  gestacess_template_inst.dac_id =  gestacess_dac.id
                    left join  gestacess_regras_lista_ticket
                      on (  gestacess_transacoes.titulo =  gestacess_regras_lista_ticket.titulo
                        and  gestacess_regras_lista_ticket.va1 = (
                          select
                        code
                          from
                         gestacess_evento
                          where
                        id = '" . $event_id . "'
                        )
                      )
                    left join  gestacess_valores_variaveis_acesso
                      on ( gestacess_regras_lista_ticket.va3 =  gestacess_valores_variaveis_acesso.valor
                        and  gestacess_valores_variaveis_acesso.variavelacesso_id = 3)
                      where
                     gestacess_transacoes.entry = 1
                    and  gestacess_transacoes.apagado = 0
                    and  gestacess_transacoes.isEntry  = 1
                    and  gestacess_transacoes.eventID_id = '" . $event_id . "'
                    and  gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "'
                    and  gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    and  gestacess_transacoes.areaentryid <> 330
                    and  gestacess_transacoes.titulo in (
                      select
                        titulo
                      from
                         gestacess_regras_lista_ticket
                      where
                        va3 = '91'
                      and va1 = (
                        select
                          code
                        from
                           gestacess_evento
                        where
                          id = '" . $event_id . "'
                      )
                      ) 
                    order by
                      datahora 
                
                ) as gs_entradas
                
                where gs_erros.Titulo = gs_entradas.codigobarras
                and (gs_erros.erro = 'JA ENTROU' and timediff(gs_erros.Datetime, gs_entradas.datahora)>'00:00:60')
                group by gs_erros.Socio,gs_erros.NSocio,gs_erros.Categoria,gs_erros.Titulo ) as gs_ja_entrou_tu) as gs_jaentrou_maior_minuto ) as query_final
                
                order by Quantidade desc
        ");

        if ($event_id == 841) {

            foreach ($result as $line) {

                if (empty($line->{'Mensagem'})) {

                    $line->{'Mensagem'} = 'Controlo TCT';

                }

            }

        }

        return response()->json($result, 200);

    }

    public function getEntriesByCamWithExtra(Request $request, $event_id)
    {

        try {

            $event_options = $this->getEventOptions($event_id);

            $entries_cam = DB::connection('access')->select("
                SELECT
                    rules.VA24 AS 'cam',
                    rules.VA26 AS 'entity',
                    COUNT(DISTINCT(transactions.titulo)) AS 'total',
                    0 AS 'extra_child',
                    0 AS 'extra_adult'
                FROM
                    `gestacess_regras_lista_ticket` AS rules
                    LEFT JOIN
                        `gestacess_transacoes` AS transactions 
                        ON (
                            transactions.titulo = rules.titulo  
                        )
                     LEFT JOIN
                        `gestacess_evento` AS events
                        ON (
                            events.id = transactions.eventId_id
                        )
                WHERE
                    events.id = '" . (int)$event_id . "'
                    AND
                    rules.VA8 LIKE '%Cam%'
                    AND
                    rules.VA24 < 90
                    AND
                    rules.VA3 <> 91
                    AND
                    transactions.dataTransac = '" . $event_options['dataInicio'] . "'
                    AND
                    transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    transactions.isEntry = 1
                    AND
                    transactions.entry = 1
                    AND
                    transactions.apagado = 0
                    AND
                    transactions.testMode = 0
                    AND
                    transactions.areaEntryId <> 330
                    AND
                    rules.VA26 IS NOT NULL
                GROUP BY
                    rules.VA24,
                    rules.VA26
                ORDER BY
                    LENGTH(rules.VA24) , rules.VA24
            ");

            $entries_cam_extra = DB::connection('access')->select("
                SELECT
                    *
                FROM
                    gestacess_transacoes
                WHERE
                    eventId_id = '" . (int)$event_id . "'
                    AND
                    vas LIKE '%TCPVA5%'
            ");

            foreach ($entries_cam_extra as $entry_extra) {

                preg_match('/;TCPVA5:(\d{1,2});VA24:(\d{1,2})/', $entry_extra->vas, $result);

                if (isset($result[1], $result[2])) {

                    $exists = $this->checkBoxExists($entries_cam, $result[2]);

                    if ($exists > -1) {

                        if ($result[1] == 24) {

                            $entries_cam[$exists]->extra_child = $entries_cam[$exists]->extra_child + 1;

                        }

                        if ($result[1] == 21) {

                            $entries_cam[$exists]->extra_adult = $entries_cam[$exists]->extra_adult + 1;

                        }

                    }

                }

            }

            return response()->json($entries_cam, 200);

        } catch (\Exception $e) {

            Log::error('Get Entries By Cam');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

    public function getBoxResume(Request $request, $event_id)
    {

        try {

            $event_options = $this->getEventOptions($event_id);

            $entries_cam = DB::connection('access')->select("
                SELECT
                    rules.VA24 AS 'cam',
                    rules.VA26 AS 'entity',
                    COUNT(DISTINCT(transactions.titulo)) AS 'total'
                FROM
                    `gestacess_regras_lista_ticket` AS rules
                    LEFT JOIN
                        `gestacess_transacoes` AS transactions 
                        ON (
                            transactions.titulo = rules.titulo  
                        )
                     LEFT JOIN
                        `gestacess_evento` AS events
                        ON (
                            events.id = transactions.eventId_id
                        )
                WHERE
                    events.id = '" . (int)$event_id . "'
                    AND
                    rules.VA8 LIKE '%Cam%'
                    AND
                    rules.VA24 < 90
                    AND
                    rules.VA3 <> 91
                    AND
                    transactions.dataTransac = '" . $event_options['dataInicio'] . "'
                    AND
                    transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    transactions.isEntry = 1
                    AND
                    transactions.entry = 1
                    AND
                    transactions.apagado = 0
                    AND
                    transactions.testMode = 0
                    AND
                    transactions.areaEntryId <> 330
                    AND
                    rules.VA26 IS NOT NULL
                GROUP BY
                    rules.VA24,
                    rules.VA26
                ORDER BY
                    LENGTH(rules.VA24) , rules.VA24
            ");

            return response()->json($entries_cam, 200);

        } catch (\Exception $e) {

            Log::error('Get Entries By Cam');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

}