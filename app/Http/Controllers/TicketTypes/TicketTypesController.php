<?php

namespace App\Http\Controllers\TicketTypes;

use App\Http\Controllers\Controller;
use http\Env\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use App\Models\TicketTypes;

class TicketTypesController extends Controller {

    /**
     * Create a new controller instance
     * 
     * @return void
     */

     public function __construct() {
         $this->middleware('auth');
     }

     public function getTicketTypes(Request $request) {

        try {

            /**
             * 
             * @return TicketTypesList
             * 
             */

            $ticket_types = TicketTypes::all();
        
            return view('tickettypes.show-tickettypes', compact('ticket_types'));

        } catch (\Exception $e) {

            Log::error('Ticket Types Request Error');
            Log::error($e);

            return view('tickettypes.show-tickettypes', compact(['error' => 'unavailable']));

        }

     }

     public function show(Request $request, $tickettype_id) {

         //Log::info('... Should do something right');

        try {

            $ticket_type = TicketTypes::where('id', '=', $tickettype_id)
                                    ->get();

            $tickettype = $ticket_type[0];

            //return response()->json($ticket_type, 200);

            return view('tickettypes.view-tickettypes', compact('tickettype'));

        } catch (\Exception $e) {

            Log::error('Ticket Type View Request Error');
            Log::error($e);

            return view('tickettypes.view-tickettpes', compact(['error' => 'unavailable']));

        }

     }

     public function create(Request $request) {

         $data = [
             'action'            => 'add'
         ];

         return view('tickettypes.edit-tickettype')->with($data);

     }

     public function add(Request $request) {

         try {

             $this->validate($request, [
                 'description'      => 'required',
                 'available'        => 'required',
                 'active'           => 'required',
                 'value'            => 'required',
                 'external_id'      => 'required'
             ]);

             $description = $request['description'];
             $available = ($request['available'] == 'true') ? 1 : 0;
             $active = ($request['active'] == 'true') ? 1 : 0;
             $value = $request['value'];
             $external_id = $request['external_id'];

             $set_external_id = TicketTypes::where('external_id', $external_id)->get();
             $set_value = TicketTypes::where('value', $value)->get();

             if (count($set_external_id) > 0 && count($set_value) > 0) {

                 return response()->json(['error' => array('external_id', 'value')], 200);

             } else if (count($set_external_id) > 0) {

                 return response()->json(['error' => array('external_id')], 200);

             } else if (count($set_value) > 0) {

                 return response()->json(['error' => array('value')], 200);

             }

             $tickettype = new TicketTypes;

             $tickettype->description = $description;
             $tickettype->available_filters = $available;
             $tickettype->active = $active;
             $tickettype->value = $value;
             $tickettype->external_id = $external_id;
             $tickettype->deleted = 0;

             $tickettype->save();

             return response()->json(['success' => 'Created'], 200);

         } catch (ModelNotFoundException $exception) {

             return view('pages.status')
                 ->with('error', trans('cantAccessPage'))
                 ->with('error', trans('cantAccessPage'));

         }

     }

     public function edit(Request $request, $tickettype_id) {

        try {

            $tickettype = $this->getTicketTypeById($tickettype_id);

        } catch (ModelNotFoundException $exception) {

            return view('pages.status')
                    ->with('error', trans('cantAccessPage'))
                    ->with('error', trans('cantAccessPage'));

        }

        $data = [
            'tickettype'        => $tickettype,
            'action'            => 'update'
        ];

        return view('tickettypes.edit-tickettype')->with($data);

     }

     public function delete(Request $request, $tickettype_id) {

         try {

             $tickettype = DB::delete('DELETE FROM ticket_types WHERE id = "' . $tickettype_id . '"');

             return response()->json(['success' => 'Deleted'], 200);

         } catch (ModelNotFoundException $exception) {

             return view('pages.status')
                 ->with('error', trans('cantAccessPage'))
                 ->with('error', trans('cantAccessPage'));

         }

     }

     public function update(Request $request, $tickettype_id) {

         try {

             $this->validate($request, [
                 'description'  => 'required',
                 'available'  => 'required',
                 'active'  => 'required'
             ]);

             $description = $request['description'];
             $available = ($request['available'] == 'true') ? 1 : 0;
             $active = ($request['active'] == 'true') ? 1 : 0;

             $tickettype = TicketTypes::find($tickettype_id);

             $tickettype->description = $description;
             $tickettype->available_filters = $available;
             $tickettype->active = $active;
             $tickettype->updated_at = date("Y-m-d H:i:s");

             $tickettype->save();

             return response()->json(['success' => 'Updated'], 200);

         } catch (ModelNotFoundException $exception) {

             return view('pages.status')
                 ->with('error', trans('cantAccessPage'))
                 ->with('error', trans('cantAccessPage'));

         }

     }

     public function getTicketTypeById($tickettype_id) {

        return TicketTypes::find($tickettype_id);
        
     }

     public function getActiveTicketTypes() {

        try {

            $ticket_types = TicketTypes::where('active', '=', '1')
                                    ->where('available_filters', '=', '1')
                                    ->get();

            return response()->json($ticket_types, 200);

        } catch(\Exception $e) {

            Log::error('Ticket Types Active Request Error');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

     }

}