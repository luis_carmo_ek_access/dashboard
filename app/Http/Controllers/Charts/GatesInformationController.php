<?php

namespace App\Http\Controllers\Charts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use App\Flowrate;

class GatesInformationController extends Controller {

    /**
     *
     * Create a new controller instance.
     *
     * @return void
     *
     */

    public function __construct()
    {

        $this->middleware('auth');

    }

    public function getGatesInformation(Request $request, $event_id) {

        try {

            /**
             * @return GatesInformation
             */

            $options = DB::connection('access')->select("
                SELECT
                    code,
                    dataInicio,
                    dataFim,
                    horaAberturaPortas
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
            ");

            $event_options = (sizeof($options) > 0) ? (array) $options[0] : array();

            $entries = DB::connection('access')->select("
                SELECT
                    temp_gate.id AS 'gate_id',
                    temp_gate.descricao AS 'gate_desc',
                    COUNT(DISTINCT transactions.titulo) AS 'entries'
                FROM
                    gestacess_template_inst AS temp_dac
                LEFT JOIN gestacess_template_inst AS temp_gate
                ON
                    (
                        temp_gate.id = temp_dac.parent_id AND temp_gate.isPorta = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_block
                ON
                    (
                        temp_block.id = temp_gate.parent_id AND temp_block.isZona = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_template
                ON
                    (
                        temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                    )
                LEFT JOIN gestacess_template AS template
                ON
                    (
                        template.id = temp_template.template_id
                    )
                LEFT JOIN gestacess_transacoes AS transactions
                ON
                    (
                        transactions.template_inst_id = temp_dac.id
                        AND
                        transactions.entry = 1
                        AND
                        transactions.isEntry = 1
                        AND
                        transactions.apagado = 0
                        AND
                        transactions.testMode = 0
                        AND
                        transactions.areaEntryId <> 330
                    )
                LEFT JOIN gestacess_evento ON (
                            transactions.eventId_id = gestacess_evento.id
                        )
                LEFT JOIN gestacess_regras_lista_ticket ON (
                            transactions.titulo = gestacess_regras_lista_ticket.titulo
                            AND
                            gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                        )
               	LEFT JOIN gestacess_valores_variaveis_acesso ON (
                            gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                            AND
                            gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                        )
                WHERE
                    transactions.eventId_id = '" . (int)$event_id . "'
                    AND
                    transactions.dataTransac >= '" . $event_options['dataInicio'] . "' 
                    AND
                    transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_valores_variaveis_acesso.id != '10'
                GROUP BY
                    gate_id,
                    gate_desc
                ORDER BY
                    gate_desc
                ASC
            ");

            $entries_master = DB::connection('access')->select("
                SELECT
                    temp_gate.id AS 'gate_id',
                    temp_gate.descricao AS 'gate_desc',
                    COUNT(transactions.titulo) AS 'entries'
                FROM
                    gestacess_template_inst AS temp_dac
                LEFT JOIN gestacess_template_inst AS temp_gate
                ON
                    (
                        temp_gate.id = temp_dac.parent_id AND temp_gate.isPorta = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_block
                ON
                    (
                        temp_block.id = temp_gate.parent_id AND temp_block.isZona = 1
                    )
                LEFT JOIN gestacess_template_inst AS temp_template
                ON
                    (
                        temp_template.id = temp_block.parent_id AND temp_template.isTemplate = 1
                    )
                LEFT JOIN gestacess_template AS template
                ON
                    (
                        template.id = temp_template.template_id
                    )
                LEFT JOIN gestacess_transacoes AS transactions
                ON
                    (
                        transactions.template_inst_id = temp_dac.id
                        AND
                        transactions.entry = 1
                        AND
                        transactions.isEntry = 1
                        AND
                        transactions.apagado = 0
                        AND
                        transactions.testMode = 0
                        AND
                        transactions.areaEntryId <> 330
                    )
                LEFT JOIN gestacess_evento ON (
                            transactions.eventId_id = gestacess_evento.id
                        )
                LEFT JOIN gestacess_regras_lista_ticket ON (
                            transactions.titulo = gestacess_regras_lista_ticket.titulo
                            AND
                            gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                        )
               	LEFT JOIN gestacess_valores_variaveis_acesso ON (
                            gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                            AND
                            gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                        )
                WHERE
                    transactions.eventId_id = '" . (int)$event_id . "'
                    AND
                    transactions.dataTransac >= '" . $event_options['dataInicio'] . "' 
                    AND
                    transactions.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_valores_variaveis_acesso.id = '10'
                GROUP BY
                    gate_id,
                    gate_desc
                ORDER BY
                    gate_desc
                ASC
            ");

            $entries_extra = DB::connection('access')->select("
                SELECT
                    IF(template_inst.descricao = 'Tribuna Sócios - Nascente', '15', '1') AS gate_id,
                    IF(template_inst.descricao = 'Tribuna Sócios - Nascente', 'Porta 15', 'Porta 1') AS gate_desc,
                    COUNT(transactions.titulo) AS 'entries'
                FROM
                    gestacess_transacoes AS transactions
                    LEFT JOIN
                        gestacess_template_inst AS template_inst
                        ON (
                            template_inst.id = transactions.areaEntryId
                        )
                WHERE
                    transactions.eventId_id = '" . (int)$event_id . "'
                    AND
                    transactions.vas LIKE '%TCPVA5%'
                GROUP BY
                    gate_id,
                    gate_desc
            ");

            $imported_tickets = DB::connection('access')->select("
                SELECT
                    regras_lista_ticket.VA2 AS access_variable,
                    valores_variaveis_acesso.descricao AS description,
                    COUNT(import_list.titulo) AS imported
                FROM
                    gestacess_import_list AS import_list
                LEFT JOIN gestacess_regras_lista_ticket AS regras_lista_ticket
                ON
                    (
                        regras_lista_ticket.titulo = import_list.titulo
                    )
                LEFT JOIN gestacess_valores_variaveis_acesso AS valores_variaveis_acesso
                ON
                    (
                        valores_variaveis_acesso.valor = regras_lista_ticket.VA2 AND valores_variaveis_acesso.variavelAcesso_id = 2
                    )
                WHERE
                    import_list.codEvent = '" . $event_options["code"] . "'
                    AND
                    regras_lista_ticket.evento = import_list.codEvent
                    AND
                    regras_lista_ticket.VA1 = import_list.codEvent
                    AND
                    import_list.isWhiteList = 1
                    AND
                    regras_lista_ticket.VA2 <> '50'
                GROUP BY
                    regras_lista_ticket.VA2,
                    valores_variaveis_acesso.descricao
            ");

            $cleaned_import = array();

            $special_gates_14_16 = array(
                'access_variable'   => '',
                'description'       => 'Porta 14 e 16',
                'imported'          => 0
            );

            $special_gates_2_28 = array(
                'access_variable'   => '',
                'description'       => 'Porta 2 e 28',
                'imported'          => 0
            );

            $special_gates_19_20 = array(
                'access_variable'   => '',
                'description'       => 'Porta 19 e 20',
                'imported'          => 0
            );

            foreach ($imported_tickets as $imported) {

                $array = array(
                    'access_variable'   => $imported->access_variable,
                    'description'       => '',
                    'imported'          => $imported->imported
                );

                if (strpos($imported->description, 'Dragao - ') !== false) {

                    $splited = explode( 'Dragao - ', $imported->description );

                    if (count($splited) > 0) {

                        switch ($splited[1]) {
                            case 'Cam. Presidencial':
                                $array['description'] = 'Camarote Presidencial';
                                break;
                            case 'Porta 8 PMR':
                                $array['description'] = 'Porta 8 - PMR';
                                break;
                            case 'Porta 22 PMR':
                                $array['description'] = 'Porta 22 - PMR';
                                break;
                            case 'Porta 14':
                                $special_gates_14_16['imported'] = $special_gates_14_16['imported'] + $imported->imported;
                                break;
                            case 'Porta 16':
                                $special_gates_14_16['imported'] = $special_gates_14_16['imported'] + $imported->imported;
                                break;
                            case 'Porta 2':
                                $special_gates_2_28['imported'] = $special_gates_2_28['imported'] + $imported->imported;
                                break;
                            case 'Porta 28':
                                $special_gates_2_28['imported'] = $special_gates_2_28['imported'] + $imported->imported;
                                break;
                            case 'Porta 19':
                                $special_gates_19_20['imported'] = $special_gates_19_20['imported'] + $imported->imported;
                                break;
                            case 'Porta 20':
                                $special_gates_19_20['imported'] = $special_gates_19_20['imported'] + $imported->imported;
                                break;
                            default:
                                $array['description'] = $splited[1];
                                break;
                        }

                    } else {

                        $array['description'] = $splited;

                    }

                } else {

                    $array['description'] = $imported->description;

                }

                array_push($cleaned_import, $array);

            }

            array_push($cleaned_import, $special_gates_14_16);
            array_push($cleaned_import, $special_gates_2_28);
            array_push($cleaned_import, $special_gates_19_20);

            foreach ($entries as $entry) {

                $imported_index = array_search($entry->gate_desc, array_column($cleaned_import, 'description'));

                if (!$imported_index > -1) {

                    $cleaned_import[] = array(
                        'access_variable'   => '',
                        'description'       => $entry->gate_desc,
                        'imported'          => 0,
                        'entries'           => $entry->entries
                    );

                } else {

                    $cleaned_import[$imported_index]['entries'] = $entry->entries;

                }

            }

            foreach ($entries_master as $entry) {

                $imported_index = array_search($entry->gate_desc, array_column($cleaned_import, 'description'));

                if (!$imported_index > -1) {

                    $cleaned_import[] = array(
                        'access_variable'   => '',
                        'description'       => $entry->gate_desc,
                        'imported'          => 0,
                        'entries'           => $entry->entries
                    );

                } else {

                    $cleaned_import[$imported_index]['entries'] = $cleaned_import[$imported_index]['entries'] + $entry->entries;

                }

            }

            foreach ($entries_extra as $entry) {

                $imported_index = array_search($entry->gate_desc, array_column($cleaned_import, 'description'));

                if (!$imported_index > -1) {

                    $cleaned_import[] = array(
                        'access_variable'   => '',
                        'description'       => $entry->gate_desc,
                        'imported'          => 0,
                        'entries'           => $entry->entries
                    );

                } else {

                    $cleaned_import[$imported_index]['entries'] = $cleaned_import[$imported_index]['entries'] + $entry->entries;

                }

            }

            $gates_desc = array();

            foreach ($cleaned_import as $key => $value) {

                $gates_desc[$key] = $value["description"];

            }

            array_multisort($gates_desc, SORT_NATURAL, $cleaned_import);

            return response()->json($cleaned_import, 200);

        } catch (\Exception $e) {

            Log::error('Gates Information Error');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

}