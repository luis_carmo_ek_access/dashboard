<?php

namespace App\Http\Controllers\Charts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use App\FillLevel;

class FillLevelController extends Controller {

    /**
     * Create a new controller instance.
     * 
     * @return void
     */

     public function __construct() {
         $this->middleware('auth');
     }

     public function getFillLevel(Request $request, $event_id) {

        try {

            /**
             * 
             * @return filllevel
             * 
             */

            $options = DB::connection('access')->select("
                SELECT
                    dataInicio,
                    dataFim,
                    horaAberturaPortas
                FROM
                    gestacess_evento
                WHERE
                    id = " . (int)$event_id . "
            ");

            $event_options = (array) $options[0];

            $fillLevel_value = DB::connection('access')->select("
                SELECT
                    COUNT(DISTINCT gestacess_transacoes.titulo) AS 'total'
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                    LEFT JOIN gestacess_regras_lista_ticket ON (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
                    LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
                WHERE
                    gestacess_evento.id = '" . (int)$event_id . "'
                    AND
                    gestacess_evento.code = gestacess_regras_lista_ticket.evento
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $event_options['dataFim'] . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                    AND
                    gestacess_valores_variaveis_acesso.id != '10'
            ");

            $fillLevel_value_master = DB::connection('access')->select("
                SELECT
                    COUNT(gestacess_transacoes.titulo) AS 'total'
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                    LEFT JOIN gestacess_regras_lista_ticket ON (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
                    LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
                WHERE
                    gestacess_evento.id = '" . (int)$event_id . "'
                    AND
                    gestacess_evento.code = gestacess_regras_lista_ticket.evento
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $event_options['dataFim'] . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                    AND
                    gestacess_valores_variaveis_acesso.id = '10'
            ");

            $filllevel_extra = DB::connection('access')->select("
                    SELECT
                        'Entradas Extra TCP' AS 'tickettype',
                        COUNT(id) AS 'total'
                    FROM
                        `gestacess_transacoes`
                    WHERE
                        `eventId_id` = '" . (int)$event_id ."' AND `vas` LIKE '%TCPVA5%'
                ");

            $total = array('total' => $fillLevel_value[0]->total + $fillLevel_value_master[0]->total + $filllevel_extra[0]->total);

            return response()->json($total, 200);

        } catch (\Exception $e) {

            Log::error('Fill Level Request Error');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

     }

     public function getFillLevelFiltered(Request $request, $event_id, $filter_type) {

        $tickets_filter = null;
        $hours_filter = array();
        $filter = '';
        $search_type = '';

        $selection = '';
        $group_by = '';
        $order_by = '';

        $need_master_query = 0;
        $need_tcp_query = 0;

        if ($filter_type == 'tickettype') {

            $this->validate($request, [
                'ticket_types' => 'required'
            ]);
    
            $tickets_filter = $request['ticket_types'];

            $has_ticket = explode(',', $tickets_filter);

            if (in_array('10', $has_ticket)) {

                unset($has_ticket[array_search('10', $has_ticket)]);

                $tickets_filter = implode(',', $has_ticket);

                $need_master_query = 1;

            }

            if (in_array('999999999', $has_ticket)) {

                unset($has_ticket[array_search('999999999', $has_ticket)]);

                $tickets_filter = implode(',', $has_ticket);

                $need_tcp_query = 1;

            }

            $selection = "
                gestacess_valores_variaveis_acesso.descricao AS 'tickettype',
                COUNT(DISTINCT gestacess_transacoes.titulo) AS 'entries'
            ";

            $group_by = "
                GROUP BY
                    gestacess_valores_variaveis_acesso.descricao
            ";

            $order_by = "
                ORDER BY
                    gestacess_valores_variaveis_acesso.descricao
            ";

        } else if ($filter_type == 'hour') {

            $this->validate($request, [
                'hours'         => 'required',
                'type_search'   => 'required'
            ]);

            $hours_filter = explode(',', $request['hours']);
            $search_type = $request['type_search'];

            if ($search_type == 'tickettype') {

                $need_master_query = 1;
                $need_tcp_query = 1;

                $selection = "
                    gestacess_valores_variaveis_acesso.descricao AS 'tickettype',
                    COUNT(DISTINCT gestacess_transacoes.titulo) AS 'entries'
                ";

                $group_by = "
                    GROUP BY
                        gestacess_valores_variaveis_acesso.descricao
                ";

                $order_by = "
                    ORDER BY
                        gestacess_valores_variaveis_acesso.descricao
                ";

            } else if ($search_type == 'default') {

                $selection = "
                    COUNT(DISTINCT gestacess_transacoes.titulo) AS 'total'
                ";

            }

        }

        
        if (strlen($tickets_filter) > 0) {

            $filters = 'AND gestacess_valores_variaveis_acesso.id IN (' . $tickets_filter . ')';

        } else {

            $filters = '';

        }

        try {

            $options = DB::connection('access')->select("
                SELECT
                    dataInicio,
                    dataFim,
                    horaAberturaPortas
                FROM
                    gestacess_evento
                WHERE
                    id = " . (int)$event_id . "
            ");

            $event_options = (array) $options[0];

            $day_now   = date("Y-m-d");

            if (strlen($tickets_filter) > 0 && $tickets_filter !== null) {

                $filters = "
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $day_now . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_valores_variaveis_acesso.id IN (" . $tickets_filter . ")
                ";
    
            } else if (count($hours_filter) > 0 && $hours_filter !== null) {
    
                $filters = "
                    AND
                    TIMESTAMP(gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) BETWEEN '" . $hours_filter[0] . "' AND '" . $hours_filter[1] . "'
                ";
    
            } else {
    
                $filters = "
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $day_now . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                ";
    
            }

            $filllevel = DB::connection('access')->select("
                SELECT
                    " . $selection . "
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                    LEFT JOIN gestacess_regras_lista_ticket ON (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
                    LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
                WHERE
                    gestacess_evento.id = '" . (int)$event_id . "'
                    AND
                    gestacess_evento.code = gestacess_regras_lista_ticket.evento
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                    " . $filters . "
                    " . $group_by . "
                    " . $order_by . "
            ");

            $filllevel_master = array();

            $filllevel_extra = array();

            if ($need_master_query == 1 ) {

                $selection = "
                    gestacess_valores_variaveis_acesso.descricao AS 'tickettype',
                    COUNT(gestacess_transacoes.titulo) AS 'entries'
                ";

                $filters = "
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $day_now . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_valores_variaveis_acesso.id IN (10)
                ";

                $filllevel_master = DB::connection('access')->select("
                    SELECT
                        " . $selection . "
                    FROM
                        gestacess_transacoes
                        LEFT JOIN gestacess_evento ON (
                            gestacess_transacoes.eventId_id = gestacess_evento.id
                        )
                        LEFT JOIN gestacess_template_inst ON (
                            gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                            AND
                            gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                        )
                        LEFT JOIN gestacess_dac ON (
                            gestacess_dac.id = gestacess_template_inst.dac_id
                        )
                        LEFT JOIN gestacess_regras_lista_ticket ON (
                            gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                            AND
                            gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                        )
                        LEFT JOIN gestacess_valores_variaveis_acesso ON (
                            gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                            AND
                            gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                        )
                    WHERE
                        gestacess_evento.id = '" . (int)$event_id . "'
                        AND
                        gestacess_evento.code = gestacess_regras_lista_ticket.evento
                        AND
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                        AND
                        gestacess_transacoes.entry = '1'
                        AND
                        gestacess_transacoes.apagado = '0'
                        AND
                        gestacess_transacoes.isEntry = '1'
                        AND
                        gestacess_transacoes.testMode = '0'
                        AND
                        gestacess_transacoes.areaEntryId <> '330'
                        " . $filters . "
                        " . $group_by . "
                        " . $order_by . "
                ");

            }

            if ($need_tcp_query == 1 ) {

                $filllevel_extra = DB::connection('access')->select("
                    SELECT
                        'Entradas Extra TCP' AS 'tickettype',
                        COUNT(id) AS 'entries'
                    FROM
                        `gestacess_transacoes`
                    WHERE
                        `eventId_id` = '" . (int)$event_id . "' AND `vas` LIKE '%TCPVA5%'
                ");

            }

            if ($filter_type == 'hour' && $search_type == 'default') {
                return response()->json($filllevel[0], 200);
            }

            return response()->json(array_merge($filllevel, $filllevel_master, $filllevel_extra), 200);

        } catch (\Exception $e) {

            Log::error('Flowrate Request Error');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

     }

}