<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 15-07-2018
 * Time: 13:06
 */

namespace App\Http\Controllers\Charts;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class ChartsController extends Controller {

    public function __construct()
    {
    }

    public function index() {

        $chartjs = app()->chartjs
            ->name('pieChartTest')
            ->type('pie')
            ->size(['width' => 400, 'height' => 200])
            ->labels(['Label x', 'Label y'])
            ->datasets([
                [
                    'backgroundColor' => ['#FF6384', '#36A2EB'],
                    'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                    'data' => [69, 59]
                ]
            ])
            ->options([]);

        Log::info('RETURNING AT SOME POINT YOU DIDN\'T KNOW');

        return view('cards.chart-graph-card', compact('chartjs'));

    }

}