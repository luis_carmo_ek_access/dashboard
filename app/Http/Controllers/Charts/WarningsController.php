<?php

namespace App\Http\Controllers\Charts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use App\Warnings;

class WarningsController extends Controller {

    public function __construct() {

        $this->middleware('auth');

    }

    public function getWarnings(Request $request, $event_id) {

        try {

            /**
             * Event Options
             */

            $options = DB::connection('access')->select("
                SELECT
                    dataInicio,
                    dataFim,
                    horaAberturaPortas
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
            ");

            $event_options = (array) $options[0];

            $warnings = DB::connection('access')->select("
                SELECT
                REPLACE
                    (
                    REPLACE
                        (
                        REPLACE
                            (
                            REPLACE
                                (
                                REPLACE
                                    (
                                    REPLACE
                                        (
                                        REPLACE
                                            (
                                            REPLACE
                                                (
                                                REPLACE
                                                    (
                                                    REPLACE
                                                        (
                                                        REPLACE
                                                            (
                                                            REPLACE
                                                                (
                                                                REPLACE
                                                                    (
                                                                    REPLACE
                                                                        (
                                                                        REPLACE
                                                                            (
                                                                            REPLACE
                                                                                (
                                                                                REPLACE
                                                                                    (
                                                                                    REPLACE
                                                                                        (
                                                                                        REPLACE
                                                                                            (
                                                                                            REPLACE
                                                                                                (
                                                                                                REPLACE
                                                                                                    (
                                                                                                    REPLACE
                                                                                                        (
                                                                                                        REPLACE
                                                                                                            (
                                                                                                            REPLACE
                                                                                                                (
                                                                                                                REPLACE
                                                                                                                    (
                                                                                                                    REPLACE
                                                                                                                        (
                                                                                                                        REPLACE
                                                                                                                            (
                                                                                                                            REPLACE
                                                                                                                                (
                                                                                                                                REPLACE
                                                                                                                                    (
                                                                                                                                    REPLACE
                                                                                                                                        (
                                                                                                                                        REPLACE
                                                                                                                                            (
                                                                                                                                            REPLACE
                                                                                                                                                (
                                                                                                                                                REPLACE
                                                                                                                                                    (
                                                                                                                                                    REPLACE
                                                                                                                                                        (
                                                                                                                                                        REPLACE
                                                                                                                                                            (
                                                                                                                                                                CONVERT(
                                                                                                                                                                    BINARY CONVERT(
                                                                                                                                                                        (
                                                                                                                                                                            CASE WHEN Erro <> 'BLACK LIST' THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist IS NULL THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist = '' THEN Erro ELSE Mensagem_Blacklist
                                                                                                                                                                        END
                                                                                                                                                                    ) USING utf8
                                                                                                                                                                ) USING latin1
                                                                                                                                                            ),
                                                                                                                                                            'Ãº',
                                                                                                                                                            'u'
                                                                                                                                                    ),
                                                                                                                                                    'Ã¡',
                                                                                                                                                    'a'
                                                                                                                                                ),
                                                                                                                                                'Ã¤',
                                                                                                                                                'a'
                                                                                                                                            ),
                                                                                                                                            'Ã©',
                                                                                                                                            'e'
                                                                                                                                        ),
                                                                                                                                        'í©',
                                                                                                                                        'e'
                                                                                                                                    ),
                                                                                                                                    'Ã³',
                                                                                                                                    'o'
                                                                                                                                ),
                                                                                                                                'íº',
                                                                                                                                'u'
                                                                                                                            ),
                                                                                                                            'Ã±',
                                                                                                                            'n'
                                                                                                                        ),
                                                                                                                        'í‘',
                                                                                                                        'N'
                                                                                                                    ),
                                                                                                                    'Ã­',
                                                                                                                    'i'
                                                                                                                ),
                                                                                                                'â€“',
                                                                                                                '-'
                                                                                                            ),
                                                                                                            'â€™',
                                                                                                            '\''
                                                                                                        ),
                                                                                                        'â€¦',
                                                                                                        '...'
                                                                                                    ),
                                                                                                    'â€“',
                                                                                                    '-'
                                                                                                ),
                                                                                                'â€œ',
                                                                                                ''
                                                                                            ),
                                                                                            'â€',
                                                                                            ''
                                                                                        ),
                                                                                        'â€˜',
                                                                                        '\''
                                                                                    ),
                                                                                    'â€¢',
                                                                                    '-'
                                                                                ),
                                                                                'â€¡',
                                                                                'c'
                                                                            ),
                                                                            'Â',
                                                                            ''
                                                                        ),
                                                                        'i§',
                                                                        'c'
                                                                    ),
                                                                    'iµ',
                                                                    'o'
                                                                ),
                                                                'i£',
                                                                'a'
                                                            ),
                                                            'iª',
                                                            'e'
                                                        ),
                                                        'Ã‰',
                                                        'E'
                                                    ),
                                                    'Ã‡',
                                                    'C'
                                                ),
                                                'Ãƒ',
                                                'A'
                                            ),
                                            'Ãµ',
                                            'o'
                                        ),
                                        'Ã¢',
                                        'a'
                                    ),
                                    'Ã£',
                                    'a'
                                ),
                                'Ãª',
                                'e'
                            ),
                            'Ã§',
                            'c'
                        ),
                        'Âª',
                        'a'
                    ),
                    'Âº',
                    'o'
                ),
                'Ã ',
                'a'
                ) AS error_message,
                COUNT(DISTINCT(titulo)) AS quantity
                FROM
                    (
                    SELECT
                        gestacess_regras_lista_ticket.VA20 AS 'Socio',
                        gestacess_regras_lista_ticket.VA21 AS 'NSocio',
                        gestacess_regras_lista_ticket.VA5 AS 'Categoria',
                        LEFT(
                            gestacess_dac.code,
                            (LENGTH(gestacess_dac.code) -5)
                        ) AS 'Porta',
                        gestacess_transacoes.titulo AS 'Titulo',
                        CASE WHEN gestacess_import_list.isWhitelist = 1 THEN 'WL' ELSE 'BL'
                END AS 'Tipo',
                gestacess_codigosmensagem.mensagemoperador AS 'Erro',
                gestacess_import_list.blmessage AS 'Mensagem_Blacklist',
                TIMESTAMP(
                    gestacess_transacoes.dataTransac,
                    gestacess_transacoes.horaTransac
                ) AS 'Datetime',
                gestacess_dac.code AS 'DAC'
                FROM
                    gestacess_transacoes
                LEFT JOIN gestacess_import_list ON gestacess_transacoes.titulo = gestacess_import_list.titulo
                LEFT JOIN gestacess_regras_lista_ticket ON(
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo AND gestacess_regras_lista_ticket.evento =(
                        SELECT CODE
                    FROM
                        gestacess_evento
                    WHERE
                        id = '" . (int)$event_id . "'
                    )
                    )
                LEFT JOIN gestacess_template_inst ON gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
                LEFT JOIN gestacess_codigosmensagem ON gestacess_transacoes.erro_id = gestacess_codigosmensagem.id
                WHERE
                    gestacess_transacoes.eventID_id = '" . (int)$event_id . "' AND gestacess_import_list.codEvent =(
                    SELECT CODE
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
                ) AND gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.dataTransac <= '" . $event_options['dataFim'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "' AND erro_id NOT IN(
                    3,
                    4,
                    8,
                    9,
                    10,
                    11,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    23,
                    26
                )
                UNION ALL
                SELECT
                    '' AS 'Socio',
                    '' AS 'NSocio',
                    '' AS 'Categoria',
                    LEFT(
                        gestacess_dac.code,
                        (LENGTH(gestacess_dac.code) -5)
                    ) AS 'Porta',
                    gestacess_transacoes.titulo AS 'Titulo',
                    '' AS 'Tipo',
                    gestacess_codigosmensagem.mensagemoperador AS 'Erro',
                    '' AS 'Mensagem_Blacklist',
                    TIMESTAMP(
                        gestacess_transacoes.dataTransac,
                        gestacess_transacoes.horaTransac
                    ) AS 'Datetime',
                    gestacess_dac.code AS 'DAC'
                FROM
                    gestacess_transacoes,
                    gestacess_template_inst,
                    gestacess_dac,
                    gestacess_codigosmensagem
                WHERE
                    gestacess_transacoes.template_inst_id = gestacess_template_inst.id AND gestacess_template_inst.dac_id = gestacess_dac.id AND gestacess_transacoes.erro_id = gestacess_codigosmensagem.id AND gestacess_transacoes.eventID_id = '" . (int)$event_id . "' AND gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.dataTransac <= '" . $event_options['dataFim'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "' AND erro_id NOT IN(
                        3,
                        4,
                        8,
                        9,
                        10,
                        11,
                        13,
                        14,
                        15,
                        16,
                        17,
                        18,
                        19,
                        20,
                        23,
                        26
                    ) AND gestacess_transacoes.titulo NOT IN(
                    SELECT
                        gestacess_transacoes.titulo
                    FROM
                        gestacess_transacoes
                    LEFT JOIN gestacess_import_list ON gestacess_transacoes.titulo = gestacess_import_list.titulo
                    LEFT JOIN gestacess_regras_lista_ticket ON(
                            gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo AND gestacess_regras_lista_ticket.evento =(
                            SELECT CODE
                        FROM
                            gestacess_evento
                        WHERE
                            id = '" . (int)$event_id . "'
                        )
                        )
                    LEFT JOIN gestacess_template_inst ON gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                    LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
                    LEFT JOIN gestacess_codigosmensagem ON gestacess_transacoes.erro_id = gestacess_codigosmensagem.id
                    WHERE
                        gestacess_transacoes.eventID_id = '" . (int)$event_id . "' AND gestacess_import_list.codEvent =(
                        SELECT CODE
                    FROM
                        gestacess_evento
                    WHERE
                        id = '" . (int)$event_id . "'
                    ) AND gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.dataTransac <= '" . $event_options['dataFim'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "' AND erro_id NOT IN(
                        3,
                        4,
                        8,
                        9,
                        10,
                        11,
                        13,
                        14,
                        15,
                        16,
                        17,
                        18,
                        19,
                        20,
                        23,
                        26
                    )
                )
                ORDER BY DATETIME
                ) AS sub
                GROUP BY
                    error_message
                ORDER BY
                    `quantity`
                DESC
            ");

            return response()->json($warnings, 200);

        } catch (\Exception $e) {

            Log::error('Warnings Request Error');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

    public function getWarningsFiltered(Request $request, $event_id, $filter_type) {

        $tickets_filter = null;
        $hours_filter = array();
        $filter = '';
        $search_type = '';

        $selection = '';
        $group_by = '';
        $order_by = '';

        if ($filter_type == 'tickettype') {

        } else if ($filter_type == 'hour') {

            $this->validate($request, [
                'hours'         => 'required',
                'type_search'   => 'required'
            ]);

            $hours_filter = explode(',', $request['hours']);
            $search_type = $request['type_search'];
            

        }

        if (strlen($tickets_filter > 0)) {

            $filters = 'AND gestacess_valores_variaveis_acesso.id IN (' . $tickets_filter . ')';

        } else {

            $filters = '';

        }


        try {

            /**
             * Event Options
             */

            $options = DB::connection('access')->select("
                SELECT
                    dataInicio,
                    dataFim,
                    horaAberturaPortas
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
            ");

            $event_options = (array) $options[0];

            $day_now   = date("Y-m-d");

            if (strlen($tickets_filter) > 0 && $tickets_filter !== null) {

                // AND gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                 
                $filters = "
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $day_now . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_valores_variaveis_acesso.id IN (" . $tickets_filter . ")
                ";
    
            } else if (count($hours_filter) > 0 && $hours_filter !== null) {
    
                $filters = "
                    AND
                    TIMESTAMP(gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) BETWEEN '" . $hours_filter[0] . "' AND '" . $hours_filter[1] . "'
                ";
    
            } else {

                // AND gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
    
                $filters = "
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $day_now . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                ";
    
            }

            $warnings = DB::connection('access')->select("
            SELECT
            REPLACE
                (
                REPLACE
                    (
                    REPLACE
                        (
                        REPLACE
                            (
                            REPLACE
                                (
                                REPLACE
                                    (
                                    REPLACE
                                        (
                                        REPLACE
                                            (
                                            REPLACE
                                                (
                                                REPLACE
                                                    (
                                                    REPLACE
                                                        (
                                                        REPLACE
                                                            (
                                                            REPLACE
                                                                (
                                                                REPLACE
                                                                    (
                                                                    REPLACE
                                                                        (
                                                                        REPLACE
                                                                            (
                                                                            REPLACE
                                                                                (
                                                                                REPLACE
                                                                                    (
                                                                                    REPLACE
                                                                                        (
                                                                                        REPLACE
                                                                                            (
                                                                                            REPLACE
                                                                                                (
                                                                                                REPLACE
                                                                                                    (
                                                                                                    REPLACE
                                                                                                        (
                                                                                                        REPLACE
                                                                                                            (
                                                                                                            REPLACE
                                                                                                                (
                                                                                                                REPLACE
                                                                                                                    (
                                                                                                                    REPLACE
                                                                                                                        (
                                                                                                                        REPLACE
                                                                                                                            (
                                                                                                                            REPLACE
                                                                                                                                (
                                                                                                                                REPLACE
                                                                                                                                    (
                                                                                                                                    REPLACE
                                                                                                                                        (
                                                                                                                                        REPLACE
                                                                                                                                            (
                                                                                                                                            REPLACE
                                                                                                                                                (
                                                                                                                                                REPLACE
                                                                                                                                                    (
                                                                                                                                                    REPLACE
                                                                                                                                                        (
                                                                                                                                                            CONVERT(
                                                                                                                                                                BINARY CONVERT(
                                                                                                                                                                    (
                                                                                                                                                                        CASE WHEN Erro <> 'BLACK LIST' THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist IS NULL THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist = '' THEN Erro ELSE Mensagem_Blacklist
                                                                                                                                                                    END
                                                                                                                                                                ) USING utf8
                                                                                                                                                            ) USING latin1
                                                                                                                                                        ),
                                                                                                                                                        'Ãº',
                                                                                                                                                        'u'
                                                                                                                                                ),
                                                                                                                                                'Ã¡',
                                                                                                                                                'a'
                                                                                                                                            ),
                                                                                                                                            'Ã¤',
                                                                                                                                            'a'
                                                                                                                                        ),
                                                                                                                                        'Ã©',
                                                                                                                                        'e'
                                                                                                                                    ),
                                                                                                                                    'í©',
                                                                                                                                    'e'
                                                                                                                                ),
                                                                                                                                'Ã³',
                                                                                                                                'o'
                                                                                                                            ),
                                                                                                                            'íº',
                                                                                                                            'u'
                                                                                                                        ),
                                                                                                                        'Ã±',
                                                                                                                        'n'
                                                                                                                    ),
                                                                                                                    'í‘',
                                                                                                                    'N'
                                                                                                                ),
                                                                                                                'Ã­',
                                                                                                                'i'
                                                                                                            ),
                                                                                                            'â€“',
                                                                                                            '-'
                                                                                                        ),
                                                                                                        'â€™',
                                                                                                        '\''
                                                                                                    ),
                                                                                                    'â€¦',
                                                                                                    '...'
                                                                                                ),
                                                                                                'â€“',
                                                                                                '-'
                                                                                            ),
                                                                                            'â€œ',
                                                                                            ''
                                                                                        ),
                                                                                        'â€',
                                                                                        ''
                                                                                    ),
                                                                                    'â€˜',
                                                                                    '\''
                                                                                ),
                                                                                'â€¢',
                                                                                '-'
                                                                            ),
                                                                            'â€¡',
                                                                            'c'
                                                                        ),
                                                                        'Â',
                                                                        ''
                                                                    ),
                                                                    'i§',
                                                                    'c'
                                                                ),
                                                                'iµ',
                                                                'o'
                                                            ),
                                                            'i£',
                                                            'a'
                                                        ),
                                                        'iª',
                                                        'e'
                                                    ),
                                                    'Ã‰',
                                                    'E'
                                                ),
                                                'Ã‡',
                                                'C'
                                            ),
                                            'Ãƒ',
                                            'A'
                                        ),
                                        'Ãµ',
                                        'o'
                                    ),
                                    'Ã¢',
                                    'a'
                                ),
                                'Ã£',
                                'a'
                            ),
                            'Ãª',
                            'e'
                        ),
                        'Ã§',
                        'c'
                    ),
                    'Âª',
                    'a'
                ),
                'Âº',
                'o'
            ),
            'Ã ',
            'a'
            ) AS error_message,
            COUNT(DISTINCT(titulo)) AS quantity
            FROM
                (
                SELECT
                    gestacess_regras_lista_ticket.VA20 AS 'Socio',
                    gestacess_regras_lista_ticket.VA21 AS 'NSocio',
                    gestacess_regras_lista_ticket.VA5 AS 'Categoria',
                    LEFT(
                        gestacess_dac.code,
                        (LENGTH(gestacess_dac.code) -5)
                    ) AS 'Porta',
                    gestacess_transacoes.titulo AS 'Titulo',
                    CASE WHEN gestacess_import_list.isWhitelist = 1 THEN 'WL' ELSE 'BL'
            END AS 'Tipo',
            gestacess_codigosmensagem.mensagemoperador AS 'Erro',
            gestacess_import_list.blmessage AS 'Mensagem_Blacklist',
            TIMESTAMP(
                gestacess_transacoes.dataTransac,
                gestacess_transacoes.horaTransac
            ) AS 'Datetime',
            gestacess_dac.code AS 'DAC'
            FROM
                gestacess_transacoes
            LEFT JOIN gestacess_import_list ON gestacess_transacoes.titulo = gestacess_import_list.titulo
            LEFT JOIN gestacess_regras_lista_ticket ON
                (
                    gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo AND gestacess_regras_lista_ticket.evento =(
                    SELECT CODE
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
                )
                )
            LEFT JOIN gestacess_template_inst ON gestacess_transacoes.template_inst_id = gestacess_template_inst.id
            LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
            LEFT JOIN gestacess_codigosmensagem ON gestacess_transacoes.erro_id = gestacess_codigosmensagem.id
            WHERE
                gestacess_transacoes.eventID_id = '" . (int)$event_id . "' AND gestacess_import_list.codEvent =(
                SELECT CODE
            FROM
                gestacess_evento
            WHERE
                id = '" . (int)$event_id . "'
            ) " . $filters . " AND erro_id NOT IN(
                3,
                4,
                8,
                9,
                10,
                11,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20,
                23,
                26
            )
            UNION ALL
            SELECT
                '' AS 'Socio',
                '' AS 'NSocio',
                '' AS 'Categoria',
                LEFT(
                    gestacess_dac.code,
                    (LENGTH(gestacess_dac.code) -5)
                ) AS 'Porta',
                gestacess_transacoes.titulo AS 'Titulo',
                '' AS 'Tipo',
                gestacess_codigosmensagem.mensagemoperador AS 'Erro',
                '' AS 'Mensagem_Blacklist',
                TIMESTAMP(
                    gestacess_transacoes.dataTransac,
                    gestacess_transacoes.horaTransac
                ) AS 'Datetime',
                gestacess_dac.code AS 'DAC'
            FROM
                gestacess_transacoes,
                gestacess_template_inst,
                gestacess_dac,
                gestacess_codigosmensagem
            WHERE
                gestacess_transacoes.template_inst_id = gestacess_template_inst.id AND gestacess_template_inst.dac_id = gestacess_dac.id AND gestacess_transacoes.erro_id = gestacess_codigosmensagem.id AND gestacess_transacoes.eventID_id = '" . (int)$event_id . "' " . $filters . " AND erro_id NOT IN(
                    3,
                    4,
                    8,
                    9,
                    10,
                    11,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    23,
                    26
                ) AND gestacess_transacoes.titulo NOT IN(
                SELECT
                    gestacess_transacoes.titulo
                FROM
                    gestacess_transacoes
                LEFT JOIN gestacess_import_list ON gestacess_transacoes.titulo = gestacess_import_list.titulo
                LEFT JOIN gestacess_regras_lista_ticket ON
                    (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo AND gestacess_regras_lista_ticket.evento =(
                        SELECT CODE
                    FROM
                        gestacess_evento
                    WHERE
                        id = '" . (int)$event_id . "'
                    )
                    )
                LEFT JOIN gestacess_template_inst ON gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
                LEFT JOIN gestacess_codigosmensagem ON gestacess_transacoes.erro_id = gestacess_codigosmensagem.id
                WHERE
                    gestacess_transacoes.eventID_id = '" . (int)$event_id . "' AND gestacess_import_list.codEvent =(
                    SELECT CODE
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
                ) " . $filters . " AND erro_id NOT IN(
                    3,
                    4,
                    8,
                    9,
                    10,
                    11,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    23,
                    26
                )
            )
            ORDER BY DATETIME
            ) AS sub
            GROUP BY
                error_message
            ORDER BY
                `quantity`
            DESC
            ");

            return response()->json($warnings, 200);

        } catch (\Excepction $e) {

            Log::error('Warnings Request Error');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

    public function getWarningsByGates(Request $request, $event_id) {

        try {

            $options = DB::connection('access')->select("
                SELECT
                    dataInicio,
                    dataFim,
                    horaAberturaPortas
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
            ");

            $event_options = (array) $options[0];

            $warnings = DB::connection('access')->select("
                SELECT
                    dacs.insts AS 'Instalacao',
                    dacs.zona AS 'Bancada',
                    dacs.porta AS 'Porta',
                    SUM(
                        IF(
                            msgerro = 'PORTA ERRADA',
                            quantidade,
                            NULL
                        )
                    ) PE,
                    SUM(
                        IF(
                            msgerro = 'TITULO DESCONHECIDO',
                            quantidade,
                            NULL
                        )
                    ) TD,
                    SUM(
                        IF(
                            msgerro = 'QUOTAS EM ATRASO',
                            quantidade,
                            NULL
                        )
                    ) QA,
                    SUM(
                        IF(
                            msgerro = 'BIL. ESTORNADO',
                            quantidade,
                            NULL
                        )
                    ) BE,
                    SUM(
                        IF(
                            msgerro = 'VIA ANULADA',
                            quantidade,
                            NULL
                        )
                    ) VA,
                    SUM(
                        IF(
                            msgerro = 'Solicite Apoio Controlo',
                            quantidade,
                            NULL
                        )
                    ) SAC,
                    SUM(
                        IF(
                            msgerro = 'JA ENTROU',
                            quantidade,
                            NULL
                        )
                    ) JE,
                    SUM(
                        IF(
                            msgerro = 'Socio sem renumeracao',
                            quantidade,
                            NULL
                        )
                    ) SSR,
                    SUM(
                        IF(
                            msgerro = 'SOCIO C/ BILHETE',
                            quantidade,
                            NULL
                        )
                    ) SCB,
                    SUM(
                        IF(
                            msgerro = 'NAOSOCIO C/ BIL.',
                            quantidade,
                            NULL
                        )
                    ) NSCB,
                    SUM(
                        IF(
                            msgerro = 'CARTAO ANTIGO',
                            quantidade,
                            NULL
                        )
                    ) CA,
                    SUM(
                        IF(
                            msgerro = 'LA ANULADO',
                            quantidade,
                            NULL
                        )
                    ) LAA,
                    SUM(
                        IF(
                            msgerro = 'Controlo TCT',
                            quantidade,
                            NULL
                        )
                    ) CTCT
                FROM
                    (
                    SELECT
                        (
                            CASE WHEN Erro <> 'BLACK LIST' THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist IS NULL THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist = '' THEN Erro ELSE Mensagem_Blacklist
                        END
                ) AS msgerro,
                ANY_VALUE(LEFT(dac,(LENGTH(dac) -5))) AS porta,
                ANY_VALUE(iddac) AS iddac,
                COUNT(DISTINCT(titulo)) AS quantidade
                FROM
                    (
                    SELECT
                        gestacess_regras_lista_ticket.VA20 AS 'Socio',
                        gestacess_regras_lista_ticket.VA21 AS 'NSocio',
                        gestacess_regras_lista_ticket.VA5 AS 'Categoria',
                        LEFT(
                            gestacess_dac.code,
                            (LENGTH(gestacess_dac.code) -5)
                        ) AS 'Porta',
                        gestacess_transacoes.titulo AS 'Titulo',
                        CASE WHEN gestacess_import_list.isWhitelist = 1 THEN 'WL' ELSE 'BL'
                END AS 'Tipo',
                ANY_VALUE(
                    gestacess_codigosmensagem.mensagemoperador
                ) AS 'Erro',
                gestacess_transacoes.msg_blacklist AS 'Mensagem_Blacklist',
                TIMESTAMP(
                    gestacess_transacoes.dataTransac,
                    gestacess_transacoes.horaTransac
                ) AS 'Datetime',
                gestacess_dac.code AS 'DAC',
                ANY_VALUE(gestacess_template_inst.id) AS 'iddac'
                FROM
                    gestacess_transacoes
                LEFT JOIN gestacess_import_list ON gestacess_transacoes.titulo = gestacess_import_list.titulo
                LEFT JOIN gestacess_regras_lista_ticket ON(
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo AND gestacess_regras_lista_ticket.evento =(
                        SELECT CODE
                    FROM
                        gestacess_evento
                    WHERE
                        id = '" . (int)$event_id ."'
                    )
                    )
                LEFT JOIN gestacess_template_inst ON gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
                LEFT JOIN gestacess_codigosmensagem ON gestacess_transacoes.erro_id = gestacess_codigosmensagem.id
                WHERE
                    gestacess_transacoes.eventID_id = '" . (int)$event_id ."' AND gestacess_import_list.codEvent =(
                    SELECT CODE
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id ."'
                ) AND gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] ."' AND gestacess_transacoes.erro_id NOT IN(
                    3,
                    4,
                    8,
                    9,
                    10,
                    11,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    23,
                    26
                )
                UNION ALL
                SELECT
                    '' AS 'Socio',
                    '' AS 'NSocio',
                    '' AS 'Categoria',
                    LEFT(
                        gestacess_dac.code,
                        (LENGTH(gestacess_dac.code) -5)
                    ) AS 'Porta',
                    gestacess_transacoes.titulo AS 'Titulo',
                    '' AS 'Tipo',
                    ANY_VALUE(
                        gestacess_codigosmensagem.mensagemoperador
                    ) AS 'Erro',
                    '' AS 'Mensagem_Blacklist',
                    TIMESTAMP(
                        gestacess_transacoes.dataTransac,
                        gestacess_transacoes.horaTransac
                    ) AS 'Datetime',
                    gestacess_dac.code AS 'DAC',
                    ANY_VALUE(gestacess_template_inst.id) AS 'iddac'
                FROM
                    gestacess_transacoes,
                    gestacess_template_inst,
                    gestacess_dac,
                    gestacess_codigosmensagem
                WHERE
                    gestacess_transacoes.template_inst_id = gestacess_template_inst.id AND gestacess_template_inst.dac_id = gestacess_dac.id AND gestacess_transacoes.erro_id = gestacess_codigosmensagem.id AND gestacess_transacoes.eventID_id = '" . (int)$event_id ."' AND gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] ."' AND gestacess_transacoes.erro_id NOT IN(
                        3,
                        4,
                        8,
                        9,
                        10,
                        11,
                        13,
                        14,
                        15,
                        16,
                        17,
                        18,
                        19,
                        20,
                        23,
                        26
                    ) AND gestacess_transacoes.titulo NOT IN(
                    SELECT
                        gestacess_transacoes.titulo
                    FROM
                        gestacess_transacoes
                    LEFT JOIN gestacess_import_list ON gestacess_transacoes.titulo = gestacess_import_list.titulo
                    LEFT JOIN gestacess_regras_lista_ticket ON(
                            gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo AND gestacess_regras_lista_ticket.evento =(
                            SELECT CODE
                        FROM
                            gestacess_evento
                        WHERE
                            id = '" . (int)$event_id ."'
                        )
                        )
                    LEFT JOIN gestacess_template_inst ON gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                    LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
                    LEFT JOIN gestacess_codigosmensagem ON gestacess_transacoes.erro_id = gestacess_codigosmensagem.id
                    WHERE
                        gestacess_transacoes.eventID_id = '" . (int)$event_id ."' AND gestacess_import_list.codEvent =(
                        SELECT CODE
                    FROM
                        gestacess_evento
                    WHERE
                        id = '" . (int)$event_id ."'
                    ) AND gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] ."' AND gestacess_transacoes.erro_id NOT IN(
                        3,
                        4,
                        8,
                        9,
                        10,
                        11,
                        13,
                        14,
                        15,
                        16,
                        17,
                        18,
                        19,
                        20,
                        23,
                        26
                    )
                )
                ORDER BY DATETIME
                ) AS sub
                GROUP BY
                    (
                        CASE WHEN Erro <> 'BLACK LIST' THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist IS NULL THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist = '' THEN Erro ELSE Mensagem_Blacklist
                    END
                ),
                LEFT(dac,(LENGTH(dac) -5))
                ORDER BY
                    (
                        CASE WHEN Erro <> 'BLACK LIST' THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist IS NULL THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist = '' THEN Erro ELSE Mensagem_Blacklist
                    END
                ),
                ANY_VALUE(
                    CAST(
                        SUBSTRING_INDEX(
                            RIGHT(dac,(LENGTH(dac) -5)),
                            '-',
                            1
                        ) AS UNSIGNED
                    )
                )
                ) AS sub
                LEFT JOIN(
                    SELECT insts.id AS 'insts_id',
                        (
                            CASE WHEN insts.id = 3 THEN 'Pedroso' WHEN insts.id = 21 THEN 'Futebol Dragao' WHEN insts.id = 177 THEN 'Dragao Caixa' WHEN insts.id = 209 THEN 'CTPG' WHEN insts.id = 238 THEN 'Museu' WHEN insts.id = 261 THEN 'Acreditacao' ELSE ''
                        END
                ) AS 'insts',
                zonas.id AS 'zona_id',
                zonas.descricao AS 'zona',
                portas.id AS 'porta_id',
                portas.descricao AS 'porta',
                gestacess_template_inst.id AS 'dac_id',
                gestacess_dac.descricao AS 'dac'
                FROM
                    gestacess_template_inst
                LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
                LEFT JOIN(
                    SELECT
                        gestacess_template_inst.id,
                        gestacess_template_inst.descricao,
                        gestacess_template_inst.parent_id
                    FROM
                        gestacess_template_inst
                    WHERE
                        isporta = 1
                ) AS portas
                ON
                    gestacess_template_inst.parent_id = portas.id
                LEFT JOIN(
                    SELECT
                        gestacess_template_inst.id,
                        gestacess_template_inst.descricao,
                        gestacess_template_inst.parent_id
                    FROM
                        gestacess_template_inst
                    WHERE
                        iszona = 1
                ) AS zonas
                ON
                    portas.parent_id = zonas.id
                LEFT JOIN(
                    SELECT
                        gestacess_template_inst.id,
                        gestacess_template_inst.descricao,
                        gestacess_template_inst.parent_id
                    FROM
                        gestacess_template_inst
                    WHERE
                        istemplate = 1
                ) AS insts
                ON
                    zonas.parent_id = insts.id
                WHERE
                    gestacess_template_inst.isdac = 1
                ) AS dacs
                ON
                    sub.iddac = dacs.dac_id
                GROUP BY
                    dacs.insts,
                    dacs.zona,
                    dacs.porta
                ORDER BY
                    dacs.insts,
                    dacs.zona,
                    dacs.porta
            ");

            $parse_attributes = array("Instalacao","Bancada","Porta");

            $structure = array();

            foreach ($warnings as $warning) {

                $venue_index = array_search($warning->Instalacao, array_column($structure, 'description'));

                if ($venue_index > -1) {

                    foreach ($warning as $key => $value) {

                        if (!in_array($key, $parse_attributes)) {

                            $structure[$venue_index][$key] = $structure[$venue_index][$key] + $value;

                        }

                    }

                    $zone_index = array_search($warning->Bancada, array_column($structure[$venue_index]['zones'], 'description'));

                    if ($zone_index > -1) {

                        $gate_index = array_search($warning->Porta, array_column($structure[$venue_index]['zones'][$zone_index]['gates'], 'description'));

                        if (!($gate_index > -1)) {

                            $structure[$venue_index]['zones'][$zone_index]['gates'][] = array(
                                'description'   => $warning->Porta,
                            );

                            $new_gate_index = array_search($warning->Porta, array_column($structure[$venue_index]['zones'][$zone_index]['gates'], 'description'));

                            foreach ($warning as $key => $value) {

                                if (!in_array($key, $parse_attributes)) {

                                    $structure[$venue_index]['zones'][$zone_index][$key] = $structure[$venue_index]['zones'][$zone_index][$key] + $value;
                                    $structure[$venue_index]['zones'][$zone_index]['gates'][$new_gate_index][$key] = $value;

                                }

                            }

                        }


                    } else {

                        $structure[$venue_index]['zones'][] = array(
                            'description'       => $warning->Bancada,
                            'gates'             => array(
                                array(
                                    'description'       => $warning->Porta
                                )
                            )
                        );

                        $new_zone_index = array_search($warning->Bancada, array_column($structure[$venue_index]['zones'], 'description'));
                        $new_gate_index = array_search($warning->Porta, array_column($structure[$venue_index]['zones'][$new_zone_index]['gates'], 'description'));

                        foreach ($warning as $key => $value) {

                            if (!in_array($key, $parse_attributes)) {

                                $structure[$venue_index]['zones'][$new_zone_index][$key] = $value;
                                $structure[$venue_index]['zones'][$new_zone_index]['gates'][$new_gate_index][$key] = $value;

                            }

                        }

                    }

                } else {

                    $structure[] = array(
                        'description'   => $warning->Instalacao,
                        'zones'         => array(
                            array(
                                'description'   => $warning->Bancada,
                                'gates'         => array(
                                    array(
                                        'description'       => $warning->Porta
                                    )
                                )
                            )
                        )
                    );

                    $venue_index = array_search($warning->Instalacao, array_column($structure, 'description'));

                    $zone_index = array_search($warning->Bancada, array_column($structure[$venue_index]['zones'], 'description'));

                    $gate_index = array_search($warning->Porta, array_column($structure[$venue_index]['zones'][$zone_index]['gates'], 'description'));

                    foreach ($warning as $key => $value) {

                        if (!in_array($key, $parse_attributes)) {

                            $structure[$venue_index][$key] = $value;
                            $structure[$venue_index]['zones'][$zone_index][$key] = $value;
                            $structure[$venue_index]['zones'][$zone_index]['gates'][$gate_index][$key] = $value;

                        }

                    }

                }

            }

            return response()->json($structure, 200);

        } catch (\Exception $e) {

            Log::error('Warnings By Gates');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

    public function getWarningsByGatesFiltered(Request $request, $event_id) {

        try {

            $options = DB::connection('access')->select("
                SELECT
                    dataInicio,
                    dataFim,
                    horaAberturaPortas
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
            ");

            $event_options = (array) $options[0];

            $warnings = DB::connection('access')->select("
                SELECT
                    dacs.insts AS 'Instalacao',
                    dacs.zona AS 'Bancada',
                    dacs.porta AS 'Porta',
                    SUM(
                        IF(
                            msgerro = 'PORTA ERRADA',
                            quantidade,
                            NULL
                        )
                    ) PE,
                    SUM(
                        IF(
                            msgerro = 'TITULO DESCONHECIDO',
                            quantidade,
                            NULL
                        )
                    ) TD,
                    SUM(
                        IF(
                            msgerro = 'QUOTAS EM ATRASO',
                            quantidade,
                            NULL
                        )
                    ) QA,
                    SUM(
                        IF(
                            msgerro = 'BIL. ESTORNADO',
                            quantidade,
                            NULL
                        )
                    ) BE,
                    SUM(
                        IF(
                            msgerro = 'VIA ANULADA',
                            quantidade,
                            NULL
                        )
                    ) VA,
                    SUM(
                        IF(
                            msgerro = 'Solicite Apoio Controlo',
                            quantidade,
                            NULL
                        )
                    ) SAC,
                    SUM(
                        IF(
                            msgerro = 'JA ENTROU',
                            quantidade,
                            NULL
                        )
                    ) JE,
                    SUM(
                        IF(
                            msgerro = 'Socio sem renumeracao',
                            quantidade,
                            NULL
                        )
                    ) SSR,
                    SUM(
                        IF(
                            msgerro = 'SOCIO C/ BILHETE',
                            quantidade,
                            NULL
                        )
                    ) SCB,
                    SUM(
                        IF(
                            msgerro = 'NAOSOCIO C/ BIL.',
                            quantidade,
                            NULL
                        )
                    ) NSCB,
                    SUM(
                        IF(
                            msgerro = 'CARTAO ANTIGO',
                            quantidade,
                            NULL
                        )
                    ) CA,
                    SUM(
                        IF(
                            msgerro = 'LA ANULADO',
                            quantidade,
                            NULL
                        )
                    ) LAA,
                    SUM(
                        IF(
                            msgerro = 'Controlo TCT',
                            quantidade,
                            NULL
                        )
                    ) CTCT
                FROM
                    (
                    SELECT
                        (
                            CASE WHEN Erro <> 'BLACK LIST' THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist IS NULL THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist = '' THEN Erro ELSE Mensagem_Blacklist
                        END
                ) AS msgerro,
                ANY_VALUE(LEFT(dac,(LENGTH(dac) -5))) AS porta,
                ANY_VALUE(iddac) AS iddac,
                COUNT(DISTINCT(titulo)) AS quantidade
                FROM
                    (
                    SELECT
                        gestacess_regras_lista_ticket.VA20 AS 'Socio',
                        gestacess_regras_lista_ticket.VA21 AS 'NSocio',
                        gestacess_regras_lista_ticket.VA5 AS 'Categoria',
                        LEFT(
                            gestacess_dac.code,
                            (LENGTH(gestacess_dac.code) -5)
                        ) AS 'Porta',
                        gestacess_transacoes.titulo AS 'Titulo',
                        CASE WHEN gestacess_import_list.isWhitelist = 1 THEN 'WL' ELSE 'BL'
                END AS 'Tipo',
                ANY_VALUE(
                    gestacess_codigosmensagem.mensagemoperador
                ) AS 'Erro',
                gestacess_transacoes.msg_blacklist AS 'Mensagem_Blacklist',
                TIMESTAMP(
                    gestacess_transacoes.dataTransac,
                    gestacess_transacoes.horaTransac
                ) AS 'Datetime',
                gestacess_dac.code AS 'DAC',
                ANY_VALUE(gestacess_template_inst.id) AS 'iddac'
                FROM
                    gestacess_transacoes
                LEFT JOIN gestacess_import_list ON gestacess_transacoes.titulo = gestacess_import_list.titulo
                LEFT JOIN gestacess_regras_lista_ticket ON(
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo AND gestacess_regras_lista_ticket.evento =(
                        SELECT CODE
                    FROM
                        gestacess_evento
                    WHERE
                        id = '" . (int)$event_id ."'
                    )
                    )
                LEFT JOIN gestacess_template_inst ON gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
                LEFT JOIN gestacess_codigosmensagem ON gestacess_transacoes.erro_id = gestacess_codigosmensagem.id
                WHERE
                    gestacess_transacoes.eventID_id = '" . (int)$event_id ."' AND gestacess_import_list.codEvent =(
                    SELECT CODE
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id ."'
                ) AND gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] ."' AND gestacess_transacoes.erro_id NOT IN(
                    3,
                    4,
                    8,
                    9,
                    10,
                    11,
                    13,
                    14,
                    15,
                    16,
                    17,
                    18,
                    19,
                    20,
                    23,
                    26
                )
                UNION ALL
                SELECT
                    '' AS 'Socio',
                    '' AS 'NSocio',
                    '' AS 'Categoria',
                    LEFT(
                        gestacess_dac.code,
                        (LENGTH(gestacess_dac.code) -5)
                    ) AS 'Porta',
                    gestacess_transacoes.titulo AS 'Titulo',
                    '' AS 'Tipo',
                    ANY_VALUE(
                        gestacess_codigosmensagem.mensagemoperador
                    ) AS 'Erro',
                    '' AS 'Mensagem_Blacklist',
                    TIMESTAMP(
                        gestacess_transacoes.dataTransac,
                        gestacess_transacoes.horaTransac
                    ) AS 'Datetime',
                    gestacess_dac.code AS 'DAC',
                    ANY_VALUE(gestacess_template_inst.id) AS 'iddac'
                FROM
                    gestacess_transacoes,
                    gestacess_template_inst,
                    gestacess_dac,
                    gestacess_codigosmensagem
                WHERE
                    gestacess_transacoes.template_inst_id = gestacess_template_inst.id AND gestacess_template_inst.dac_id = gestacess_dac.id AND gestacess_transacoes.erro_id = gestacess_codigosmensagem.id AND gestacess_transacoes.eventID_id = '" . (int)$event_id ."' AND gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] ."' AND gestacess_transacoes.erro_id NOT IN(
                        3,
                        4,
                        8,
                        9,
                        10,
                        11,
                        13,
                        14,
                        15,
                        16,
                        17,
                        18,
                        19,
                        20,
                        23,
                        26
                    ) AND gestacess_transacoes.titulo NOT IN(
                    SELECT
                        gestacess_transacoes.titulo
                    FROM
                        gestacess_transacoes
                    LEFT JOIN gestacess_import_list ON gestacess_transacoes.titulo = gestacess_import_list.titulo
                    LEFT JOIN gestacess_regras_lista_ticket ON(
                            gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo AND gestacess_regras_lista_ticket.evento =(
                            SELECT CODE
                        FROM
                            gestacess_evento
                        WHERE
                            id = '" . (int)$event_id ."'
                        )
                        )
                    LEFT JOIN gestacess_template_inst ON gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                    LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
                    LEFT JOIN gestacess_codigosmensagem ON gestacess_transacoes.erro_id = gestacess_codigosmensagem.id
                    WHERE
                        gestacess_transacoes.eventID_id = '" . (int)$event_id ."' AND gestacess_import_list.codEvent =(
                        SELECT CODE
                    FROM
                        gestacess_evento
                    WHERE
                        id = '" . (int)$event_id ."'
                    ) AND gestacess_transacoes.dataTransac = '" . $event_options['dataInicio'] . "' AND gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] ."' AND gestacess_transacoes.erro_id NOT IN(
                        3,
                        4,
                        8,
                        9,
                        10,
                        11,
                        13,
                        14,
                        15,
                        16,
                        17,
                        18,
                        19,
                        20,
                        23,
                        26
                    )
                )
                ORDER BY DATETIME
                ) AS sub
                GROUP BY
                    (
                        CASE WHEN Erro <> 'BLACK LIST' THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist IS NULL THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist = '' THEN Erro ELSE Mensagem_Blacklist
                    END
                ),
                LEFT(dac,(LENGTH(dac) -5))
                ORDER BY
                    (
                        CASE WHEN Erro <> 'BLACK LIST' THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist IS NULL THEN Erro WHEN Erro = 'BLACK LIST' AND Mensagem_Blacklist = '' THEN Erro ELSE Mensagem_Blacklist
                    END
                ),
                ANY_VALUE(
                    CAST(
                        SUBSTRING_INDEX(
                            RIGHT(dac,(LENGTH(dac) -5)),
                            '-',
                            1
                        ) AS UNSIGNED
                    )
                )
                ) AS sub
                LEFT JOIN(
                    SELECT insts.id AS 'insts_id',
                        (
                            CASE WHEN insts.id = 3 THEN 'Pedroso' WHEN insts.id = 21 THEN 'Futebol Dragao' WHEN insts.id = 177 THEN 'Dragao Caixa' WHEN insts.id = 209 THEN 'CTPG' WHEN insts.id = 238 THEN 'Museu' WHEN insts.id = 261 THEN 'Acreditacao' ELSE ''
                        END
                ) AS 'insts',
                zonas.id AS 'zona_id',
                zonas.descricao AS 'zona',
                portas.id AS 'porta_id',
                portas.descricao AS 'porta',
                gestacess_template_inst.id AS 'dac_id',
                gestacess_dac.descricao AS 'dac'
                FROM
                    gestacess_template_inst
                LEFT JOIN gestacess_dac ON gestacess_template_inst.dac_id = gestacess_dac.id
                LEFT JOIN(
                    SELECT
                        gestacess_template_inst.id,
                        gestacess_template_inst.descricao,
                        gestacess_template_inst.parent_id
                    FROM
                        gestacess_template_inst
                    WHERE
                        isporta = 1
                ) AS portas
                ON
                    gestacess_template_inst.parent_id = portas.id
                LEFT JOIN(
                    SELECT
                        gestacess_template_inst.id,
                        gestacess_template_inst.descricao,
                        gestacess_template_inst.parent_id
                    FROM
                        gestacess_template_inst
                    WHERE
                        iszona = 1
                ) AS zonas
                ON
                    portas.parent_id = zonas.id
                LEFT JOIN(
                    SELECT
                        gestacess_template_inst.id,
                        gestacess_template_inst.descricao,
                        gestacess_template_inst.parent_id
                    FROM
                        gestacess_template_inst
                    WHERE
                        istemplate = 1
                ) AS insts
                ON
                    zonas.parent_id = insts.id
                WHERE
                    gestacess_template_inst.isdac = 1
                ) AS dacs
                ON
                    sub.iddac = dacs.dac_id
                GROUP BY
                    dacs.insts,
                    dacs.zona,
                    dacs.porta
                ORDER BY
                    dacs.insts,
                    dacs.zona,
                    dacs.porta
            ");

            $parse_attributes = array("Instalacao","Bancada","Porta");

            $structure = array();

            $gates_structure = array();

            foreach ($warnings as $warning) {

                $venue_index = array_search($warning->Instalacao, array_column($structure, 'description'));

                if ($venue_index > -1) {

                    foreach ($warning as $key => $value) {

                        if (!in_array($key, $parse_attributes)) {

                            $structure[$venue_index][$key] = $structure[$venue_index][$key] + $value;

                        }

                    }

                    $zone_index = array_search($warning->Bancada, array_column($structure[$venue_index]['zones'], 'description'));

                    if ($zone_index > -1) {

                        $gate_index = array_search($warning->Porta, array_column($structure[$venue_index]['zones'][$zone_index]['gates'], 'description'));

                        if (!($gate_index > -1)) {

                            $structure[$venue_index]['zones'][$zone_index]['gates'][] = array(
                                'description'   => $warning->Porta,
                            );

                            $gates_structure[] = array(
                                'description'   => $warning->Porta,
                            );

                            $gates_structure_index = array_search($warning->Porta, array_column($gates_structure, 'description'));

                            $new_gate_index = array_search($warning->Porta, array_column($structure[$venue_index]['zones'][$zone_index]['gates'], 'description'));

                            foreach ($warning as $key => $value) {

                                if (!in_array($key, $parse_attributes)) {

                                    $structure[$venue_index]['zones'][$zone_index][$key] = $structure[$venue_index]['zones'][$zone_index][$key] + $value;
                                    $structure[$venue_index]['zones'][$zone_index]['gates'][$new_gate_index][$key] = $value;

                                    $gates_structure[$gates_structure_index][$key] = $value;

                                }

                            }

                        }


                    } else {

                        $structure[$venue_index]['zones'][] = array(
                            'description'       => $warning->Bancada,
                            'gates'             => array(
                                array(
                                    'description'       => $warning->Porta
                                )
                            )
                        );

                        $gates_structure[] = array(
                            'description'   => $warning->Porta,
                        );

                        $gates_structure_index = array_search($warning->Porta, array_column($gates_structure, 'description'));

                        $new_zone_index = array_search($warning->Bancada, array_column($structure[$venue_index]['zones'], 'description'));
                        $new_gate_index = array_search($warning->Porta, array_column($structure[$venue_index]['zones'][$new_zone_index]['gates'], 'description'));

                        foreach ($warning as $key => $value) {

                            if (!in_array($key, $parse_attributes)) {

                                $structure[$venue_index]['zones'][$new_zone_index][$key] = $value;
                                $structure[$venue_index]['zones'][$new_zone_index]['gates'][$new_gate_index][$key] = $value;

                                $gates_structure[$gates_structure_index][$key] = $value;

                            }

                        }

                    }

                } else {

                    $structure[] = array(
                        'description'   => $warning->Instalacao,
                        'zones'         => array(
                            array(
                                'description'   => $warning->Bancada,
                                'gates'         => array(
                                    array(
                                        'description'       => $warning->Porta
                                    )
                                )
                            )
                        )
                    );

                    $gates_structure[] = array(
                        'description'   => $warning->Porta,
                    );

                    $gates_structure_index = array_search($warning->Porta, array_column($gates_structure, 'description'));

                    $venue_index = array_search($warning->Instalacao, array_column($structure, 'description'));

                    $zone_index = array_search($warning->Bancada, array_column($structure[$venue_index]['zones'], 'description'));

                    $gate_index = array_search($warning->Porta, array_column($structure[$venue_index]['zones'][$zone_index]['gates'], 'description'));

                    foreach ($warning as $key => $value) {

                        if (!in_array($key, $parse_attributes)) {

                            $structure[$venue_index][$key] = $value;
                            $structure[$venue_index]['zones'][$zone_index][$key] = $value;
                            $structure[$venue_index]['zones'][$zone_index]['gates'][$gate_index][$key] = $value;

                            $gates_structure[$gates_structure_index][$key] = $value;

                        }

                    }

                }

            }

            $gates_desc = array();

            foreach ($gates_structure as $key => $value) {

                $gates_desc[$key] = $value["description"];

            }

            array_multisort($gates_desc, SORT_NATURAL, $gates_structure);

            return response()->json($gates_structure, 200);

        } catch (\Exception $e) {

            Log::error('Warnings By Gates');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

}