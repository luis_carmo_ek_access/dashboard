<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Exception\GuzzleException;
use Mockery\Exception;
use GuzzleHttp\Client;
use App\Events;

class EventsController extends Controller {

    protected $connection = 'access';

    /**
     * Create a new controller instance
     * 
     * @return void
     */
    public function __contruct() {
        $this->middleware('auth');
    }

    /**
     * Get Events
     * 
     * @return array
     */
    public function getEvents() {

        try {

            /**
             * 
             * Production Query
             * 
             */

            $events_list = DB::connection('access')->select("
                SELECT
                    event.id,
                    event.code, 
                    CAST(event.code AS SIGNED) AS bin_code,
                    event.descricao,
                    event_associated.template_inst_id,
                    (
                        SELECT 
                            SUM(template_inst.lotacao) AS total
                        FROM
                            gestacess_template_inst AS template_inst
                        WHERE
                            template_inst.parent_id = event_associated.template_inst_id
                    ) AS total,
                    CONCAT(event.dataInicio, ' ', event.horaAberturaPortas) AS open_gates
                FROM
                    gestacess_evento AS event
                RIGHT JOIN
                    gestacess_evento_associado AS event_associated
                    ON (event_associated.evento_id = event.id)
                WHERE
                    dataInicio >= '" . date("Y-01-01") . "'
                    AND
                    titulos_arquivados = 0
                ORDER BY
                    bin_code ASC,
                    event.code ASC
            ");

            return response()->json($events_list, 200);

        } catch (\Exception $e) {

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

    public function getEventsReports() {

        try {

            $current_year_date = date('Y-01-01');

            $events_list = DB::connection('access')->select("
                SELECT
                    event.id,
                    event.code,
                    CAST(event.code AS SIGNED) AS bin_code,
                    event.descricao,
                    CONCAT(event.dataInicio, ' ', event.horaAberturaPortas) AS open_gates
                FROM
                    gestacess_evento AS event
                WHERE
                    dataInicio >= '" . $current_year_date . "'
                    AND
                    titulos_arquivados = 0
                ORDER BY
                    bin_code ASC,
                    event.code ASC
            ");

            return response()->json($events_list, 200);

        } catch (\Exception $e) {

            Log::warning('Error Report Events');

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

}