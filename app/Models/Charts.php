<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 18-07-2018
 * Time: 13:49
 */

namespace App\Models;


use App\Events\GraphUpdate;

class Charts extends Model {

    protected $dispatchesEvents = [
        'update' => GraphUpdate::class
    ];

    /**
     *  The attributes that are mass assignable.
     *
     * @var array
     *
     */
    protected $fillable = [
        'chart_id',
        'name',
        'description',
        'value'
    ];

}