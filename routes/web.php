<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| Middleware options can be located in `app/Http/Kernel.php`
|
 */

// Homepage Route
//Route::get('/', 'WelcomeController@welcome')->name('welcome');

// Authentication Routes
use App\Events\TransactionCreated;

Auth::routes();

// Public Resource Route
Route::get('material.min.css.template', 'ThemesManagementController@template');

// Public Routes
Route::group(['middleware' => 'web'], function () {
    // Activation Routes
    Route::get('/activate', ['as' => 'activate', 'uses' => 'Auth\ActivateController@initial']);

    Route::get('/activate/{token}', ['as' => 'authenticated.activate', 'uses' => 'Auth\ActivateController@activate']);
    Route::get('/activation', ['as'       => 'authenticated.activation-resend', 'uses'       => 'Auth\ActivateController@resend']);
    Route::get('/exceeded', ['as'         => 'exceeded', 'uses'         => 'Auth\ActivateController@exceeded']);

    // Socialite Register Routes
    Route::get('/social/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
    Route::get('/social/handle/{provider}', ['as'   => 'social.handle', 'uses'   => 'Auth\SocialController@getSocialHandle']);

    // Route to for user to reactivate their user deleted account.
    Route::get('/re-activate/{token}', ['as' => 'user.reactivate', 'uses' => 'RestoreUserController@userReActivate']);
});

// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated']], function () {

    // Homepage Route
    Route::get('/home', ['as' => 'public.home', 'uses' => 'UserController@index']);
    Route::get('/', 'ChartController@index');

    // Activation Routes
    Route::get('/activation-required', ['uses' => 'Auth\ActivateController@activationRequired'])->name('activation-required');
    Route::get('/logout', ['uses'              => 'Auth\LoginController@logout'])->name('logout');

    //  Homepage Route - Redirect based on user role is in controller.
    Route::get('/home', ['as' => 'public.home', 'uses' => 'UserController@index']);

    // Show users profile - viewable by other users.
    Route::get('profile/{username}', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@show',
    ]);

    // Route to show user avatar
    Route::get('images/profile/{id}/avatar/{image}', [
        'uses' => 'ProfilesController@userProfileAvatar',
    ]);

    // Route for user profile background image
    Route::get('images/profile/{id}/background/{image}', [
        'uses' 		=> 'ProfilesController@userProfileBackgroundImage',
    ]);

    // Charts Page Route
    Route::get('/charts', 'ChartController@index');
    Route::get('/chart_view', 'ChartController@chart');

});

// Registered, activated, and is current user routes.
Route::group(['middleware' => ['auth', 'activated', 'currentUser']], function () {
    // User Profile and Account Routes
    Route::resource(
        'profile',
        'ProfilesController', [
            'only' => [
                'account',
                'show',
                'edit',
                'update',
                'create',
            ],
        ]
    );
    Route::put('profile/{username}/updateUserAccount', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@updateUserAccount',
    ]);
    Route::put('profile/{username}/updateUserPassword', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@updateUserPassword',
    ]);
    Route::delete('profile/{username}/deleteUserAccount', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@deleteUserAccount',
    ]);

    // Route for user profile background image
    Route::get('account', [
        'as'   	=> '{username}',
        'uses' 	=> 'ProfilesController@account',
    ]);

    // Update User Profile Ajax Route
    Route::post('profile/{username}/updateAjax', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@update',
    ]);

    // Route to upload user avatar.
    Route::post('avatar/upload', ['as' => 'avatar.upload', 'uses' => 'ProfilesController@upload']);

    // Route to uplaod user background image
    Route::post('background/upload', ['as' => 'background.upload', 'uses' => 'ProfilesController@uploadBackground']);

    // User Tasks Routes
    Route::resource('/tasks', 'TasksController');
});

// Registered, activated, and is admin routes.
Route::group(['middleware' => ['auth', 'activated', 'role:admin']], function () {
    Route::resource('/users/deleted', 'SoftDeletesController', [
        'only' => [
            'index', 'show', 'update', 'destroy',
        ],
    ]);
    Route::resource('users', 'UsersManagementController', [
        'names'    => [
            'index'   => 'users',
            'create'  => 'create',
            'destroy' => 'user.destroy',
        ],
        'except' => [
            'deleted',
        ],
    ]);
    Route::resource('themes', 'ThemesManagementController', [
        'names'    => [
            'index'   => 'themes',
            'destroy' => 'themes.destroy',
        ],
    ]);
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('php', 'AdminDetailsController@listPHPInfo');
    Route::get('routes', 'AdminDetailsController@listRoutes');

    Route::get('chart_view', 'ChartController@chart');
    Route::get('chart_api', 'ChartController@response');
    Route::get('charts', 'ChartController@index');

    Route::get('reports', 'Reports\ReportsController@getReports');
    Route::get('reports/{report_id}/generate', 'Reports\ReportsController@addReport');
    Route::get('reports/{report_id}/error', 'Reports\ReportsController@errorReport');
    Route::get('reports/{report_id}/extra_tcp', 'Reports\ReportsController@extraTcpReport');
    Route::get('reports/{report_id}/box_resume', 'Reports\ReportsController@boxResume');

    Route::get('reports/{report_id}/csvs', 'Reports\ReportsController@csvsReport');
    Route::post('reports/csvs/generate', 'Reports\CsvReportsController@csvsReportGenerate');
    Route::post('reports/csvs/generate/whitelist', 'Reports\CsvReportsController@csvsReportGenerateWhitelist');
    Route::post('reports/csvs/generate/whitelist_masters', 'Reports\CsvReportsController@csvsReportGenerateWhitelistMasters');
    Route::post('reports/csvs/generate/blacklist', 'Reports\CsvReportsController@csvsReportGenerateBlacklist');
    Route::post('reports/csvs/generate/entries', 'Reports\CsvReportsController@csvsReportGenerateEntries');
    Route::post('reports/csvs/generate/entries_app', 'Reports\CsvReportsController@csvsReportGenerateEntriesApp');
    Route::post('reports/csvs/generate/entries_presidential_box', 'Reports\CsvReportsController@csvsReportGenerateEntriesPresidentialBox');
    Route::post('reports/csvs/generate/entries_boxes', 'Reports\CsvReportsController@csvsReportGenerateEntriesBoxes');
    Route::post('reports/csvs/generate/entries_gate_category', 'Reports\CsvReportsController@csvsReportGenerateEntriesGateCategory');
    Route::post('reports/csvs/generate/errors_unique_title', 'Reports\CsvReportsController@csvsReportGenerateErrorsUniqueTitle');
    Route::post('reports/csvs/generate/errors_transactions', 'Reports\CsvReportsController@csvsReportGenerateErrorsTransactions');
    Route::post('reports/csvs/generate/fill_level_minute', 'Reports\CsvReportsController@csvsReportGenerateFillLevelMinte');
    Route::post('reports/csvs/generate/entries_minute_by_title', 'Reports\CsvReportsController@csvsReportGenerateEntriesMinuteByTitle');
    Route::post('reports/csvs/generate/transactions_minute_entries', 'Reports\CsvReportsController@csvsReportGenerateTransactionsMinuteEntries');

});

Route::group(['middleware' => ['auth', 'activated']], function () {
    
    /**
     * return @events
     */

    Route::get('events/get', 'EventsController@getEvents');
    Route::get('events/report/get', 'EventsController@getEventsReports');
    Route::get('tickettypes', 'TicketTypes\TicketTypesController@getTicketTypes');

});

Route::group(['prefix' => 'event', 'middleware' => ['auth', 'activated']], function () {
    
    /**
     * return @charts
     */

    Route::get('{event_id}/flowrate', 'Charts\FlowrateController@getFlowrate')->name('flowrate');
    Route::post('{event_id}/flowrate/{filter_type}/filter', 'Charts\FlowrateController@getFlowrateFiltered');

    Route::get('{event_id}/filllevel', 'Charts\FillLevelController@getFillLevel');
    Route::post('{event_id}/filllevel/{filter_type}/filter', 'Charts\FillLevelController@getFillLevelFiltered');

    Route::get('{event}/warnings', 'Charts\WarningsController@getWarnings');
    Route::post('{event_id}/warnings/{filter_type}/filter', 'Charts\WarningsController@getWarningsFiltered');
    Route::get('{event_id}/warnings/gates_information', 'Charts\WarningsController@getWarningsByGates');
    Route::get('{event_id}/warnings/gates_information_filtered', 'Charts\WarningsController@getWarningsByGatesFiltered');

    Route::get('{event_id}/gates_information', 'Charts\GatesInformationController@getGatesInformation')->name('gates_information');

    /**
     * Transactions
     */

    Route::get('{event_id}/transactions', 'Transactions\Search@getAllTransactions');
    Route::post('{event_id}/{search_type}', 'Transactions\Search@searchTicket');

});

/**
 * 
 * Ticket Types
 * 
 */

 Route::group(['middleware' => ['auth', 'activated']], function() {

    Route::get('tickettypes/active', 'TicketTypes\TicketTypesController@getActiveTicketTypes');

    /**
     * 
     * return @TicketTypes
     * 
     */

    /*
    Route::resource(
        'tickettypes',
        'TicketTypes\TicketTypesController', [
            'only' => [
                //'show',
                'edit',
                'update',
            ],
        ]
    );
    */

     Route::get('tickettypes/create', 'TicketTypes\TicketTypesController@create');
     Route::post('tickettypes/add', 'TicketTypes\TicketTypesController@add');
     Route::put('tickettypes/{id}/update', 'TicketTypes\TicketTypesController@update');
     Route::get('tickettypes/{id}/edit', 'TicketTypes\TicketTypesController@edit');

 });

Route::group(['middleware' => ['auth', 'activated'], 'prefix' => 'tickettypes/{tickettype_id}'], function() {

    Route::post('update', 'TicketTypes\TicketTypesController@update');
    Route::delete('delete', 'TicketTypes\TicketTypesController@delete');
    //Route::get('show', 'TicketTypes\TicketTypesController@show');

});

/**
 *
 * return Report
 *
 */

Route::group(['prefix' => 'event/{event_id}/report'], function () {

    Route::get('zone_entries', 'Reports\ReportsController@getZoneEntries');
    Route::get('masters_entries', 'Reports\ReportsController@getMastersEntries');
    Route::get('cam_entries', 'Reports\ReportsController@getEntriesByCam');
    Route::get('cam_entries_extra', 'Reports\ReportsController@getEntriesByCamWithExtra');
    Route::get('box_resume', 'Reports\ReportsController@getBoxResume');
    Route::get('ticket_type_entries', 'Reports\ReportsController@getTicketTypeEntries');
    Route::get('flowrate', 'Reports\ReportsController@getFlowrate');
    Route::get('entries_by_group/park/{event_park_id}/credential/{event_credential_id}', 'Reports\ReportsController@getEntriesByGroup');
    Route::get('cred/top_entities', 'Reports\ReportsController@getTopEntitiesEntries');
    Route::get('transactions_error_message', 'Reports\ReportsController@getTransactionsErrorByMessage');
    Route::get('unique_title_error_message', 'Reports\ReportsController@getUniqueTitleErrorByMessage');

    Route::get('errors', 'Reports\ReportsController@generateErrorReport');
    //Route::get('errors', 'Reports\ReportsController@generateErrorReport');

});