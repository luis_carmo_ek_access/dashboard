<?php
/**
 * User: luis carmo
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'templateTitle'                     => 'Editar Tipo Bilhete',
    'addTicketType'                     => 'Adicionar Tipo Bilhete',
    'editTicketTypeTitle'               => 'Configurações Tipo Bilhete',

    'showing_tickettypes'               => 'Lista Tipos de Bilhete',
    'single_total_row'                  => 'Tipo Bilhete',
    'total_rows'                        => 'Tipos Bilhete',
    'empty_rows'                        => 'Não Existem Tipos de Bilhete',
    'description'                       => 'Descrição',
    'value'                             => 'Valor',
    'available_filters'                 => 'Disponível p/ Filtragem',
    'active'                            => 'Ativo',
    'created_at'                        => 'Adicionado',
    'updated_at'                        => 'Atualizado',
    'actions'                           => 'Ações',
    'view'                              => 'Ver Tipo Bilhete',
    'edit'                              => 'Editar Tipo Bilhete',
    'delete'                            => 'Eliminar Tipo Bilhete',
    'add_new_tickettype'                => 'Adicionar Tipo Bilhete',
    'show_deleted_tickettypes'          => 'Tipos Bilhete Eliminados',
    'details'                           => 'Detalhes',
    'return'                            => 'Voltar Tipos Bilhete',
    'cantAccessPage'                    => 'Impossivel entrar na página',
    'description'                       => 'Descricação',
    'external_id'                       => 'ID Externo (CA)',
    'access_value'                      => 'Valor Variável Acesso',
    'back_to_tickettypes'               => 'Voltar',
    'save_ticket_type'                  => 'Guardar',
    'delete'                            => 'Apagar',

    'error_description'                 => 'Letras e Números apenas',
    'error_external_id'                 => 'Números apenas e Deverá ser Único',
    'error_access_value'                => 'Letras e Números apenas e Deverá ser Único'
];