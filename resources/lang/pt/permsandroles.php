<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-07-2018
 * Time: 13:31
 */

return [

    // Permissions
    'permissionView'      => 'Ver',
    'permissionCreate'    => 'Adicionar',
    'permissionEdit'      => 'Editar',
    'permissionDelete'    => 'Remover',

];
