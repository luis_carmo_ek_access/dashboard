<?php
/**
 * User: luis carmo
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'refresh'                   => 'Atualizar',
    'print'                     => 'Imprimir',
    'message'                   => 'Mensagens Erro',
    'quantity'                  => 'Quantidade',
    'event_select'              => 'Selecionar Evento',
    'hour_select'               => 'Selecionar Hora',
    'event'                     => 'Evento',
    'button_select'             => 'Selecionar',
    'button_cancel'             => 'Cancelar',
    'choose_event'              => 'Escolher Evento',
    'refresh_all'               => 'Atualizar',
    'select-time-date'          => 'Selecionar Data',
    'begin_filter_date'         => 'Data/Hora Inicio',
    'end_filter_date'           => 'Data/Hora Fim',
    'occurrences'               => 'Ocorrências',
    'occurrences_at'            => 'Occorências em ',
    
    /**
     * Filters
     */

    'default_filter'            => 'Padrão',
    'ticket_type_filter'        => 'Tipos Bilhete',
    'zone_dac_filter'           => 'Zona / DAC',
    'hour_filter'               => 'Hora',
    'filter_hour_general'       => 'Global',
    'filter_hour_ticket_type'   => 'Tipos de Bilhete',
    'filter_type_search'        => 'Tipo Pesquisa',
    'total'                     => 'Total',
    'unavailable'               => 'Indisponivel',
    'search_by_barcode'         => 'Pesquisar Titulo',
    'search_by_gate'            => 'Pesquisar Porta',
    'ticket_barcode'            => 'Código Barras',
    'gate_filter'               => 'Porta',

    /**
     * 
     * labels
     * 
     */

    'ticket_type_label'         => 'Tipo Bilhete',
    'zone_label'                => 'Zona',
    'dac_label'                 => 'Dac',
    'text_gates_error'          => 'Erros por Porta',
    'description'               => 'Descrição',
    'gate'                      => 'Porta',

    /**
     *
     * Columns Names
     *
     */

    'column_pe'                 => 'PE: Porta Errada',
    'column_td'                 => 'TD: Titulo Desconhecido',
    'column_qa'                 => 'QA: Quotas em Atraso',
    'column_be'                 => 'BE: Bilhete Estornado',
    'column_va'                 => 'VA: Via Anulada',
    'column_je'                 => 'JE: Já Entrou',
    'column_scb'                => 'SCB: Sócio com Bilhete',
    'column_nscb'               => 'NSCB: Não Sócio com Bilhete',
    'column_ca'                 => 'CA: Cartão Anulado',
    'column_laa'                => 'LAA: Lugar Anual Anulado',
    'column_ctct'               => 'CTCT: Controlo TCT',
    'column_sac'                => 'SAC: Solicite Apoio Controlo',
    'column_ssr'                => 'SSR: Sócio Sem Renumeração',

    /**
     *
     * Search Table
     *
     */

    'text_barcode'              => 'Titulo',
    'text_zone'                 => 'Zona',
    'text_dac'                  => 'Dac',
    'text_is_entry'             => 'Entrada',
    'text_message'              => 'Mensagem',
    'text_datetime'             => 'Data / Hora',
    'text_yes'                  => 'Sim',
    'text_no'                   => 'no',

    /**
     *
     * errors
     *
     */

    'error_not_number'          => 'Por favor insira um número válido!',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
