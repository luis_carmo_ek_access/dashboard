<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-07-2018
 * Time: 13:46
 */

return [

    // Flash Messages
    'createSuccess'     => 'Utilizador criado com sucesso! ',
    'updateSuccess'     => 'Utilizador modificado com sucesso! ',
    'deleteSuccess'     => 'Utilizador removido com sucesso! ',
    'deleteSelfError'   => 'Não Pode Remover a Sua Conta! ',

    // Show User Tab
    'viewProfile'		     => 'Ver Perfil',
    'editUser'			     => 'Editar Utilizador',
    'deleteUser'		     => 'Remover Utilizador',
    'usersBackBtn'           => 'Voltar a Utilizadores',
    'usersPanelTitle'        => 'Informações Utilizador',
    'labelUserName'          => 'Username:',
    'labelEmail'             => 'E-mail:',
    'labelFirstName'         => 'Primeiro Nome:',
    'labelLastName'          => 'Último Nome:',
    'labelRole'              => 'Grupo:',
    'labelStatus'            => 'Estado:',
    'labelAccessLevel'       => 'Acesso',
    'labelPermissions'       => 'Permissões:',
    'labelCreatedAt'         => 'Criado a:',
    'labelUpdatedAt'         => 'Modificado a:',
    'labelIpEmail'           => 'IP E-mail Acesso:',
    'labelIpEmail'           => 'IP E-mail Acesso:',
    'labelIpConfirm'         => 'IP Acesso:',
    'labelIpSocial'          => 'IP Acesso Social:',
    'labelIpAdmin'           => 'IP Acesso Administração:',
    'labelIpUpdate'          => 'Última Atualização IP:',
    'labelDeletedAt'	     => 'Removido a',
    'labelIpDeleted'	     => 'IP Removido:',
    'usersDeletedPanelTitle' => 'Remover Informações Utilizador',
    'usersBackDelBtn'	     => 'Voltar a Utilizadores Removidos',

    'successRestore' 	     => 'Utilizador Restaurado com Sucesso.',
    'successDestroy' 	     => 'Registo Utilizador Removido Permanentemente.',
    'errorUserNotFound'      => 'Utilizador Não Encontrado.',

    'labelUserLevel'	     => 'Nível',
    'labelUserLevels'	     => 'Níveis',

];
