<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-07-2018
 * Time: 13:41
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Socialite and Social Media Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'denied'          => 'Não partilhou o seu perfil com a nossa aplicação. ',
    'noProvider'      => 'Fonte Desconhecida. ',
    'registerSuccess' => 'Registo Efectuado com Sucesso! ',

];
