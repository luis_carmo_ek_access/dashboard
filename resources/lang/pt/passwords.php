<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-07-2018
 * Time: 13:28
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords devem ter no mínimo 6 (seis) caracteres e ser igual à confirmação da mesma.',
    'reset'    => 'A sua password foi alterada!',
    'sent'     => 'Foi-lhe enviado um e-mail com o link de alteração da password!',
    'token'    => 'O token de alteração de password é inválido.',
    'user'     => "Não foi possível encontrar um utilizador associado ao e-mail.",

];
