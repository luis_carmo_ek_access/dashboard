<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-07-2018
 * Time: 13:19
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Emails Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various emails that
    | we need to display to the user. You are free to modify these
    | language lines according to your application's requirements.
    |
    */

    /*
     * Activate new user account email.
     *
     */

    'activationSubject'  => 'Ativação Requerida',
    'activationGreeting' => 'Bem-Vindo!',
    'activationMessage'  => 'É necessário ativar o seu E-mail para que possa usar todos os nossos serviços.',
    'activationButton'   => 'Ativar',
    'activationThanks'   => 'Obrigado!',

    /*
     * Goobye email.
     *
     */
    'goodbyeSubject'    => 'Lamenta-mos vê-lo partir...',
    'goodbyeGreeting'   => 'Olá :username,',
    'goodbyeMessage'    => 'Lamenta-mos vê-lo partir. Queremos que saiba que a sua conta foi removida. Obrigado pelo tempo que passou connosco. Tem '.config('settings.restoreUserCutoff').' dias para restaurar a sua conta.',
    'goodbyeButton'     => 'Restaurar Conta',
    'goodbyeThanks'     => 'Esperamos vê-lo em breve!',

];
