<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-07-2018
 * Time: 12:46
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Dados de Acesso Inválidos.',
    'throttle' => 'Demasiadas Tentativas de Login. Por favor tente novamente daqui a :seconds segundos.',

    // Titles
    'loginPageTitle'    => 'Login',

    // Activation items
    'sentEmail'         => 'Foi enviado um E-mail para :email.',
    'clickInEmail'      => 'Por favor clique no link para ativar a sua conta.',
    'anEmailWasSent'    => 'Um E-mail foi enviado para :email a :date.',
    'clickHereResend'   => 'Clique aqui para re-enviar o E-mail.',
    'successActivated'  => 'Sucesso: A sua conta foi ativada.',
    'unsuccessful'      => 'A sua conta não pode ser ativada de momento. Tente novamente.',
    'notCreated'        => 'A sua conta não pode ser criada. Tente novamente.',
    'tooManyEmails'     => 'Foram enviados demasiados E-mails de ativação para :email. <br />Por favor tente novamente daqui a <span class="label label-danger">:hours horas</span>.',
    'regThanks'         => 'Obrigado por se registrar, ',
    'invalidToken'      => 'Token Ativação Inválidos. ',
    'activationSent'    => 'E-maill de ativação enviado. ',
    'alreadyActivated'  => 'A conta já está ativa. ',
    'confirmation'      => 'Confirmação Enviada',

    // Labels
    'whoops'            => 'Whoops!',
    'someProblems'      => 'Erro',
    'email'             => 'E-Mail',
    'password'          => 'Password',
    'rememberMe'        => ' Lembrar-me',
    'login'             => 'Login',
    'logout'            => 'Logout',
    'forgot'            => 'Esqueceu a Password?',
    'forgot_message'    => 'Problemas com a Password?',
    'name'              => 'Username',
    'first_name'        => 'Primeiro Nome',
    'last_name'         => 'Último Nome',
    'confirmPassword'   => 'Confirmar Password',
    'register'          => 'Registar',
    'success'           => 'Sucesso',
    'message'           => 'Ver Mensagem',

    'noticeTitle'       => 'Aviso',

    // Placeholders
    'ph_name'           => 'Username',
    'ph_email'          => 'E-mail',
    'ph_firstname'      => 'Primeiro Nome',
    'ph_lastname'       => 'Último Nome',
    'ph_password'       => 'Password',
    'ph_password_conf'  => 'Confirmar Password',

    // User flash messages
    'sendResetLink'     => 'Enviar Link Alterar Passowrd',
    'resetPassword'     => 'Alterar Password',
    'loggedIn'          => 'Bem-Vindo!',

    // email links
    'pleaseActivate'    => 'Por favor ative a sua conta.',
    'clickHereReset'    => 'Clique aqui para alterar a password: ',
    'clickHereActivate' => 'Clique aqui para ativar a conta: ',

    // Validators
    'userNameTaken'     => 'Username Registado',
    'userNameRequired'  => 'Username Requerido',
    'fNameRequired'     => 'Primeiro Nome Requerido',
    'lNameRequired'     => 'Último Nome Requerido',
    'emailRequired'     => 'E-mail Requerido',
    'emailInvalid'      => 'E-mail Inválido',
    'passwordRequired'  => 'Password Requerida',
    'PasswordMin'       => 'Password tem que ter no mínimo 6 caracteres',
    'PasswordMax'       => 'Password tem que ter no máximo 20 caracteres',
    'captchaRequire'    => 'Captcha Requerido',
    'CaptchaWrong'      => 'Captcha Errado, por favor tente novamente.',
    'roleRequired'      => 'Grupo Requerido.',

    'emailLoginError'   => 'Por favor insira um E-mail Válido.',
    'pwLoginError'      => 'Por favor insira a Password.',

];
