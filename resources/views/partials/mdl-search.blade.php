<div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable search-white">
	<label class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-button--icon" for="search_table" title="{{ trans('general.search') }}">
	  	<i class="material-icons">search</i>
	  	<span class="sr-only">{{ trans('general.search') }}</span>
	</label>
	<div class="mdl-textfield__expandable-holder">
	  	<input class="mdl-textfield__input" type="search" id="search_table" placeholder="{{ trans('general.search') }}">
	  	<label class="mdl-textfield__label" for="search_table">
			{{ trans('general.search') }}
	  	</label>
	</div>
</div>