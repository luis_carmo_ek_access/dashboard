<style scoped>

    #report-load-charts {
        border-radius: 100px;
    }

    #report-load-charts > .mdl-snackbar__text {

        width: 100%;
        text-align: center;

    }

    #report-load-charts > .mdl-snackbar__action {

        display: none;

    }

    #report-body #header-logo #header-image {

        width: 100%;

    }

    #report-dialog {

        width: 250mm;

    }

    #report-dialog-content {

        /*width: 250mm;*/

    }

    .align-right {
        float: right;
    }

    div#report-body {
        overflow-x: auto;
        overflow-y: auto;
        max-height: 700px;
    }

    #report-header {

        width: 100%;
        padding-top: 40px;
        padding-bottom: 40px;

    }

    .report-header-title {

        width: 100%;
        text-align: center;

    }

    table#cam-entries-table {
        width: 70%;
        margin-left: 15%;
        margin-right: 15%;
    }

    th.cam-name, th.cam-entity, th.cam-entries, th.cam-extra-child th.cam-extra-adult {

        text-align: center !important;

    }

    td.cam-name, td.cam-entity, td.cam-entries, td.cam-extra-child, td.cam-extra-adult {

        text-align: center !important;

    }

    td.cam-total, td.cam-total-value {

        text-align: center !important;

    }

    tr.total-cam-entries {

        background-color: #30BDCC !important;

    }

    @media only screen and (max-width: 720px) {

        #report-dialog {
            background: #ffffff !important;
            -webkit-print-color-adjust: exact;
        }

        dialog#report-dialog {

            width: 100%;

        }

        #report-dialog {

            width: 100%;

        }

        #report-dialog-content {

            width: 100%;

        }

        #report-header {

            width: 100%;
            padding-top: 40px;
            padding-bottom: 40px;

        }

        .report-header-title {

            width: 100%;
            text-align: center;
            color: #30BDCC;

        }

    }

    @media print {

        #report-dialog {
            background: #ffffff !important;
            -webkit-print-color-adjust: exact;
        }

        .report-header-title:first-letter {
            color: #30BDCC !important;
        }

        .report-header-title {
            width: 100% !important;
            text-align: center !important;
            color: #30BDCC !important;
            page-break-after: always !important;
        }

    }

</style>

@extends('layouts.dashboard')

@section('template_title')
    {{ trans('reports.showing_reports') }}
@endsection

@section('template_linked_css')
@endsection

@section('header')
    {{ trans('reports.showing_reports') }}
@endsection

@section('breadcrumbs')

    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/reports">
            <span itemprop="name">
                {{ trans('reports.reports_text')  }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="2" />
    </li>
    <li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/reports/{{ $report->id }} . /generate">
        <span itemprop="name">
            {{ $report->description }}
        </span>
        </a>
        <meta itemprop="position" content="3" />
    </li>

@endsection

@section('content')

    <div id="report-options" class="mdl-grid full-grid margin-top-0 padding-0">
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">
            <div class="mdl-card card-new-report" style="width:100%;" itemscope itemtype="http://schema.org/Person">

                <div class="mdl-card__title mdl-card--expand mdl-color--primary mdl-color-text--white">
                    <h2 class="mdl-card__title-text logo-style">{{ trans('reports.generate_report') }}</h2>
                </div>

                <div class="mdl-card__supporting-text">
                    <div class="mdl-grid full-grid padding-0">

                        <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">

                            <div id="report-details" class="mdl-grid ">

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="event-options mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-select mdl-select__fullwidth is-dirty">
                                        <select id="report-event" class="mdl-selectfield__select mdl-textfield__input" name="report-event">
                                        </select>
                                        <label for="report-event">
                                            <i class="mdl-icon-toggle__label material-icons">arrow_drop_down</i>
                                        </label>
                                        {!! Form::label('event', trans('forms.label-event_id'), array('class' => 'mdl-textfield__label mdl-selectfield__label')); !!}
                                        <span id="error-select-event" class="mdl-textfield__error">{{ trans('reports.error_event')  }}</span>
                                    </div>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-dirty">
                                        {!! Form::text('report_name', '', array('id' => 'report_name', 'class' => 'mdl-textfield__input is-dirty')) !!}
                                        <label class="mdl-textfield__label" for="report_name">{{ trans('reports.event_report_name') }}</label>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

                <div class="mdl-card__menu mdl-color-text--white">

                    <span class="save-actions">
                        {!! Form::button('<i class="material-icons">remove_red_eye</i>', array('id' => 'view-report', 'class' => 'dialog-button-icon-save mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect', 'disabled' => 'disabled', 'title' => trans('reports.view_report')  )) !!}
                    </span>

                    <a href="{{ url('/reports/') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="{{ trans('reports.back_to_reports') }}">
                        <i class="material-icons">reply</i>
                        <span class="sr-only">{{ trans('reports.back_to_reports') }}</span>
                    </a>

                </div>

            </div>
        </div>
    </div>

    <dialog id="report-dialog" class="mdl-dialog" style="background: white !important;">

        <div id ="report-dialog-content" class="mdl-dialog__content">

            <div id="report-body">

                <div id="header-logo"><img id="header-image" src="/images/bannerHeaderEK.png"> </div>

                <div id="report-header">
                    <h2 class="report-header-title" style="color: #30BDCC !important;">{{trans('reports.report_box_resume_header') }}</h2>
                </div>

                <div id="report-data" class="break-page">

                    <table id="cam-entries-table" class="mdl-data-table mdl-js-data-table mdl-data-table">
                        <thead class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th class="cam-name">{{trans('reports.text_cam_name')}}</th>
                            <th class="cam-entity">{{trans('reports.text_entity')}}</th>
                            <th class="cam-entries">{{trans('reports.text_entries')}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>

                </div>

            </div>

        </div>

        <div id="report-actions" class="mdl-dialog__actions--full-width">

            <button type="button" id="close-report-view-button" class="mdl-button close">{{ trans('charts.button_cancel') }}</button>
            <button type="button" id="print-report-view-button" class="mdl-button confirm-action align-right print-report">{{ trans('reports.generate_report') }}</button>

        </div>

    </dialog>

    <div id="report-load-charts" class="mdl-js-snackbar mdl-snackbar">
        <div class="mdl-snackbar__text"></div>
        <button class="mdl-snackbar__action" type="button"></button>
    </div>

@endsection

@section('footer_scripts')

    <script>

        $(window).ready(function () {

            $('#report_name').parent().addClass('is-dirty');

            componentHandler.upgradeDom();

        });

        $('#report-event').on('change', function () {

            removeSelectionError('event');
            validateReportLoading();

        });

        $('#view-report').on('click', function() {

            var dialog = document.querySelector('#report-dialog');

            dialog.showModal();

        });

        $('#close-report-view-button').on('click', function() {

            var dialog = document.querySelector('#report-dialog');

            dialog.close();

        });

        function removeSelectionError(event_type) {

            if (event_type == 'event') {

                var event_id = $('#report-event option:selected').val();

                if (event_id !== "undefined" && event_id != '') {

                    document.getElementById("error-select-event").style.visibility = "hidden";
                    $('#report-event').parent().removeClass('is-invalid');

                }

            }
        }

        function validateReportLoading() {

            var event_id = $('#report-event option:selected').val();

            if (event_id !== "undefined" && event_id != '') {

                removeSelectionError(event_id);

                loadReport(event_id);

            } else {

                eventsSelectionError(event_id);

            }

        }

        function eventsSelectionError(event_id) {

            if (event_id == "undefined" || event_id == '') {
                document.getElementById("error-select-event").style.visibility = "visible";
                $('#report-event').parent().addClass('is-invalid');
            }


        }

        function loadReport(event_id) {

            var snackbarContainer = document.querySelector('#report-load-charts');

            var data = {message: '{{ trans('reports.loading_charts') }}' };

            snackbarContainer.MaterialSnackbar.showSnackbar(data);

            $('#cam-entries-table tbody > *').remove();

            $('#report-event').attr('disabled', 'disabled');
            $('#view-report').attr('disabled', 'disabled');

            $('.logo-style').parent().append('<div id="loading-report-animation" class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div>');

            componentHandler.upgradeDom();

            $.when(

                $.ajax({
                    url: '/event/' + event_id + '/report/cam_entries_extra/',
                    dataType: 'json',
                    success: function(data) {

                        var html = '';

                        var total_cam_entries = 0;

                        var total_entries = 0;

                        data.forEach(function(cam) {

                            total_cam_entries = total_cam_entries + cam.total;

                            html += '<tr class="cam-entrie-entity">';
                            html += '   <td class="cam-name">' + cam.cam + '</td>';
                            html += '   <td class="cam-entity">' + cam.entity + '</td>';
                            html += '   <td class="cam-entries">' + cam.total + '</td>';
                            html += '</tr>';

                        });

                        total_entries = total_cam_entries;

                        html += '<tr class="total-cam-entries">';
                        html += '   <td colspan="2" class="cam-total">{{trans('reports.text_total')}}</td>';
                        html += '   <td class="cam-total-value">' + total_cam_entries + '</td>';
                        html += '</tr>';

                        $('#cam-entries-table tbody').append(html);

                    }
                })

            ).then(function () {

                $('#view-report').removeAttr('disabled');
                $('#loading-report-animation').remove();
                $('#report-event').removeAttr('disabled');
                $('#report-event-park').removeAttr('disabled');
                $('#report-event-credential').removeAttr('disabled');

            });

        }

    </script>

@endsection