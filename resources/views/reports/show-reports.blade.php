<style scooped>

    th.mdl-data-table__cell--non-numeric.pull-right {
        text-align: right;
    }

    td.mdl-data-table__cell--non-numeric.pull-right {
        text-align: right;
    }

</style>

@extends('layouts.dashboard')

@section('template_title')
    {{ trans('reports.showing_reports') }}
@endsection

@section('template_linked_css')
@endsection

@section('header')
    {{ trans('reports.showing_reports') }}
@endsection

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/reports" disabled>
            <span itemprop="name">
                {{ trans('reports.reports_text')  }}
            </span>
        </a>
        <meta itemprop="position" content="2" />
    </li>
@endsection


@section('content')

    <div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--12-col-desktop margin-top-0">

        <div class="mdl-card__title mdl-color--primary mdl-color-text--white">
            <h2 class="mdl-card__title-text logo-style">
                {{ trans('reports.reports_text') }}
            </h2>
        </div>

        <div class="mdl-card__supporting-text mdl-color-text--grey-600 padding-0 context">

            <div class="table-responsive material-table">

                <table id="tickettype_table" class="mdl-data-table mdl-js-data-table data-table" cellspacing="0" width="100%">

                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">{{ trans('reports.description') }}</th>
                        <th class="mdl-data-table__cell--non-numeric pull-right">{{ trans('reports.actions') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($reports as $report)

                        @if ($report->code == 'game_report')

                            <tr>
                                <td class="mdl-data-table__cell--non-numeric"><a href="{{ URL::to('reports/' . $report->id) . '/generate' }}">{{ $report->description }}</a></td>

                                <td class="mdl-data-table__cell--non-numeric pull-right">

                                    {{-- VIEW TICKET TYPE ICON BUTTON --}}
                                    <a href="{{ URL::to('reports/' . $report->id . '/generate') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="{{ trans('tickettypes.view') }}">
                                        <i class="material-icons mdl-color-text--green">file_copy</i>
                                    </a>

                                </td>
                            </tr>

                        @elseif ($report->code == 'error_report')

                            <tr>
                                <td class="mdl-data-table__cell--non-numeric"><a href="{{ URL::to('reports/' . $report->id) . '/error' }}">{{ $report->description }}</a></td>

                                <td class="mdl-data-table__cell--non-numeric pull-right">

                                    {{-- VIEW TICKET TYPE ICON BUTTON --}}
                                    <a href="{{ URL::to('reports/' . $report->id . '/error') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="{{ trans('report.text_create_report') }}">
                                        <i class="material-icons mdl-color-text--green">file_copy</i>
                                    </a>

                                </td>
                            </tr>

                        @elseif ($report->code == 'game_csvs')

                            <tr>
                                <td class="mdl-data-table__cell--non-numeric"><a href="{{ URL::to('reports/' . $report->id . '/csvs') }}">{{ $report->description }}</a></td>

                                <td class="mdl-data-table__cell--non-numeric pull-right">

                                    <a href="{{ URL::to('reports/' . $report->id . '/csvs') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="{{ trans('report.text_create_report') }}">
                                        <i class="material-icons mdl-color-text--green">file_copy</i>
                                    </a>

                                </td>
                            </tr>

                        @elseif ($report->code == 'extra_tcp_report')

                            <tr>

                                <td class="mdl-data-table__cell--non-numeric"><a href="{{ URL::to('reports/' . $report->id . '/extra_tcp') }}">{{ $report->description }}</a></td>

                                <td class="mdl-data-table__cell--non-numeric pull-right">

                                    <a href="{{ URL::to('reports/' . $report->id . '/extra_tcp') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="{{trans('report.text_create_report')}}">
                                        <i class="material-icons mdl-color-text--green">file_copy</i>
                                    </a>

                                </td>

                            </tr>

                        @elseif ($report->code == 'box_resume')

                            <tr>

                                <td class="mdl-data-table__cell--non-numeric"><a href="{{ URL::to('reports/' . $report->id . '/box_resume') }}">{{ $report->description }}</a></td>

                                <td class="mdl-data-table__cell--non-numeric pull-right">

                                    <a href="{{ URL::to('reports/' . $report->id . '/box_resume') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="{{trans('report.text_create_report')}}">
                                        <i class="material-icons mdl-color-text--green">file_copy</i>
                                    </a>

                                </td>

                            </tr>


                        @endif

                    @endforeach
                    </tbody>

                </table>

            </div>

        </div>

        <div class="mdl-card__menu" style="top: -4px;">

            @include('partials.mdl-highlighter')

            @include('partials.mdl-search')

            <a href="{{ url('/tickettypes/create') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="{{ trans('tickettypes.add_new_tickettype') }}">
                <i class="material-icons">add</i>
                <span class="sr-only">{{ trans('tickettypes.add_new_tickettype') }}</span>
            </a>

            <a href="{{ url('tickettypes/deleted') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="{{ trans('tickettypes.show_deleted_tickettypes') }}">
                <i class="material-icons">delete_sweep</i>
                <span class="sr-only">{{ trans('tickettypes.show_deleted_tickettypes') }}</span>
            </a>

        </div>

    </div>

@endsection