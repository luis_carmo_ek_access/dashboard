<style scoped>

    .spinner {
        margin-top: -35px;
        text-align: center;
    }

    .flatpickr-calendar.hasTime.animate.showTimeInput.arrowTop.open {
        top: 55px !important;
        left: 0px !important;
    }

    .flatpickr-calendar.hasTime.animate.arrowTop.open {
        top: 55px !important;
        left: 0px !important;
    }

    .flatpickr-calendar.hasTime.animate.open.arrowBottom {
        top: 55px !important;
        left: 0px !important;
    }

    .flatpickr-calendar.animate.open.arrowTop {
        display: none;
    }

    .flatpickr-calendar .flatpickr-calendar.open {
        display: none;
    }

    div#event-datetime .flatpickr-calendar.open {
        display: inline-block;
    }

    .flatpickr-calendar.animate.open.arrowBottom {
        display: none;
    }

    #report-body #header-logo #header-image {

        width: 100%;

    }

    #report-dialog-content {

        width: 250mm; /*210.00197555866663mm; /*80%;*/
        /*max-height: 297.0006773335mm; /*600px;*/
        /*
        max-height: 500px;
        overflow-x: auto;
        overflow-y: auto;
        */

    }

    #report-header {

        width: 100%;
        padding-top: 40px;
        padding-bottom: 40px;

    }

    .report-header-title {

        width: 100%;
        text-align: center;
        /*color: #30BDCC;*/

    }

    .mdl-color--primary {
        /*background: #30BDCC !important;*/
    }

    #report-event-details{

        width: 100%;
        padding: 0 40px 0 40px;

    }

    #report-activity-registry {

        width: 100%;
        padding: 0px 40px 0 40px;

    }
    #report-data {

        width: 100%;
        padding: 80px 40px 0 0px;

    }

    #imports-entries-by-ticket-type {
        display: none;
        width: 100%;
    }

    #entries-by-masters {

        display: none;
        width: 100%;

    }

    #ticket-types-entries-list {

        display: none;
        width: 100%;

    }

    #transaction-error-message-list {

        display: none;
        width: 100%;

    }

    #unique-title-error-message-list {

        display: none;
        width: 100%;

    }

    #maximum-flowrate {

        display: none;
        width: 100%;

    }

    #total-entries-group {

        display: none;
        width: 100%;

    }

    #total-imports-group {

        display: none;
        width: 100%;

    }

    #report-occurencies-registry {

        width: 100%;
        padding: 0px 40px 0px 40px;

    }

    .align-left {
        float: left;
    }

    .align-right {
        float: right;
    }

    #report-dialog {

        width: 250mm; /*210.00197555866663mm; /*80%;*/
        /*max-height: 297.0006773335mm; /*600px;*/

    }

    .mdl-dialog__content {
        padding: 0 !important;
    }

    div#report-body {
        overflow-x: auto;
        overflow-y: auto;
        max-height: 700px;
    }

    #report-load-charts > .mdl-snackbar__text {

        width: 100%;
        text-align: center;

    }

    #report-load-charts > .mdl-snackbar__action {

        display: none;

    }

    .logo-style {
        width: 90% !important;
    }

    .mdl-spinner__layer-1 {
        border-color: #ffffff !important;
    }
    .mdl-spinner__layer-2 {
        border-color: #ffffff !important;
    }

    .mdl-spinner__layer-3 {
        border-color: #ffffff !important;
    }

    .mdl-spinner__layer-4 {
        border-color: #ffffff !important;
    }

    #report-load-charts {
        border-radius: 100px;
    }

    .datetimepicker-event-date {
        display: none;
    }

    input.flatpickr-event-date-report.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label.is-upgraded.is-focused.flatpickr-input.flatpickr-mobile {
        display: none;
    }

    .mdl-data-table tbody tr {
        height: 30px !important;
        font-size: 10px;
    }

    .mdl-data-table td {
        height: 30px !important;
    }

    @media screen and (max-width: 1024px) {

        .breadcrumb {
            width: 100%;
            padding: 1em 1em !important;
        }

        table#cam-entries-table {

            width: 100%;
            margin-left: 0%;
            margin-right: 0%;

        }

        #top-entities-credentials {

            width: 100%;
            margin-left: 0%;
            margin-right: 0%;

        }

    }

    @media only screen and (max-width: 720px) {

        #report-dialog {
            background: #ffffff !important;
            -webkit-print-color-adjust: exact;
        }

        .mdl-color--primary {
            background-color: #30BDCC; /* rgb(63,81,181)!important;*/
        }

        .custom-header-color {
            background: #30BDCC;
        }

        #report-event-details{

            width: 100%;
            padding: 0px 60px 0px 0px;

        }

        #report-activity-registry {

            width: 100%;
            padding: 00px 40px 0px 40px;

        }

        #report-data {

            width: 100%;
            padding: 40px 60px 0px 0px;

        }

        #report-occurencies-registry {

            width: 100%;
            padding: 0px 40px 0px 40px;

        }

        dialog#report-dialog {

            width: 100%;

        }

        #report-dialog {

            width: 100%;

        }

        #report-dialog-content {

            width: 100%;

        }

        #report-header {

            width: 100%;
            padding-top: 40px;
            padding-bottom: 40px;

        }

        .report-header-title {

            width: 100%;
            text-align: center;
            color: #30BDCC;

        }

        #report-event-details{

            width: 100%;
            padding: 0 40px 0 40px;

        }

        #report-activity-registry {

            width: 100%;
            padding: 0px 40px 0 40px;
            page-break-after: always !important;

        }

        #report-occurencies-registry {

            width: 100%;
            padding: 0px 40px 0px 40px;

        }

        .logo-style {
            width: 65% !important;
        }

    }

    #imports-entries-by-ticket-type.mdl-data-table th {
        /*color: #ffffff;*/
        text-align: center;
    }

    #ticket-types-entries-list.mdl-data-table th {
        /*color: #ffffff;*/
        text-align: center;
    }

    #transaction-error-message-list.mdl-data-table th {

        text-align: center;
    }

    #unique-title-error-message-list.mdl-data-table th {

        text-align: center;

    }

    #maximum-flowrate.mdl-data-table th {
        /*color: #ffffff;*/
        text-align: center;
    }

    #total-entries-group.mdl-data-table th {
        /*color: #ffffff;*/
        text-align: center;
    }

    #total-imports-group.mdl-data-table th {
        /*color: #ffffff;*/
        text-align: center;
    }

    #entries-by-masters.mdl-data-table th {
        /*color: #ffffff;*/
        text-align: center;
    }

    #imports-entries-by-ticket-type.mdl-data-table td {
        text-align: center;
    }

    #ticket-types-entries-list.mdl-data-table td {
        text-align: center;
    }

    #transaction-error-message-list.mdl-data-table td {

        text-align: center;

    }

    #unique-title-error-message-list.mdl-data-table td {

        text-align: center;

    }

    #maximum-flowrate.mdl-data-table td {
        text-align: center;
    }

    #total-entries-group.mdl-data-table td {
        text-align: center;
    }

    #total-imports-group.mdl-data-table td {
        text-align: center;
    }

    #entries-by-masters.mdl-data-table td {
        text-align: center;
    }

    td.description {
        text-align: left !important;
    }

    @media print {

        .gates-information-result {
            padding-bottom: 25px;
        }


        #total-imports-group {
            margin-top: 70px;
        }


        .mdl-data-table td {

            padding: 0px 0px;

        }

        .report-header-title:first-letter {
            color: #30BDCC !important;
        }

        #report-dialog {
            background: #ffffff !important;
            -webkit-print-color-adjust: exact;
        }

        #report-options {
            display: none !important;
        }

        .mdl-color--primary {
            background-color: #30BDCC !important; /* rgb(63,81,181)!important;*/
        }

        .custom-header-color {
            background: #30BDCC;
        }

        #imports-entries-by-ticket-type.mdl-data-table th {
            /*color: #ffffff !important;*/
            text-align: center !important;
        }

        .report-header-title {
            width: 100% !important;
            text-align: center !important;
            color: #30BDCC !important;
            page-break-after: always !important;
        }

        #imports-entries-by-ticket-type {
            display: table !important;
            width: 100% !important;
        }

        #ticket-types-entries-list {
            display: table !important;
            width: 100% !important;
        }

        #transaction-error-message-list {
            display: table !important;
            width: 100% !important;
        }

        #unique-title-error-message-list {

            display: table !important;
            width: 100% !important;

        }

        #maximum-flowrate {
            display: table !important;
            width: 100% !important;
        }

        #total-entries-group {
            display: table !important;
            width: 100% !important;
        }

        #total-imports-group {
            display: table !important;
            width: 100% !important;
        }

        #entries-by-masters {
            display: table !important;
            width: 100% !important;
        }

        #loading-report-animation {
            background: #30BDCC !important; /*rgb(63,81,181)!important;*/
        }

        td.description {
            text-align: left !important;
        }


        .gates-information-result {
            padding-bottom: 250px;
        }


        #total-imports-group {
            margin-top: 70px;
        }

        #report-header-credentials {
            margin-top: 50px;
        }

        .report-occurencies-registry {

            margin-top: 300px;

        }

    }

    #entries-by-ticket-chart, #transaction-error-message-chart {

        width: 50%;
        margin: auto;

    }

    #unique-title-error-message-chart {

        width: 50%;
        margin: auto;

    }

    .break-page {
        page-break-before: always;
    }

    #top-entities-credentials {

        width: 50%;
        margin-left: 25%;
        margin-right: 25%;

    }

    #top-entities-credentials.mdl-data-table th:first-of-type {
        text-align: left;
    }

    td.entity-name {
        text-align: left !important;
    }

    td.gate-name {
        text-align: left !important;
    }

    tr.entity-credential {
        background: #30BDCC;

    }

    .gates-information-result {

        width: 100% !important;

    }

    #gates-information-table {

        width: 100% !important;

    }

    .venue-row {

        background-color: #30BDCC !important;

    }

    .zone-row {

        background-color: #30BDCC !important;

    }

    table#cam-entries-table {
        width: 50%;
        margin-left: 25%;
        margin-right: 25%;
    }

    th.cam-name, th.cam-entity, th.cam-entries, th.cam-extra-child th.cam-extra-adult {

        text-align: center !important;

    }

    td.cam-name, td.cam-entity, td.cam-entries, td.cam-extra-child, th.cam.extra-adult {

        text-align: center !important;

    }

    td.cam-total, td.cam-total-value {

        text-align: center !important;

    }

    tr.total-cam-entries {

        background-color: #30BDCC !important;

    }

</style>

@extends('layouts.dashboard')

@section('template_title')
    {{ trans('reports.showing_reports') }}
@endsection

@section('template_linked_css')
@endsection

@section('header')
    {{ trans('reports.showing_reports') }}
@endsection

@section('breadcrumbs')

    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/reports">
            <span itemprop="name">
                {{ trans('reports.reports_text')  }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="2" />
    </li>
    <li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/reports/{{ $report->id }} . /generate">
        <span itemprop="name">
            {{ $report->description }}
        </span>
        </a>
        <meta itemprop="position" content="3" />
    </li>

@endsection

@section('content')

    <div id="report-options" class="mdl-grid full-grid margin-top-0 padding-0">
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">
            <div class="mdl-card card-new-report" style="width:100%;" itemscope itemtype="http://schema.org/Person">

                <div class="mdl-card__title mdl-card--expand mdl-color--primary mdl-color-text--white">
                    <h2 class="mdl-card__title-text logo-style">{{ trans('reports.generate_report') }}</h2>
                </div>

                <div class="mdl-card__supporting-text">
                    <div class="mdl-grid full-grid padding-0">
                        <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">

                            <div id="report-details" class="mdl-grid ">

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="event-options mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-select mdl-select__fullwidth is-dirty">
                                        <select id="report-event" class="mdl-selectfield__select mdl-textfield__input" name="report-event">
                                        </select>
                                        <label for="report-event">
                                            <i class="mdl-icon-toggle__label material-icons">arrow_drop_down</i>
                                        </label>
                                        {!! Form::label('event', trans('forms.label-event_id'), array('class' => 'mdl-textfield__label mdl-selectfield__label')); !!}
                                        <span id="error-select-event" class="mdl-textfield__error">{{ trans('reports.error_event')  }}</span>
                                    </div>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="event-options mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-select mdl-select__fullwidth is-dirty">
                                        <select id="report-event-park" class="mdl-selectfield__select mdl-textfield__input" name="report-event-park">
                                        </select>
                                        <label for="report-event-park">
                                            <i class="mdl-icon-toggle__label material-icons">arrow_drop_down</i>
                                        </label>
                                        {!! Form::label('event-park', trans('forms.label-event_park_id'), array('class' => 'mdl-textfield__label mdl-selectfield__label')); !!}
                                        <span id="error-select-event-park" class="mdl-textfield__error">{{ trans('reports.error_event')  }}</span>
                                    </div>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="event-options mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-select mdl-select__fullwidth is-dirty">
                                        <select id="report-event-credential" class="mdl-selectfield__select mdl-textfield__input" name="report-event-credential">
                                        </select>
                                        <label for="report-event-credential">
                                            <i class="mdl-icon-toggle__label material-icons">arrow_drop_down</i>
                                        </label>
                                        {!! Form::label('event-credential', trans('forms.label-event_credential_id'), array('class' => 'mdl-textfield__label mdl-selectfield__label')); !!}
                                        <span id="error-select-event-credential" class="mdl-textfield__error">{{ trans('reports.error_event')  }}</span>
                                    </div>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div id="event-datetime" class="flatpickr-event-date-report mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-upgraded is-focused">
                                        <input id="datetime-event" type="text" class="datetimepicker-event-date mdl-textfield__input" placeholder="{{ trans('reports.event_datetime') }}" data-input>
                                        <label class="mdl-textfield__label" for="datetime-event">{{ trans('reports.event_datetime') }}</label>
                                    </div>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-dirty">
                                        {!! Form::text('report_name', '', array('id' => 'report_name', 'class' => 'mdl-textfield__input is-dirty')) !!}
                                        <label class="mdl-textfield__label" for="report_name">{{ trans('reports.event_report_name') }}</label>
                                    </div>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-dirty">
                                        {!! Form::text('competition', '', array('id' => 'competition', 'class' => 'mdl-textfield__input is-dirty')) !!}
                                        <label class="mdl-textfield__label" for="competition">{{ trans('reports.event_competition') }}</label>
                                    </div>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-dirty">
                                        {!! Form::text('installation', '', array('id' => 'installation', 'class' => 'mdl-textfield__input is-dirty')) !!}
                                        <label class="mdl-textfield__label" for="installation">{{ trans('reports.event_installation') }}</label>
                                    </div>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-dirty">
                                        <label class="mdl-textfield__label" for="activity-report">{{ trans('reports.activity_report') }}</label>
                                    </div>
                                    <textarea class="form-control" id="activity-report"></textarea>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-dirty">
                                        <label class="mdl-textfield__label" for="occurrencies-report">{{ trans('reports.occurrencies_report') }}</label>
                                    </div>
                                    <textarea class="form-control" id="occurrencies-report"></textarea>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

                <div class="mdl-card__menu mdl-color-text--white">

                    <span class="save-actions">
                        {!! Form::button('<i class="material-icons">remove_red_eye</i>', array('id' => 'view-report', 'class' => 'dialog-button-icon-save mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect', 'disabled' => 'disabled', 'title' => trans('reports.view_report')  )) !!}
                    </span>

                    <span class="stop-actions">
                        {!! Form::button('<i class="material-icons">clear</i>', array('id' => 'stop-report', 'class' => 'dialog-button-icon-save mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect', 'title' => trans('reports.stop_report')  )) !!}
                    </span>

                    <a href="{{ url('/reports/') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="{{ trans('reports.back_to_reports') }}">
                        <i class="material-icons">reply</i>
                        <span class="sr-only">{{ trans('reports.back_to_reports') }}</span>
                    </a>

                </div>

            </div>
        </div>
    </div>

    {{-- Report Dialog Show --}}

    <dialog id="report-dialog" class="mdl-dialog" style="background: white !important;">

        <div id ="report-dialog-content" class="mdl-dialog__content">

            <div id="report-body">

                <div id="header-logo"><img id="header-image" src="/images/bannerHeaderEK.png"> </div>

                <div id="report-header">
                    <h2 class="report-header-title" style="color: #30BDCC !important;">{{trans('reports.report_header') }}</h2>
                </div>

                <div id="report-event-data">
                    <table id="report-event-details">
                        <thead></thead>
                        <tbody>
                        <tr>
                            <td class="align-left">{{trans('reports.text_event')}}</td>
                            <td id="report-event-name" class="align-right"></td>
                        </tr>
                        <tr>
                            <td class="align-left">{{trans('reports.text_competition')}}</td>
                            <td id="report-event-competition" class="align-right"></td>
                        </tr>
                        <tr>
                            <td class="align-left">{{trans('reports.text_installation')}}</td>
                            <td id="report-event-installation" class="align-right"></td>
                        </tr>
                        <tr>
                            <td class="align-left">{{trans('reports.text_date')}}</td>
                            <td id="report-event-date" class="align-right"></td>
                        </tr>
                        </tbody>
                        <tfoot></tfoot>
                    </table>
                </div>

                <div id="report-header">
                    <h2 class="report-header-title" style="color: #30BDCC;">{{trans('reports.text_activity_registry') }}</h2>
                </div>

                <div id="report-activity-registry">

                </div>

                <div id="report-data" class="break-page">

                    <div id="report-header">
                        <h2 class="report-header-title" style="color: #30BDCC !important;">{{trans('reports.text_entries_by_zone') }}</h2>
                    </div>

                    <table id="imports-entries-by-ticket-type" class="mdl-data-table mdl-js-data-table data-table">

                        <thead class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th>{{ trans('reports.text_zone')  }}</th>
                            <th>{{ trans('reports.text_imported_tickets')  }}</th>
                            <th>{{ trans('reports.text_total_entries')  }}</th>
                            <th>{{ trans('reports.text_total_masters')  }}</th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>

                        <tfoot class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th>{{ trans('reports.text_zone')  }}</th>
                            <th>{{ trans('reports.text_imported_tickets')  }}</th>
                            <th>{{ trans('reports.text_total_entries')  }}</th>
                            <th>{{ trans('reports.text_total_masters')  }}</th>
                        </tr>
                        </tfoot>

                    </table>

                    <div id="report-header">
                        <h2 class="report-header-title" style="color: #30BDCC !important;">{{trans('reports.text_entries_by_master') }}</h2>
                    </div>

                    <table id="entries-by-masters" class="mdl-data-table mdl-js-data-table data-table">

                        <thead class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th>{{ trans('reports.text_master')  }}</th>
                            <th>{{ trans('reports.text_entries')  }}</th>
                        </tr>
                        </thead>

                        <tbody></tbody>

                        <tfoot class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th>{{ trans('reports.text_master')  }}</th>
                            <th>{{ trans('reports.text_entries')  }}</th>
                        </tr>
                        </tfoot>

                    </table>

                    <div id="report-header">
                        <h2 class="report-header-title" style="color: #30BDCC !important;">{{trans('reports.text_errors_gate')}}</h2>
                    </div>

                    <div class="gates-information-result" style="overflow-x: auto; overflow-y: auto">
                        <table id="gates-information-table" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                            <thead class="table-header"></thead>
                            <tbody class="table-body"></tbody>
                            <tfoot class="table-footer"></tfoot>
                        </table>
                    </div>

                    <!-- Table goes here -->

                    <div id="report-header">
                        <h2 class="report-header-title" style="color: #30BDCC !important;">{{trans('reports.text_entries_by_tickettype') }}</h2>
                    </div>

                    <div id="entries-by-ticket-chart" class="mdl-card__supporting-text">
                        <canvas id="entries-ticket"></canvas>
                    </div>

                    <div id="report-header">
                        <h2 class="report-header-title" style="color: #30BDCC !important;">{{trans('reports.text_ticket_types') }}</h2>
                    </div>

                    <table id="ticket-types-entries-list" class="mdl-data-table mdl-js-data-table data-table">

                        <thead class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th>{{ trans('reports.text_ticket_types')  }}</th>
                            <th>{{ trans('reports.text_quantity')  }}</th>
                            <th>{{ trans('reports.text_percentage')  }}</th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>

                        <tfoot class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th>{{ trans('reports.text_ticket_types')  }}</th>
                            <th>{{ trans('reports.text_quantity')  }}</th>
                            <th>{{ trans('reports.text_percentage')  }}</th>
                        </tr>
                        </tfoot>

                    </table>

                    <div id="flowrate-chart" class="mdl-card__supporting-text chart_to_print">
                        <canvas id="flowrate"></canvas>
                    </div>

                    <div id="filllevel-chart" class="mdl-card__supporting-text chart_to_print">
                        <canvas id="filllevel"></canvas>
                    </div>

                    <table id="maximum-flowrate" class="mdl-data-table mdl-js-data-table data-table">

                        <thead class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th style="background: #ffffff !important; color: #000000;" colspan="2">{{ trans('reports.text_maximum_flowrate')  }}</th>
                        </tr>
                        <tr>
                            <th>{{ trans('reports.text_hour')  }}</th>
                            <th>{{ trans('reports.text_entries')  }}</th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>

                    </table>

                    <table id="total-entries-group" class="mdl-data-table mdl-js-data-table data-table">

                        <thead class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th style="background: #ffffff !important; color: #000000;" colspan="5">{{ trans('reports.text_total_entries_group')  }}</th>
                        </tr>
                        <tr>
                            <th>{{ trans('reports.text_global')  }}</th>
                            <th>{{ trans('reports.text_app')  }}</th>
                            <th>{{ trans('reports.text_box')  }}</th>
                            <th>{{ trans('reports.text_box_extra')  }}</th>
                            <th>{{ trans('reports.text_master')  }}</th>
                            <th>{{ trans('reports.text_credentials')  }}</th>
                            <th>{{ trans('reports.text_park')  }}</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td class="total-global"></td>
                            <td class="total-app"></td>
                            <td class="total-box-entries"></td>
                            <td class="total-extra-box-entries"></td>
                            <td class="total-masters"></td>
                            <td class="total-credentials"></td>
                            <td class="total-park"></td>
                        </tr>
                        <tr>
                            <td colspan="6" class="entries" style="text-align: right !important;">{{trans('reports.text_global_info')}}</td>
                        </tr>
                        <tr>
                            <td colspan="6" class="entries" style="text-align: right !important;">{{trans('reports.text_app_info')}}</td>
                        </tr>
                        </tbody>

                    </table>

                    <table id="total-imports-group" class="mdl-data-table mdl-js-data-table data-table">

                        <thead class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th style="background: #ffffff !important; color: #000000;" colspan="5">{{ trans('reports.text_imported_titles')  }}</th>
                        </tr>
                        <tr>
                            <th style="background: #ffffff !important; color: #000000;" colspan="4">{{ trans('reports.text_whitelist')  }}</th>
                            <th style="background: #ffffff !important; color: #000000;">{{ trans('reports.text_blacklist')  }}</th>
                        </tr>
                        <tr>
                            <th>{{ trans('reports.text_stadium')  }}</th>
                            <th>{{ trans('reports.text_master')  }}</th>
                            <th>{{ trans('reports.text_credentials')  }}</th>
                            <th>{{ trans('reports.text_park')  }}</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>

                    </table>

                    <div id="report-header">
                        <h2 class="report-header-title" style="color: #30BDCC !important;">{{ trans('reports.text_transactions_error_by_message') }}</h2>
                    </div>

                    <div id="transaction-error-message-chart" class="mdl-card__supporting-text">
                        <canvas id="transaction-error-message"></canvas>
                    </div>

                    <table id="transaction-error-message-list" class="mdl-data-table mdl-js-data-table data-table">

                        <thead class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th>{{ trans('reports.text_message')  }}</th>
                            <th>{{ trans('reports.text_quantity')  }}</th>
                            <th>{{ trans('reports.text_percentage')  }}</th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>

                        <tfoot class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th>{{ trans('reports.text_message')  }}</th>
                            <th>{{ trans('reports.text_quantity')  }}</th>
                            <th>{{ trans('reports.text_percentage')  }}</th>
                        </tr>
                        </tfoot>

                    </table>

                    <div id="report-header">
                        <h2 class="report-header-title" style="color: #30BDCC !important;">{{ trans('reports.text_unique_title_error_by_message') }}</h2>
                    </div>

                    <div id="unique-title-error-message-chart" class="mdl-card__supporting-text">
                        <canvas id="unique-title-error-message"></canvas>
                    </div>

                    <table id="unique-title-error-message-list" class="mdl-data-table mdl-js-data-table data-table">

                        <thead class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th>{{ trans('reports.text_message')  }}</th>
                            <th>{{ trans('reports.text_quantity')  }}</th>
                            <th>{{ trans('reports.text_percentage')  }}</th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>

                        <tfoot class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th>{{ trans('reports.text_message')  }}</th>
                            <th>{{ trans('reports.text_quantity')  }}</th>
                            <th>{{ trans('reports.text_percentage')  }}</th>
                        </tr>
                        </tfoot>

                    </table>

                    <div id="report-header-credentials">
                        <h2 class="report-header-title" style="color: #30BDCC !important;">{{trans('reports.text_top_credentials_entries_list') }}</h2>
                    </div>

                    <table id="top-entities-credentials" class="mdl-data-table mdl-js-data-table data-table">

                        <thead class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th>{{ trans('reports.text_entity_gate') }}</th>
                            <th>{{ trans('reports.text_entries') }}</th>
                        </tr>
                        </thead>

                        <tbody></tbody>

                    </table>

                    <div id="report-header">
                        <h2 class="report-header-title" style="color: #30BDCC !important;">{{trans('reports.text_cam_entries')}}</h2>
                    </div>

                    <table id="cam-entries-table" class="mdl-data-table mdl-js-data-table mdl-data-table">
                        <thead class="mdl-color--primary custom-header-color" style="background: #30BDCC !important;">
                        <tr>
                            <th class="cam-name">{{trans('reports.text_cam_name')}}</th>
                            <th class="cam-entity">{{trans('reports.text_entity')}}</th>
                            <th class="cam-entries">{{trans('reports.text_entries')}}</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>

                </div>

                <div id="report-header" class="report-occurencies-registry">
                    <h2 class="report-header-title" style="color: #30BDCC;">{{trans('reports.text_occurencies_registry') }}</h2>
                </div>

                <div id="report-occurencies-registry">

                </div>

            </div>

        </div>

        <div id="report-actions" class="mdl-dialog__actions--full-width">

            <button type="button" id="close-report-view-button" class="mdl-button close">{{ trans('charts.button_cancel') }}</button>
            <button type="button" id="print-report-view-button" class="mdl-button confirm-action align-right print-report">{{ trans('reports.generate_report') }}</button>

        </div>

    </dialog>

    <div id="report-load-charts" class="mdl-js-snackbar mdl-snackbar">
        <div class="mdl-snackbar__text"></div>
        <button class="mdl-snackbar__action" type="button"></button>
    </div>

@endsection

@section('footer_scripts')

    <script>

        var presidential_box_entries = 0;

        $(window).ready(function () {

            if (typeof CKEDITOR != 'undefined') {

                CKEDITOR.replace('activity-report') ;
                CKEDITOR.replace('occurrencies-report');

            } else {
                setTimeout(arguments.callee, 50);
            }

            $('#report_name').parent().addClass('is-dirty');
            $('#competition').parent().addClass('is-dirty');
            $('#installation').parent().addClass('is-dirty');
            $('#report-event-park').parent().addClass('is-dirty');
            $('#report-event-park').parent().addClass('is-dirty');
            $('#report-event-credential').parent().addClass('is-dirty');

            $(".datetimepicker-event-date").flatpickr({
                appendTo: $('div.flatpickr-event-date-report')[0],
                enableTime: true,
                dateFormat: "Y-m-d H:i",
                time_24hr: true
            });

            componentHandler.upgradeDom();

        });

        $('#report-event').on('change', function () {

            removeSelectionError('event');
            validateReportLoading();

        });

        $('#report-event-park').on('change', function () {

            removeSelectionError('park');
            validateReportLoading();

        });

        $('#report-event-credential').on('change', function () {

            removeSelectionError('credential');
            validateReportLoading();

        });

        function round(value, decimals) {
            return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
        }

        $('#stop-report').on('click', function() {
            window.stop();
            $('#view-report').removeAttr('disabled');
            $('#loading-report-animation').remove();
            $('#report-event').removeAttr('disabled');
            $('#report-event-park').removeAttr('disabled');
            $('#report-event-credential').removeAttr('disabled');
            $('#view-report').prop('disabled', true);

        });

        $('#view-report').on('click', function() {

            var dialog = document.querySelector('#report-dialog');

            $('#report-event-name').text($('#report-event option:selected').text());
            $('#report-event-competition').text($('#competition').val());
            $('#report-event-installation').text($('#installation').val());
            $('#report-event-date').text($('#datetime-event').val())
            document.getElementById("report-activity-registry").innerHTML = CKEDITOR.instances['activity-report'].getData();
            document.getElementById("report-occurencies-registry").innerHTML = CKEDITOR.instances['occurrencies-report'].getData();

            dialog.showModal();

        });

        $('#close-report-view-button').on('click', function() {

            var dialog = document.querySelector('#report-dialog');

            dialog.close();

        });

        function validateReportLoading() {

            var event_id = $('#report-event option:selected').val();
            var event_park_id = $('#report-event-park option:selected').val();
            var event_credential_id = $('#report-event-credential option:selected').val();

            if (event_id !== "undefined" && event_id != '' && event_park_id !== "undefined" && event_park_id != '' && event_credential_id !== "undefined" && event_credential_id != '') {

                removeSelectionError(event_id, event_park_id, event_credential_id);

                loadReport(event_id, event_park_id, event_credential_id);

            } else {

                eventsSelectionError(event_id, event_park_id, event_credential_id);

            }

        }

        function removeSelectionError(event_type) {

            if (event_type == 'event') {

                var event_id = $('#report-event option:selected').val();

                if (event_id !== "undefined" && event_id != '') {

                    document.getElementById("error-select-event").style.visibility = "hidden";
                    $('#report-event').parent().removeClass('is-invalid');

                }

            } else if (event_type == 'park') {

                var event_park_id = $('#report-event-park option:selected').val();

                if (event_park_id !== "undefined" && event_park_id != '') {

                    document.getElementById("error-select-event-park").style.visibility = "hidden";
                    $('#report-event-park').parent().removeClass('is-invalid');

                }

            } else if (event_type == 'credential') {

                var event_credential_id = $('#report-event-credential option:selected').val();

                if (event_credential_id !== "undefined" && event_credential_id != '') {

                    document.getElementById("error-select-event-credential").style.visibility = "hidden";
                    $('#report-event-credential').parent().removeClass('is-invalid');

                }

            }

        }

        function eventsSelectionError(event_id, event_park_id, event_credential_id) {

            if (event_id == "undefined" || event_id == '') {
                document.getElementById("error-select-event").style.visibility = "visible";
                $('#report-event').parent().addClass('is-invalid');
            }

            if (event_park_id == "undefined" || !event_park_id) {
                document.getElementById("error-select-event-park").style.visibility = "visible";
                $('#report-event-park').parent().addClass('is-invalid');
            }

            if (event_credential_id == "undefined" || !event_credential_id) {
                document.getElementById("error-select-event-credential").style.visibility = "visible";
                $('#report-event-credential').parent().addClass('is-invalid');
            }

        }

        function loadReport(event_id, event_park_id, event_credential_id) {

            var snackbarContainer = document.querySelector('#report-load-charts');

            var data = {message: '{{ trans('reports.loading_charts') }}' };

            snackbarContainer.MaterialSnackbar.showSnackbar(data);

            $('#imports-entries-by-ticket-type tbody > *').remove();
            $('#entries-by-masters tbody > *').remove();
            $('#ticket-types-entries-list tbody > *').remove();
            $('#transaction-error-message-list tbody > *').remove();
            $('#top-entities-credentials tbody > *').remove();
            $('#cam-entries-table tbody > *').remove();

            $('#report-event').attr('disabled', 'disabled');
            $('#report-event-park').attr('disabled', 'disabled');
            $('#report-event-credential').attr('disabled', 'disabled');
            $('#view-report').attr('disabled', 'disabled');

            $('.logo-style').parent().append('<div id="loading-report-animation" class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div>');

            componentHandler.upgradeDom();

            $.when(

                $.ajax({

                    url: '/event/' + event_id + '/report/zone_entries/',
                    dataType: 'json',
                    success: function (data) {

                        if (data['error']) {

                            var snackbarContainer = document.querySelector('#report-load-charts');

                            var data = {message: '{{ trans('reports.error_loading_report_data') }}' };

                            snackbarContainer.MaterialSnackbar.showSnackbar(data);

                        } else {

                            var venues = data.venue;

                            var html = '';

                            for ( var template in venues) {

                                html += '<tr id="' + template + '" class="entries-venue venue-row">';
                                html += '   <td class="description">- ' + venues[template].description + '</td>';
                                html += '   <td class="imported">' + venues[template].imported + '</td>';
                                html += '   <td class="entries">' + venues[template].total_entries + '</td>';
                                html += '   <td class="masters">' + venues[template].masters + '</td>';
                                html += '</tr>';

                                var zones = venues[template].options;

                                for ( var zone in zones ) {

                                    var blocks = zones[zone];

                                    for ( var block in blocks ) {

                                        var gates = blocks[block].options;

                                        html += '<tr id="' + block + '" class="entries-block zone-row">';
                                        html += '   <td class="description">---- ' + blocks[block].description + '</td>';
                                        html += '   <td class="imported">' + blocks[block].imported + '</td>';
                                        html += '   <td class="entries">' + blocks[block].total_entries + '</td>';
                                        html += '   <td class="masters">' + blocks[block].masters + '</td>';
                                        html += '</tr>';

                                        for ( var gate in gates) {

                                            var doors = gates[gate];

                                            for ( var door in doors ) {

                                                html += '<tr id="' + door + '" class="entries-gate gate-row">';
                                                html += '   <td class="description">-------- ' + doors[door].description + '</td>';
                                                html += '   <td class="imported">' + doors[door].imported + '</td>';
                                                html += '   <td class="entries">' + doors[door].entries + '</td>';
                                                html += '   <td class="masters">' + doors[door].masters + '</td>';
                                                html += '</tr>';

                                            }

                                        }

                                    }

                                }

                            }

                            $('#imports-entries-by-ticket-type tbody').append(html);

                            $('#imports-entries-by-ticket-type').css('display', 'table');

                            presidential_box_entries = presidential_box_entries + data.presidential_box_entries;

                            $('.total-box-entries').text(presidential_box_entries);

                            componentHandler.upgradeDom();

                        }

                    }

                }),

                $.ajax({

                    url: '/event/' + event_id + '/report/masters_entries/',
                    dataType: 'json',
                    success: function (data) {

                        if (data['error']) {

                            var snackbarContainer = document.querySelector('#report-load-charts');

                            var data = {message: '{{ trans('reports.error_loading_report_data') }}' };

                            snackbarContainer.MaterialSnackbar.showSnackbar(data);

                        } else {

                            var html = '';

                            for ( var master in data) {

                                html += '<tr id="' + data[master].titulo + '" class="entries-master">';
                                html += '   <td class="barcode">' + data[master].titulo + '</td>';
                                html += '   <td class="entries">' + data[master].entries + '</td>';
                                html += '</tr>';


                            }

                            $('#entries-by-masters tbody').append(html);

                            $('#entries-by-masters').css('display', 'table');

                            componentHandler.upgradeDom();

                        }

                    }

                }),

                $.ajax({
                    url: '/event/' + event_id + '/report/ticket_type_entries/',
                    dataType: 'json',
                    success: function (data) {

                        labels_chart = [];
                        values_chart = [];
                        color_chart = [];
                        color_border = [];

                        total_entries = 0;
                        total_percentage = 0;

                        data.forEach(function(ticket_type) {

                            labels_chart.push(ticket_type['tickettype']);
                            values_chart.push(ticket_type['entries']);
                            var temp_color = "#" + Math.random().toString(16).slice(2, 8);
                            color_chart.push(temp_color);
                            color_border.push(temp_color);

                            total_entries = total_entries + ticket_type['entries'];

                        });

                        document.getElementById("entries-by-ticket-chart").innerHTML = '&nbsp;';
                        document.getElementById("entries-by-ticket-chart").innerHTML = '<canvas id="entries-ticket"></canvas>';

                        var ctx = document.getElementById("entries-ticket").getContext("2d");

                        var myChartCircle = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: labels_chart,
                                datasets: [{
                                    label: "Fill Level",
                                    data: values_chart,
                                    backgroundColor: color_chart,
                                    borderColor: color_border
                                }]
                            },
                            plugins: [{
                                beforeDraw: function(chart) {

                                    var width = chart.chart.width,
                                        height = chart.chart.height,
                                        ctx = chart.chart.ctx;

                                    ctx.restore();
                                    var fontSize = (height / 150).toFixed(2);
                                    ctx.font = fontSize + "em sans-serif";
                                    ctx.fillStyle = "#9b9b9b";
                                    ctx.textBaseline = "middle";

                                    ctx.save();

                                }
                            }],
                            options: {
                                legend: {
                                    display: true,
                                    position: 'bottom',
                                    fullWidth: true
                                },
                                responsive: true,
                                maintainAspectRatio: true,
                                cutoutPercentage: 0
                            }

                        });

                        myChartCircle.update();

                        var html = '';

                        data.forEach(function(ticket_type_list) {

                            var temp_percentage = round(((ticket_type_list.entries / total_entries ) * 100), 2);

                            total_percentage = total_percentage + temp_percentage;

                            html += '<tr class="ticket-type-list">';
                            html += '   <td class="ticket-type">' + ticket_type_list.tickettype + '</td>';
                            html += '   <td class="entries">' + ticket_type_list.entries + '</td>';
                            html += '   <td class="percentage">' + temp_percentage + '%</td>';
                            html += '</tr>';

                        });

                        html += '<tr class="ticket-type-list-total">';
                        html += '   <td class="ticket-type">Total</td>';
                        html += '   <td class="entries">' + total_entries + '</td>';
                        html += '   <td class="percentage">' + round(total_percentage, 0) + '%</td>';
                        html += '</tr>';


                        $('#ticket-types-entries-list tbody').append(html);

                        $('#ticket-types-entries-list').css('display', 'table');

                        componentHandler.upgradeDom();

                    }

                }),

                $.ajax({
                    url: '/event/' + event_id + '/report/flowrate/',
                    dataType: 'json',
                    success: function (data) {

                        /**
                         * @return Flowrate
                         */

                        var labels_chart = [];

                        var values_chart = [];

                        var fill_chart = [];

                        var temp_fill = 0;

                        data.forEach(function(time) {

                            temp_fill = temp_fill + time['entries'];

                            labels_chart.push(time['hour_transaction']);
                            values_chart.push(time['entries']);
                            fill_chart.push(temp_fill);

                        });

                        document.getElementById("flowrate-chart").innerHTML = '&nbsp;';
                        document.getElementById("flowrate-chart").innerHTML = '<canvas id="flowrate"></canvas>';

                        var ctx_flowrate = document.getElementById("flowrate").getContext('2d');

                        var flowrateChart = new Chart(ctx_flowrate, {

                            type: 'line',
                            data: {
                                labels: labels_chart,
                                datasets: [
                                    {
                                        data: values_chart,
                                        label: "Flowrate",
                                        borderColor: "#3e95cd",
                                        fill: false
                                    }
                                ]
                            },
                            options: {
                                title: {
                                    display: false,
                                    text: 'Flowrate'
                                }
                            }

                        });

                        flowrateChart.update();

                        document.getElementById("filllevel-chart").innerHTML = '&nbsp;';
                        document.getElementById("filllevel-chart").innerHTML = '<canvas id="filllevel"></canvas>';

                        var ctx_filllevel = document.getElementById("filllevel").getContext('2d');

                        var filllevelChart = new Chart(ctx_filllevel, {

                            type: 'line',
                            data: {
                                labels: labels_chart,
                                datasets: [
                                    {
                                        data: fill_chart,
                                        label: "Fill Level",
                                        borderColor: "#3e95cd",
                                        fill: true,
                                        backgroundColor: "#3e95cd",
                                    }
                                ]
                            },
                            options: {
                                title: {
                                    display: false,
                                    text: 'Fill Level'
                                }
                            }

                        });

                        filllevelChart.update();

                        componentHandler.upgradeDom();

                    }

                }),

                $.ajax({
                    url: '/event/' + event_id + '/report/entries_by_group/park/' + event_park_id + '/credential/' + event_credential_id + '/',
                    dataType: 'json',
                    success: function (data) {

                        html = '';

                        html += '<tr>';
                        html += '   <td class="hour">' + data.max_flowrate.hour + '</td>';
                        html += '   <td class="entries">' + data.max_flowrate.entries + '</td>';
                        html += '</tr>';

                        $('#maximum-flowrate tbody').append(html);

                        $('#maximum-flowrate').css('display', 'table');

                        $('.total-global').text(data.global);
                        $('.total-app').text(data.app);
                        $('.total-masters').text(data.master);
                        $('.total-credentials').text(data.credential);
                        $('.total-park').text(data.park);

                        $('#total-entries-group').css('display', 'table');

                        html = '';

                        html += '<tr>';
                        html += '   <td class="import-wl-stadium">' + data.wl_stadium + '</td>';
                        html += '   <td class="import-wl-masters">' + data.wl_masters + '</td>';
                        html += '   <td class="import-wl-credentials">' + data.wl_credentials + '</td>';
                        html += '   <td class="import-wl-park">' + data.wl_park + '</td>';
                        html += '   <td class="import-bl">' + data.bl_stadium + '</td>';
                        html += '</tr>';

                        $('#total-imports-group tbody').append(html);

                        $('#total-imports-group').css('display', 'table');

                        componentHandler.upgradeDom();

                    }

                }),

                $.ajax({
                    url: '/event/' + event_credential_id + '/report/cred/top_entities/',
                    dataType: 'json',
                    success: function (data) {

                        var html = '';

                        data.forEach(function(entity) {

                            html += '<tr class="entity-credential">';
                            html += '   <td class="entity-name">' + entity.description  + '</td>';
                            html += '   <td class="entries">' + entity.total + '</td>';
                            html += '</tr>';

                            entity.gates.forEach(function(gate) {

                                html += '<tr class="entity-credential-gate">';
                                html += '   <td class="gate-name">' + gate.description  + '</td>';
                                html += '   <td class="entries">' + gate.total + '</td>';
                                html += '</tr>';

                            });

                        });

                        $('#top-entities-credentials tbody').append(html);

                    }
                }),

                $.ajax({
                    url: '/event/' + event_id + '/report/cam_entries/',
                    dataType: 'json',
                    success: function(data) {

                        var html = '';

                        var total_cam_entries = 0;

                        data['boxs'].forEach(function(cam) {

                            total_cam_entries = total_cam_entries + cam.total;

                            html += '<tr class="cam-entrie-entity">';
                            html += '   <td class="cam-name">' + cam.cam + '</td>';
                            html += '   <td class="cam-entity">' + cam.entity + '</td>';
                            html += '   <td class="cam-entries">' + cam.total + '</td>';
                            html += '</tr>';

                        });

                        html += '<tr class="total-cam-entries">';
                        html += '   <td class="cam-total">{{trans('reports.text_total')}}</td>';
                        html += '   <td></td>';
                        html += '   <td class="cam-total-value">' + total_cam_entries + '</td>';
                        html += '</tr>';

                        $('#cam-entries-table tbody').append(html);

                        presidential_box_entries = presidential_box_entries + total_cam_entries;

                        $('.total-box-entries').text(presidential_box_entries);
                        $('.total-extra-box-entries').text(data['total_extra']);

                    }
                }),

                $.ajax({
                    type: 'GET',
                    url: '/event/' + event_id + '/warnings/gates_information',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    beforeSend: function() {

                        $('#gates-information-table .table-header > *').remove();
                        $('#gates-information-table .table-body > *').remove();
                        $('#gates-information-table .table-footer > *').remove();

                    },
                    success: function (data) {

                        var html_header = '';

                        html_header += '<tr><td>{{trans('charts.description')}}</td>';

                        var html = '';

                        var table_columns = [];

                        data.forEach(function (venue, index) {

                            table_columns = Object.keys(venue);

                            table_columns.forEach(function(column) {

                                if (column != 'description' && column != 'zones') {

                                    html_header += '<td>' + column + '</td>';

                                }

                            });

                            var description_index = table_columns.indexOf('description');
                            (description_index > -1) ? table_columns.splice(description_index, 1) : '';

                            var zones_index = table_columns.indexOf('zones');
                            (zones_index > -1) ? table_columns.splice(zones_index, 1) : '';

                            html += '<tr class="venue-row">';
                            html += '<td>' + venue.description + '</td>';
                            table_columns.forEach(function(value) {

                                var venue_errors_value = (venue[value] != undefined) ? venue[value] : 0;

                                html += '<td>' + venue_errors_value  + '</td>';

                            });
                            html += '</tr>';

                            venue['zones'].forEach(function(zone) {

                                html += '<tr class="zone-row">';
                                html += '<td>' + zone.description + '</td>';

                                table_columns.forEach(function(value) {

                                    var zone_errors_value = (zone[value] != undefined) ? zone[value] : 0;

                                    html += '<td>' + zone_errors_value + '</td>';

                                });

                                html += '</tr>';

                                zone['gates'].forEach(function(gate) {

                                    html += '<tr class="gate-row">';
                                    html += '<td>' + gate.description + '</td>';

                                    table_columns.forEach(function(value) {

                                        var gate_errors_value = (gate[value] != undefined) ? gate[value] : 0;

                                        html += '<td>' + gate_errors_value + '</td>';

                                    });

                                    html += '</tr>';

                                });

                            });

                        });

                        html_header += '</tr>';

                        var colspan_size = table_columns.length + 1;

                        table_columns.forEach(function(column) {

                            var column_translation = '';

                            switch (column) {

                                default:
                                    column_translation = column;
                                    break;
                                case 'PE':
                                    column_translation = '{{trans('charts.column_pe')}}';
                                    break;
                                case 'TD':
                                    column_translation = '{{trans('charts.column_td')}}';
                                    break;
                                case 'QA':
                                    column_translation = '{{trans('charts.column_qa')}}';
                                case 'BE':
                                    column_translation = '{{trans('charts.column_be')}}';
                                    break;
                                case 'VA':
                                    column_translation = '{{trans('charts.column_va')}}';
                                    break;
                                case 'JE':
                                    column_translation = '{{trans('charts.column_je')}}';
                                    break;
                                case 'SCB':
                                    column_translation = '{{trans('charts.column_scb')}}';
                                    break;
                                case 'NSCB':
                                    column_translation = '{{trans('charts.column_nscb')}}';
                                    break;
                                case 'CA':
                                    column_translation = '{{trans('charts.column_ca')}}';
                                    break;
                                case 'LAA':
                                    column_translation = '{{trans('charts.column_laa')}}';
                                    break;
                                case 'CTCT':
                                    column_translation = '{{trans('charts.column_ctct')}}';
                                    break;
                                case 'SAC':
                                    column_translation = '{{trans('charts.column_sac')}}';
                                    break;
                                case 'SSR':
                                    column_translation = '{{trans('charts.column_ssr')}}';
                                    break;

                            }

                            html += '<tr>';
                            html += '<td style="text-align: left !important;" colspan="' + colspan_size + '">' + column_translation + '</td>';
                            html += '</tr>';

                        });

                        $("#gates-information-table .table-header").append(html_header);
                        $("#gates-information-table .table-body").append(html);

                        componentHandler.upgradeDom();

                    }
                }),

                $.ajax({
                    url: '/event/' + event_id + '/report/transactions_error_message/',
                    dataType: 'json',
                    success: function (data) {

                        labels_chart = [];
                        values_chart = [];
                        color_chart = [];
                        color_border = [];

                        total_entries = 0;
                        total_percentage = 0;

                        data.forEach(function(ticket_type) {

                            labels_chart.push(ticket_type['Mensagem']);
                            values_chart.push(ticket_type['Quantidade']);
                            var temp_color = "#" + Math.random().toString(16).slice(2, 8);
                            color_chart.push(temp_color);
                            color_border.push(temp_color);

                            total_entries = total_entries + ticket_type['Quantidade'];

                        });

                        document.getElementById("transaction-error-message-chart").innerHTML = '&nbsp;';
                        document.getElementById("transaction-error-message-chart").innerHTML = '<canvas id="transaction-error-message"></canvas>';

                        var ctx = document.getElementById("transaction-error-message").getContext("2d");

                        var myChartCircle = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: labels_chart,
                                datasets: [{
                                    label: "Fill Level",
                                    data: values_chart,
                                    backgroundColor: color_chart,
                                    borderColor: color_border
                                }]
                            },
                            plugins: [{
                                beforeDraw: function(chart) {

                                    var width = chart.chart.width,
                                        height = chart.chart.height,
                                        ctx = chart.chart.ctx;

                                    ctx.restore();
                                    var fontSize = (height / 150).toFixed(2);
                                    ctx.font = fontSize + "em sans-serif";
                                    ctx.fillStyle = "#9b9b9b";
                                    ctx.textBaseline = "middle";

                                    ctx.save();

                                }
                            }],
                            options: {
                                legend: {
                                    display: true,
                                    position: 'bottom',
                                    fullWidth: true
                                },
                                responsive: true,
                                maintainAspectRatio: true,
                                cutoutPercentage: 0
                            }

                        });

                        myChartCircle.update();

                        var html = '';

                        data.forEach(function(ticket_type_list) {

                            var temp_percentage = round(((ticket_type_list['Quantidade'] / total_entries ) * 100), 2);

                            total_percentage = total_percentage + temp_percentage;

                            html += '<tr class="ticket-type-list">';
                            html += '   <td class="ticket-type">' + ticket_type_list['Mensagem'] + '</td>';
                            html += '   <td class="entries">' + ticket_type_list['Quantidade'] + '</td>';
                            html += '   <td class="percentage">' + temp_percentage + '%</td>';
                            html += '</tr>';

                        });

                        html += '<tr class="ticket-type-list-total">';
                        html += '   <td class="ticket-type">Total</td>';
                        html += '   <td class="entries">' + total_entries + '</td>';
                        html += '   <td class="percentage">' + round(total_percentage, 0) + '%</td>';
                        html += '</tr>';

                        $('#transaction-error-message-list tbody').append(html);

                        $('#transaction-error-message-list').css('display', 'table');

                        componentHandler.upgradeDom();

                    }

                }),

                $.ajax({
                    url: '/event/' + event_id + '/report/unique_title_error_message/',
                    dataType: 'json',
                    success: function (data) {

                        labels_chart = [];
                        values_chart = [];
                        color_chart = [];
                        color_border = [];

                        total_entries = 0;
                        total_percentage = 0;

                        data.forEach(function(ticket_type) {

                            labels_chart.push(ticket_type['Mensagem']);
                            values_chart.push(ticket_type['Quantidade']);
                            var temp_color = "#" + Math.random().toString(16).slice(2, 8);
                            color_chart.push(temp_color);
                            color_border.push(temp_color);

                            total_entries = total_entries + ticket_type['Quantidade'];

                        });

                        document.getElementById("unique-title-error-message-chart").innerHTML = '&nbsp;';
                        document.getElementById("unique-title-error-message-chart").innerHTML = '<canvas id="unique-title-error-message"></canvas>';

                        var ctx = document.getElementById("unique-title-error-message").getContext("2d");

                        var myChartCircle = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: labels_chart,
                                datasets: [{
                                    label: "Fill Level",
                                    data: values_chart,
                                    backgroundColor: color_chart,
                                    borderColor: color_border
                                }]
                            },
                            plugins: [{
                                beforeDraw: function(chart) {

                                    var width = chart.chart.width,
                                        height = chart.chart.height,
                                        ctx = chart.chart.ctx;

                                    ctx.restore();
                                    var fontSize = (height / 150).toFixed(2);
                                    ctx.font = fontSize + "em sans-serif";
                                    ctx.fillStyle = "#9b9b9b";
                                    ctx.textBaseline = "middle";

                                    ctx.save();

                                }
                            }],
                            options: {
                                legend: {
                                    display: true,
                                    position: 'bottom',
                                    fullWidth: true
                                },
                                responsive: true,
                                maintainAspectRatio: true,
                                cutoutPercentage: 0
                            }

                        });

                        myChartCircle.update();

                        var html = '';

                        data.forEach(function(ticket_type_list) {

                            var temp_percentage = round(((ticket_type_list['Quantidade'] / total_entries ) * 100), 2);

                            total_percentage = total_percentage + temp_percentage;

                            html += '<tr class="ticket-type-list">';
                            html += '   <td class="ticket-type">' + ticket_type_list['Mensagem'] + '</td>';
                            html += '   <td class="entries">' + ticket_type_list['Quantidade'] + '</td>';
                            html += '   <td class="percentage">' + temp_percentage + '%</td>';
                            html += '</tr>';

                        });

                        html += '<tr class="ticket-type-list-total">';
                        html += '   <td class="ticket-type">Total</td>';
                        html += '   <td class="entries">' + total_entries + '</td>';
                        html += '   <td class="percentage">' + round(total_percentage, 0) + '%</td>';
                        html += '</tr>';

                        $('#unique-title-error-message-list tbody').append(html);

                        $('#unique-title-error-message-list').css('display', 'table');

                        componentHandler.upgradeDom();

                    }

                }),

            ).then(function () {

                $('#view-report').removeAttr('disabled');
                $('#loading-report-animation').remove();
                $('#report-event').removeAttr('disabled');
                $('#report-event-park').removeAttr('disabled');
                $('#report-event-credential').removeAttr('disabled');

            });

        }

    </script>

@endsection