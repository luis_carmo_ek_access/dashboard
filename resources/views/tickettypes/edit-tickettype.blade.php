<style scoped>

	.mdl-color--primary {
		background-color: #30BDCC !important; /* rgb(63,81,181)!important;*/
	}

	.logo-style {
		width: 90% !important;
	}

	body .mdl-card__title {
		height: ;
	}

</style>

@extends('layouts.dashboard')

@section('template_title')
	{{ trans('tickettypes.templateTitle') }}
@endsection

@section('header')
	<small>
		{{ trans('tickettypes.editTicketTypeTitle') }} | {{ isset($tickettype) ? $tickettype->description : '' }}
	</small>
@endsection

@section('breadcrumbs')

	<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="{{url('/')}}">
			<span itemprop="name">
				{{ trans('titles.app') }}
			</span>
		</a>
		<i class="material-icons">chevron_right</i>
		<meta itemprop="position" content="1" />
	</li>

	<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="{{ url('/tickettypes') }}">
			<span itemprop="name">
				{{ trans('titles.ticketTypes') }}
			</span>
		</a>
		<i class="material-icons">chevron_right</i>
		<meta itemprop="position" content="2" />
	</li>
	@if (isset($tickettype))
		<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active">
			<a itemprop="item" href="{{ url('/tickettypes/'.$tickettype->id.'/edit') }}" class="hidden">
				<span itemprop="name">
					{{ trans('titles.editTicketType') }}
				</span>
			</a>
			<meta itemprop="position" content="3" />
			{{ trans('titles.editTicketType') }}
		</li>
	@else
		<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">

			<span itemprop="name">
				{{ trans('tickettypes.addTicketType') }}
			</span>
			<meta itemprop="position" content="3" />
		</li>
	@endif

@endsection

@section('template-form-status')
	@include('partials.form-status-ajax')
@endsection

@section('content')

    <div class="mdl-grid full-grid margin-top-0 padding-0">

        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cel--12-col-phone mdl-cell--8-col-table mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">

            <div class="md-card" style="width:100%; height: auto;" itemscope itemtype="http://schema.org/Person">

				<div class="mdl-card__title mdl-card--expand mdl-color--primary mdl-color-text--white">
					<h2 class="mdl-card__title-text logo-style">{{ isset($tickettype) ? trans('tickettypes.templateTitle') :  trans('tickettypes.addTicketType') }}</h2>
				</div>

				<div id="ticket-type" style="display: none" data-id="{{ isset($tickettype) ? $tickettype->id : 0 }}" data-action="{{ $action }}"></div>

				<div class="mdl-card__supporting-text">

					<div class="mdl-grid ">

						<div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('name') ? 'is-invalid' :'' }}">
								{!! Form::text('description', isset($tickettype) ? $tickettype->description : '', array('id' => 'description', 'class' => 'mdl-textfield__input')) !!}
								{!! Form::label('description', trans('tickettypes.description') , array('class' => 'mdl-textfield__label')); !!}
								<span class="mdl-textfield__error">{{ trans('tickettype.error_description') }}</span>
							</div>
						</div>

						@if (!isset($tickettype))

							<div class="mdl-cell mdl-cell--6-col-tablet mdl-cell--6-col-desktop">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('external_id') ? 'is-invalid' :'' }}">
									{!! Form::text('external_id', '', array('id' => 'external-id', 'class' => 'mdl-textfield__input', 'pattern' => '[0-9]+')) !!}
									{!! Form::label('external_id', trans('tickettypes.external_id') , array('class' => 'mdl-textfield__label')); !!}
									<span id="error-external-id-set" class="mdl-textfield__error">{{ trans('tickettypes.error_external_id') }}</span>
								</div>
							</div>

							<div class="mdl-cell mdl-cell--6-col-tablet mdl-cell--6-col-desktop">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('access_value') ? 'is-invalid' :'' }}">
									{!! Form::text('access_value', '', array('id' => 'access-value', 'class' => 'mdl-textfield__input', 'pattern' => '[A-Z,a-z,0-9]*')) !!}
									{!! Form::label('access_value', trans('tickettypes.access_value') , array('class' => 'mdl-textfield__label')); !!}
									<span id="error-value-set" class="mdl-textfield__error">{{ trans('tickettypes.error_access_value') }}</span>
								</div>
							</div>

						@endif

						<div class="mdl-cell mdl-cell--6-col">
							<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="available">
								<input type="checkbox" id="available" class="mdl-checkbox__input" {{ isset($tickettype) ? $tickettype->available_filters ? 'checked' : '' : '' }}>
								<span class="mdl-checkbox__label">{{ trans('tickettypes.available_filters') }}</span>
							</label>
						</div>

						<div class="mdl-cell mdl-cell--6-col">
							<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="active">
								<input type="checkbox" id="active" class="mdl-checkbox__input" {{ isset($tickettype) ? $tickettype->active ? 'checked' : '' : ''}}>
								<span class="mdl-checkbox__label">{{ trans('tickettypes.active') }}</span>
							</label>
						</div>

					</div>

				</div>

				<div class="mdl-card__menu mdl-color-text--white">

                    <span class="save-actions">
                        {!! Form::button('<i class="material-icons">save</i>', array('id' => 'save-ticket', 'class' => 'dialog-button-icon-save mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect', 'title' => trans('tickettypes.save_ticket_type')  )) !!}
                    </span>

					<a href="{{ url('/tickettypes/') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="{{ trans('tickettypes.back_to_tickettypes') }}">
						<i class="material-icons">reply</i>
						<span class="sr-only">{{ trans('tickettypes.back_to_tickettypes') }}</span>
					</a>

				</div>

            </div>

        </div>

    </div>

@endsection

@section('footer_scripts')

	<script>

		$('#save-ticket').on('click', function() {

		    var action = $('#ticket-type').attr('data-action');
		    var tickettype_id = $('#ticket-type').attr('data-id');

		    var description = $('#description').val();
		    var available = $('#available').parent().hasClass('is-checked');
		    var active = $('#active').parent().hasClass('is-checked');

		    if (action == 'update') {

                $.ajax({
                    type: 'PUT',
                    url: '/tickettypes/' + tickettype_id + '/update/',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    data: {description: description, available: available, active: active},
                    success: function (data) {

                        if (data['success']) {

                            window.location.href = "/tickettypes";

                        } else {

                            console.log('error');

                        }

                    }

                });

			} else if (action == 'add') {

		        var value = $('#access-value').val();
		        var external_id = $('#external-id').val();

                $.ajax({
                    type: 'POST',
                    url: '/tickettypes/add',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    data: {description: description, available: available, active: active, external_id: external_id, value: value},
                    success: function (data) {

                        if (data['error']) {

                            if (data['error'].includes('external_id')) {

                                document.getElementById("error-external-id-set").style.visibility = "visible";
                                $('#external-id').parent().addClass('is-invalid');

							} else {

                                document.getElementById("error-external-id-set").style.visibility = "hidden";
                                $('#external-id').parent().removeClass('is-invalid');

							}

							if (data['error'].includes('value')) {

                                document.getElementById("error-value-set").style.visibility = "visible";
                                $('#access-value').parent().addClass('is-invalid');

							} else {

                                document.getElementById("error-value-set").style.visibility = "hidden";
                                $('#access-value').parent().removeClass('is-invalid');

							}

                            componentHandler.upgradeDom();

						} else if (data['success']) {

                            window.location.href = "/tickettypes";

                        } else {

                            console.log('error');

                        }

                    },
					error: function (error) {

                        console.log('Error');
                        console.log(error);

					}
                });

			}

		});

	</script>

@endsection