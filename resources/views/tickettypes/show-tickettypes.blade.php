<style scoped>
    .align-right {
        float: right;
    }
    .align-center {
        text-align: center;
    }
</style>
@extends('layouts.dashboard')

@section('template_title')
    {{ trans('tickettypes.showing_tickettypes') }}
@endsection

@section('template_linked_css')
@endsection

@section('header')
    {{ trans('tickettypes.showing_tickettypes') }}
@endsection

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/tickettypes" disabled>
            <span itemprop="name">
                Tipos de Bilhete
            </span>
        </a>
        <meta itemprop="position" content="2" />
    </li>
@endsection

@section('content')

    <div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--12-col-desktop margin-top-0">
        
        <div class="mdl-card__title mdl-color--primary mdl-color-text--white">
            <h2 class="mdl-card__title-text logo-style">
                @if ($totalTicketTypes === 1)
                    {{ $totalTicketTypes }} {{ trans('tickettypes.single_total_row') }}
                @elseif ($totalTicketTypes > 1)
                    {{ $totalTicketTypes }} {{ trans('tickettypes.total_rows') }}
                @else
                   {{ trans('tickettypes.empty_rows') }}
                @endif
            </h2>
        </div>

        <div class="mdl-card__supporting-text mdl-color-text--grey-600 padding-0 context">

            <div class="table-responsive material-table">

                <table id="tickettype_table" class="mdl-data-table mdl-js-data-table data-table" cellspacing="0" width="100%">

                    <thead>
                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">{{ trans('tickettypes.description') }}</th>
                            <th class="mdl-data-table__cell--non-numeric">{{ trans('tickettypes.value') }}</th>
                            <th class="mdl-data-table__cell--non-numeric">{{ trans('tickettypes.available_filters') }}</th>
                            <th class="mdl-data-table__cell--non-numeric">{{ trans('tickettypes.active') }}</th>
                            <th class="mdl-data-table__cell--non-numeric">{{ trans('tickettypes.created_at') }}</th>
                            <th class="mdl-data-table__cell--non-numeric">{{ trans('tickettypes.updated_at') }}</th>
                            <th class="mdl-data-table__cell--non-numeric">{{ trans('tickettypes.actions') }}</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($ticket_types as $ticket_type)

                            <tr>
                                <td class="mdl-data-table__cell--non-numeric"><a class="description-ticket" href="{{ URL::to('tickettypes/' . $ticket_type->id) . '/edit' }}">{{ $ticket_type->description }}</a></td>
                                <td class="mdl-data-table__cell--non-numeric"><a href="{{ URL::to('tickettypes/' . $ticket_type->id) . '/edit' }}">{{ $ticket_type->value }}</a></td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <a href="{{ URL::to('tickettypes/' . $ticket_type->id) . '/show' }}">
                                        @if ($ticket_type->available_filters)

                                            <i class="material-icons">done</i>

                                        @else

                                            <i class="material-icons">clear</i>

                                        @endif
                                    </a>
                                </td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <a href="{{ URL::to('tickettypes/' . $ticket_type->id) . '/edit' }}">
                                    @if ($ticket_type->active)

                                        <i class="material-icons">done</i>

                                    @else

                                        <i class="material-icons">clear</i>

                                    @endif
                                    </a>
                                </td>
                                <td class="mdl-data-table__cell--non-numeric"><a href="{{ URL::to('tickettypes/' . $ticket_type->id) . '/edit' }}">{{ $ticket_type->created_at }}</a></td>
                                <td class="mdl-data-table__cell--non-numeric"><a href="{{ URL::to('tickettypes/' . $ticket_type->id) . '/edit' }}">{{ $ticket_type->updated_at }}</a></td>
                                <td class="mdl-data-table__cell--non-numeric">

                                    {{-- EDIT TICKET TYPE ICON BUTTON --}}
                                    <a href="{{ URL::to('tickettypes/' . $ticket_type->id . '/edit') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="{{ trans('tickettypes.edit') }}">
                                        <i class="material-icons mdl-color-text--orange">edit</i>
                                    </a>

                                    {{-- DELETE ICON BUTTON AND FORM CALL --}}
                                    <a href="#dialog_delete" data-tickettype="{{ $ticket_type->id }}" onclick="confirmDeleteTicketType({{ $ticket_type->id }}, '{{ $ticket_type->description }}');" class="delete-tickettype mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="{{ trans('tickettypes.delete') }}">
                                        <i class="material-icons mdl-color-text--red">delete</i>
                                    </a>

                                </td>
                            </tr>

                        @endforeach
                    </tbody>

                </table>

            </div>

        </div>
    
        <div class="mdl-card__menu" style="top: -4px;">

            @include('partials.mdl-highlighter')
            
            @include('partials.mdl-search')

            <a href="{{ url('/tickettypes/create') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="{{ trans('tickettypes.add_new_tickettype') }}">
                <i class="material-icons">add</i>
                <span class="sr-only">{{ trans('tickettypes.add_new_tickettype') }}</span>
            </a>

        </div>

    </div>

    <dialog id="delete-tickettype-dialog" class="mdl-dialog" style="background: white !important;">

        <div id ="report-dialog-content" class="mdl-dialog__content">

            <input id="ticket-type-selected" value="undefined" data-ticket-name="" type="hidden">

            <div id="ticket-type-name" class="align-center"></div>

        </div>

        <div id="report-actions" class="mdl-dialog__actions--full-width">

            <button type="button" id="close-delete-tickettype-button" class="mdl-button close">{{ trans('charts.button_cancel') }}</button>
            <button type="button" id="delete-tickettype-button" class="mdl-button confirm-action align-right">{{ trans('tickettypes.delete') }}</button>

        </div>

    </dialog>

@endsection

@section('footer_scripts')

    @include('scripts.highlighter-script')

    @include('scripts.mdl-datatables')

    <script type="text/javascript">

        function confirmDeleteTicketType(tickettype_id, description) {

            var dialog = document.querySelector('#delete-tickettype-dialog');

            $('#ticket-type-selected').val(tickettype_id);
            $('#ticket-type-selected').attr('data-ticket-name', description);
            $('#ticket-type-name').text("");
            $('#ticket-type-name').append("{{ trans('tickettypes.delete') }} " + description + "?");

            dialog.showModal();

        }

        $('#close-delete-tickettype-button').on('click', function () {

            var dialog = document.querySelector('#delete-tickettype-dialog');
            dialog.close();

        });

        $('#delete-tickettype-button').on('click', function() {

            var tickettype_id =$('#ticket-type-selected').val();

            $.ajax({
                type: 'DELETE',
                url: '/tickettypes/' + tickettype_id + '/delete/',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                success: function (data) {

                    if (data['success']) {

                        window.location.href = "/tickettypes";

                    } else {

                        console.log('error');

                    }

                }

            });

        });

    </script>

@endsection