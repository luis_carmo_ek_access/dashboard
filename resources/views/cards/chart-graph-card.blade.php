<style scooped>

    @media print {

        body * {
            visibility: hidden;
        }

        .chart_to_print, .chart_to_print * {
            visibility: visible;
        }

        .chart_to_print {
            position: absolute;
            left: 0;
            top: 0;
            max-width: 100%;
            max-height: 100%;
            background: yellow;
        }

    }

    #scrollable-filllevel {

        overflow-y: auto;
        overflow-x: hidden;
        max-height: 600px;

    }

    #fill-level-data-table {

        width: 100%;
        overflow-y: auto;
        overflow-x: auto;

    }

    #fill-level-data-table td {
        text-align: center;
    }

    .mdl-card__actions {
        display: flex;
        box-sizing:border-box;
        align-items: center;
    }
    .mdl-card__actions > .mdl-button--icon {
        margin-right: 3px;
        margin-left: 3px;
    }

    button#select-event-button {
        float: right;
    }

    button#select-hour-list-button {
        float: right;
    }

    button#select-ticket-type-list-button {
        float: right;
    }

    #warnings-list {

        width: 100%;

    }

    #warnings-list thead tr {

        height: 32px;

    }

    .mdl-data-tabl tbody tr {

        height: 32px;

    }

    .text-align-right {

        float: right;

    }

    #warnings-list .mdl-badge[data-badge]:after {

        align-items: center !important;
        position: absolute;
        top: -5px !important;
        right: 10px !important;
        font-family: "Roboto","Helvetica","Arial",sans-serif !important;
        font-weight: 600 !important;
        font-size: 12px !important;
        width: 22px !important;
        height: 22px !important;
        border-radius: 50% !important;
        background: #f0ad4e !important;
        color: #fff !important;
        padding: 5px !important;

    }

    @media screen and (max-width: 1920px) {

        #warnings-list .mdl-badge[data-badge]:after {

            /*
                top: 0px !important;
                right: 10px !important;
                left: 420px !important;
                bottom: 15px !important;
            */

            top: -10px !important;
            right: 10px !important;
            /*left: 400px !important;*/
            bottom: 15px !important;
            padding: 7px !important;

        }

    }

    @media only screen and (max-width: 600px) {

        #warnings-list .mdl-badge[data-badge]:after {

            top: 0px !important;
            right: 10px !important;
            /*left: 300px !important;*/
            bottom: 15px !important;

        }

        #ticket-type-filter-dialog {

            width: 320px;

        }

    }

    @media only screen and (max-width: 1400px) {

        #warnings-list .mdl-badge[data-badge]:after {

            top: 0px !important;
            right: 10px !important;
            /*left: 250px !important;*/
            bottom: 15px !important;

        }

        #ticket-type-filter-dialog {

            width: 320px;

        }

    }

    @media screen and (max-width: 1440px) {

        #warnings-list .mdl-badge[data-badge]:after {

            /*
                top: 0px !important;
                right: 10px !important;
                left: 250px !important;
                bottom: 15px !important;
            */

            top: -15px !important;
            right: 10px !important;
            /*left: 220px !important;*/
            bottom: 15px !important;
            padding: 7px !important;

        }

        #ticket-type-filter-dialog {

            width: 320px;

        }

    }

    @media only screen and (width: 1440px) {

        #warnings-list .mdl-badge[data-badge]:after {

            /*
                top: 0px !important;
                right: 10px !important;
                left: 300px !important;
                bottom: 15px !important;
            */

            top: -10px !important;
            right: 0px !important;
            /*left: 270px !important;*/
            bottom: 15px !important;
            padding: 7px !important;

        }

        #ticket-type-filter-dialog {

            width: 320px;

        }

    }

    html, body, .spinner{
        height:100%;
        overflow: hidden;
    }

    .mdl-spinner{
        height:100px;
        width:100px;
    }

    .spinner{
        position:relative;
        display: flex;
        align-items: center;
        justify-content: center;
        min-height: 50px;
    }

    .spinner-on {
        display: flex;
    }

    button#refresh-all {

        float: right;

    }

    dialog#load-data-dialog {

        background-color: rgba(255, 255, 255, 0);
        box-shadow: 0 9px 46px 8px rgba(0,0,0,.0), 0 11px 15px -7px rgba(0,0,0,0), 0 24px 38px 3px rgba(0,0,0,0);

    }

    table#ticket-types-list {
        width: 100%;
    }

    table#ticket-types-list td {
        text-align: left;
    }

    #ticketTypes.mdl-dialog__content {

        padding: 20px 0px 20px 0px;

    }

    .flatpickr-calendar.hasTime.animate.showTimeInput.arrowTop.open {
        top: 55px !important;
        left: 0px !important;
    }

    .flatpickr-calendar.hasTime.animate.arrowTop.open {
        top: 55px !important;
        left: 0px !important;
    }

    .flatpickr-calendar.hasTime.animate.open.arrowBottom {
        top: 55px !important;
        left: 0px !important;
    }

    .dialog-search-text {
        font-size: 12px !important;
    }

    #close-search-ticket-barcode {
        float: left !important;
    }

    #close-gates-error {
        float: left !important;
    }
    
    #select-search-ticket-barcode {
        float: right !important;
    }

    dialog#search-ticket-barcode-dialog {
        width: 80% !important;
    }

    dialog#gates-information-errors-dialog {
        width: 80% !important;
        top: 10% !important;
    }

    table#gates-information-table {
        width: 100% !important;
    }

    table#gates-information-table td {
        text-align: center !important;
    }

    table#gates-information-table-header td {
        text-align: center !important;
    }

    table#gates-information-table-header {
        width: 100% !important;
    }

    table#transactions-history {
        width: 100% !important;
    }

    #transaction-badge:after {
        margin-top: 10px;
        background: #4166c6;
    }

    table#transactions-list {
        width: 100%;
    }

    div#warnings-card {
        max-height: 500px;
    }

     div#transactions-card {
        max-height: 500px;
    }

    #transactions-card .mdl-card__menu .material-icons {
        color: #000000;
    }

    input#highlight_info {
        color: #000;
    }

    input#search_table {
        color: #000;
    }

    .transactions-search-result {
        max-height: 400px;
    }

    #gates-error {

        overflow-x: auto;
        overflow-y: auto;
        max-height: 600px;

    }

    #gates-error-header {

        overflow-y: auto;

    }

    .venue-row {

        background-color: #30BDCC !important;

    }

    .zone-row {

        background-color: #30BDCC !important;

    }

    table#gates-information-errors-table {

        width: 100%;

    }

    dialog#error-by-gates-filtered {

        width: 600px;
        max-height: 600px;

    }

    div#gates-error-filtered {
        overflow-x: auto !important;
        overflow-y: auto !important;
        max-height: 480px;
    }

    table#warnings-list-filtered td:first-of-type {
        text-align: left;
    }

    #warnings-list-filtered {

        width: 100% !important;
        overflow-x: auto !important;
        overflow-y: auto !important;

    }

    @media (max-width: 839px) and (min-width: 480px) {

        .mdl-cell--4-col, .mdl-cell--4-col-tablet.mdl-cell--4-col-tablet {
            width: calc(50% - 16px);
            max-height: 700px !important;
        }

        #warnings-list .mdl-badge[data-badge]:after {

            top: -5px !important;
            right: 0px !important;
            left: -25px !important;
            bottom: 0px !important;
            padding: 6px !important;

        }

        #transaction-badge:after {
            margin-top: 10px;
            left: 20px;
            background: #4166c6;
        }

    }

</style>

@extends('layouts.dashboard')

@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('breadcrumbs')

    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
			<span itemprop="name">
				{{ trans('titles.app') }}
			</span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li class="active">
        {{ trans('titles.dashboard') }}
    </li>

@endsection

@section('content')

    {{-- Event Selected Data --}}
    <input id="event-selected" value="undefined" data-code="undefined" data-lotation="undefined" data-open-gates="undefined" type="hidden">
    {{-- Flowrate Filter Data --}}
    <input id="flowrate-filter-selected" value="default" data-search-type="default" data-update-filter="" data-options="" type="hidden">
    {{-- FillLevel Filter Data --}}
    <input id="filllevel-filter-selected" value="default" data-search-type="default" data-update-filter="" data-options="" type="hidden">
    {{-- Entries x Exits Filter Data --}}
    <input id="entries-exits-filter-selected" value="default" data-search-type="default" data-update-filter="" data-options="" type="hidden">
    {{-- Warnings Filter Data --}}
    <input id="warnings-filter-selected" value="default" data-search-type="default" data-update-filter="" data-options="" type="hidden">
    {{-- Transactions Filter Data --}}
    <input id="transactions-filter-selected" value="default" data-search-type="default" data-update-filter="" data-options="" type="hidden">
    {{-- Gates Information Filter Data --}}
    <input id="gates-filter-selected" value="default" data-search-type="default" data-update-filter="" data-options="" type="hidden">

    {{-- _______________________________________________________________________________________________________ --}}

    {{-- Options Buttons --}}
    <div class="mdl-cell mdl-cell--12-col">

        {{-- Select Event --}}
        <button type="button" id="choose-event" class="mdl-chip">
            <span class="mdl-chip__text">{{ trans('charts.choose_event') }}</span>
        </button>

        {{-- Refresh All --}}
        <button type="button" id="refresh-all" class="mdl-chip align-right">
            <span class="mdl-chip__text">{{ trans('charts.refresh_all') }}</span>
        </button>

        <div id="select-event-warning" class="mdl-js-snackbar mdl-snackbar">
            <div class="mdl-snackbar__text"></div>
            <button class="mdl-snackbar__action" type="button"></button>
        </div>

    </div>

    {{-- _______________________________________________________________________________________________________ --}}

    {{-- flowrate --}}

    <div class="mdl-card mdl-shadow--4dp mdl-cell margin-top-0-important mdl-cell--2-col mdl-cell--12-col-tablet md-cell--2-col-phone mdl-cell--8-col">

        <div class="mdl-card__menu">

            <button id="flowrate-filters" class="mdl-button mdl-js-button mdl-button--icon">
                <i class="material-icons">more_vert</i>
            </button>

            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="flowrate-filters">
                <li class="mdl-menu__item" onclick="defaultFilter('flowrate');">{{ trans('charts.default_filter') }}</li>
                <li class="mdl-menu__item" onclick="ticketTypeFilter('flowrate');">{{ trans('charts.ticket_type_filter') }}</li>
                {{-- <li class="mdl-menu__item" disabled onclick="zoneDacFilter('flowrate');">{{ trans('charts.zone_dac_filter') }}</li> --}}
                <li class="mdl-menu__item" onclick="hourFilter('flowrate');">{{ trans('charts.hour_filter') }}</li>
            </ul>

        </div>

        <div class="mdl-card__title">
            <div id="refresh-flowrate" style="display: flex;" class="align-right"><i class="material-icons">refresh</i></div>
        </div>

        <div id="flowrate" class="mdl-card__supporting-text chart_to_print">
            <canvas id="flowrate-venue"></canvas>
        </div>

        <div class="mdl-card__actions mdl-card--border">

            <div class="mdl-layout-spacer"></div>
            <button id="print-flowrate" class="mdl-button mdl-button--icon mdl-button--colored"><i class="material-icons">print</i></button>

        </div>

    </div>

    {{-- Fill Level --}}

    <div class="mdl-card mdl-shadow--4dp mdl-cell margin-top-0-important mdl-cell--2-col mdl-cell--4-col-tablet mdl-cell--4-col">

        <div class="mdl-card__menu">

            <button id="fill-level-filters" class="mdl-button mdl-js-button mdl-button--icon">
                <i class="material-icons">more_vert</i>
            </button>

            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="fill-level-filters">
                <li class="mdl-menu__item" onclick="defaultFilter('filllevel');">{{ trans('charts.default_filter') }}</li>
                <li class="mdl-menu__item" onclick="ticketTypeFilter('filllevel');">{{ trans('charts.ticket_type_filter') }}</li>
                {{-- <li class="mdl-menu__item" disabled onclick="zoneDacFilter('filllevel');">{{ trans('charts.zone_dac_filter') }}</li> --}}
                <li class="mdl-menu__item" onclick="hourFilter('filllevel');">{{ trans('charts.hour_filter') }}</li>
            </ul>

        </div>

        <div class="mdl-card__title">
            <div id="refresh-fill-level-venue" style="display: flex;" class="align-right"><i class="material-icons">refresh</i></div>
        </div>

        <div id="fill-level" class="mdl-card__supporting-text chart_to_print">
            <div id="scrollable-filllevel">
                <canvas id="fill-level-venue"></canvas>
            </div>
        </div>

        <div class="mdl-card__actions mdl-card--border">
            <div class="mdl-layout-spacer"></div>
            <button id="print-fill-level" class="mdl-button mdl-button--icon mdl-button--colored"><i class="material-icons">print</i></button>
        </div>

    </div>

    {{-- Warnings --}}

    <div id="warnings-card" class="mdl-card mdl-shadow--4dp mdl-cell margin-top-0-important mdl-cell--2-col mdl-cell--4-col-tablet mdl-cell--6-col">

        <div class="mdl-card__menu">

            <button id="warnings-filters" class="mdl-button mdl-js-button mdl-button--icon">
                <i class="material-icons">more_vert</i>
            </button>

            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="warnings-filters">
                <li class="mdl-menu__item" onclick="defaultFilter('warnings');">{{ trans('charts.default_filter') }}</li>
                {{-- <li class="mdl-menu__item" disabled onclick="ticketTypeFilter('warnings');">{{ trans('charts.ticket_type_filter') }}</li> --}}
                {{-- <li class="mdl-menu__item" disabled onclick="zoneDacFilter('warnings');">{{ trans('charts.zone_dac_filter') }}</li> --}}
                <li class="mdl-menu__item" onclick="hourFilter('warnings');">{{ trans('charts.hour_filter') }}</li>
                <li class="mdl-menu__item" onclick="gatesErrors('warnings');">{{ trans('charts.gate_filter') }}</li>
            </ul>

        </div>

        <div class="mdl-card__title">
            <div id="refresh-warnings-venue" style="display: flex;" class="align-right"><i class="material-icons">refresh</i></div>
        </div>

        <div id="warnings" class="mdl-card__supporting-text chart_to_print" style="overflow-x: auto; overflow-y: auto">
            <table id="warnings-list" class="mdl-data-table mdl-js-data-table">
                <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">{{ trans('charts.message') }}</th>
                        <th class="mdl-data-table__cell">{{ trans('charts.occurrences') }}</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

        <div class="mdl-card__actions mdl-card--border">
            <div class="mdl-layout-spacer"></div>
            <button id="print-warnings" class="mdl-button mdl-button--icon mdl-button--colored"><i class="material-icons">print</i></button>
        </div>

    </div>

    {{-- Transactions --}}

    <div id="transactions-card" class="mdl-card mdl-shadow--4dp mdl-cell margin-top-0-important mdl-cell--2-col mdl-cell--12-col-tablet md-cel--2-col-phone mdl-cell--6-col">

        <div class="mdl-card__menu" style="top: -4px;">

            @include('partials.mdl-highlighter')
            @include('partials.mdl-search')

            <button id="transactions-filters" class="mdl-button mdl-js-button mdl-button--icon">
                <i class="material-icons">more_vert</i>
            </button>

            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="transactions-filters">
                <li class="mdl-menu__item" onclick="defaultFilter('transactions');">{{ trans('charts.default_filter') }}</li>
                <li class="mdl-menu__item" onclick="searchTicketBarcode();">{{ trans('charts.search_by_barcode') }}</li>
                {{-- <li class="mdl-menu__item" disabled onclick="zoneDacFilter('transactions');">{{ trans('charts.zone_dac_filter') }}</li> --}}
                {{--<li class="mdl-menu__item" onclick="">{{ trans('charts.search_by_gate') }}</li>--}}
            </ul>

        </div>

        <div class="mdl-card__title">
            <div id="refresh-transactions-venue" style="display: flex;" class="align-right"><i class="material-icons">refresh</i></div>
        </div>

        <div id="transactions" class="mdl-card__supporting-text context chart_to_print" style="overflow-x: auto; overflow-y: auto">

            <div class="table-responsive material-table">

                <table id="transactions-list" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                    <thead>
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric">{{ trans('charts.text_barcode')  }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ trans('charts.text_zone')  }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ trans('charts.text_dac')  }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ trans('charts.text_is_entry')  }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ trans('charts.text_message')  }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ trans('charts.text_datetime')  }}</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric">{{ trans('charts.text_barcode')  }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ trans('charts.text_zone')  }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ trans('charts.text_dac')  }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ trans('charts.text_is_entry')  }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ trans('charts.text_message')  }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ trans('charts.text_datetime')  }}</td>
                        </tr>
                    </tfoot>
                </table>

            </div>

        </div>

        <div class="mdl-card__actions mdl-card--border">
            <div class="mdl-layout-spacer"></div>
            <button id="print-transactions" class="mdl-button mdl-button--icon mdl-button--colored"><i class="material-icons">print</i></button>
        </div>

    </div>

    {{-- Gates Information --}}

    <div id="gates-card" class="mdl-card mdl-shadow--4dp mdl-cell margin-top-0-important mdl-cell--12-col">

        <div class="mdl-card__menu">

            <button id="gates-filters" class="mdl-button mdl-js-button mdl-button--icon">
                <i class="material-icons">more_vert</i>
            </button>

            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="gates-filters">
                <li class="mdl-menu__item" onclick="defaultFilter('gates');">{{ trans('charts.default_filter') }}</li>
            </ul>

        </div>

        <div class="mdl-card__title">
            <div id="refresh-gates-information" style="display: flex;" class="align-right"><i class="material-icons">refresh</i></div>
        </div>

        <div id="gates-information" class="mdl-card__supporting-text chart_to_print" style="overflow-x: auto; overflow-y: auto">
                <canvas id="gates-information-venue"></canvas>
        </div>

        <div class="mdl-card__actions mdl-card--border">
            <div class="mdl-layout-spacer"></div>
            <button id="print-gates-information" class="mdl-button mdl-button--icon mdl-button--colored"><i class="material-icons">print</i></button>
        </div>

    </div>

    {{-- _______________________________________________________________________________________________________ --}}

    {{-- Event Dialog --}}

    <dialog id="events-dialog" class="mdl-dialog">

        <h4 class="mdl-dialog__title">{{ trans('charts.event_select') }}</h4>
        <div id ="events-list" class="mdl-dialog__content">

        </div>
        <div class="mdl-dialog__actions--full-width">

            <button type="button" id="close-event-button" class="mdl-button close">{{ trans('charts.button_cancel') }}</button>
            <button type="button" id="select-event-button" class="mdl-button confirm-action pull-right">{{ trans('charts.button_select') }}</button>

            {{-- Snackbar Event Required --}}
            <div id="demo-snackbar-example" class="mdl-js-snackbar mdl-snackbar">
                <div class="mdl-snackbar__text"></div>
                <button class="mdl-snackbar__action" type="button"></button>
            </div>

        </div>
    </dialog>

    {{-- Loading Data Dialog --}}

    <dialog id="load-data-dialog" class="mdl-dialog">
        <div class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div>
    </dialog>

    {{-- Ticket Type Filter Dialog --}}

    <dialog id="ticket-type-filter-dialog" class="mdl-dialog">

        <input id="ticket-type-filter-edit" value="" type="hidden">
        <input id="ticket-types-all-checked" value="false" type="hidden">

        <div id="ticketTypes" class="mdl-dialog__content">

            <table id="ticket-types-list" class="mdl-data-table mdl-js-data-table">
                <thead>
                <tr>
                    <th>
                        <label class = "mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect"
                               for = "select-all-tickets">
                            <input type = "checkbox" id = "select-all-tickets" class = "mdl-checkbox__input">
                            <span class = "mdl-checkbox__label"></span>
                        </label>
                    </th>
                    <th class="mdl-data-table__cell--non-numeric">{{ trans('charts.ticket_type_label') }}</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>

        <div class="mdl-dialog__actions--full-width">

            <input id="hour-filter-edit" value="" type="hidden">

            <button type="button" id="select-ticket-type-list-button" class="mdl-button pull-right">{{ trans('charts.button_select') }}</button>
            <button type="button" id="close-ticket-type-list-button" class="mdl-button close">{{ trans('charts.button_cancel') }}</button>

            {{-- Snackbar Ticket Types Required --}}
            <div id="select-ticket-type-warning" class="mdl-js-snackbar mdl-snackbar">
                <div class="mdl-snackbar__text"></div>
                <button class="mdl-snackbar__action" type="button"></button>
            </div>

        </div>

    </dialog>

    {{-- Zone / Dac Filter Dialog --}}

    <dialog id="zone-dac-filter-dialog" class="mdl-dialog">

        <input id="zone-dac-filter-edit" value="" type="hidden">

        <div id="zonesDacsAvailable" class="mdl-card__supporting-text" style="overflow-x: auto; overflow-y: auto">

            <table id="zones-dacs-list" class="mdl-data-table mdl-js-data-table">
                <thead>
                <tr>
                    <th class="mdl-data-table__cell--non-numeric">{{ trans('charts.zone_label') }}</th>
                    <th class="mdl-data-table__cell--non-numeric">{{ trans('charts.dac_label') }}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

        <div class="mdl-dialog__actions">

            <button type="button" id="select-zone-dac-list-button" class="mdl-button">{{ trans('charts.button_select') }}</button>
            <button type="button" id="close-zone-dac-list-button" class="mdl-button close">{{ trans('charts.button_cancel') }}</button>

            {{-- Snackbar Zones / Dacs Required --}}
            <div id="select-zones-dacs-warning" class="mdl-js-snackbar mdl-snackbar">
                <div class="mdl-snackbar__text"></div>
                <button class="mdl-snackbar__action" type="button"></button>
            </div>

        </div>

    </dialog>

    {{-- Hour Filter Dialog --}}

    <dialog id="hour-filter-dialog" class="mdl-dialog">

        <input id="hour-filter-edit" value="" type="hidden">

        <h4 class="mdl-dialog__title">{{ trans('charts.hour_select') }}</h4>
        <div id="day-hour-select" class="mdl-dialog__content">
        </div>

        <div class="mdl-dialog__actions--full-width">

            <button type="button" id="close-hour-list-button" class="mdl-button close">{{ trans('charts.button_cancel') }}</button>
            <button type="button" id="select-hour-list-button" class="mdl-button pull-right">{{ trans('charts.button_select') }}</button>

            {{-- Snackbar Zones / Dacs Required --}}
            <div id="select-hour-warning" class="mdl-js-snackbar mdl-snackbar">
                <div class="mdl-snackbar__text"></div>
                <button class="mdl-snackbar__action" type="button"></button>
            </div>

        </div>

    </dialog>

    {{-- Search Ticket Barcode --}}

    <dialog id="search-ticket-barcode-dialog" class="mdl-dialog">

        <h4 class="mdl-dialog__title">{{ trans('charts.search_by_barcode')  }}</h4>

        <div id="barcode-number" class="mdl-dialog__content">
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height">
                <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="barcode-search">
                <label class="mdl-textfield__label" for="barcode-search">{{ trans('charts.ticket_barcode') }}</label>
                <span class="mdl-textfield__error">{{ trans('charts.error_not_number')  }}</span>
            </div>
            <div class="transactions-search-result" style="overflow-x: auto; overflow-y: auto">

                <table id="transactions-history" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                    <thead>
                    <tr>
                        <td>{{ trans('charts.text_barcode')  }}</td>
                        <td>{{ trans('charts.text_zone')  }}</td>
                        <td>{{ trans('charts.text_dac')  }}</td>
                        <td>{{ trans('charts.text_is_entry')  }}</td>
                        <td>{{ trans('charts.text_message')  }}</td>
                        <td>{{ trans('charts.text_datetime')  }}</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    <tr>
                        <td>{{ trans('charts.text_barcode')  }}</td>
                        <td>{{ trans('charts.text_zone')  }}</td>
                        <td>{{ trans('charts.text_dac')  }}</td>
                        <td>{{ trans('charts.text_is_entry')  }}</td>
                        <td>{{ trans('charts.text_message')  }}</td>
                        <td>{{ trans('charts.text_datetime')  }}</td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>

        <div class="mdl-dialog__actions--full-width">

            <button type="button" id="close-search-ticket-barcode" class="mdl-button close">{{ trans('charts.button_cancel')  }}</button>
            <button type="button" id="select-search-ticket-barcode" class="mdl-button pull-right">{{ trans('charts.button_select') }}</button>

            <div id="select-search-ticket-barcode-warning" class="mdl-js-snackbar mdl-snackbar">
                <div class="mdl-snackbar__text"></div>
                <button class="mdl-snackbar__action" type="button"></button>
            </div>

        </div>

    </dialog>

    {{-- Gates Error Information --}}

    <dialog id="gates-information-errors-dialog" class="mdl-dialog">

        <h4 class="mdl-dialog__title">{{ trans('charts.text_gates_error')  }}</h4>

        <div id="gates-error-header" class="mdl-dialog__content"></div>

        <div id="gates-error" class="mdl-dialog__content"></div>

        <div class="mdl-dialog__actions--full-width">

            <button type="button" id="close-gates-error" class="mdl-button close">{{ trans('charts.button_cancel')  }}</button>

        </div>

    </dialog>

    {{-- _______________________________________________________________________________________________________ --}}

    {{-- Display Errors By Gates --}}

    <dialog id="error-by-gates-filtered" class="mdl-dialog">

        <h4 class="mdl-dialog__title">{{ trans('charts.occurrences_at') }}</h4>

        <div id="gates-error-filtered" class="mdl-dialog__content">

            <table id="warnings-list-filtered" class="mdl-data-table mdl-js-data-table">
                <thead>
                <tr>
                    <th class="mdl-data-table__cell--non-numeric sort" data-sort="description">{{ trans('charts.gate') }}</th>
                    <th class="mdl-data-table__cell sort" data-sort="quantity">{{ trans('charts.occurrences') }}</th>
                </tr>
                </thead>
                <tbody class="list"></tbody>
            </table>

        </div>

        <div class="mdl-dialog__actions--full-width">

            <button type="button" id="close-gates-error-filtered" class="mdl-button close">{{trans('charts.button_cancel')}}</button>

        </div>

    </dialog>

@endsection

@section('footer_scripts')

    @include('scripts.highlighter-script')

    @include('scripts.mdl-datatables')

    <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>

    <script>

        /**
         *
         * Events Actions
         *
         */

        function selectEvent(event) {

            $('#event-options').addClass('is-dirty');
            $('#event-select').val($(event).text());
            $('#event-selected').val($(event).attr('data-val'));
            $('#event-selected').attr('data-code', $(event).attr('data-code'));
            $('#event-selected').attr('data-lotation', $(event).attr('data-lotation'));
            $('#event-selected').attr('data-open-gates', $(event).attr('data-open-gates'));
            document.getElementById("error-select-event").style.visibility = "hidden";
            $("#event-options").removeClass("is-invalid");

        }

        /**
         *
         * Filter Chart Actions
         *
         */

        function defaultFilter(chart_type) {

            var event_id = $('#event-selected').val();

            updateChartFilterSelected(chart_type, 'default');

            /**
             *
             * Load Chart
             *
             */

            if (chart_type == 'filllevel') {

                /**
                 * Load Fill Level
                 *
                 * @return Object
                 */

                $.ajax({
                    url: '/event/' + event_id + '/filllevel/',
                    dataType: 'json',
                    success: function (data) {

                        /**
                         * @return filllevel
                         */

                        document.getElementById("fill-level").innerHTML = '&nbsp;';
                        document.getElementById("fill-level").innerHTML = '<div id="scrollable-filllevel"><canvas id="fill-level-venue"></canvas><table id="fill-level-data-table"></table></div>';

                        $('#fill-level-data-table').addClass('mdl-data-table mdl-js-data-table');

                        var ctx = document.getElementById("fill-level-venue").getContext("2d");

                        var total_lotation = $("#event-selected").attr("data-lotation");

                        if (total_lotation < 1) {
                            total_lotation = data.total;
                        }

                        var missing_lotation = total_lotation - data.total;

                        var filled_in = data.total;

                        var fill_by_total = data.total / total_lotation;

                        var fill_percentage = fill_by_total * 100;

                        var symbol;

                        if (fill_percentage != 0 && !isNaN(fill_percentage)) {
                            symbol = '%';
                        } else if (fill_percentage < 1 && !isNaN(fill_percentage)) {
                            symbol = '%';
                        }

                        var backgroundColorChartLine;

                        if (fill_percentage.toFixed(0) < 40) {
                            backgroundColorChartLine = "#2DA0CB";
                        } else if (fill_percentage.toFixed(0) >= 40 && fill_percentage.toFixed(0) < 80) {
                            backgroundColorChartLine = "#3852A8";
                        } else if (fill_percentage.toFixed(0) >= 80) {
                            backgroundColorChartLine = "#1b3066";
                        }

                        var fill_table_data = '';

                        fill_table_data += '<thead><tr><td>Presenças</td><td>Disponivel</td></tr></thead>';
                        fill_table_data += '<tbody><tr><td>' + data.total + '</td><td>' + missing_lotation + '</td></tr></tbody>';
                        fill_table_data += '<tfoot><tr><td>Presenças</td><td>Disponivel</td></tr></tfoot>';

                        $('#fill-level-data-table').append(fill_table_data);

                        var myChartCircle = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Presenças", "Disponivel"],
                                datasets: [{
                                    label: "Fill Level",
                                    backgroundColor: [backgroundColorChartLine],
                                    data: [data.total, missing_lotation]
                                }]
                            },
                            plugins: [{
                                beforeDraw: function(chart) {
                                    var width = chart.chart.width,
                                        height = chart.chart.height,
                                        ctx = chart.chart.ctx;

                                    ctx.restore();
                                    var fontSize = (height / 150).toFixed(2);
                                    ctx.font = fontSize + "em sans-serif";
                                    ctx.fillStyle = backgroundColorChartLine;
                                    ctx.textBaseline = "middle";

                                    var text = (isNaN(fill_percentage) ? filled_in : fill_percentage.toFixed(2) + symbol),
                                        textX = Math.round((width - ctx.measureText(text).width) / 2),
                                        textY = height / 2;

                                    ctx.fillText(text, textX, textY);
                                    ctx.save();
                                }
                            }],
                            options: {
                                legend: {
                                    display: true,
                                    onClick: function(e, legendItem) {

                                        var index = legendItem.index;
                                        var ci = this.chart;
                                        var meta = ci.getDatasetMeta(0);

                                        if (meta.data[index].hidden == true) {

                                            if (index == '0') {

                                                var width = this.chart.chart.width,
                                                    height = this.chart.chart.height,
                                                    ctx = this.chart.chart.ctx;

                                                var text = total_lotation,
                                                    textX = Math.round((width - ctx.measureText(text).width) / 2),
                                                    textY = height / 2;

                                                ctx.fillText(text, textX, textY);
                                                ctx.save();

                                            } else if (index == '1') {

                                                var width = this.chart.chart.width,
                                                    height = this.chart.chart.height,
                                                    ctx = this.chart.chart.ctx;

                                                var text = missing_lotation,
                                                    textX = Math.round((width - ctx.measureText(text).width) / 2),
                                                    textY = height / 2;

                                                ctx.fillText(text, textX, textY);
                                                ctx.save();

                                            }

                                            meta.data[index].hidden = false;

                                        } else {

                                            if (index == '0') {

                                                var width = this.chart.chart.width,
                                                    height = this.chart.chart.height,
                                                    ctx = this.chart.chart.ctx;

                                                var text = missing_lotation,
                                                    textX = Math.round((width - ctx.measureText(text).width) / 2),
                                                    textY = height / 2;

                                                ctx.fillText(text, textX, textY);
                                                ctx.save();

                                            } else if (index == '1') {

                                                var width = this.chart.chart.width,
                                                    height = this.chart.chart.height,
                                                    ctx = this.chart.chart.ctx;

                                                var text = total_lotation,
                                                    textX = Math.round((width - ctx.measureText(text).width) / 2),
                                                    textY = height / 2;

                                                ctx.fillText(text, textX, textY);
                                                ctx.save();

                                            }

                                            meta.data[index].hidden = true;

                                        }

                                        ci.update();

                                    },
                                },
                                responsive: true,
                                maintainAspectRatio: true,
                                cutoutPercentage: 85
                            }

                        });

                    }

                });

            } else if (chart_type == 'flowrate') {

                $.ajax({
                    url: '/event/' + event_id + '/flowrate/',
                    dataType: 'json',
                    success: function (data) {

                        /**
                         * @return Flowrate
                         */

                        var labels_chart = [];

                        var values_chart = [];

                        data['flowrate'].forEach(function(time) {

                            labels_chart.push(time['hour_transaction']);
                            values_chart.push(time['entries']);

                        });

                        document.getElementById("flowrate").innerHTML = '&nbsp;';
                        document.getElementById("flowrate").innerHTML = '<canvas id="flowrate-venue"></canvas>';

                        var ctx_flowrate = document.getElementById("flowrate-venue").getContext('2d');

                        var flowrateChart = new Chart(ctx_flowrate, {

                            type: 'line',
                            data: {
                                labels: labels_chart,
                                datasets: [
                                    {
                                        data: values_chart,
                                        label: "Entries",
                                        borderColor: "#3e95cd",
                                        fill: false
                                    }
                                ]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: 'Flowrate'
                                }
                            }

                        });

                    }

                });

            } else if (chart_type == 'warnings') {

                $.ajax({

                    url: '/event/' + event_id + '/warnings/',
                    dataType: 'json',
                    beforeSend: function() {

                        var card_height = $('#warnings-card').height();
                        $('#warnings-card').css('height', card_height);

                        $("#warnings-list tbody > *").remove();

                        var html = '';
                        html += '<tr>';
                        html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="2"><div class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div></td>';
                        html +=	'</tr>'

                        $("#warnings-list tbody").append(html);

                        componentHandler.upgradeDom();

                    },
                    success: function (data) {

                        if (data['error']) {

                            $("#warnings-list tbody > *").remove();

                            var html = '';
                            html += '<tr>';
                            html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="2">{{ trans('chart.unavailable') }}</td>';
                            html +=	'</tr>'

                            $("#warnings-list tbody").append(html);

                            $('#warnings-card').removeAttr("style");

                            componentHandler.upgradeDom();

                        } else {

                            $("#warnings-list tbody > *").remove();

                            /**
                             * @return filllevel
                             */

                            var html = '';
                            var total_errors = 0;

                            data.forEach(function(error) {

                                total_errors = total_errors + error['quantity'];

                                html += '<tr>';
                                html +=	'	<td class="mdl-data-table__cell--non-numeric">' + error['error_message'] + '</td>';
                                html +=	'	<td class="mdl-data-table__cell"><span class="mdl-badge" data-badge="' + error['quantity'] + '"></span></td>';
                                html +=	'</tr>';

                            });

                            html += '<tr>';
                            html +=	'	<td class="mdl-data-table__cell--non-numeric">{{ trans('charts.total') }}</td>';
                            html +=	'	<td class="mdl-data-table__cell"><span class="mdl-badge" data-badge="' + total_errors + '"></span></td>';
                            html +=	'</tr>'

                            $("#warnings-list tbody").append(html);

                            $('#warnings-card').removeAttr("style");

                            componentHandler.upgradeDom();

                        }

                    }

                });

            } else if (chart_type == 'transactions') {

                $.ajax({
                    url: '/event/' + event_id + '/transactions/',
                    dataType: 'json',
                    beforeSend: function () {

                        $("#transactions-list tbody > *").remove();

                        var html = '';
                        html += '<tr>';
                        html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="6"><div class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div></td>';
                        html +=	'</tr>'

                        $("#transactions-list tbody").append(html);

                        componentHandler.upgradeDom();

                    },
                    success: function (data) {

                        if (data['error']) {

                            $("#transactions-list tbody > *").remove();

                            var html = '';
                            html += '<tr>';
                            html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="6">{{ trans('chart.unavailable') }}</td>';
                            html +=	'</tr>'

                            $("#transactions-list tbody").append(html);

                            $('#transactions-card').removeAttr("style");

                            componentHandler.upgradeDom();

                        } else {

                            $("#transactions-list tbody > *").remove();

                            var html = '';

                            var table_data = data['data'];
                            var next_page = data['next_page_url'];

                            if (table_data.length < 1) {

                                html += '<tr>';
                                html += '<td colspan="7" style="text-align: center;">{{ trans('chart.unavailable') }}</td>';
                                html += '</tr>';

                            } else {

                                table_data.forEach(function (transaction) {

                                    var badge_text = ((transaction.isEntry == 1) ? 'Sim' : 'Não');

                                    html += '<tr>';
                                    html += '<td>' + transaction.titulo + '</td>';
                                    html += '<td>' + transaction.zonaDescricao + '</td>';
                                    html += '<td>' + transaction.dacDescricao + '</td>';
                                    html += '<td><span id="transaction-badge" class="mdl-badge" data-badge="' + badge_text + '" ' + transaction.isEntry + ' style="margin-top: 10px !important;"></span></td>';
                                    html += '<td>' + transaction.erro_mensagemOperador + '</td>';
                                    html += '<td>' + transaction.date + '</td>';
                                    html += '</tr>';

                                });

                                html += '<tr>';
                                html += '<td colspan="7" style="text-align: center;"><div id="load-more-transactions" data-page="' + next_page + '"><i class="material-icons">add</i></div></td>';
                                html += '</tr>';

                            }

                            $("#transactions-list tbody").append(html);

                            $('#transactions-card').removeAttr("style");

                            componentHandler.upgradeDom();

                        }

                    }
                });

            } else if (chart_type == 'gates') {

                $.ajax({
                    url: '/event/' + event_id + '/gates_information/',
                    dataType: 'json',
                    success: function (data) {

                        document.getElementById("gates-information").innerHTML = '&nbsp;';
                        document.getElementById("gates-information").innerHTML = '<canvas id="gates-information-venue"></canvas>';

                        var gates = [];
                        var entries = [];
                        var entries_back_color = [];
                        var imported = [];
                        var imported_back_color = [];

                        data.forEach(function(gate) {

                            if (gate.description !== '') {

                                var entries_percentage = ( gate.entries / gate.imported ) * 100;

                                gates.push(gate.description + ' ( ' + entries_percentage.toFixed(2) + '% )');
                                entries.push(gate.entries);
                                imported.push(gate.imported);

                                entries_back_color.push("rgba(54, 162, 235, 1)");
                                imported_back_color.push("#1A3563");

                            }

                        });

                        document.getElementById("gates-information").innerHTML = '&nbsp;';
                        document.getElementById("gates-information").innerHTML = '<canvas id="gates-information-venue"></canvas>';

                        var ctx_gates = document.getElementById("gates-information-venue").getContext('2d');

                        var gatesInformation = new Chart(ctx_gates, {
                            type: 'bar',
                            data: {
                                labels: gates, //["Stand X", "Stand Y", "Stand Z"],
                                datasets: [{
                                    label: 'Importados',
                                    data: imported, //[3425, 986, 10261],
                                    backgroundColor: imported_back_color, //["rgba(54, 162, 235, 1)", "rgba(54, 162, 235, 1)", "rgba(54, 162, 235, 1)"],
                                    borderColor: imported_back_color, //["rgba(54, 162, 235, 1)", "rgba(54, 162, 235, 1)", "rgba(54, 162, 235, 1)"],
                                    borderWidth: 1
                                }, {
                                    label: 'Entradas',
                                    data: entries, //[1205, 265, 2358],
                                    backgroundColor: entries_back_color, //["#ff0000", "#ff0000", "#ff0000"],
                                    borderColor: entries_back_color, //["#ff0000", "#ff0000", "#ff0000"],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                },
                                "hover": {
                                    "animationDuration": 0
                                },
                                legend: {
                                    "display": false
                                },
                                tooltips: {
                                    "enabled": false
                                },
                                "animation": {
                                    "duration": 1,
                                    "onComplete": function() {
                                        var chartInstance = this.chart,
                                            ctx = chartInstance.ctx;

                                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                        ctx.textAlign = 'center';
                                        ctx.textBaseline = 'bottom';

                                        this.data.datasets.forEach(function(dataset, i) {
                                            var meta = chartInstance.controller.getDatasetMeta(i);
                                            meta.data.forEach(function(bar, index) {
                                                var data = dataset.data[index];
                                                ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                            });
                                        });
                                    }
                                }
                            }
                        });

                        gatesInformation.update();

                        componentHandler.upgradeDom();

                    }
                });

            }

        }

        function ticketTypeFilter(chart_type) {

            updateChartFilterSelected(chart_type, 'ticket_type');

            $('#ticket-type-filter-edit').val(chart_type);

            switch(chart_type) {

                case 'flowrate':

                    var filters_selected = $('#flowrate-filter-selected').attr('data-options');
                    var check_options = filters_selected.split(',');

                    check_options.forEach(function(value) {
                        $('#ticket-types-list tbody tr td label[data-external="'+value+'"]').addClass('is-checked');
                    });

                    break;

                case 'filllevel':

                    var filters_selected = $('#filllevel-filter-selected').attr('data-options');
                    var check_options = filters_selected.split(',');

                    check_options.forEach(function(value) {
                        $('#ticket-types-list tbody tr td label[data-external="'+value+'"]').addClass('is-checked');
                    });

                    break;

                case 'warnings':

                    // Do Something

                    break;

                case 'transactions':

                    // Do Something

                    break;

                default:
                    // Do Something
                    break;

            }

            var dialog = document.querySelector('#ticket-type-filter-dialog');

            dialog.showModal();

        }

        function zoneDacFilter(chart_type) {

            console.log('Filter Zone/Dac Triggered From ' + chart_type);

            updateChartFilterSelected(chart_type, 'zone_dac');

            $('#zone-dac-filter-edit').val(chart_type);

            var dialog = document.querySelector('#zone-dac-filter-dialog');

            dialog.showModal();

            /**
             *
             * Load Zones / Dacs
             *
             */

        }

        function hourFilter(chart_type) {

            $('#day-hour-select > *').remove();

            updateChartFilterSelected(chart_type, 'hour');

            $('#hour-filter-edit').val(chart_type);

            var filters_selected;
            var options = [];

            switch(chart_type) {

                case 'flowrate':

                    if ($('#flowrate-filter-selected').val() == 'hour' ) {

                        var filters_selected = $('#flowrate-filter-selected').attr('data-options');
                        var options = filters_selected.split(',');

                    } else {

                        datetimenow = getNow();

                        options.push(datetimenow);
                        options.push(datetimenow);

                    }

                    break;

                case 'filllevel':

                    if ($('#filllevel-filter-selected').val() == 'hour' ) {

                        var filters_selected = $('#filllevel-filter-selected').attr('data-options');
                        var options = filters_selected.split(',');

                    } else {

                        datetimenow = getNow();

                        options.push(datetimenow);
                        options.push(datetimenow);

                    }

                    break;

                case 'warnings':

                    if ($('#warnings-filter-selected').val() == 'hour') {

                        var filters_selected = $('#warnings-filter-selected').attr('data-options');
                        var options = filters_selected.split(',');

                    } else {

                        datetimenow = getNow();

                        options.push(datetimenow);
                        options.push(datetimenow);

                    }

                    break;

                case 'transactions':

                    // Do Something

                    break;

                default:
                    break;

            }

            var start_date = ((options[0]) ? options[0] : getNow());
            var end_date = ((options[1]) ? options[1] : getNow());

            var is_checked = ((chart_type == 'warnings') ? 'default' : updateSwitchValue(chart_type));
            var checked_name = ((is_checked) ? '{{ trans('charts.filter_hour_ticket_type') }}' : '{{ trans('charts.filter_hour_general') }}');

            var datetimepicker = '';
            datetimepicker += ' <div id="date-time-picker-filters">';
            datetimepicker += '     <table style="' + ((chart_type == 'warnings') ? 'display: none;' : '') + '">';
            datetimepicker += '         <tr>';
            datetimepicker += '             <td>';
            datetimepicker += '                 <label class="dialog-search-text">{{ trans('charts.filter_type_search') }}</label>';
            datetimepicker += '             </td>';
            datetimepicker += '             <td>';
            datetimepicker += '                 <label id="hour-filter-search-type" class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="hour-filter-data">';
            datetimepicker += '                     <input type="checkbox" id="hour-filter-data" class="mdl-switch__input" ' + is_checked + '>';
            datetimepicker += '                     <span id="hour-filter-data-label" class="mdl-switch__label dialog-search-text">' + checked_name + '</span>';
            datetimepicker += '                 </label>';
            datetimepicker += '             </td>';
            datetimepicker += '         </tr>';
            datetimepicker += '     </table>';
            datetimepicker += '     <div id="start-datetime-filter" class="flatpickr-start mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-upgraded is-focused">';
            datetimepicker += '         <input id="datetime-start" type="text" class="datetimepicker-start mdl-textfield__input" placeholder="{{ trans('charts.hour_select') }}" data-input>';
            datetimepicker += '         <label class="mdl-textfield__label" for="datetime-start">{{ trans('charts.begin_filter_date') }}</label>';
            datetimepicker += '     </div>';
            datetimepicker += '     <div id="end-datetime-filter" class="flatpickr-end mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-upgraded is-focused">';
            datetimepicker += '         <input id="datetime-end" type="text" class="datetimepicker-end mdl-textfield__input" placeholder="{{ trans('charts.hour_select') }}" data-input>';
            datetimepicker += '         <label class="mdl-textfield__label" for="datetime-end">{{ trans('charts.begin_filter_date') }}</label>';
            datetimepicker += '     </div>';
            datetimepicker += ' </div>';

            $('#day-hour-select').append(datetimepicker);

            $(".datetimepicker-start").flatpickr({
                appendTo: $('div.flatpickr-start')[0],
                enableTime: true,
                dateFormat: "Y-m-d H:i",
                defaultDate: start_date,
                minDate: $('#event-selected').attr('data-open-gates'),
                time_24hr: true
            });

            $(".datetimepicker-end").flatpickr({
                appendTo: $('div.flatpickr-end')[0],
                enableTime: true,
                dateFormat: "Y-m-d H:i",
                defaultDate: end_date,
                minDate: $('#event-selected').attr('data-open-gates'),
                time_24hr: true
            });

            setSwitchStatus(chart_type);

            /**
             * Show Modal
             */

            var dialog = document.querySelector('#hour-filter-dialog');

            dialog.showModal();

            componentHandler.upgradeDom();

        }

        function gatesErrors() {

            var event_id = $('#event-selected').val();

            var dialog = document.querySelector('#gates-information-errors-dialog');

            dialog.showModal();

            componentHandler.upgradeDom();

            $.ajax({
                type: 'GET',
                url: '/event/' + event_id + '/warnings/gates_information',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                beforeSend: function() {

                    $('#gates-error-header > *').remove();
                    $('#gates-error > *').remove();

                    var loading = '<div class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div>';

                    $('#gates-error').append(loading);

                    componentHandler.upgradeDom();

                },
                success: function (data) {

                    var html_header = '';

                    html_header += '<div class="gates-information-result-header" style="overflow-y: auto"><table id="gates-information-table-header" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp"><thead></thead><tbody><tr><td>{{trans('charts.description')}}</td>';

                    var html = '<div class="gates-information-result" style="overflow-x: auto; overflow-y: auto"><table id="gates-information-table" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">';

                    var table_columns = [];

                    data.forEach(function (venue, index) {

                        table_columns = Object.keys(venue);

                        table_columns.forEach(function(column) {

                            if (column != 'description' && column != 'zones') {

                                html_header += '<td>' + column + '</td>';

                            }

                        });

                        var description_index = table_columns.indexOf('description');
                        (description_index > -1) ? table_columns.splice(description_index, 1) : '';

                        var zones_index = table_columns.indexOf('zones');
                        (zones_index > -1) ? table_columns.splice(zones_index, 1) : '';

                        html += '<thead>';
                        html += '</thead>';

                        html += '<tbody>';
                        html += '<tr class="venue-row">';
                        html += '<td>' + venue.description + '</td>';
                        table_columns.forEach(function(value) {

                            var venue_errors_value = (venue[value] != undefined) ? venue[value] : 0;

                            html += '<td>' + venue_errors_value  + '</td>';

                        });
                        html += '</tr>';

                        venue['zones'].forEach(function(zone) {

                            html += '<tr class="zone-row">';
                            html += '<td>' + zone.description + '</td>';

                            table_columns.forEach(function(value) {

                                var zone_errors_value = (zone[value] != undefined) ? zone[value] : 0;

                                html += '<td>' + zone_errors_value + '</td>';

                            });

                            html += '</tr>';

                            zone['gates'].forEach(function(gate) {

                                html += '<tr class="gate-row">';
                                html += '<td>' + gate.description + '</td>';

                                table_columns.forEach(function(value) {

                                    var gate_errors_value = (gate[value] != undefined) ? gate[value] : 0;

                                    html += '<td>' + gate_errors_value + '</td>';

                                });

                                html += '</tr>';

                            });

                        });

                        html += '</tbody>';

                        html += '<tfoot>';
                        html += '</tfoot>';

                    });

                    html_header += '</tbody>';
                    html_header += '<tfoot>';
                    html_header += '</tfoot>';
                    html_header += '</table></div>';

                    html += '</table>';

                    html += '<table id="gates-information-errors-table" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">';
                    html += '<thead>';
                    html += '</thead>';
                    html += '<tbody>';

                    var colspan_size = table_columns.length + 1;

                    table_columns.forEach(function(column) {

                        var column_translation = '';

                        switch (column) {

                            default:
                                column_translation = column;
                                break;
                            case 'PE':
                                column_translation = '{{trans('charts.column_pe')}}';
                                break;
                            case 'TD':
                                column_translation = '{{trans('charts.column_td')}}';
                                break;
                            case 'QA':
                                column_translation = '{{trans('charts.column_qa')}}';
                            case 'BE':
                                column_translation = '{{trans('charts.column_be')}}';
                                break;
                            case 'VA':
                                column_translation = '{{trans('charts.column_va')}}';
                                break;
                            case 'JE':
                                column_translation = '{{trans('charts.column_je')}}';
                                break;
                            case 'SCB':
                                column_translation = '{{trans('charts.column_scb')}}';
                                break;
                            case 'NSCB':
                                column_translation = '{{trans('charts.column_nscb')}}';
                                break;
                            case 'CA':
                                column_translation = '{{trans('charts.column_ca')}}';
                                break;
                            case 'LAA':
                                column_translation = '{{trans('charts.column_laa')}}';
                                break;
                            case 'CTCT':
                                column_translation = '{{trans('charts.column_ctct')}}';
                                break;
                            case 'SAC':
                                column_translation = '{{trans('charts.column_sac')}}';
                                break;
                            case 'SSR':
                                column_translation = '{{trans('charts.column_ssr')}}';
                                break;

                        }

                        html += '<tr>';
                        html += '<td style="text-align: left !important;" colspan="' + colspan_size + '">' + column_translation + '</td>';
                        html += '</tr>';

                    });

                    html += '</tbody>';
                    html += '<tfoot>';
                    html += '</tfoot>';
                    html += '</table></div>';

                    $('#gates-error > *').remove();
                    $("#gates-error-header").append(html_header);
                    $("#gates-error").append(html);

                    componentHandler.upgradeDom();

                }
            });

        }

        function searchTicketBarcode() {

            var dialog = document.querySelector('#search-ticket-barcode-dialog');

            dialog.showModal();

        }

        function getNow() {

            var today = new Date();
            var dd = today.getDate();

            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear();

            var hour = today.getHours();
            var minutes = today.getMinutes();

            if(dd<10)
            {
                dd='0'+dd;
            }

            if(mm<10)
            {
                mm='0'+mm;
            }

            date = yyyy + '-' + mm + '-' + dd + ' ' + hour + ':' + minutes;

            return date;

        }

        /**
         *
         * Update Chart Filter Selected
         *
         */

        function updateChartFilterSelected(chart_type, filter_type) {

            if (chart_type == 'flowrate') {

                switch(filter_type) {
                    case 'default':
                        $('#flowrate-filter-selected').val(filter_type);
                        break;
                    default:
                        $('#flowrate-filter-selected').attr('data-update-filter', filter_type);
                        break;
                }

            }
            else if (chart_type == 'filllevel') {

                switch(filter_type) {
                    case 'default':
                        $('#filllevel-filter-selected').val(filter_type);
                        break;
                    default:
                        $('#filllevel-filter-selected').attr('data-update-filter', filter_type);
                        break;
                }

            }
            else if (chart_type == 'warnings') {

                switch(filter_type) {
                    case 'default':
                        $('#warnings-filter-selected').val(filter_type);
                        break;
                    default:
                        $('#warnings-filter-selected').attr('data-update-filter', filter_type);
                        break;
                }

            }
            else if (chart_type == 'transactions') {

                switch(filter_type) {
                    case 'default':
                        $('#transactions-filter-selected').val(filter_type);
                        break;
                    default:
                        $('#transactions-filter-selected').attr('data-update-filter', filter_type);
                        break;
                }

            }

        }

        function updateSwitchValue(chart_type) {

            if (chart_type == 'flowrate') {

                var checked = $('#flowrate-filter-selected').attr('data-search-type');

                return ((checked == 'default') ? '' : 'checked');

            } else if (chart_type == 'filllevel') {

                var checked = $('#filllevel-filter-selected').attr('data-search-type');

                return ((checked == 'default') ? '' : 'checked');

            } else if (chart_type == 'warnings') {

                var checked = $('#warnings-filter-selected').attr('data-search-type');

                return ((checked == 'default') ? '' : 'checked');

            } else if (chart_type == 'transactions') {

                var checked = $('#transactions-filter-selected').attr('data-search-type');

                return ((checked == 'default') ? '' : 'checked');

            } else {

                return '';

            }

        }

        function setSwitchStatus(chart_type) {

            $('#hour-filter-search-type input').change(function(){
                if($(this).is(':checked')) {
                    $(this).next().text('{{ trans('charts.filter_hour_ticket_type') }}');
                } else {
                    $(this).next().text('{{ trans('charts.filter_hour_general') }}');
                }
            });

        }

        function loadGateErrorCode(row) {

            var event_id = $('#event-selected').val();

            var code = $(row).attr('data-message-code');

            var code_translation = '';

            switch (code) {

                default:
                    code_translation = column;
                    break;
                case 'PE':
                    code_translation = '{{trans('charts.column_pe')}}';
                    break;
                case 'TD':
                    code_translation = '{{trans('charts.column_td')}}';
                    break;
                case 'QA':
                    code_translation = '{{trans('charts.column_qa')}}';
                case 'BE':
                    code_translation = '{{trans('charts.column_be')}}';
                    break;
                case 'VA':
                    code_translation = '{{trans('charts.column_va')}}';
                    break;
                case 'JE':
                    code_translation = '{{trans('charts.column_je')}}';
                    break;
                case 'SCB':
                    code_translation = '{{trans('charts.column_scb')}}';
                    break;
                case 'NSCB':
                    code_translation = '{{trans('charts.column_nscb')}}';
                    break;
                case 'CA':
                    code_translation = '{{trans('charts.column_ca')}}';
                    break;
                case 'LAA':
                    code_translation = '{{trans('charts.column_laa')}}';
                    break;
                case 'CTCT':
                    code_translation = '{{trans('charts.column_ctct')}}';
                    break;
                case 'SAC':
                    code_translation = '{{trans('charts.column_sac')}}';
                    break;
                case 'SSR':
                    code_translation = '{{trans('charts.column_ssr')}}';
                    break;

            }

            $('#error-by-gates-filtered > h4').text('{{trans('charts.occurrences_at')}}' + ' ' + code_translation);

            $.ajax({

                url: '/event/' + event_id + '/warnings/gates_information_filtered',
                dataType: 'json',
                beforeSend: function() {

                    $("#warnings-list-filtered tbody > *").remove();

                    var html = '';
                    html += '<tr>';
                    html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="2"><div class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div></td>';
                    html +=	'</tr>'

                    $("#warnings-list-filtered tbody").append(html);

                    componentHandler.upgradeDom();

                },
                success: function (data) {

                    $('#warnings-list-filtered > tbody > *').remove();

                    var html = '';

                    var table_columns = [];

                    data.forEach(function (gate, index) {


                        var error_value = (gate[code] != undefined ) ? gate[code] : '0';

                        html += '<tr class="gate-row">';
                        html += '<td>' + gate.description + '</td>';
                        html += '<td>' + error_value + '</td>';
                        html += '</tr>';

                    });

                    $('#warnings-list-filtered > tbody').append(html);

                    componentHandler.upgradeDom();

                }

            });

            var dialog = document.querySelector('#error-by-gates-filtered');

            dialog.showModal();

        }

    </script>

@endsection

{!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js', array('type' => 'text/javascript')) !!}