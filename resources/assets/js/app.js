require('./bootstrap');
window.Vue = require('vue');
require('hideshowpassword');

const Vue = require('vue');

const app = new Vue({
    el: '#app',
    data: {
        stockData: null
    },
    created() {
        this.setupStream();
    },
    methods: {
        setupStream() {
            // Not a real URL, just using for demo purposes
            let es = new EventSource('http://awesomestockdata.com/feed');

            es.addEventListener('message', event => {
                let data = JSON.parse(event.data);
                this.stockData = data.stockData;
            }, false);

            es.addEventListener('error', event => {
                if (event.readyState == EventSource.CLOSED) {
                    console.log('Event was closed');
                    console.log(EventSource);
                }
            }, false);
        }
    }
});

/**
 *
 * End of Streaming Data
 *
 **/

var Dropzone = require('dropzone');

var password = require('password-strength-meter');

var labels_chart = [];

var values_chart = [];

var color_chart = [];

var total_search = 0;

var default_value = 0;

var default_value_remove = 0;

$.fn.extend({

    toggleText: function(a, b){
        return this.text(this.text() == b ? a : b);
    },

    alterClass: function(removals, additions) {

        var self = this;

        if(removals.indexOf('*') === -1) {
            self.removeClass(removals);
            return !additions ? self : self.addClass(additions);
        }

        var patt = new RegExp( '\\s' +
            removals.
            replace( /\*/g, '[A-Za-z0-9-_]+' ).
            split( ' ' ).
            join( '\\s|\\s' ) +
            '\\s', 'g' );

        self.each(function(i, it) {
            var cn = ' ' + it.className + ' ';
            while(patt.test(cn)) {
                cn = cn.replace( patt, ' ' );
            }
            it.className = $.trim(cn);
        });

        return !additions ? self : self.addClass(additions);
    }

});

/**
 *
 * Refresh Actions
 *
 */

$("#refresh-flowrate").on('click', function() {

    var event_id = $('#event-selected').val();
    var updating_value = $('#flowrate-filter-selected').val();
    var data_options = $('#flowrate-filter-selected').attr('data-options');
    var update_type_search = $('#flowrate-filter-selected').attr('data-search-type');

    /**
     *
     * Load Flow Rate
     *
     * @return Object
     *
     */

    updateFlowrateChart('refresh', event_id, update_type_search, updating_value, data_options);

});

$("#refresh-fill-level-venue").on('click', function() {

    var event_id = $('#event-selected').val();
    var updating_value = $('#filllevel-filter-selected').val();
    var data_options = $('#filllevel-filter-selected').attr('data-options');
    var update_type_search = $('#filllevel-filter-selected').attr('data-search-type');

    /**
     * Load Fill Level
     *
     * @return Object
     */

    updateFillLevelChart('refresh', event_id, update_type_search, updating_value, data_options);

});

$("#refresh-warnings-venue").on('click', function() {

    var event_id = $("#event-selected").val();
    var updating_value = $('#warnings-filter-selected').val();
    var data_options = $('#warnings-filter-selected').attr('data-options');
    var update_type_search = $('#warnings-filter-selected').attr('data-search-type');

    /**
     * Load Warnings
     *
     * @return Object
     */

    updateWarningsChart('refresh', event_id, update_type_search, updating_value, data_options);

});

$('#refresh-transactions-venue').on('click', function(){

    var event_id = $("#event-selected").val();
    var updating_value = $('#transactions-filter-selected').val();
    var data_options = $('#transactions-filter-selected').attr('data-options');
    var update_type_search = $('#transactions-filter-selected').attr('data-search-type');
    var transactions_first_load = true;

    updateTransactionsTable('refresh', event_id, update_type_search, updating_value, data_options, transactions_first_load);

});

$('#refresh-all').on('click', function() {

    loadCharts();

});

$('#refresh-gates-information').on('click', function(){

    console.log('here');

    var event_id = $("#event-selected").val();
    var updating_value = $('#gates-filter-selected').val();
    var data_options = $('#gates-filter-selected').attr('data-options');
    var update_type_search = $('#gates-filter-selected').attr('data-search-type');
    var transactions_first_load = true;

    updateGatesInformationChart('refresh', event_id, update_type_search, updating_value, data_options, transactions_first_load);

});

/**
 *
 * Update Actions
 *
 */

function updateFlowrateChart(action, event_id, update_type_search, updating_value, data_options) {

    var options;

    if (action == 'update') {
        options = data_options.join();
    } else if (action == 'refresh') {
        options = data_options;
    }

    var flowrateChart;

    if (updating_value == 'default') {

        $.ajax({
            url: '/event/' + event_id + '/flowrate/',
            dataType: 'json',
            success: function (data) {

                /**
                 * @return Flowrate
                 */

                var labels_chart = [];

                var values_chart = [];

                data['flowrate'].forEach(function(time) {

                    labels_chart.push(time['hour_transaction']);
                    values_chart.push(time['entries']);

                });

                document.getElementById("flowrate").innerHTML = '&nbsp;';
                document.getElementById("flowrate").innerHTML = '<canvas id="flowrate-venue"></canvas>';

                var ctx_flowrate = document.getElementById("flowrate-venue").getContext('2d');

                var flowrateChart = new Chart(ctx_flowrate, {

                    type: 'line',
                    data: {
                        labels: labels_chart,
                        datasets: [
                            {
                                data: values_chart,
                                label: "Entries",
                                borderColor: "#3e95cd",
                                fill: false
                            }
                        ]
                    },
                    options: {
                        title: {
                            display: true,
                            text: 'Flowrate'
                        }
                    }

                });

                flowrateChart.update();

                componentHandler.upgradeDom();

            }

        });

    } else if (updating_value == 'ticket_type') {

        $.ajax({
            type: 'POST',
            url: '/event/' + event_id + '/flowrate/tickettype/filter',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            data: {ticket_types: options},
            success: function (data) {

                document.getElementById("flowrate").innerHTML = '&nbsp;';
                document.getElementById("flowrate").innerHTML = '<canvas id="flowrate-venue"></canvas>';

                var ctx_flowrate = document.getElementById("flowrate-venue").getContext('2d');

                var flowrateChart = new Chart(ctx_flowrate, {

                    type: 'line',
                    data: {
                        labels: data['chart_labels'],
                        datasets: data['dataset']
                    },
                    options: {
                        title: {
                            display: true,
                            text: 'Flowrate'
                        }
                    }

                });

            }
        });

    } else if (updating_value == 'hour') {

        $.ajax({
            type: 'POST',
            url: '/event/' + event_id + '/flowrate/hour/filter',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            data: {hours: options, type_search: update_type_search},
            success: function (data) {

                if (update_type_search == 'default') {

                    var labels_chart = [];

                    var values_chart = [];

                    data['flowrate'].forEach(function(time) {

                        labels_chart.push(time['hour_transaction']);
                        values_chart.push(time['entries']);

                    });

                    document.getElementById("flowrate").innerHTML = '&nbsp;';
                    document.getElementById("flowrate").innerHTML = '<canvas id="flowrate-venue"></canvas>';

                    var ctx_flowrate = document.getElementById("flowrate-venue").getContext('2d');

                    var flowrateChart = new Chart(ctx_flowrate, {

                        type: 'line',
                        data: {
                            labels: labels_chart,
                            datasets: [
                                {
                                    data: values_chart,
                                    label: "Entries",
                                    borderColor: "#3e95cd",
                                    fill: false
                                }
                            ]
                        },
                        options: {
                            title: {
                                display: true,
                                text: 'Flowrate'
                            }
                        }

                    });

                } else {

                    document.getElementById("flowrate").innerHTML = '&nbsp;';
                    document.getElementById("flowrate").innerHTML = '<canvas id="flowrate-venue"></canvas>';

                    var ctx_flowrate = document.getElementById("flowrate-venue").getContext('2d');

                    var flowrateChart = new Chart(ctx_flowrate, {

                        type: 'line',
                        data: {
                            labels: data['chart_labels'],
                            datasets: data['dataset']
                        },
                        options: {
                            title: {
                                display: true,
                                text: 'Flowrate'
                            }
                        }

                    });

                }

                flowrateChart.update();

                componentHandler.upgradeDom();

            }
        });

    }

}

function updateFillLevelChart(action, event_id, update_type_search, updating_value, data_options) {

    var options;

    if (action == 'update') {
        options = data_options.join();
    } else if (action == 'refresh') {
        options = data_options;
    }

    var myChartCircle;

    if (updating_value == 'default') {

        $.ajax({
            url: '/event/' + event_id + '/filllevel/',
            dataType: 'json',
            success: function (data) {

                /**
                 * @return filllevel
                 */

                document.getElementById("fill-level").innerHTML = '&nbsp;';
                document.getElementById("fill-level").innerHTML = '<div id="scrollable-filllevel"><canvas id="fill-level-venue"></canvas><table id="fill-level-data-table"></table></div>';

                $('#fill-level-data-table').addClass('mdl-data-table mdl-js-data-table');

                var ctx = document.getElementById("fill-level-venue").getContext("2d");

                var total_lotation = $("#event-selected").attr("data-lotation");

                if (total_lotation < 1) {
                    total_lotation = data.total;
                }

                var missing_lotation = total_lotation - data.total;

                var filled_in = data.total;

                var fill_by_total = data.total / total_lotation;

                var fill_percentage = fill_by_total * 100;

                var symbol;

                if (fill_percentage != 0 && !isNaN(fill_percentage)) {
                    symbol = '%';
                } else if (fill_percentage < 1 && !isNaN(fill_percentage)) {
                    symbol = '%';
                }

                var backgroundColorChartLine;

                if (fill_percentage.toFixed(0) < 40) {
                    backgroundColorChartLine = "#2DA0CB";
                } else if (fill_percentage.toFixed(0) >= 40 && fill_percentage.toFixed(0) < 80) {
                    backgroundColorChartLine = "#3852A8";
                } else if (fill_percentage.toFixed(0) >= 80) {
                    backgroundColorChartLine = "#1b3066";
                }

                var fill_table_data = '';

                fill_table_data += '<thead><tr><td>Presenças</td><td>Disponivel</td></tr></thead>';
                fill_table_data += '<tbody><tr><td>' + data.total + '</td><td>' + missing_lotation + '</td></tr></tbody>';
                fill_table_data += '<tfoot><tr><td>Presenças</td><td>Disponivel</td></tr></tfoot>';

                $('#fill-level-data-table').append(fill_table_data);

                var myChartCircle = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ["Presenças", "Disponivel"],
                        datasets: [{
                            label: "Fill Level",
                            backgroundColor: [backgroundColorChartLine],
                            data: [data.total, missing_lotation]
                        }]
                    },
                    plugins: [{
                        beforeDraw: function(chart) {
                            var width = chart.chart.width,
                                height = chart.chart.height,
                                ctx = chart.chart.ctx;

                            ctx.restore();
                            var fontSize = (height / 150).toFixed(2);
                            ctx.font = fontSize + "em sans-serif";
                            ctx.fillStyle = backgroundColorChartLine;
                            ctx.textBaseline = "middle";

                            var text = (isNaN(fill_percentage) ? filled_in : fill_percentage.toFixed(2) + symbol),
                                textX = Math.round((width - ctx.measureText(text).width) / 2),
                                textY = height / 2;

                            ctx.fillText(text, textX, textY);
                            ctx.save();
                        }
                    }],
                    options: {
                        legend: {
                            display: true,
                            onClick: function(e, legendItem) {

                                var index = legendItem.index;
                                var cix = this.chart;
                                var meta = cix.getDatasetMeta(0);

                                if (meta.data[index].hidden == true) {

                                    if (index == '0') {
                                        default_value_remove = filled_in;
                                    } else {
                                        default_value_remove = missing_lotation;
                                    }

                                    var width = this.chart.chart.width,
                                        height = this.chart.chart.height,
                                        ctxi = this.chart.chart.ctx;

                                    total_search = total_lotation - default_value_remove;

                                    var textX = Math.round((width - ctxi.measureText(total_search).width) / 2),
                                        textY = height / 2;

                                    ctxi.fillText(total_search, textX, textY);
                                    ctxi.save();

                                    meta.data[index].hidden = false;

                                } else {

                                    if (index == '0') {
                                        default_value_remove = filled_in;
                                    } else {
                                        default_value_remove = missing_lotation;
                                    }

                                    var width = this.chart.chart.width,
                                        height = this.chart.chart.height,
                                        ctxi = this.chart.chart.ctx;

                                    total_search = total_lotation + default_value_remove;

                                    var textX = Math.round((width - ctxi.measureText(total_search).width) / 2),
                                        textY = height / 2;

                                    ctxi.fillText(total_search, textX, textY);
                                    ctxi.save();

                                    meta.data[index].hidden = true;

                                }

                                cix.update();

                            },
                        },
                        responsive: true,
                        maintainAspectRatio: true,
                        cutoutPercentage: 85
                    }

                });

                myChartCircle.update();

                componentHandler.upgradeDom();

            }

        });

    } else if (updating_value == 'ticket_type') {

        $.ajax({
            type: 'POST',
            url: '/event/' + event_id + '/filllevel/tickettype/filter',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            data: {ticket_types: options},
            success: function (data) {

                document.getElementById("fill-level").innerHTML = '&nbsp;';
                document.getElementById("fill-level").innerHTML = '<div id="scrollable-filllevel"><canvas id="fill-level-venue"></canvas><table id="fill-level-data-table"></table></div>';

                $('#fill-level-data-table').addClass('mdl-data-table mdl-js-data-table');

                /**
                 * @return filllevel
                 */

                var ctx = document.getElementById("fill-level-venue").getContext("2d");

                var fill_table_data = '';

                fill_table_data += '<thead><tr><td>Tipo Titulo</td><td>Entradas</td></tr></thead>';

                total_search = 0;

                labels_chart = [];
                values_chart = [];
                color_chart = [];

                data.forEach(function(ticket_type) {

                    labels_chart.push(ticket_type['tickettype']);
                    values_chart.push(ticket_type['entries']);
                    color_chart.push("#" + Math.random().toString(16).slice(2, 8));

                    total_search = total_search + ticket_type['entries'];

                    fill_table_data += '<tbody><tr><td>' + ticket_type['tickettype'] + '</td><td>' + ticket_type['entries'] + '</td></tr></tbody>';

                });

                fill_table_data += '<tbody><tr><td>Total</td><td>' + total_search + '</td></tr></tbody>';
                fill_table_data += '<tfoot><tr><td>Tipo Titulo</td><td>Entradas</td></tr></tfoot>';

                $('#fill-level-data-table').append(fill_table_data);

                var myChartCircle = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: labels_chart,
                        datasets: [{
                            label: "Fill Level",
                            data: values_chart,
                            backgroundColor: color_chart
                        }]
                    },
                    plugins: [{
                        beforeDraw: function(chart) {

                            var width = chart.chart.width,
                                height = chart.chart.height,
                                ctx = chart.chart.ctx;

                            ctx.restore();
                            var fontSize = (height / 150).toFixed(2);
                            ctx.font = fontSize + "em sans-serif";
                            ctx.fillStyle = "#3852A8";
                            ctx.textBaseline = "middle";

                            var text = 'Total ' + total_search,
                                textX = Math.round((width - ctx.measureText(text).width) / 2),
                                textY = height / 2;

                            ctx.fillText(text, textX, textY);
                            ctx.save();

                        }
                    }],
                    options: {
                        legend: {
                            display: true,
                            onClick: function(e, legendItem) {

                                var index = legendItem.index;
                                var ci = this.chart;
                                var meta = ci.getDatasetMeta(0);

                                if (meta.data[index].hidden == true) {

                                    var width = this.chart.chart.width,
                                        height = this.chart.chart.height,
                                        ctx = this.chart.chart.ctx;

                                    total_search = total_search + values_chart[index];

                                    var textX = Math.round((width - ctx.measureText(total_search).width) / 2),
                                        textY = height / 2;

                                    ctx.fillText(total_search, textX, textY);
                                    ctx.save();

                                    meta.data[index].hidden = false;

                                } else {

                                    var width = this.chart.chart.width,
                                        height = this.chart.chart.height,
                                        ctx = this.chart.chart.ctx;

                                    total_search = total_search - values_chart[index];

                                    var textX = Math.round((width - ctx.measureText(total_search).width) / 2),
                                        textY = height / 2;

                                    ctx.fillText(total_search, textX, textY);
                                    ctx.save();

                                    meta.data[index].hidden = true;

                                }

                                ci.update();

                            },
                        },
                        responsive: true,
                        maintainAspectRatio: true,
                        cutoutPercentage: 85
                    }

                });

                myChartCircle.update();

                componentHandler.upgradeDom();

            },
            error: function (error) {
                console.log(error);
            }

        });

    } else if (updating_value == 'hour') {

        $.ajax({
            type: 'POST',
            url: '/event/' + event_id + '/filllevel/hour/filter',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            data: {hours: options, type_search: update_type_search},
            success: function(data) {

                if (update_type_search == 'default') {

                    document.getElementById("fill-level").innerHTML = '&nbsp;';
                    document.getElementById("fill-level").innerHTML = '<div id="scrollable-filllevel"><canvas id="fill-level-venue"></canvas><table id="fill-level-data-table"></table></div>';

                    $('#fill-level-data-table').addClass('mdl-data-table mdl-js-data-table');

                    var ctx = document.getElementById("fill-level-venue").getContext("2d");

                    var total_lotation = $("#event-selected").attr("data-lotation");

                    if (total_lotation < 1) {
                        total_lotation = data.total;
                    }

                    var missing_lotation = total_lotation - data.total;

                    var filled_in = data.total;

                    var fill_by_total = data.total / total_lotation;

                    var fill_percentage = fill_by_total * 100;

                    var backgroundColorChartLine;

                    if (fill_percentage.toFixed(0) < 40) {
                        backgroundColorChartLine = "#2DA0CB";
                    } else if (fill_percentage.toFixed(0) >= 40 && fill_percentage.toFixed(0) < 80) {
                        backgroundColorChartLine = "#3852A8";
                    } else if (fill_percentage.toFixed(0) >= 80) {
                        backgroundColorChartLine = "#1b3066";
                    }

                    var fill_table_data = '';

                    fill_table_data += '<thead><tr><td>Presenças</td><td>Disponivel</td></tr></thead>';
                    fill_table_data += '<tbody><tr><td>' + data.total + '</td><td>' + missing_lotation + '</td></tr></tbody>';
                    fill_table_data += '<tfoot><tr><td>Presenças</td><td>Disponivel</td></tr></tfoot>';

                    $('#fill-level-data-table').append(fill_table_data);

                    var myChartCircle = new Chart(ctx, {
                        type: 'doughnut',
                        data: {
                            labels: ["Presenças", "Disponivel"],
                            datasets: [{
                                label: "Fill Level",
                                backgroundColor: [backgroundColorChartLine],
                                data: [data.total, missing_lotation]
                            }]
                        },
                        plugins: [{
                            beforeDraw: function(chart) {
                                var width = chart.chart.width,
                                    height = chart.chart.height,
                                    ctx = chart.chart.ctx;

                                ctx.restore();
                                var fontSize = (height / 150).toFixed(2);
                                ctx.font = fontSize + "em sans-serif";
                                ctx.fillStyle = backgroundColorChartLine;
                                ctx.textBaseline = "middle";

                                var text = fill_percentage.toFixed(2) + "%",
                                    textX = Math.round((width - ctx.measureText(text).width) / 2),
                                    textY = height / 2;

                                ctx.fillText(text, textX, textY);
                                ctx.save();
                            }
                        }],
                        options: {
                            legend: {
                                display: true,
                                onClick: function(e, legendItem) {

                                    var index = legendItem.index;
                                    var cix = this.chart;
                                    var meta = cix.getDatasetMeta(0);

                                    if (meta.data[index].hidden == true) {

                                        if (index == '0') {
                                            default_value_remove = filled_in;
                                        } else {
                                            default_value_remove = missing_lotation;
                                        }

                                        var width = this.chart.chart.width,
                                            height = this.chart.chart.height,
                                            ctxi = this.chart.chart.ctx;

                                        total_search = total_lotation - default_value_remove;

                                        var textX = Math.round((width - ctxi.measureText(total_search).width) / 2),
                                            textY = height / 2;

                                        ctxi.fillText(total_search, textX, textY);
                                        ctxi.save();

                                        meta.data[index].hidden = false;

                                    } else {

                                        if (index == '0') {
                                            default_value_remove = filled_in;
                                        } else {
                                            default_value_remove = missing_lotation;
                                        }

                                        var width = this.chart.chart.width,
                                            height = this.chart.chart.height,
                                            ctxi = this.chart.chart.ctx;

                                        total_search = total_lotation + default_value_remove;

                                        var textX = Math.round((width - ctxi.measureText(total_search).width) / 2),
                                            textY = height / 2;

                                        ctxi.fillText(total_search, textX, textY);
                                        ctxi.save();

                                        meta.data[index].hidden = true;

                                    }

                                    cix.update();

                                },
                            },
                            responsive: true,
                            maintainAspectRatio: true,
                            cutoutPercentage: 85
                        }

                    });

                } else {

                    document.getElementById("fill-level").innerHTML = '&nbsp;';
                    document.getElementById("fill-level").innerHTML = '<div id="scrollable-filllevel"><canvas id="fill-level-venue"></canvas><table id="fill-level-data-table"></table></div>';

                    $('#fill-level-data-table').addClass('mdl-data-table mdl-js-data-table');

                    /**
                     * @return filllevel
                     */

                    var ctx = document.getElementById("fill-level-venue").getContext("2d");

                    var fill_table_data = '';

                    fill_table_data += '<thead><tr><td>Tipo Titulo</td><td>Entradas</td></tr></thead>';

                    total_search = 0;

                    labels_chart = [];
                    values_chart = [];
                    color_chart = [];

                    data.forEach(function(ticket_type) {

                        labels_chart.push(ticket_type['tickettype']);
                        values_chart.push(ticket_type['entries']);
                        color_chart.push("#" + Math.random().toString(16).slice(2, 8));

                        total_search = total_search + ticket_type['entries'];

                        fill_table_data += '<tbody><tr><td>' + ticket_type['tickettype'] + '</td><td>' + ticket_type['entries'] + '</td></tr></tbody>';

                    });

                    fill_table_data += '<tbody><tr><td>Total</td><td>' + total_search + '</td></tr></tbody>';
                    fill_table_data += '<tfoot><tr><td>Tipo Titulo</td><td>Entradas</td></tr></tfoot>';

                    $('#fill-level-data-table').append(fill_table_data);

                    var myChartCircle = new Chart(ctx, {
                        type: 'doughnut',
                        data: {
                            labels: labels_chart,
                            datasets: [{
                                label: "Fill Level",
                                data: values_chart,
                                backgroundColor: color_chart
                            }]
                        },
                        plugins: [{
                            beforeDraw: function(chart) {

                                var width = chart.chart.width,
                                    height = chart.chart.height,
                                    ctx = chart.chart.ctx;

                                ctx.restore();
                                var fontSize = (height / 150).toFixed(2);
                                ctx.font = fontSize + "em sans-serif";
                                ctx.fillStyle = "#9b9b9b";
                                ctx.textBaseline = "middle";

                                var text = 'Total ' + total_search,
                                    textX = Math.round((width - ctx.measureText(text).width) / 2),
                                    textY = height / 2;

                                ctx.fillText(text, textX, textY);
                                ctx.save();

                            }
                        }],
                        options: {
                            legend: {
                                display: true,
                                onClick: function(e, legendItem) {

                                    var index = legendItem.index;
                                    var ci = this.chart;
                                    var meta = ci.getDatasetMeta(0);

                                    if (meta.data[index].hidden == true) {

                                        var width = this.chart.chart.width,
                                            height = this.chart.chart.height,
                                            ctx = this.chart.chart.ctx;

                                        total_search = total_search + values_chart[index];

                                        var textX = Math.round((width - ctx.measureText(total_search).width) / 2),
                                            textY = height / 2;

                                        ctx.fillText(total_search, textX, textY);
                                        ctx.save();

                                        meta.data[index].hidden = false;

                                    } else {

                                        var width = this.chart.chart.width,
                                            height = this.chart.chart.height,
                                            ctx = this.chart.chart.ctx;

                                        total_search = total_search - values_chart[index];

                                        var textX = Math.round((width - ctx.measureText(total_search).width) / 2),
                                            textY = height / 2;

                                        ctx.fillText(total_search, textX, textY);
                                        ctx.save();

                                        meta.data[index].hidden = true;

                                    }

                                    ci.update();

                                },
                            },
                            responsive: true,
                            maintainAspectRatio: true,
                            cutoutPercentage: 85
                        }

                    });

                }

                myChartCircle.update();

                componentHandler.upgradeDom();

            }
        });

    }

}

function updateWarningsChart(action, event_id, update_type_search, updating_value, data_options, first_load) {

    var trans = {
        message: "Mensagem Erro",
        total: "Total",
        unavailable: "Indisponivel"
    }

    var options;

    if (action == 'update') {
        options = data_options.join();
    } else if (action == 'refresh') {
        options = data_options;
    }

    if (updating_value == 'default') {

        $.ajax({

            url: '/event/' + event_id + '/warnings/',
            dataType: 'json',
            beforeSend: function() {

                if (!first_load) {

                    var card_height = $('#warnings-card').height();
                    $('#warnings-card').css('height', card_height);

                }

                $("#warnings-list tbody > *").remove();

                var html = '';
                html += '<tr>';
                html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="2"><div class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div></td>';
                html +=	'</tr>'

                $("#warnings-list tbody").append(html);

                componentHandler.upgradeDom();

            },
            success: function (data) {

                if (data['error']) {

                    $("#warnings-list tbody > *").remove();

                    var html = '';
                    html += '<tr>';
                    html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="2">' + trans.unavailable + '</td>';
                    html +=	'</tr>'

                    $("#warnings-list tbody").append(html);

                    $('#warnings-card').removeAttr("style");

                    componentHandler.upgradeDom();

                } else {

                    $("#warnings-list tbody > *").remove();

                    /**
                     * @return filllevel
                     */

                    var html = '';
                    var total_errors = 0;

                    data.forEach(function(error) {

                        total_errors = total_errors + error['quantity'];

                        var message_code = '';

                        switch (error['error_message']) {

                            default:
                                message_code = '';
                                break;
                            case 'PORTA ERRADA':
                                message_code = 'PE';
                                break;
                            case 'TITULO DESCONHECIDO':
                                message_code = 'TD';
                                break;
                            case 'QUOTAS EM ATRASO':
                                message_code = 'QA';
                                break;
                            case 'BIL. ESTORNADO':
                                message_code = 'BE';
                                break;
                            case 'VIA ANULADA':
                                message_code = 'VA';
                                break;
                            case 'Solicite Apoio Controlo':
                                message_code = 'SAC';
                                break;
                            case 'JA ENTROU':
                                message_code = 'JE';
                                break;
                            case 'Socio sem renumeracao':
                                message_code = 'SSR';
                                break;
                            case 'SOCIO C/ BILHETE':
                                message_code = 'SCB';
                                break;
                            case 'NAOSOCIO C/ BIL.':
                                message_code = 'NSCB';
                                break;
                            case 'CARTAO ANTIGO':
                                message_code = 'CA';
                                break;
                            case 'LA ANULADO':
                                message_code = 'LAA';
                                break;
                            case 'Controlo TCT':
                                message_code = 'CTCT';
                                break;
                        }

                        html += '<tr data-message-code="' + message_code + '" onclick="loadGateErrorCode(this);">';
                        html +=	'	<td class="mdl-data-table__cell--non-numeric">' + error['error_message'] + '</td>';
                        html +=	'	<td class="mdl-data-table__cell"><span class="mdl-badge" data-badge="' + error['quantity'] + '"></span></td>';
                        html +=	'</tr>';

                    });

                    html += '<tr>';
                    html +=	'	<td class="mdl-data-table__cell--non-numeric">' + trans.total + '</td>';
                    html +=	'	<td class="mdl-data-table__cell"><span class="mdl-badge" data-badge="' + total_errors + '"></span></td>';
                    html +=	'</tr>'

                    $("#warnings-list tbody").append(html);

                    $('#warnings-card').removeAttr("style");

                    componentHandler.upgradeDom();

                }

            }
        });

    } else if (updating_value == 'ticket_type') {
        console.log('Filter Warnings By Ticket Type');
    } else if (updating_value == 'hour') {
        console.log('Filter Warnings By Hour');

        $.ajax({
            type: 'POST',
            url: '/event/' + event_id + '/warnings/hour/filter',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            data: {hours: options, type_search: update_type_search},
            beforeSend: function() {

                if (!first_load) {

                    var card_height = $('#warnings-card').height();
                    $('#warnings-card').css('height', card_height);

                }

                $("#warnings-list tbody > *").remove();

                var html = '';
                html += '<tr>';
                html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="2"><div class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div></td>';
                html +=	'</tr>'

                $("#warnings-list tbody").append(html);

                componentHandler.upgradeDom();

            },
            success: function(data) {

                if (data['error']) {

                    var html = '';
                    html += '<tr>';
                    html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="2">' + trans.unavailable + '</td>';
                    html +=	'</tr>'

                    $("#warnings-list tbody").append(html);

                    $('#warnings-card').removeAttr("style");

                    componentHandler.upgradeDom();

                } else {

                    $("#warnings-list tbody > *").remove();

                    /**
                     * @return filllevel
                     */

                    var html = '';
                    var total_errors = 0;

                    data.forEach(function(error) {

                        total_errors = total_errors + error['quantity'];

                        html += '<tr>';
                        html +=	'	<td class="mdl-data-table__cell--non-numeric">' + error['error_message'] + '</td>';
                        html +=	'	<td class="mdl-data-table__cell--non-numeric"><span class="mdl-badge" data-badge="' + error['quantity'] + '"></span></td>';
                        html +=	'</tr>';

                    });

                    html += '<tr>';
                    html +=	'	<td class="mdl-data-table__cell--non-numeric">' + trans.total + '</td>';
                    html +=	'	<td class="mdl-data-table__cell"><span class="mdl-badge" data-badge="' + total_errors + '"></span></td>';
                    html +=	'</tr>'

                    $("#warnings-list tbody").append(html);

                    $('#warnings-card').removeAttr("style");

                    componentHandler.upgradeDom();

                }


            }
        });

    }

}

function updateTransactionsTable(action, event_id, update_type_search, updating_value, data_options, first_load) {

    var trans = {
        'undefined': 'Indisponivel',
        'insert_valid_number' : 'Por favor insira um número válido!',
        'no_data' : 'Sem Resultados!',
        'yes' : 'Sim',
        'no' : 'Não',
    }

    var options;

    if (action == 'update') {
        options = data_options.join();
    } else if (action == 'refresh') {
        options = data_options;
    }

    if (updating_value == 'default') {
    
        $.ajax({
           url: '/event/' + event_id + '/transactions/',
           dataType: 'json',
           beforeSend: function () {

               if (!first_load) {

                   var card_height = $('#warnings-card').height();
                   $('#warnings-card').css('height', card_height);

               }

               $("#transactions-list tbody > *").remove();

               var html = '';
               html += '<tr>';
               html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="6"><div class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div></td>';
               html +=	'</tr>'

               $("#transactions-list tbody").append(html);

               componentHandler.upgradeDom();

           },
           success: function (data) {

                if (data['error']) {

                    $("#transactions-list tbody > *").remove();

                    var html = '';
                    html += '<tr>';
                    html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="6">' + trans.unavailable + '</td>';
                    html +=	'</tr>'

                    $("#transactions-list tbody").append(html);

                    $('#transactions-card').removeAttr("style");

                    componentHandler.upgradeDom();

                } else {

                    $("#transactions-list tbody > *").remove();

                    var html = '';

                    var table_data = data['data'];
                    var next_page = data['next_page_url'];

                    if (table_data.length < 1) {

                        html += '<tr>';
                        html += '<td class="mdl-data-table__cell--non-numeric" colspan="7" style="text-align: center;">' + trans.no_data + '</td>';
                        html += '</tr>';

                    } else {

                        table_data.forEach(function (transaction) {

                            html += '<tr class="load-barcode-history" data-barcode="' + transaction.titulo + '">';
                            html += '<td>' + transaction.titulo + '</td>';
                            html += '<td>' + transaction.zonaDescricao + '</td>';
                            html += '<td>' + transaction.dacDescricao + '</td>';
                            html += '<td><span id="transaction-badge" class="mdl-badge" data-badge="' + ((transaction.isEntry == 1) ? trans.yes : trans.no ) + '" ' + transaction.isEntry + ' style="margin-top: 10px !important;"></span></td>';
                            html += '<td>' + transaction.erro_mensagemOperador + '</td>';
                            html += '<td>' + transaction.date + '</td>';
                            html += '</tr>';

                        });

                        html += '<tr>';
                        html += '<td class="mdl-data-table__cell--non-numeric" colspan="7" style="text-align: center;"><div id="load-more-transactions" data-page="' + next_page + '"><i class="material-icons">add</i></div></td>';
                        html += '</tr>';

                    }

                    $("#transactions-list tbody").append(html);

                    $('#transactions-card').removeAttr("style");

                    componentHandler.upgradeDom();

                }

            }
        });
        
    }

}

function updateGatesInformationChart(action, event_id, update_gates_search, updating_value, data_options, first_load) {

    var options;

    if (action == 'update') {
        options = data_options.join();
    } else if (action == 'refresh') {
        options = data_options;
    }

    var myChart;

    if (updating_value == 'default') {

        $.ajax({
            url: '/event/' + event_id + '/gates_information/',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {

                var gates = [];
                var entries = [];
                var entries_back_color = [];
                var imported = [];
                var imported_back_color = [];

                data.forEach(function(gate) {

                    if (gate.description !== '') {

                        var entries_percentage = ( gate.entries / gate.imported ) * 100;

                        gates.push(gate.description + ' ( ' + entries_percentage.toFixed(2) + '% )');
                        entries.push(gate.entries);
                        imported.push(gate.imported);

                        entries_back_color.push("rgba(54, 162, 235, 1)");
                        imported_back_color.push("#1A3563");

                    }

                });

                document.getElementById("gates-information").innerHTML = '&nbsp;';
                document.getElementById("gates-information").innerHTML = '<canvas id="gates-information-venue"></canvas>';

                var ctx_gates = document.getElementById("gates-information-venue").getContext('2d');

                var gatesInformation = new Chart(ctx_gates, {
                    type: 'bar',
                    data: {
                        labels: gates,
                        datasets: [{
                            label: 'Importados',
                            data: imported,
                            backgroundColor: imported_back_color,
                            borderColor: imported_back_color,
                            borderWidth: 1
                        }, {
                            label: 'Entradas',
                            data: entries,
                            backgroundColor: entries_back_color,
                            borderColor: entries_back_color,
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true,
                                }
                            }],
                            xAxes: [{
                               ticks: {
                                   autoSkip: false
                               }
                            }],
                        },
                        "hover": {
                            "animationDuration": 0
                        },
                        legend: {
                            "display": false
                        },
                        tooltips: {
                            "enabled": false
                        },
                        "animation": {
                            "duration": 1,
                            "onComplete": function() {
                                var chartInstance = this.chart,
                                    ctx = chartInstance.ctx;

                                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'bottom';

                                this.data.datasets.forEach(function(dataset, i) {
                                    var meta = chartInstance.controller.getDatasetMeta(i);
                                    meta.data.forEach(function(bar, index) {
                                        var data = dataset.data[index];
                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                    });
                                });
                            }
                        },
                    }
                });

                gatesInformation.update();

                componentHandler.upgradeDom();

            }
        });

    }

}

$(document).on('click', '#load-more-transactions', function() {

    var event_id = $('#event-selected').val();
    var action = 'refresh';
    var updating_value = $('#transactions-filter-selected').val();
    var data_options = $('#transactions-filter-selected').attr('data-options');
    var update_type_search = $('#transactions-filter-selected').attr('data-search-type');
    var first_load = false;
    var request_page = $('#load-more-transactions').attr("data-page");

    var trans = {
        'undefined': 'Indisponivel',
        'insert_valid_number' : 'Por favor insira um número válido!',
        'no_data' : 'Sem Resultados!',
        'yes' : 'Sim',
        'no' : 'Não',
    }

    var options;

    if (action == 'update') {
        options = data_options.join();
    } else if (action == 'refresh') {
        options = data_options;
    }

    if (updating_value == 'default') {

        $.ajax({
            url: request_page,
            dataType: 'json',
            beforeSend: function () {

                $('#transactions-list tbody #load-more-transactions').parent().parent().remove();

                var html = '';
                html += '<tr>';
                html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="6"><div id="loading-more-transactions" class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div></td>';
                html +=	'</tr>'

                $("#transactions-list tbody").append(html);

                componentHandler.upgradeDom();

            },
            success: function (data) {

                if (data['error']) {

                    $("#transactions-list tbody > *").remove();

                    var html = '';
                    html += '<tr>';
                    html +=	'	<td class="mdl-data-table__cell--non-numeric" colspan="6">' + trans.unavailable + '</td>';
                    html +=	'</tr>'

                    $("#transactions-list tbody").append(html);

                    $('#transactions-card').removeAttr("style");

                    componentHandler.upgradeDom();

                } else {

                    $("#transactions-list tbody #loading-more-transactions").parent().parent().remove();

                    var html = '';

                    var table_data = data['data'];
                    var next_page = data['next_page_url'];

                    if (table_data.length < 1) {

                        html += '<tr>';
                        html += '<td colspan="7" style="text-align: center;">' + trans.no_data + '</td>';
                        html += '</tr>';

                    } else {

                        table_data.forEach(function (transaction) {

                            html += '<tr class="load-barcode-history" data-barcode="' + transaction.titulo + '">';
                            html += '<td>' + transaction.titulo + '</td>';
                            html += '<td>' + transaction.zonaDescricao + '</td>';
                            html += '<td>' + transaction.dacDescricao + '</td>';
                            html += '<td><span id="transaction-badge" class="mdl-badge" data-badge="' + ((transaction.isEntry == 1) ? trans.yes : trans.no ) + '" ' + transaction.isEntry + ' style="margin-top: 10px !important;"></span></td>';
                            html += '<td>' + transaction.erro_mensagemOperador + '</td>';
                            html += '<td>' + transaction.date + '</td>';
                            html += '</tr>';

                        });

                        html += '<tr>';
                        html += '<td colspan="7" style="text-align: center;"><div id="load-more-transactions" data-page="' + next_page + '"><i class="material-icons">add</i></div></td>';
                        html += '</tr>';

                    }

                    $("#transactions-list tbody").append(html);

                    $('#transactions-card').removeAttr("style");

                    componentHandler.upgradeDom();

                }

            }
        });

    }

});

$(document).on('click', '.load-barcode-history', function() {

    var barcode = $(this).attr('data-barcode');

    var dialog = document.querySelector('#search-ticket-barcode-dialog');

    dialog.showModal();

    $('#barcode-search').val(barcode);

    document.getElementById('select-search-ticket-barcode').click();

});

/**
 *
 * Print Actions
 *
 */

$("#print-flowrate").on('click', function() {

    var canvas = document.querySelector('#flowrate-venue');

    var canvasImage = canvas.toDataURL("image/png", 1.0);

    var doc = new jsPDF('landscape');
    doc.setFontSize(20);
    doc.text(15, 15, "Flowrate");
    doc.addImage(canvasImage, 'PNG', 10, 10, 280, 150);
    doc.save('flowrate.pdf');

});

$('#print-fill-level').on('click', function() {

    var canvas = document.querySelector('#fill-level-venue');

    var canvasImage = canvas.toDataURL("image/png", 1.0);

    var doc = new jsPDF();
    doc.setFontSize(20);
    doc.text(15, 15, "Fill Level");
    doc.addImage(canvasImage, 'PNG', 50, 20, 100, 100);
    doc.save('fill-level.pdf');

});

$('#print-entries-exits').on('click', function() {

    var canvas = document.querySelector('#entries-exits-venue');

    var canvasImage = canvas.toDataURL("image/png", 1.0);

    var doc = new jsPDF('landscape');
    doc.setFontSize(20);
    doc.text(15, 15, "Entries x Exits");
    doc.addImage(canvasImage, 'PNG', 10, 10, 280, 150);
    doc.save('entries_x_exits.pdf');

});

import html2canvas from 'html2canvas';

$('#print-warnings').on('click', function() {

    html2canvas(document.querySelector("#transactions-list")).then(canvas => {

        var canvasImage = canvas.toDataURL("image/png", 1.0);

        var doc = new jsPDF();
        doc.setFontSize(20);
        doc.text(15, 15, "Warnings");
        doc.addImage(canvasImage, 'PNG', 30, 30, 120, 100);
        doc.save('warnings.pdf');

    });

});

/**
 *
 * Events Actions
 *
 */

window.onload = function() {

    loadEvents();

};

$('#choose-event').on('click', function() {

    loadEvents();

});

$('#select-event-button').on('click', function() {

    var dialog = document.querySelector('dialog');

    var event_selected = $('#event-selected').val();

    if (event_selected !== "undefined") {

        document.getElementById("error-select-event").style.visibility = "hidden";

        $("#event-options").removeClass("is-invalid");

        loadZones();

        loadTicketTypes();

        loadCharts();

        dialog.close();

    } else {

        document.getElementById("error-select-event").style.visibility = "visible";

        $("#event-options").addClass("is-invalid");


    }

});

$('#close-event-button').on('click', function() {

    var dialog = document.querySelector('dialog');

    dialog.close();

});

function loadEvents() {

    var trans_pt = {
        event: "Evento",
        event_required: "Evento Obrigatório!",
        error_loading: "Indisponivel!",
    }

    var trans_en = {
        event: "Event",
        event_required: "Event Required!",
        error_loading: "Unavailable!",
    }

    var dialog = document.querySelector('#events-dialog');

    if (dialog != undefined && dialog != null) {

        dialog.showModal();

        $.ajax({
            url: '/events/get',
            dataType: 'json',
            beforeSend: function () {

                $('#events-list > *').remove();

                var loading = '<div class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div>';
                $('#events-list').append(loading);

                componentHandler.upgradeDom();

            },
            success: function (data) {

                if (data['error']) {

                    $('#events-list > .spinner').remove();

                    var events =	''+
                        '<div id="event-options" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height">' +
                        '	<input type="text" value="" class="mdl-textfield__input" id="event-select" readonly>' +
                        '	<input type="hidden" value="" name="event-select">' +
                        '	<label for="event-select" class="mdl-textfield__label">' + trans_pt.error_loading + '</label>' +
                        '	<span id="error-select-event" class="mdl-textfield__error">' + trans_pt.event_required + '</span>' +
                        '</div>';

                    $('#events-list').append(events);

                } else {

                    $('#events-list > .spinner').remove();

                    var events =	''+
                        '<div id="event-options" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height">' +
                        '	<input type="text" value="" class="mdl-textfield__input" id="event-select" readonly>' +
                        '	<input type="hidden" value="" name="event-select">' +
                        '	<label for="event-select" class="mdl-textfield__label">' + trans_pt.event + '</label>' +
                        '	<span id="error-select-event" class="mdl-textfield__error">' + trans_pt.event_required + '</span>' +
                        '	<ul for="event-select" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">';

                    data.forEach(function (event) {

                        events += '	<li class="mdl-menu__item event-option" onclick="selectEvent(this)" data-val="' + event.id + '" data-code="' + event.code + '" data-lotation="' + event.total + '" data-open-gates="' + event.open_gates + '">' + event.descricao + '</li>';

                    });

                    events += 	'	</ul>' +
                        '</div>';



                    $('#events-list').append(events);

                }

                componentHandler.upgradeDom();

            },
            error: function (error) {

                $('#events-list > .spinner').remove();

                var events =	''+
                    '<div id="event-options" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height">' +
                    '	<input type="text" value="" class="mdl-textfield__input" id="event-select" readonly>' +
                    '	<input type="hidden" value="" name="event-select">' +
                    '	<label for="event-select" class="mdl-textfield__label">' + trans.error_loading + '</label>' +
                    '	<span id="error-select-event" class="mdl-textfield__error">' + trans.event_required + '</span>' +
                    '</div>';

                $('#events-list').append(events);

                componentHandler.upgradeDom();

            }
        });

    } else {

        var report_event_input = document.querySelector('#report-event');

        if (report_event_input != undefined && report_event_input != null) {

            $.ajax({
                url: '/events/report/get',
                dataType: 'json',
                beforeSend: function () {

                    $('#report-event > *').remove();

                    var loading = '<div id="loading-events" class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div>';

                    $('#report-event').parent().append(loading);

                    componentHandler.upgradeDom();

                },
                success: function (data) {

                    if (data['error']) {

                        $('#loading-events').remove();

                        var events =	'<option value="error">' + trans_pt.error_loading + '</option>';

                        $('#report-event').append(events);

                    } else {

                        $('#loading-events').remove();

                        var events =	'';

                        events += '<option value="""></option>';

                        data.forEach(function (event) {

                            events += '<option value="' + event.id + '">' + event.descricao + '</option>';

                        });

                        events += 	'	</ul>' +
                            '</div>';

                        $('#report-event').append(events);
                        $('#report-event-park').append(events);
                        $('#report-event-credential').append(events);

                        $('#report-event').parent().addClass('is-dirty is-upgraded');
                        $('#report-event-park').parent().addClass('is-dirty is-upgraded');
                        $('#report-event-credential').parent().addClass('is-dirty is-upgraded');

                        $('#competition').parent().addClass('is-dirty is-upgraded');

                        $('#installation').parent().addClass('is-dirty is-upgraded');

                    }

                    componentHandler.upgradeDom();

                },
                error: function (error) {

                    $('#events-list > .spinner').remove();

                    var events =	'<option value="error">' + trans_pt.error_loading + '</option>';

                    $('#report-event').append(events);

                    componentHandler.upgradeDom();

                }
            });

        }

    }

}

function loadCharts() {

    var trans = {
        'undefined': 'Indisponivel',
        'select_event_warning' : 'Seleccione um Evento!',
    }

    var event_id = $('#event-selected').val();

    if (event_id == 'undefined') {

        var snackbarContainer = document.querySelector('#select-event-warning');

        var data = {message: trans.select_event_warning};

        snackbarContainer.MaterialSnackbar.showSnackbar(data);

    } else {

        var load_dialog = document.querySelector('#load-data-dialog');

        load_dialog.showModal();

        /**
         * @return flowrate
         */

        var flowrate_updating_value = $('#flowrate-filter-selected').val();
        var flowrate_data_options = $('#flowrate-filter-selected').attr('data-options');
        var flowrate_search_type = $('#flowrate-filter-selected').attr('data-search-type');

        updateFlowrateChart('refresh', event_id, flowrate_search_type, flowrate_updating_value, flowrate_data_options);

        /**
         * Load Fill Level
         *
         * @return Object
         */

        var filllevel_updating_value = $('#filllevel-filter-selected').val();
        var filllevel_data_options = $('#filllevel-filter-selected').attr('data-options');
        var filllevel_search_type = $('#filllevel-filter-selected').attr('data-search-type');

        updateFillLevelChart('refresh', event_id, filllevel_search_type, filllevel_updating_value, filllevel_data_options);

        /**
         * @return warningsVenue
         */

        var warnings_updating_value = $('#warnings-filter-selected').val();
        var warnings_data_options = $('#warnings-filter-selected').attr('data-options');
        var warnings_search_type = $('#warnings-filter-selected').attr('data-search-type');
        var first_load = true;

        updateWarningsChart('refresh', event_id, warnings_search_type, warnings_updating_value, warnings_data_options, first_load);

        var gates_updating_value = $('#gates-filter-selected').val();
        var gates_data_options = $('#gates-filter-selected').attr('data-options');
        var gates_search_type = $('#gates-filter-selected').attr('data-search-type');
        var gates_first_load = true;

        updateGatesInformationChart('refresh', event_id, gates_search_type, gates_updating_value, gates_data_options, first_load);

        var transactions_updating_value = $('#transactions-filter-selected').val();
        var transactions_data_options = $('#transactions-filter-selected').attr('data-options');
        var transactions_search_type = $('#transactions-filter-selected').attr('data-search-type');
        var transactions_first_load = true;

        updateTransactionsTable('refresh', event_id, transactions_search_type, transactions_updating_value, transactions_data_options, transactions_first_load);

        load_dialog.close();

    }

}

function loadZones() {

    /**
     *
     * Request To Load And Mount Zones
     *
     */

}

function loadTicketTypes() {

    $("#ticket-types-list tbody > *").remove();

    $.ajax({
        url: '/tickettypes/active/',
        dataType: 'json',
        success: function (data) {

            var html = 	'';

            data.forEach(function (tickettype) {

                html += '<tr>';
                html += '	<td>';
                html += '		<label id="' + tickettype.id + '" data-external="' + tickettype.external_id + '" data-value="' + tickettype.value + '" class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="ticket-' + tickettype.id + '">';
                html += '			<input type="checkbox" id="ticket-' + tickettype.id + '" class="mdl-checkbox__input">';
                html += '			<span class="mdl-checkbox__label"></span>';
                html += '		</label>';
                html += '	</td>';
                html += '	<td>' + tickettype.description + '</td>';
                html +=	'</tr>';

            });

            $("#ticket-types-list tbody").append(html);

            componentHandler.upgradeDom();

        },
        error: function (error) {

        }
    });

}

/**
 *
 * Dialog Actions Select
 *
 */

$('#select-ticket-type-list-button').on('click', function() {

    var trans = {
        'undefined': 'Indisponivel',
        'select_ticket_type_warning' : 'Seleccione pelo menos um Tipo de Bilhete!',
    };

    /**
     * Update Params
     */

    var event_id = $('#event-selected').val();

    var dialog = document.querySelector('#ticket-type-filter-dialog');

    var updating_graph = $('#ticket-type-filter-edit').val();

    var updating_value = '';

    var update_options = [];

    var update_type_search;

    $('#ticket-types-list tbody tr td label.is-checked').each(function(row, item) {

        update_options.push($(item).attr('data-external'));

    });

    if (update_options.length > 0) {

        switch(updating_graph) {

            case 'flowrate':

                updating_value = $('#flowrate-filter-selected').attr('data-update-filter');
                update_type_search = $('#flowrate-filter-selected').attr('flowrate-filter-selected');
                $('#flowrate-filter-selected').attr('data-update-filter', '');
                $('#flowrate-filter-selected').attr('data-options', update_options);
                $('#flowrate-filter-selected').val(updating_value);

                // Update Chart

                updateFlowrateChart('update', event_id, update_type_search, updating_value, update_options);

                break;
            case 'filllevel':

                updating_value = $('#filllevel-filter-selected').attr('data-update-filter');
                update_type_search = $('#filllevel-filter-selected').attr('flowrate-filter-selected');
                $('#filllevel-filter-selected').attr('data-update-filter', '');
                $('#filllevel-filter-selected').attr('data-options', update_options);
                $('#filllevel-filter-selected').val(updating_value);

                // Update Chart

                updateFillLevelChart('update', event_id, update_type_search, updating_value, update_options);

                break;
            case 'warnings':
                updating_value = $('#warnings-filter-selected').attr('data-update-filter');
                $('#warnings-filter-selected').attr('data-update-filter', '');
                $('#warnings-filter-selected').attr('data-options', update_options);
                $('#warnings-filter-selected').val(updating_value);
                break;
            case 'transactions':
                updating_value = $('#transactions-filter-selected').attr('data-update-filter');
                $('#transactions-filter-selected').attr('data-update-filter', '');
                $('#transactions-filter-selected').attr('data-options', update_options);
                $('#transactions-filter-selected').val(updating_value);
                break;
            default:
                updating_value = '';
                break;
        }

        dialog.close();

        $('#ticket-types-list tbody tr td label.is-checked').each(function(row, item) {

            $(item).removeClass('is-checked');

        });

    } else {

        var snackbarContainer = document.querySelector('#select-ticket-type-warning');

        var data = {message: trans.select_ticket_type_warning};

        snackbarContainer.MaterialSnackbar.showSnackbar(data);

    }

});

$('#select-zone-dac-list-button').on('click', function() {

    var trans = {
        'undefined': 'Indisponivel',
        'select_zone_dac_warning' : 'Seleccione uma Zona ou os seus Dacs!',
    }

    var dialog = document.querySelector('#zone-dac-filter-dialog');

    var selected_zones = [];

    $('#ticketTypes input[type=checkbox]:checked').each(function () {
        selected_zones.push($(this).val());
    });

    if (selected_zones.length > 0) {

        /**
         * Load Charts With Filter
         */

        dialog.close();

    } else {

        var snackbarContainer = document.querySelector('#select-zones-dacs-warning');

        var data = {message: trans.select_zone_dac_warning};

        snackbarContainer.MaterialSnackbar.showSnackbar(data);

    }

});

$('#select-hour-list-button').on('click', function() {

    var trans = {
        'undefined': 'Indisponivel',
        'select_valid_dates' : 'Selecione uma data Válida!',
    };

    /**
     *
     * Get Data
     *
     */

    var event_id = $('#event-selected').val();
    var dialog = document.querySelector('#hour-filter-dialog');
    var start_filter = $('#datetime-start').val();
    var end_filter = $('#datetime-end').val();
    var updating_graph = $('#hour-filter-edit').val();
    var updating_value = '';
    var update_options = [];
    var update_type_search;
    var regexr = /^\d{4}\-\d{2}\-\d{2}\s\d{2}\:\d{2}(\:\d{2})?$/;
    var valid_start_filter = regexr.test(start_filter);
    var valid_end_filter = regexr.test(end_filter);

    if (start_filter !== "" && end_filter !== "" && valid_start_filter && valid_end_filter) {
        update_options.push(start_filter, end_filter);
    }

    if (update_options.length > 0) {

        switch(updating_graph) {

            case 'flowrate':

                updating_value = $('#flowrate-filter-selected').attr('data-update-filter');
                update_type_search = (($('#hour-filter-search-type').hasClass('is-checked')) ? 'tickettype' : 'default');

                $('#flowrate-filter-selected').attr('data-update-filter', '');
                $('#flowrate-filter-selected').attr('data-options', update_options);
                $('#flowrate-filter-selected').attr('data-search-type', update_type_search);
                $('#flowrate-filter-selected').val(updating_value);

                // Update Chart

                updateFlowrateChart('update', event_id, update_type_search, updating_value, update_options);

                break;
            case 'filllevel':

                updating_value = $('#filllevel-filter-selected').attr('data-update-filter');
                update_type_search = (($('#hour-filter-search-type').hasClass('is-checked')) ? 'tickettype' : 'default');

                $('#filllevel-filter-selected').attr('data-update-filter', '');
                $('#filllevel-filter-selected').attr('data-options', update_options);
                $('#filllevel-filter-selected').attr('data-search-type', update_type_search);
                $('#filllevel-filter-selected').val(updating_value);

                // Update Chart

                updateFillLevelChart('update', event_id, update_type_search, updating_value, update_options);

                break;
            case 'warnings':

                updating_value = $('#warnings-filter-selected').attr('data-update-filter');
                update_type_search = (($('#hour-filter-search-type').hasClass('is-checked')) ? 'tickettype' : 'default');

                var first_load = false;

                $('#warnings-filter-selected').attr('data-update-filter', '');
                $('#warnings-filter-selected').attr('data-options', update_options);
                $('#warnings-filter-selected').val(updating_value);
                $('#warnings-filter-selected').attr(updating_value);

                // Update Table

                updateWarningsChart('update', event_id, update_type_search, updating_value, update_options, first_load);

                break;

            case 'transactions':
                updating_value = $('#transactions-filter-selected').attr('data-update-filter');
                $('#transactions-filter-selected').attr('data-update-filter', '');
                $('#transactions-filter-selected').attr('data-options', update_options);
                $('#transactions-filter-selected').val(updating_value);
                break;
            default:
                updating_value = '';
                break;
        }

        dialog.close();

        $('#ticket-types-list tbody tr td label.is-checked').each(function(row, item) {

            $(item).removeClass('is-checked');

        });

    } else {

        var snackbarContainer = document.querySelector('#select-hour-warning');

        var data = {message: trans.select_valid_dates};

        snackbarContainer.MaterialSnackbar.showSnackbar(data);

    }

});

$('#select-search-ticket-barcode').on('click', function() {

    var trans = {
        'undefined': 'Indisponivel',
        'insert_valid_number' : 'Por favor insira um número válido!',
        'no_data' : 'Sem Resultados!',
        'yes' : 'Sim',
        'no' : 'Não',
    };

    /**
     *
     * Get Data
     *
     */

    var event_id = $('#event-selected').val();
    var dialog = document.querySelector('#select-search-ticket-barcode');

    var search_value = $('#barcode-search').val();

    if (search_value.length > 0) {

        $.ajax({
            type: 'POST',
            url: '/event/' + event_id + '/barcode',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            data: {barcode: search_value},
            beforeSend: function() {

                $('#transactions-history tbody  > *').remove();

                var loading = '<tr><td colspan="7"><div class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div></td></tr>';

                $('#transactions-history tbody').append(loading);

                componentHandler.upgradeDom();

            },
            success: function (data) {

                //Barcode 5954474567693905

                var html = '';

                if (data.length < 1) {

                    html += '<tr>';
                    html += '<td colspan="7" style="text-align: center;">' + trans.no_data + '</td>';
                    html += '</tr>';

                } else {

                    data.forEach(function (transaction) {

                        html += '<tr>';
                        html += '<td>' + transaction.titulo + '</td>';
                        html += '<td>' + transaction.zonaDescricao + '</td>';
                        html += '<td>' + transaction.dacDescricao + '</td>';
                        html += '<td><span id="transaction-badge" class="mdl-badge" data-badge="' + ((transaction.isEntry == 1) ? trans.yes : trans.no ) + '" ' + transaction.isEntry + ' style="margin-top: 10px !important;"></span></td>';
                        html += '<td>' + transaction.erro_mensagemOperador + '</td>';
                        html += '<td>' + transaction.date + '</td>';
                        html += '</tr>';

                        // <span class="mdl-badge" data-badge="' + error['quantity'] + '">' + error['error_message'] + '</span>

                    });

                }

                $('#transactions-history tbody  > *').remove();
                $("#transactions-history tbody").append(html);

            }
        });

    } else {

        var snackbarContainer = document.querySelector('#select-search-ticket-barcode-warning');

        var data = {message: trans.insert_valid_number};

        snackbarContainer.MaterialSnackbar.showSnackbar(data);

    }

});

/**
 *
 * Dialog Actions Close
 *
 */

$('#close-ticket-type-list-button').on('click', function() {

    $('#ticket-types-list tbody tr td label.is-checked').each(function(row, item) {

        $(item).removeClass('is-checked');

    });

    var dialog = document.querySelector('#ticket-type-filter-dialog');

    dialog.close();

});

$('#close-zone-dac-list-button').on('click', function() {

    var dialog = document.querySelector('#zone-dac-filter-dialog');

    dialog.close();

});

$('#close-hour-list-button').on('click', function() {

    var dialog = document.querySelector('#hour-filter-dialog');

    dialog.close();

    $('#day-hour-select > *').remove();

});

$('#close-search-ticket-barcode').on('click', function() {

    var dialog = document.querySelector('#search-ticket-barcode-dialog');

    dialog.close();

});

$('#close-gates-error').on('click', function(){

    var dialog = document.querySelector('#gates-information-errors-dialog');

    dialog.close();

});

$('#close-gates-error-filtered').on('click', function() {

    var dialog = document.querySelector('#error-by-gates-filtered');

    dialog.close();

});

/**
 *
 * Dialog Actions CheckBox's
 *
 */

$('#select-all-tickets').on('click', function() {

    var all_checked = $('#ticket-types-all-checked').val();

    if (all_checked == 'true') {

        $('#ticket-types-list tbody tr td label').removeClass('is-checked');
        $('#ticket-types-all-checked').val(false);

    } else {

        $('#ticket-types-list tbody tr td label').addClass('is-checked');
        $('#ticket-types-all-checked').val(true);

    }

});

/**
 *
 * Date Time Picker
 *
 */

const flatpickr = require("flatpickr");

import { Portuguese } from "flatpickr/dist/l10n/pt.js";

require("flatpickr/dist/themes/material_blue.css");

flatpickr('#start-datetime-filter', {
    "locale": Portuguese
});

flatpickr('#end-datetime-filter', {
    "locale": Portuguese
});

flatpickr('#event-datetime', {
    "locale": Portuguese
});

// import printThis from "print-this";

$('.print-report').on('click', function() {

    $('#report-actions').css('display', 'none');
    $('#report-body').css('overflow-x', 'hidden');
    $('#report-body').css('overflow-y', 'hidden');
    $('#report-body').css('max-height', 'none');
    $('#report-dialog').css('top', '0px');

    var title = document.title;

    document.title = $('#report_name').val();

    window.print();

    $('#report-actions').css('display', 'block');
    $('#report-body').css('overflow-x', 'auto');
    $('#report-body').css('overflow-y', 'auto');
    $('#report-body').css('max-height', '700px');
    $('#report-dialog').css('top', '');

    document.title = title;

});